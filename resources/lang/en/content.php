<?php
$customAvailabilitySlots = [
    [
        'id' => '05:00',
        'name' => '5:00 AM',
        'status' => false,
    ],
    [
        'id' => '05:30',
        'name' => '5:30 AM',
        'status' => false,
    ],
    [
        'id' => '06:00',
        'name' => '6:00 AM',
        'status' => false,
    ],
    [
        'id' => '06:30',
        'name' => '6:30 AM',
        'status' => false,
    ],
    [
        'id' => '07:00',
        'name' => '7:00 AM',
        'status' => false,
    ],
    [
        'id' => '07:30',
        'name' => '7:30 AM',
        'status' => false,
    ],
    [
        'id' => '08:00',
        'name' => '8:00 AM',
        'status' => false,
    ],
    [
        'id' => '08:30',
        'name' => '8:30 AM',
        'status' => false,
    ],
    [
        'id' => '09:00',
        'name' => '9:00 AM',
        'status' => false,
    ],
    [
        'id' => '09:30',
        'name' => '9:30 AM',
        'status' => false,
    ],
    [
        'id' => '10:00',
        'name' => '10:00 AM',
        'status' => false,
    ],
    [
        'id' => '10:30',
        'name' => '10:30 AM',
        'status' => false,
    ],
    [
        'id' => '11:00',
        'name' => '11:00 AM',
        'status' => false,
    ],
    [
        'id' => '11:30',
        'name' => '11:30 AM',
        'status' => false,
    ],
    [
        'id' => '12:00',
        'name' => '12:00 PM',
        'status' => false,
    ],
    [
        'id' => '12:30',
        'name' => '12:30 PM',
        'status' => false,
    ],
    [
        'id' => '13:00',
        'name' => '1:00 PM',
        'status' => false,
    ],
    [
        'id' => '13:30',
        'name' => '1:30 PM',
        'status' => false,
    ],
    [
        'id' => '14:00',
        'name' => '2:00 PM',
        'status' => false,
    ],
    [
        'id' => '14:30',
        'name' => '2:30 PM',
        'status' => false,
    ],
    [
        'id' => '15:00',
        'name' => '3:00 PM',
        'status' => false,
    ],
    [
        'id' => '15:30',
        'name' => '3:30 PM',
        'status' => false,
    ],
    [
        'id' => '16:00',
        'name' => '4:00 PM',
        'status' => false,
    ],
    [
        'id' => '16:30',
        'name' => '4:30 PM',
        'status' => false,
    ],
    [
        'id' => '17:00',
        'name' => '5:00 PM',
        'status' => false,
    ],
    [
        'id' => '17:30',
        'name' => '5:30 PM',
        'status' => false,
    ],
    [
        'id' => '18:00',
        'name' => '6:00 PM',
        'status' => false,
    ],
    [
        'id' => '18:30',
        'name' => '6:30 PM',
        'status' => false,
    ],
    [
        'id' => '19:00',
        'name' => '7:00 PM',
        'status' => false,
    ],
    [
        'id' => '19:30',
        'name' => '7:30 PM',
        'status' => false,
    ],
    [
        'id' => '20:00',
        'name' => '8:00 PM',
        'status' => false,
    ],
    [
        'id' => '20:30',
        'name' => '8:30 PM',
        'status' => false,
    ],
    [
        'id' => '21:00',
        'name' => '9:00 PM',
        'status' => false,
    ],
    [
        'id' => '21:30',
        'name' => '9:30 PM',
        'status' => false,
    ],
    [
        'id' => '22:00',
        'name' => '10:00 PM',
        'status' => false,
    ],
    [
        'id' => '22:30',
        'name' => '10:30 PM',
        'status' => false,
    ],
    [
        'id' => '23:00',
        'name' => '11:00 PM',
        'status' => false,
    ],
    [
        'id' => '23:30',
        'name' => '11:30 PM',
        'status' => false,
    ],
];

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'category_heading' => 'Lorem Ipsum is simply dummy text of the print and typesetting industry Lorem Ipsum has in the industry\'s standards dummy text ever since  the 1500s when the printer took.',
    'terms_n_condition_heading' => 'Tell us a little about your #CATEGORY#',
    'terms_n_condition_body' => [
        'title' => '',
        'content' => 'This information will help us identify the rest of your #CATEGORY# details, which you can review later.',
    ],
    'allowed_item_heading' => 'It is a long established fact that a reader will be the distracted by the readable content of the page when looking at the layout.',
    'availability_heading' => 'Lorem Ipsum is simply dummy text of the print and typesetting industry Lorem Ipsum has in the industry\'s standards dummy text ever since  the 1500s when the printer took.',
    'split_payment_sms_content' => 'Hey! #NAME# is requesting you to be part of the experience booked with Dolphi. Click on the link to add your payment method.',
    'availability_data' => [
        [
            'id' => 1,
            'name' => 'Default Availability',
            'data' => [
                'title' => 'Your boat will be available ',
                'sub_title' => 'Monday to Sunday from 8:00 AM to 6:00 PM',
                'is_expandable' => false,
                'api_data' => [
                    [
                        'title' => 'Monday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Tuesday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Wednesday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Thursday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Friday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Saturday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Sunday',
                        'data' => $customAvailabilitySlots,
                    ],
                ],
                'fe_data' => [

                ]
            ],
        ],
        [
            'id' => 2,
            'name' => 'Custom Availability',
            'data' => [
                'title' => '',
                'sub_title' => 'Customize start times for each day of the week.',
                'is_expandable' => true,
                'api_data' => [
                    [
                        'title' => 'Monday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Tuesday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Wednesday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Thursday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Friday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Saturday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Sunday',
                        'data' => $customAvailabilitySlots,
                    ],
                ],
                'fe_data' => [
                    [
                        'title' => 'Monday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Tuesday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Wednesday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Thursday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Friday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Saturday',
                        'data' => $customAvailabilitySlots,
                    ],
                    [
                        'title' => 'Sunday',
                        'data' => $customAvailabilitySlots,
                    ],
                ]
            ],
        ],
    ],
];