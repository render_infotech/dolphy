@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('Edit Specifications and Allowed items'),
        'headerData' => __('Boat/Events Settings') ,
        'url' => 'admin/boat-settings' ,
    ])

    <div class="section-body">
        <div class="row">
            <div class="col-lg-8"><h2 class="section-title"> {{__('Edit Specifications and Allowed items')}}</h2></div>
        </div>
            {{-- <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> --}}
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                   {{ $error}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endforeach
            @endif
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                    <form method="post" action="{{route('admin.boat-settings.update-items', [$items->id])}}" enctype="multipart/form-data" >
                        @csrf

                        <div class="row">
                            <div class="col-lg-3">
                                <div class="form-group center">
                                    <label>{{__('Image')}}</label>
                                    <div id="image-preview" class="image-preview">
                                        <label for="image-upload" id="image-label"> <i class="fas fa-plus"></i></label>
                                        <input type="file" name="picture" id="image-upload" />
                                    </div>
                                        <img src="/{{$items->image}}" alt="{{$items->name}}"/>
                                    @error('image')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>{{__('Name')}}</label>
                                <input type="text" name="name" value="{{$items->name}}" class="form-control @error('name')? is-invalid @enderror">
                                @error('name')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>

                            <div class="form-group">
                                <label>{{__('Feature/Item')}}</label>
                                <select name="is_feature" class="select2">
                                    <option value="1" {{$items->is_feature == 1 ? 'selected':''}}>Feature</option>
                                    <option value="0" {{$items->is_feature == 0 ? 'selected':''}}>Item</option>
                                </select>
                                @error('is_feature')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                            <div class="form-group">
                                <label>{{__('Status')}}</label>
                                <select name="status" class="select2">
                                    <option value="1" {{$items->status == 1? 'selected':''}}>Active</option>
                                    <option value="0" {{$items->status == 0? 'selected':''}}>Inactive</option>
                                </select>
                                @error('status')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection
