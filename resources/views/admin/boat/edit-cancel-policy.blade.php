@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('Edit Cancellation Policy'),
        'headerData' => __('Boat/Events Settings') ,
        'url' => 'admin/boat-settings' ,
    ])

    <div class="section-body">
        <div class="row">
            <div class="col-lg-8"><h2 class="section-title"> {{__('Edit Cancellation Policy')}}</h2></div>
        </div>
            {{-- <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> --}}
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                   {{ $error}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endforeach
            @endif
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                    <form method="post" action="{{route('admin.boat-settings.update-policy', [$policy->id])}}" enctype="multipart/form-data" >
                        @csrf

                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label>{{__('Name')}}</label>
                                <label class="form-control @error('name')? is-invalid @enderror">{{$policy->name}}</label>
                            </div>
                            <div class="form-group col-lg-12">
                                <label>{{__('Description')}}</label>
                                <input type="text" name="policy_description" value="{{$policy->policy_description}}" class="form-control @error('policy_description')? is-invalid @enderror">
                                @error('policy_description')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection
