@extends('master')

@section('content')
    <section class="section">
        @include('admin.layout.breadcrumbs', [
            'title' => __('Edit Split Payment Commission'),
            'headerData' => __('Boat/Events Settings') ,
            'url' => 'admin/boat-settings' ,
        ])

        <div class="section-body">
            <div class="row">
                <div class="col-lg-8"><h2 class="section-title"> {{__('Edit Split Payment Commission')}}</h2></div>
            </div>
            {{-- <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> --}}
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ $error}}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @endforeach
            @endif
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <form method="post" action="{{route('admin.boat-settings.update-split-payment', [$models->id])}}" enctype="multipart/form-data" >
                                @csrf

                                <div class="row">
                                    <div class="form-group col-lg-12">
                                        <label>{{__('Split Payment Commission')}}</label>
                                        <input type="text" name="split_payment_commission" value="{{$models->price}}" class="form-control @error('split_payment_commission')? is-invalid @enderror">
                                        @error('split_payment_commission')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">{{__('Update')}}</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
