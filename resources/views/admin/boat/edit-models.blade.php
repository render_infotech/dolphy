@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('Edit Boat Model'),
        'headerData' => __('Boat/Events Settings') ,
        'url' => 'admin/boat-settings' ,
    ])

    <div class="section-body">
        <div class="row">
            <div class="col-lg-8"><h2 class="section-title"> {{__('Edit Boat Model')}}</h2></div>
        </div>
            {{-- <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{ session('status') }}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div> --}}
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                   {{ $error}}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endforeach
            @endif
        <div class="row">
            <div class="col-12">
              <div class="card">
                <div class="card-body">
                <form method="post" action="{{route('admin.boat-settings.update-models', [$models->id])}}" enctype="multipart/form-data" >
                        @csrf

                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label>{{__('Boat Make Company')}}</label>
                                <input type="text" name="make_company" value="{{$models->make_company}}" class="form-control @error('make_company')? is-invalid @enderror">
                                @error('make_company')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label>{{__('Boat Category')}}</label>
                                <input type="text" name="boat_category" value="{{$models->boat_category}}" class="form-control @error('boat_category')? is-invalid @enderror">
                                @error('boat_category')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group col-lg-12">
                                <label>{{__('Boat Model Name')}}</label>
                                <input type="text" name="name" value="{{$models->name}}" class="form-control @error('name')? is-invalid @enderror">
                                @error('name')
                                    <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label>{{__('Status')}}</label>
                            <select name="status" class="select2">
                                <option value="1" {{$models->status == 1 ? 'selected' : ''}}>Active</option>
                                <option value="0" {{$models->status == 0 ? 'selected' : ''}}>Inactive</option>
                            </select>
                            @error('status')
                                <div class="invalid-feedback">{{$message}}</div>
                            @endif
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection
