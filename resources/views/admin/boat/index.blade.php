@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('Boat/Events Settings'),
    ]) 

    <div class="section-body">         
       
        <div class="row">
            <div class="col-12">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>
            <div class="col-12">
              <div class="card">     
                <div class="card-body row">
                    <div class="col-lg-6 col-sm-12">
                        <div class="col-lg-8"><h2 class="section-title mt-0"> {{__('Location Type')}}</h2></div>
                        <div class="row mb-4 mt-2">
                                <div class="col-lg-4 text-right">
                                    <button class="btn btn-primary add-button"><a href="{{url('admin/boat-settings/create-maina')}}"><i class="fas fa-plus"></i> {{__('Add New')}}</a></button>
                                </div>
                            </div>   
                        <div class="table-responsive">
                            <table class="table" id="report_table1">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Status')}}</th>
                                        <th>{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($maniaNumbers as $item)
                                        <tr>
                                            <td></td>
                                            <td>{{$item->name}}</td>
                                            <td>                                        
                                                <h5><span class="badge {{$item->status==1?'badge-success': 'badge-warning'}}  m-1">{{$item->status=="1"?'Active': 'Inactive'}}</span></h5>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.boat-settings.edit-maina', $item->id) }}" class="btn-icon"><i class="fas fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach                           
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-lg-6 col-sm-12">
                        <div class="col-lg-8"><h2 class="section-title mt-0"> {{__('Specifications and Allowed items')}}</h2></div>
                        <div class="row mb-4 mt-2">
                                <div class="col-lg-4 text-right">
                                    <button class="btn btn-primary add-button"><a href="{{url('admin/boat-settings/create-items')}}"><i class="fas fa-plus"></i> {{__('Add New')}}</a></button>
                                </div>
                            </div>   
                        <div class="table-responsive">
                            <table class="table" id="report_table2">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th>{{__('Name')}}</th>
                                        <th>{{__('Feature/Items')}}</th>
                                        <th>{{__('Status')}}</th>
                                        <th>{{__('Action')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($allowedItems as $item)
                                        <tr>
                                            <td></td>
                                            <td><img src="/{{$item->image}}" width="30px" height="30px"/>{{$item->name}}</td>
                                            <td>{{$item->is_feature == 0 ? 'Item' : 'Feature'}}</td>
                                            <td>                                        
                                                <h5><span class="badge {{$item->status==1?'badge-success': 'badge-warning'}}  m-1">{{$item->status=="1"?'Active': 'Inactive'}}</span></h5>
                                            </td>
                                            <td>
                                                <a href="{{ route('admin.boat-settings.edit-items', $item->id) }}" class="btn-icon"><i class="fas fa-edit"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach                           
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <hr/>
                <div class="card-body row">
                    <div class="col-lg-12 col-sm-12">
                        <div class="col-lg-12"><h2 class="section-title mt-0"> {{__('Boat Models')}}</h2></div>
                        <div class="row mb-4 mt-2">
                            <div class="col-lg-4 text-right">
                                <button class="btn btn-primary add-button"><a href="{{url('admin/boat-settings/create-models')}}"><i class="fas fa-plus"></i> {{__('Add New')}}</a></button>
                            </div>
                        </div>
                        <div class="table-responsive">
                            <table class="table" id="report_table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{__('Boat Make Company')}}</th>
                                    <th>{{__('Boat Category')}}</th>
                                    <th>{{__('Boat Model Name')}}</th>
                                    <th>{{__('Status')}}</th>
                                    <th>{{__('Action')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($boatModels as $item)
                                <tr>
                                    <td></td>
                                    <td>{{$item->make_company}}</td>
                                    <td>{{$item->boat_category}}</td>
                                    <td>{{$item->name}}</td>
                                    <td>
                                    <h5><span class="badge {{$item->status==1?'badge-success': 'badge-warning'}}  m-1">{{$item->status=="1"?'Active': 'Inactive'}}</span></h5>
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.boat-settings.edit-models', $item->id) }}" class="btn-icon"><i class="fas fa-edit"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$boatModels->appends($request->all())->links()}}
                    </div>
                </div>
                  <hr/>
                  <div class="card-body row">
                      <div class="col-lg-12 col-sm-12">
                          <div class="col-lg-12"><h2 class="section-title mt-0"> {{__('Split Payment Commission')}}</h2></div>
                          <div class="table-responsive">
                              <table class="table" id="">
                                  <thead>
                                  <tr>
                                      <th></th>
                                      <th>{{__('Split Payment Commission')}}</th>
                                      <th>{{__('Status')}}</th>
                                      <th>{{__('Action')}}</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                      <tr>
                                          <td></td>
                                          <td>{{$splitPaymentCommission->price}}</td>
                                          <td>
                                              <h5><span class="badge {{$splitPaymentCommission->status==1?'badge-success': 'badge-warning'}}  m-1">{{$splitPaymentCommission->status=="1"?'Active': 'Inactive'}}</span></h5>
                                          </td>
                                          <td>
                                              <a href="{{ route('admin.boat-settings.edit-split-payment', $splitPaymentCommission->id) }}" class="btn-icon"><i class="fas fa-edit"></i></a>
                                          </td>
                                      </tr>
                                  </tbody>
                              </table>
                          </div>
                          {{$boatModels->appends($request->all())->links()}}
                      </div>
                  </div>
                  <hr/>
                  <div class="card-body row">
                      <div class="col-lg-12 col-sm-12">
                          <div class="col-lg-12"><h2 class="section-title mt-0"> {{__('Cancellation Policy')}}</h2></div>
                          <div class="table-responsive">
                              <table class="table" id="report_table3">
                                  <thead>
                                  <tr>
                                      <th colspan="4">{{__('Boat')}}</th>
                                  </tr>
                                  <tr>
                                      <th></th>
                                      <th>{{__('Name')}}</th>
                                      <th>{{__('Policy Description')}}</th>
                                      <th>{{__('Action')}}</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  @foreach ($cancellationPolicy["1"] as $item)
                                      <tr>
                                          <td></td>
                                          <td>{{$item["name"]}}</td>
                                          <td>{{$item["policy_description"]}}</td>
                                          <td>
                                              <a href="{{ route('admin.boat-settings.edit-policy', $item["id"]) }}" class="btn-icon"><i class="fas fa-edit"></i></a>
                                          </td>
                                      </tr>
                                  @endforeach
                                  </tbody>
                                  <thead>
                                  <tr>
                                      <th colspan="4">{{__('Events')}}</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  @foreach ($cancellationPolicy["2"] as $item)
                                      <tr>
                                          <td></td>
                                          <td>{{$item["name"]}}</td>
                                          <td>{{$item["policy_description"]}}</td>
                                          <td>
                                              <a href="{{ route('admin.boat-settings.edit-policy', $item["id"]) }}" class="btn-icon"><i class="fas fa-edit"></i></a>
                                          </td>
                                      </tr>
                                  @endforeach
                                  </tbody>
                                  <thead>
                                  <tr>
                                      <th colspan="4">{{__('Water Sports')}}</th>
                                  </tr>
                                  </thead>
                                  <tbody>
                                  @foreach ($cancellationPolicy["3"] as $item)
                                      <tr>
                                          <td></td>
                                          <td>{{$item["name"]}}</td>
                                          <td>{{$item["policy_description"]}}</td>
                                          <td>
                                              <a href="{{ route('admin.boat-settings.edit-policy', $item["id"]) }}" class="btn-icon"><i class="fas fa-edit"></i></a>
                                          </td>
                                      </tr>
                                  @endforeach
                                  </tbody>
                              </table>
                          </div>
                      </div>
                  </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection
