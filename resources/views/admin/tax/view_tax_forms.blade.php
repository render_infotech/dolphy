@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
    'title' => __('Tax Forms'),
    ])
    <div class="section-body">

        <div class="row">
            <div class="col-12">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-4 mt-2">
                            <div class="col-lg-8"><h2 class="section-title mt-0"> {{__('View Tax Forms')}}</h2></div>
                        </div>
                        @if(Auth::user()->hasRole('admin'))
                        <form type="GET">
                            <div class="card row">
                                <div>Filter</div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <select class="select2" name="organiser">
                                            <option value="">--Select a Organizer--</option>
                                            @foreach($organisers as $value)
                                            <option value="{{$value->id}}" {{(isset($request->organiser) && $request->organiser == $value->id)? 'selected' : '' }}>{{$value->first_name}} {{$value->last_name}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="select2" name="month">
                                            <option value="">--Select a Month--</option>
                                            @for($i = 1; $i < 13; $i++)
                                            <option value="{{$i}}" {{(isset($request->month) && $request->month == $i)? 'selected' : '' }}>{{date('F', mktime(0, 0, 0, $i, 10))}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <select class="select2" name="year">
                                            <option value="">--Select a Year--</option>
                                            @for($i = date("Y"); $i >= 2000; $i--)
                                            <option value="{{$i}}" {{(isset($request->year) && $request->year == $i)? 'selected' : '' }}>{{$i}}</option>
                                            @endfor
                                        </select>
                                    </div>
                                    <div class="col-lg-3">
                                        <input type="submit" value="Search" class="btn btn-primary">
                                    </div>
                                </div>
                            </div>
                        </form>
                        @endif
                        <div class="table-responsive">
                            <table class="table" id="report_table">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>{{__('Name')}}</th>
                                    <th>{{__('Tax Form')}}</th>
                                    <th>{{__('Month')}}</th>
                                    <th>{{__('Year')}}</th>
                                    <th>{{__('Upload Date')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($taxes as $item)
                                <tr>
                                    <td></td>
                                    <td>{{$item->organization->first_name}} {{$item->organization->last_name}}</td>
                                    <td><a href="{{$item->file_path}}" download>Download</a></td>
                                    <td>{{date('F', mktime(0, 0, 0, $item->month, 10))}}</td>
                                    <td>{{$item->year}}</td>
                                    <td>{{date('d M Y h:i A',strtotime($item->created_at))}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        {{$taxes->appends($request->all())->links()}}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
