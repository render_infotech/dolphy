@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('Add User'),  
        'headerData' => __('Users') ,
        'url' => 'users' ,          
    ]) 

    <div class="section-body">
        <div class="row">
            <div class="col-lg-8"><h2 class="section-title"> {{__('Add User')}}</h2></div>           
        </div>       
       
        <div class="row">
            <div class="col-12">
              <div class="card">     
                <div class="card-body">
                    <form method="post" action="{{url('users')}}">
                        @csrf
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{__('First Name')}}</label>
                                    <input type="text" name="first_name" placeholder="First Name" value="{{old('first_name')}}" class="form-control @error('first_name')? is-invalid @enderror">
                                    @error('first_name')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{__('Last Name')}}</label>
                                    <input type="text" name="last_name" placeholder="Last Name" value="{{old('last_name')}}" class="form-control @error('last_name')? is-invalid @enderror">
                                    @error('last_name')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                      
                        <div class="form-group">
                            <label>{{__('Email')}}</label>
                            <input type="email" name="email" placeholder="Email ID" value="{{old('email')}}" class="form-control @error('email')? is-invalid @enderror">
                            @error('email')
                                <div class="invalid-feedback">{{$message}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{__('Phone')}}</label>
                            <input type="text" name="phone" placeholder="Phone" value="{{old('phone')}}" class="form-control @error('phone')? is-invalid @enderror">
                            @error('phone')
                                <div class="invalid-feedback">{{$message}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{__('Password')}}</label>
                            <input type="password" name="password" placeholder="Password" class="form-control @error('password')? is-invalid @enderror">
                            @error('password')
                                <div class="invalid-feedback">{{$message}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            <label>{{__('Roles')}}</label>
                            <select name="roles[]" class="form-control select2" multiple="multiple" id="role">
                                @foreach ($roles as $per)
                                    <option value="{{$per['id']}}">{{$per['name']}}</option>
                                @endforeach
                            </select>
                            @error('roles')
                                <div class="invalid-feedback">{{$message}}</div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>{{__('Tax Details')}}</label>
                            <div class="row">
                                <div class="col-lg-4">
                                    <label>{{__('Boat Tax')}}</label>
                                    <input type="number" name="boat" value="{{old('boat')}}" class="form-control @error('boat')? is-invalid @enderror">
                                    @error('boat')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                                <div class="col-lg-4">
                                    <label>{{__('Events Tax')}}</label>
                                    <input type="number" name="event" value="{{old('event')}}" class="form-control @error('event')? is-invalid @enderror">
                                    @error('event')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                                <div class="col-lg-4">
                                    <label>{{__('Water Sports Tax')}}</label>
                                    <input type="number" name="game" value="{{old('game')}}" class="form-control @error('game')? is-invalid @enderror">
                                    @error('game')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="form-group display-none" id="org">
                            <label>{{__('Organization')}}</label>
                            <select name="organization" class="form-control select2">
                                @foreach ($orgs as $org)
                                    <option value="{{$org['id']}}">{{$org['first_name'] .' '. $org['last_name']}}</option>
                                @endforeach
                            </select>
                            @error('organization')
                                <div class="invalid-feedback">{{$message}}</div>
                            @endif
                        </div>
                        <div class="form-group">
                            
                            <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection
