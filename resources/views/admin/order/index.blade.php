@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('View Orders'),            
    ]) 

    <div class="section-body">            
       
        <div class="row">
            <div class="col-12">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif  
            </div>
            <div class="col-12">
              <div class="card">     
                <div class="card-body">
                    <div class="row mb-4 mt-2">
                        <div class="col-lg-8"><h2 class="section-title mt-0"> {{__('Order List')}}</h2></div>
                        <div class="col-lg-4 text-right">                            
                        </div>
                    </div> 
                  <div class="table-responsive">
                    
                    <table class="table" id="report_table">
                        <thead>
                            <tr>
                                <th></th>              
                                <th>{{__('Order Id')}}</th>                 
                                <th>{{__('Customer Name')}}</th>
                                <th>{{__('Event Name')}}</th>
                                <th>{{__('Date')}}</th>                                
                                <th>{{__('Sold Ticket')}}</th>                                
                                <th>{{__('Payment')}}</th>  
                                <th>{{__('Payment Gateway')}}</th>
                                <th>{{__('Order Status')}}</th> 
                                <th>{{__('Payment Status')}}</th>                                                                
                                <th>{{__('Action')}}</th>                               
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders as $item)
                                <?php 
                                    if($item->order_status=="Pending"){
                                        $status = "badge-warning";
                                    }
                                    else if($item->order_status=="Cancel"){
                                        $status = "badge-danger";
                                    }
                                    else if($item->order_status=="Complete"){
                                        $status = "badge-success";
                                    }
                                ?>
                                <tr>
                                    <td></td>
                                    <td>{{$item->order_id}} </td>
                                    <td>{{@$item->customer->name.' '.@$item->customer->last_name}}</td>
                                    <td>
                                        <h6 class="mb-0">{{$item->event->name}}</h6>
                                        <p class="mb-0">{{$item->event->start_time}}</p>
                                    </td>
                                    <td>
                                        <p class="mb-0">{{$item->created_at->format('Y-m-d')}}</p>
                                        <p class="mb-0">{{$item->created_at->format('h:i a')}}</p>
                                    </td>                                    
                                    <td>{{$item->quantity.' ticket'}}</td>
                                    <td>{{$currency.$item->payment}}</td>
                                    <td>{{$item->payment_type}}</td>
                                    <td><h6><span class="badge {{$status}}  m-1">{{$item->order_status}}</span></h6></td>                                
                                    <td><h6><span class="badge {{$item->payment_status=="1"?'badge-success': 'badge-warning'}}  m-1">{{$item->payment_status=="1"?'Completed': 'pending'}}</span></h6></td>                                
                                    <td>                                    
                                        {{-- <a href="{{url('orders/'.$item->id)}}" class="btn-icon text-primary"><i class="far fa-eye"></i></a>                                                                     --}}
                                        <a href="{{url('order-invoice/'.$item->id)}}" class="btn-icon text-primary"><i class="far fa-eye"></i></a>                                    
                                    </td>                                 
                                </tr>
                            @endforeach                           
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection

