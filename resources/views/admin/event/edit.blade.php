@extends('master')
<?php
$eventName = 'Event';
if($event->applicable_type == 3) {
    $eventName = 'Water Sports';
}
?>
@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
    'title' => __('Edit '.$eventName),
    'headerData' => __('Services') ,
    'url' => 'events' ,
    ])

    <div class="section-body">
        <div class="row">
            <div class="col-lg-8"><h2 class="section-title"> {{__('Edit '.$eventName)}}</h2></div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="POST" class="event-form" action="{{ route("events.update", [$event->id]) }}">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="form-group col-lg-6">
                                    <label>{{__('Name')}}</label>
                                    <input type="text" name="name" value="{{$event->name}}" placeholder="Name" class="form-control @error('name')? is-invalid @enderror">
                                    <input type="hidden" name="applicable_type" value="{{$event->applicable_type}}">
                                    @error('name')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                                <div class="form-group col-lg-6">
                                    <label>{{__('Category')}}</label>
                                    <select name="category_id" class="form-control select2">
                                        <option value="">Select Category</option>
                                        @foreach ($category as $item)
                                        <option value="{{$item->id}}" {{$item->id == $event->category_id ? 'Selected' : ''}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                    @error('category_id')
                                    <div class="invalid-feedback block">{{$message}}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Start Time')}}</label>
                                        <input type="text" name="start_time" id="start_time" value="{{$event->start_time}}" placeholder="Choose Start time" class="form-control date @error('start_time')? is-invalid @enderror">
                                        @error('start_time')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('End Time')}}</label>
                                        <input type="text" name="end_time" id="end_time" value="{{$event->end_time}}" placeholder="Choose End time" class="form-control date @error('end_time')? is-invalid @enderror">
                                        @error('end_time')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @if(Auth::user()->hasRole('admin'))
                            <div class="form-group">
                                <label>{{__('Organization')}}</label>
                                <select name="user_id" required class="form-control select2" id="org-for-event">
                                    <option  value="">{{__('Choose Organization')}}</option>
                                    @foreach ($users as $item)
                                    <option value="{{$item->id}}" {{$item->id==$event->user_id?'Selected':''}}>{{$item->first_name.' '.$item->last_name}}</option>
                                    @endforeach
                                </select>
                                @error('user_id')
                                <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                            @else
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            @endif
                            <div class="form-group">
                                <label>{{__('Scanner')}}</label>
                                <select name="scanner_id" class="form-control scanner_id select2">
                                    <option  value="">{{__('Choose Scanner')}}</option>
                                    @foreach ($scanner as $item)
                                    <option value="{{$item->id}}" {{$item->id==$event->scanner_id?'Selected':''}}>{{$item->first_name.' '.$item->last_name}}</option>
                                    @endforeach
                                </select>
                                @error('scanner_id')
                                <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                            <div class="row">
                                <?php if(Auth::user()->hasRole('admin')){ ?>
                                <div class="col-lg-4">
                                    <?php } else { ?>
                                    <div class="col-lg-12">
                                        <?php } ?>
                                        <div class="form-group">
                                            <label>{{__('Maximum people will join in this '.$eventName)}}</label>
                                            <input type="number" name="people" id="people" value="{{$event->people}}" placeholder="Maximum people will join in this {{$eventName}}" class="form-control @error('people')? is-invalid @enderror">
                                            @error('people')
                                            <div class="invalid-feedback">{{$message}}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <?php if(Auth::user()->hasRole('admin')){ ?>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{__('status')}}</label>
                                                <select name="status" class="form-control select2">
                                                    <option value="1" {{$event->status == 1 ? 'Selected' : ''}}>Active</option>
                                                    <option value="0" {{$event->status == 0 ? 'Selected' : ''}}>Inactive</option>
                                                </select>
                                                @error('status')
                                                <div class="invalid-feedback">{{$message}}</div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <label>{{__('Show in Popular List?')}}</label>
                                                <select name="is_popular" class="form-control select2">
                                                    <option value="1" {{$event->is_popular == 1 ? 'Selected' : ''}}>Yes</option>
                                                    <option value="0" {{$event->is_popular == 0 ? 'Selected' : ''}}>No</option>
                                                </select>
                                                @error('is_popular')
                                                <div class="invalid-feedback">{{$message}}</div>
                                                @endif
                                            </div>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="form-group">
                                    <label>{{__('Tags')}}</label>
                                    <input type="text" name="tags" value="{{$event->tags}}" class="form-control inputtags @error('tags')? is-invalid @enderror">
                                    @error('tags')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label>{{__('Description')}}</label>
                                    <textarea name="description" Placeholder ="Description" class="textarea_editor @error('description')? is-invalid @enderror">
                                {{$event->description}}
                            </textarea>
                                    @error('description')
                                    <div class="invalid-feedback block">{{$message}}</div>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-6">
                                        <h6 class="text-muted mt-4 mb-4">{{__('Location Detail')}}</h6>
                                        <div class="selectgroup">
                                            <label class="selectgroup-item">
                                                <input type="radio" name="type" {{$event->type=="online"? '' : 'checked'}} checked value="offline" class="selectgroup-input" checked="">
                                                <span class="selectgroup-button">{{__('Venue')}}</span>
                                            </label>
                                            <label class="selectgroup-item">
                                                <input type="radio" {{$event->type=="online"? 'checked' : ''}} name="type" value="online" class="selectgroup-input">
                                                <span class="selectgroup-button">{{__('Online Event')}}</span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-12">
                                        <label>{{__('Venue Title')}}</label>
                                        <input type="text" name="venue_title" id="venue_title" value="{{$event->venue_title}}" placeholder="Venue Title" class="form-control @error('venue_title')? is-invalid @enderror">
                                        @error('venue_title')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="location-detail {{old('type')=="online"? 'hide' : ''}}">
                                <div class="form-group">
                                    <label>{{__($eventName.' Address')}}</label>
                                    <input type="text" name="address" id="address" value="{{$event->address}}" placeholder="{{$eventName}} Address" class="form-control @error('address')? is-invalid @enderror">
                                    @error('address')
                                    <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{__('Latitude')}}</label>
                                            <input type="text" name="lat" id="lat" value="{{$event->lat}}" placeholder="Latitude" class="form-control @error('lat')? is-invalid @enderror">
                                            @error('lat')
                                            <div class="invalid-feedback">{{$message}}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{__('Longitude')}}</label>
                                            <input type="text" name="lang" id="lang" value="{{$event->lang}}" placeholder="Longitude" class="form-control @error('lang')? is-invalid @enderror">
                                            @error('lang')
                                            <div class="invalid-feedback">{{$message}}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="custom-switches-stacked mt-2">
                                    <label class="custom-switch pl-0">
                                        <input type="radio" name="security" {{$event->security=="0"? '' : 'checked'}} value="1" class="custom-switch-input">
                                        <span class="custom-switch-indicator"></span>
                                        <span class="custom-switch-description">{{__('Public')}}</span>
                                    </label>
                                    <label class="custom-switch pl-0">
                                        <input type="radio" name="security"  {{$event->security=="0"? 'checked' : ''}} value="0" class="custom-switch-input">
                                        <span class="custom-switch-indicator"></span>
                                        <span class="custom-switch-description">{{__('Private')}}</span>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
<style>
    .preview-container {
        display: inline-block;
    }
    .dynamicImage {
        width: 180px;
        height: 180px;
        margin-right: 10px;
        object-fit: cover;
        border: 1px solid #cacaca;
        box-shadow: 0px 0px 5px 0px #cacaca;
        border-radius: 5px;
        margin-top: 10px;
    }
</style>