@extends('master')
<?php
$eventName = 'Boat';
?>
@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
    'title' => __('Edit '.$eventName),
    'headerData' => __('Services') ,
    'url' => 'events' ,
    ])

    <div class="section-body">
        <div class="row">
            <div class="col-lg-8"><h2 class="section-title"> {{__('Edit '.$eventName)}}</h2></div>
        </div>

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <form method="post" class="event-form" action="{{ route("events.update", [$event->id]) }}" enctype="multipart/form-data" >
                            @csrf
                            @method('PUT')
                            <input type="hidden" name="id" value="{{$event->id}}">
                            <div class="form-group">
                                <label>{{__('Listing Title')}}</label>
                                <input type="text" required name="name" value="{{$event->name}}" placeholder="Name" class="form-control @error('name')? is-invalid @enderror">
                                <input type="hidden" name="applicable_type" value="1">
                                @error('name')
                                <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                            @if(Auth::user()->hasRole('admin'))
                            <div class="form-group">
                                <label>{{__('Organization')}}</label>
                                <select name="user_id" required class="form-control select2" id="org-for-event">
                                    <option  value="">{{__('Choose Organization')}}</option>
                                    @foreach ($users as $item)
                                    <option value="{{$item->id}}" {{$item->id==$event->user_id?'Selected':''}}>{{$item->first_name.' '.$item->last_name}}</option>
                                    @endforeach
                                </select>
                                @error('user_id')
                                <div class="invalid-feedback">{{$message}}</div>
                                @endif
                            </div>
                            @else
                            <input type="hidden" name="user_id" value="{{Auth::user()->id}}">
                            @endif
                            <div class="form-group">
                                <label>{{__('Description')}}</label>
                                <textarea name="description" Placeholder ="Description" class="textarea_editor @error('description')? is-invalid @enderror">
                                    {{$event->description}}
                                </textarea>
                                @error('description')
                                <div class="invalid-feedback block">{{$message}}</div>
                                @endif
                                <input type="hidden" name="type" value="offline">
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Location Type')}}</label>
                                        <input type="text" required name="location_type" id="location_type" value="{{$event->location_type}}" placeholder="Enter location type" class="form-control @error('location_type')? is-invalid @enderror">
                                        @error('location_type')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Maina Name')}}</label>
                                        <select name="maina_name" required class="form-control select2">
                                            <option  value="">{{__('Choose Maina Name')}}</option>
                                            @foreach ($maina as $item)
                                            <option value="{{$item->id}}" {{$item->id==$event->maina_name?'Selected':''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('maina_name')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Slip Number')}}</label>
                                        <input type="text" required name="slip_number" id="slip_number" value="{{$event->slip_number}}" placeholder="Enter slip number" class="form-control @error('slip_number')? is-invalid @enderror">
                                        @error('slip_number')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Dock/Floor')}}</label>
                                        <select name="dock_type" required class="form-control select2">
                                            <option  value="">{{__('Choose Dock type')}}</option>
                                            @foreach ($dockType as $item)
                                            <option value="{{$item->id}}" {{$item->id==$event->dock_type?'Selected':''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('dock_type')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Address line 1')}}</label>
                                        <input type="text" required name="address" id="address" value="{{$event->address}}" placeholder="Enter address line 1" class="form-control @error('address')? is-invalid @enderror">
                                        @error('address')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Address line 2')}}</label>
                                        <input type="text" required name="address_2" id="address_2" value="{{$event->address_2}}" placeholder="Enter address line 2" class="form-control @error('address_2')? is-invalid @enderror">
                                        @error('address_2')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('Country')}}</label>
                                        <select name="country" required class="form-control select2">
                                            <option  value="">{{__('Choose the country')}}</option>
                                            @foreach ($countries as $item)
                                            <option value="{{$item->id}}" {{$item->id==$event->country?'Selected':''}}>{{$item->country}}</option>
                                            @endforeach
                                        </select>
                                        @error('country')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('City')}}</label>
                                        <input type="text" required name="city" id="city" value="{{$event->city}}" placeholder="Enter City" class="form-control @error('city')? is-invalid @enderror">
                                        @error('city')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('Pincode')}}</label>
                                        <input type="text" required name="pincode" id="pincode" value="{{$event->pincode}}" placeholder="Enter Pincode" class="form-control @error('pincode')? is-invalid @enderror">
                                        @error('pincode')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('Allowed Items')}}</label>
                                        <select name="allowed_items[]" required class="form-control select2" multiple>
                                            <option  value="">{{__('Choose the Allowed Items')}}</option>
                                            @foreach ($allowedItems as $item)
                                            <option value="{{$item->id}}" {{ in_array($item->id,$allowedItemsReference)?'Selected':''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('allowed_items')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('Advance Notice')}}</label>
                                        <?php $advance_notice = json_decode($event->advance_notice,true); ?>
                                        <select name="advance_notice[]" required class="form-control select2" multiple>
                                            <option  value="">{{__('Choose the Advance Notice')}}</option>
                                            @foreach ($advanceNotice as $item)
                                            <option value="{{$item->id}}" {{in_array($item->id,$advance_notice)?'Selected':''}}>{{$item->time}} hour(s)</option>
                                            @endforeach
                                        </select>
                                        @error('advance_notice')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('Boat Operator')}}</label>
                                        <select name="boat_operator" required class="form-control select2">
                                            <option  value="">{{__('Choose Boat Operator')}}</option>
                                            @foreach ($boatOperator as $item)
                                            <option value="{{$item->id}}" {{$item->id==$event->boat_operator?'Selected':''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('boat_operator')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('Cancellation Policy')}}</label>
                                        <select name="cancellation_policy" required class="form-control select2">
                                            <option  value="">{{__('Choose the Cancellation Policy')}}</option>
                                            @foreach ($cancellationPolicy as $item)
                                            <option value="{{$item->id}}" {{$item->id==$event->cancellation_policy?'Selected':''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('cancellation_policy')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('Payout Fuel')}}</label>
                                        <select name="payout_type" required class="form-control select2">
                                            <option  value="">{{__('Choose the Cancellation Policy')}}</option>
                                            @foreach ($payoutType as $item)
                                            <option value="{{$item->id}}" {{$item->id==$event->payout_type?'Selected':''}}>{{$item->name}}</option>
                                            @endforeach
                                        </select>
                                        @error('payout_type')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <label>{{__('Multiple Booking')}}</label>
                                        <select name="multiple_booking_allowed" required class="form-control select2">
                                            <option  value="">{{__('Choose the Multiple Booking Option')}}</option>
                                            <option value="1" {{1==$event->multiple_booking_allowed?'Selected':''}}>Yes</option>
                                            <option value="0" {{0==$event->multiple_booking_allowed?'Selected':''}}>No</option>
                                        </select>
                                        @error('multiple_booking_allowed')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                        <br/>

                                        <label>{{__('Time Gap (Only if Multiple Booking is Yes)')}}</label>
                                        <select name="time_gap" required class="form-control select2">
                                            <option value="0" {{0==$event->time_gap?'Selected':''}}>Choose Time Gap</option>
                                            <option value="1" {{1==$event->time_gap?'Selected':''}}>1 Hour</option>
                                            <option value="2" {{2==$event->time_gap?'Selected':''}}>2 Hours</option>
                                            <option value="3" {{3==$event->time_gap?'Selected':''}}>3 Hours</option>
                                            <option value="4" {{4==$event->time_gap?'Selected':''}}>4 Hours</option>
                                            <option value="5" {{5==$event->time_gap?'Selected':''}}>5 Hours</option>
                                            <option value="6" {{6==$event->time_gap?'Selected':''}}>6 Hours</option>
                                        </select>
                                        @error('time_gap')
                                        <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Do you have commercial insurance')}}</label>&nbsp;
                                        <select name="insurance" required class="form-control select2">
                                            <option value="1" {{1==$event->insurance?'Selected':''}}>Yes</option>
                                            <option value="0" {{0==$event->insurance?'Selected':''}}>No</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-lg-6">
                                    <div class="form-group">
                                        <label>{{__('Insurance Document (Required if Commercial Insurance is Yes)')}}</label>&nbsp;
                                        <input type="file" name="insurance_file" class="form-control"> @if(!is_null($event->insurance_file))<a href="{{env('APP_URL')}}/{{$event->insurance_file}}" target="_blank">Click here to view/download</a>@endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label>{{__('Boat Price')}}</label>&nbsp;
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="row boat_price">
                                            <div class="col-lg-3 card">
                                                <input type="checkbox" id="price_1" name="boat_prices[0][hours]" value="1" {{ array_key_exists(1,$boatPrice) ? 'checked' : '' }}>
                                                <label for="price_1">{{__('1 hour')}}</label>
                                                <input type="number" name="boat_prices[0][price]" value="{{ array_key_exists(1,$boatPrice) ? $boatPrice[1] : '' }}" placeholder="Price" class="boat_price">
                                            </div>
                                            <div class="col-lg-3 card">
                                                <input type="checkbox" id="price_2" name="boat_prices[1][hours]" value="2" {{ array_key_exists(2,$boatPrice) ? 'checked' : '' }}>
                                                <label for="price_2">{{__('2 hours')}}</label>
                                                <input type="number" name="boat_prices[1][price]" value="{{ array_key_exists(2,$boatPrice) ? $boatPrice[2] : '' }}" placeholder="Price" class="boat_price">
                                            </div>
                                            <div class="col-lg-3 card">
                                                <input type="checkbox" id="price_3" name="boat_prices[2][hours]" value="3" {{ array_key_exists(3,$boatPrice) ? 'checked' : '' }}>
                                                <label for="price_3">{{__('3 hours')}}</label>
                                                <input type="number" name="boat_prices[2][price]" value="{{ array_key_exists(3,$boatPrice) ? $boatPrice[3] : '' }}" placeholder="Price" class="boat_price">
                                            </div>
                                            <div class="col-lg-3 card">
                                                <input type="checkbox" id="price_4" name="boat_prices[3][hours]" value="4" {{ array_key_exists(4,$boatPrice) ? 'checked' : '' }}>
                                                <label for="price_4">{{__('4 hours')}}</label>
                                                <input type="number" name="boat_prices[3][price]" value="{{ array_key_exists(4,$boatPrice) ? $boatPrice[4] : '' }}" placeholder="Price" class="boat_price">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <div class="row boat_price">
                                            <div class="col-lg-3 card">
                                                <input type="checkbox" id="price_5" name="boat_prices[4][hours]" value="6" {{ array_key_exists(6,$boatPrice) ? 'checked' : '' }}>
                                                <label for="price_5">{{__('6 hours')}}</label>
                                                <input type="number" name="boat_prices[4][price]" value="{{ array_key_exists(6,$boatPrice) ? $boatPrice[6] : '' }}" placeholder="Price" class="boat_price">
                                            </div>
                                            <div class="col-lg-3 card">
                                                <input type="checkbox" id="price_6" name="boat_prices[5][hours]" value="8" {{ array_key_exists(8,$boatPrice) ? 'checked' : '' }}>
                                                <label for="price_6">{{__('8 hours')}}</label>
                                                <input type="number" name="boat_prices[5][price]" value="{{ array_key_exists(8,$boatPrice) ? $boatPrice[8] : '' }}" placeholder="Price" class="boat_price">
                                            </div>
                                            <div class="col-lg-3 card">
                                                <input type="checkbox" id="price_7" name="boat_prices[6][hours]" value="24" {{ array_key_exists(24,$boatPrice) ? 'checked' : '' }}>
                                                <label for="price_7">{{__('1 day')}}</label>
                                                <input type="number" name="boat_prices[6][price]" value="{{ array_key_exists(24,$boatPrice) ? $boatPrice[24] : '' }}" placeholder="Price" class="boat_price">
                                            </div>
                                            <div class="col-lg-3 card">
                                                <input type="checkbox" id="price_8" name="boat_prices[7][hours]" value="72" {{ array_key_exists(72,$boatPrice) ? 'checked' : '' }}>
                                                <label for="price_8">{{__('3 days')}}</label>
                                                <input type="number" name="boat_prices[7][price]" value="{{ array_key_exists(72,$boatPrice) ? $boatPrice[72] : '' }}" placeholder="Price" class="boat_price">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <span style="color:red"><b>Note:</b> {{$commission->price}}% commission will be deducted from each selected price from earnings as per the policy.</span>
                                    </div>
                                </div>
                            </div><br/>
                            <div class="form-group">
                                <div class="form-group">
                                    <label>{{__('Availability Type')}}</label>
                                    <select name="availability_type" required class="form-control select2" id="availability_type">
                                        <option  value="">{{__('Choose Availability Type')}}</option>
                                        <option value="1" {{1==$event->availability_type?'Selected':''}}>Default (Monday to Friday from 8:00AM to 6:00PM)</option>
                                        <option value="2" {{2==$event->availability_type?'Selected':''}}>Custom (Customize start times for each day of the week.)</option>
                                    </select>
                                </div><br/>

                                <div class="row customAvailability {{$event->availability_type==1?'hide':''}}">
                                    <?php
                                    if (!is_null($event->availability_slots) && $event->availability_type==2){
                                        $availabilitySlots = json_decode($event->availability_slots,true);
                                     } ?>
                                    @foreach($availabilitySlots as $key => $value)
                                    <div class="col-lg-3 calenderDates">
                                        <label class="title">{{ $value['title']}}</label>
                                        <div class="row calenderDatesContents">
                                            @foreach($value['data'] as $key1 => $value1)
                                            <div class="col-lg-6">
                                                <input type="checkbox" id="availability_slots_{{$key}}_{{$key1}}_status" name="availability_slots[{{$key}}][{{$key1}}][status]" {{($value1['status'] == 'true' || $value1['status'] == true)?'checked':''}} value="true">
                                                <label for="availability_slots_{{$key}}_{{$key1}}_status">{{$value1['name']}}</label>
                                            </div>
                                            @endforeach
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <?php if(Auth::user()->hasRole('admin')){ ?>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{__('Status')}}</label>
                                            <select name="status" class="form-control select2">
                                                <option value="1" {{$event->status == 1 ? 'Selected' : ''}}>Active</option>
                                                <option value="0" {{$event->status == 0 ? 'Selected' : ''}}>Inactive</option>
                                            </select>
                                            @error('status')
                                            <div class="invalid-feedback">{{$message}}</div>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>{{__('Show in Popular List?')}}</label>
                                            <select name="is_popular" class="form-control select2">
                                                <option value="1" {{$event->is_popular == 1 ? 'Selected' : ''}}>Yes</option>
                                                <option value="0" {{$event->is_popular == 0 ? 'Selected' : ''}}>No</option>
                                            </select>
                                            @error('is_popular')
                                            <div class="invalid-feedback">{{$message}}</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script>
	function readURL(input) {
		var count = input.files.length;
		if(count > 10) {
			count = 10;
		}
		for(var i =0; i< count; i++){
			if (input.files[i]) {
				var reader = new FileReader();

				reader.onload = function (e) {
					var img = $('<img class="dynamicImage">');
					img.attr('src', e.target.result);
					img.appendTo('.preview-container');
				}
				reader.readAsDataURL(input.files[i]);
			}
		}
	}
	$(document).ready(function(){
		$(".image_upload").on('change',function(){
			$('.preview-container').html("");
			readURL(this);
		});
		$("#availability_type").on("change",function() {
			if($(this).val() == 2) {
				$(".customAvailability").removeClass('hide');
			} else {
				$(".customAvailability").addClass('hide');
			}
		})
	})
</script>
<style>
    .preview-container {
        display: inline-block;
    }
    .dynamicImage {
        width: 180px;
        height: 180px;
        margin-right: 10px;
        object-fit: cover;
        border: 1px solid #cacaca;
        box-shadow: 0px 0px 5px 0px #cacaca;
        border-radius: 5px;
        margin-top: 10px;
    }
    .boat_price {
        border-width: 0px 0px 1px 0px;
    }
    .calenderDates {
        text-align: center;
        border: 1px solid rgba(0,0,0,.125);
        border-radius: 5px;
        margin: 5px;
        position: relative;
    }
    .calenderDates label.title {
        background: #f0f0f0;
        width: 100%;
        padding: 10px;
        left: 0px;
        right: 0px;
    }
    .calenderDatesContents {
        text-align: left;
    }
</style>