@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('Services'),
    ]) 
    <script type="application/javascript">
        categories = ['all'];
    </script>
    <div class="section-body">
               
        <div class="row">
            <div class="col-12">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>
            <div class="col-12">
              <div class="card">     
                <div class="card-body">
                    <div class="row mb-4 mt-2">
                        <div class="col-lg-8"><h2 class="section-title mt-0"> {{__('All Services')}}</h2></div>
                        <div class="col-lg-4 text-right">
                            Filter: <select class="btn btn-primary add-button" id="category_filter"></select>
                            @can('event_create')
                            <select class="select2 form-control" id="addNewOptions">
                                <option value="">--Choose an option to add--</option>
                                <option value="1">Boat</option>
                                <option value="2">Events</option>
                                <option value="3">Water Sports</option>
                            </select>
<!--                            <button class="btn btn-primary add-button"><a href="{{url('events/create')}}"><i class="fas fa-plus"></i> {{__('Add New')}}</a></button>                -->
                            @endcan
                        </div>
                    </div>      
                  <div class="table-responsive">
                    <table class="table" id="report_table">
                        <thead>
                            <tr>
                                <th></th>
                                <th>{{__('Service Category')}}</th>
                                <th>{{__('Image')}}</th>
                                <th>{{__('Name')}}</th>
                                @if(Auth::user()->hasRole('admin'))
                                <th>{{__('Organization')}}</th> 
                                @endif
                                @if(Auth::user()->hasRole('organization'))
                                <th>{{__('Scanner')}}</th> 
                                @endif
                                <th>{{__('Status')}}</th> 
                                @if(Gate::check('event_edit') || Gate::check('event_delete'))                                   
                                <th>{{__('Action')}}</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody class="category_body">
                            @foreach ($events as $item)
                            <?php
                                $appli = json_decode($item,true);
                                $appli = strtolower($appli['applicable_type']['name']);
                            ?>
                                <script type="application/javascript">
                                    var appli = '{{$appli}}';
                                    appli = appli.replaceAll(' ', '_');
                                    if(!categories.includes(appli)){
	                                    categories.push(appli);
                                    }
                                </script>
                                <tr class="{{str_replace(' ', '_', $appli)}}">
                                    <td></td>
                                    <td><h6 class="mb-0">{{ucwords($appli)}}</h6></td>
                                    <th> <img class="table-img" src="{{ (count($item->pictures) > 0 ) ? url($item->pictures[0]->image_path) : '#'}}"> </th>
                                    <td>
                                        <h6 class="mb-0">{{$item->name}}</h6>
                                        <p class="mb-0">{{$item->address}} </p>
                                        <p class="mb-0">{{ strlen(trim($item->start_time)) ? Carbon\Carbon::parse($item->start_time)->format('l').', '.$item->start_time->format('Y-m-d h:i A') : "NA"}}</p>
                                    </td>
                                    @if(Auth::user()->hasRole('admin'))
                                    <td>{{@$item->organization->first_name.' '.@$item->organization->last_name}}</td>
                                    @endif  
                                    @if(Auth::user()->hasRole('organization'))
                                    <td>{{$item->scanner==null?'':$item->scanner->first_name.' '.$item->scanner->last_name}}</td>   
                                    @endif                                                                           
                                    <td>
                                        <?php
                                        $statusOfEntry = $item->status=="1"?'Publish': ((int)$item->status==-1?'Declined': ((int)$item->is_complete==1?'Pending':'Draft'));
                                        ?>
                                        <h5><span class="badge {{$item->status=="1"?'badge-success': 'badge-warning'}}  m-1">{{$statusOfEntry}}</span></h5>
                                    </td>
                                    @if(Gate::check('event_edit') || Gate::check('event_delete')) 
                                    <td>
                                        @if(strtolower($statusOfEntry) !=='draft')
                                        <a href="{{ route('events.show', $item->id) }}" title="View Event" class="btn-icon"><i class="fas fa-eye"></i></a>
                                        <a href="{{url('event-gallery/'.$item->id)}}" title="Event Gallery" class="btn-icon"><i class="far fa-images"></i></a>
                                        @can('event_edit')
                                        <a href="{{ route('events.edit', $item->id) }}" title="Edit Event" class="btn-icon"><i class="fas fa-edit"></i></a>
                                        @endcan
                                        @endif
                                        @can('event_delete')
                                        <a href="#" onclick="deleteData('events','{{$item->id}}');"  title="Delete Event" class="btn-icon text-danger"><i class="fas fa-trash-alt text-danger"></i></a>
                                        @endcan
                                    </td>
                                    @endif
                                </tr>
                            @endforeach                           
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>

<script type="application/javascript">
    $(document).ready(function(){
	    var selectOptions = '';
	    categories.forEach(function(item){
		    selectOptions+= '<option value="'+item+'">'+item.toUpperCase().replaceAll('_',' ')+'</option>';
	    });
	    $('#category_filter').html(selectOptions);
	    $('#category_filter').on('change',function(){
            var selected = $(this).val();
            if(selected=='all'){
                $('.category_body tr').removeClass('hide');
            } else {
                $('.category_body tr').addClass('hide');
                $('.category_body tr.'+selected).removeClass('hide');
            }
        })
        $("#addNewOptions").on('change',function(){
            var selected = $(this).val();
            if(selected!==''){
	            window.location.href = "{{url('events/create')}}"+"?category="+selected;
            }
        })
    })
</script>
@endsection
