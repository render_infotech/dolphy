@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('Edit Location'),  
        'headerData' => __('Location') ,
        'url' => 'location' ,          
    ]) 

    <div class="section-body">
        <div class="row">
            <div class="col-lg-8"><h2 class="section-title"> {{__('Edit Location')}}</h2></div>            
        </div>       
       
        <div class="row">
            <div class="col-12">
              <div class="card">     
                <div class="card-body">
                    <form method="post" action="{{route("location.update", [$location->id])}}">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{__('Name')}}</label>
                                    <input type="text" name="name" value="{{$location->name}}" required placeholder="Name" class="form-control @error('name')? is-invalid @enderror">
                                    @error('name')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label>{{__('Address')}}</label>
                                    <input type="text" name="address" value="{{$location->address}}" required placeholder="Address" class="form-control @error('address')? is-invalid @enderror">
                                    @error('address')
                                        <div class="invalid-feedback">{{$message}}</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{__('Latitude')}}</label>
                                        <input type="text" name="lat" value="{{$location->lat}}" required placeholder="Latitude" class="form-control @error('lat')? is-invalid @enderror">
                                        @error('lat')
                                            <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{__('Longitude')}}</label>
                                        <input type="text" name="lang" value="{{$location->lang}}" required placeholder="Longitude" class="form-control @error('lang')? is-invalid @enderror">
                                        @error('lang')
                                            <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <label>{{__('status')}}</label>
                                        <select name="status" class="form-control select2">
                                            <option value="1" {{$location->status=="1" ? 'Selected' : ''}}>Active</option>   
                                            <option value="0" {{$location->status=="0" ? 'Selected' : ''}}>Inactive</option>   
                                        </select>
                                        @error('status')
                                            <div class="invalid-feedback">{{$message}}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div id="location_map" class="map"></div>
                            </div>
                        </div>
                        
                       
                        <div class="form-group">                            
                            <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>                            
                        </div>
                    </form>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection
