@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
        'title' => __('Location'),            
    ]) 

    <div class="section-body">
        <div class="row">
            <div class="col-lg-8"><h2 class="section-title"> {{__('View Location')}}</h2></div>
            <div class="col-lg-4 text-right">
                @can('role_create')
                 <button class="btn btn-primary add-button"><a href="{{url('location/create')}}"><i class="fas fa-plus"></i> {{__('Add New')}}</a></button>                
                @endcan
            </div>
        </div>       
       
        <div class="row">
            <div class="col-12">
                @if (session('status'))
                <div class="alert alert-success alert-dismissible fade show" role="alert">
                    {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
            </div>
            <div class="col-12">
              <div class="card">     
                <div class="card-body">
                  <div class="table-responsive">
                    <table class="table" id="report_table">
                        <thead>
                            <tr>
                                <th></th>                                
                                <th>{{__('name')}}</th>
                                <th>{{__('Address')}}</th>
                                <th>{{__('Status')}}</th> 
                                @if(Gate::check('location_edit') || Gate::check('location_delete'))                                   
                                <th>{{__('Action')}}</th>
                                @endif
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($location as $item)
                                <tr>
                                    <td></td>                                    
                                    <td>{{$item->name}}</td>                                                                                   
                                    <td>{{$item->address}}</td>
                                    <td><h5><span class="badge {{$item->status=="1"?'badge-success': 'badge-warning'}}  m-1">{{$item->status=="1"?'Active': 'Inactive'}}</span></h5></td>
                                    @if(Gate::check('location_edit') || Gate::check('location_delete')) 
                                    <td>
                                        @can('location_edit')
                                        <a href="{{ route('location.edit', $item->id) }}" class="btn-icon"><i class="fas fa-edit"></i></a>
                                        @endcan
                                        @can('location_delete')
                                        <a href="#" class="btn-icon text-danger"><i class="fas fa-trash-alt"></i></a>
                                        @endcan
                                    </td>
                                    @endif
                                </tr>
                            @endforeach                           
                        </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    </section>
@endsection
