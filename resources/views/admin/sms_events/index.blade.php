@extends('master')

@section('content')
<section class="section">
    @include('admin.layout.breadcrumbs', [
    'title' => __('OTP Details'),
    ])

    <div class="section-body">

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <div class="row mb-4 mt-2">
                            <div class="col-lg-8"><h2 class="section-title mt-0"> {{__('OTP Details')}}</h2></div>
                            <div class="col-lg-4 text-right">
                            </div>
                        </div>
                        <div class="table-responsive">

                            <table class="table" id="sms_report_table">
                                <thead>
                                <tr>
                                    <th>{{__('Mobile')}}</th>
                                    <th>{{__('OTP')}}</th>
                                    <th>{{__('Verify Attempts')}}</th>
                                    <th>{{__('Resend Attempts')}}</th>
                                    <th>{{__('Verified At')}}</th>
                                    <th>{{__('Valid Till')}}</th>
                                    <th>{{__('SMS Content')}}</th>
                                    <th>{{__('Created')}}</th>
                                    <th>{{__('Updated')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($sms_events as $item)
                                <?php
                                $otp = NULL;
                                if($item->is_otp){
                                    $otp = $item->otp;
                                }
                                ?>
                                <tr>
                                    <td>{{$item->mobile}}</td>
                                    <td>{{$otp}}</td>
                                    <td>{{$item->attempts}}</td>
                                    <td>{{$item->resend_attempts}}</td>
                                    <td>{{$item->verified_at}}</td>
                                    <td>{{$item->valid_till}}</td>
                                    <td>{{$item->sms_content}}</td>
                                    <td>{{$item->created_at}}</td>
                                    <td>{{$item->updated_at}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

