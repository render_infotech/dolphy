@extends('frontend.master', ['activePage' => 'profile'])

@section('content')

    <section class="agent-single">
        <div class="profile-top-section" style="background-image: url('https://wallpapercave.com/wp/q1QDvC6.jpg')">
            <div class="profile-overly"></div>
        </div>
        <div class="container">
            <div class="row">
                @if($payment->paid_status == 1)
                    <div class="col-md-12">
                        <div class="alert alert-success">
                            <h4>Payment Successful</h4>
                            <p>Your payment has been successfully completed. <br/>Your Transaction reference number: <b><u>{{$payment->stripe_payment_id}}</u></b></p>
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="alert alert-danger">
                            <h4>Payment Failed/Pending</h4>
                            <p>Your payment has been failed/pending for update, kindly refresh the page after sometime if payment is done successfully or Contact support with your Transaction reference number: <b><u>{{$payment->stripe_payment_id}}</u></b></p>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </section>


@endsection