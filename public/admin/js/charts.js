
"use strict";
var events,revenue;
$( document ).ready(function() {
    // event map
    var a = {  gray: {
        100: "#f6f9fc",
        200: "#e9ecef",
        300: "#dee2e6",
        400: "#ced4da",
        500: "#adb5bd" 
    }              
    };
    
    if($('#events').val()){
        events = JSON.parse($('#events').val()); 
              
    }

    var location_marker = [];
    if(events){            
        // for (let i = 0; i < events.length; i++) {
        //     var latlang = [];
        //     if(events[i].lat != null || events[i].lang!=null){
        //         latlang.push(events[i].lat);
        //         latlang.push(events[i].lang);
        //         location_marker.push({
        //             latLng:latlang,
        //             name:events[i].name
        //         }); 
        //     }
                          
        // }
    }

    $('#eventMap').vectorMap({
        map: 'world_mill',
        zoomOnScroll: !1,
        scaleColors: ["#f00", "#0071A4"],
        normalizeFunction: "polynomial",
        hoverOpacity: .7,
        hoverColor: !1,
        backgroundColor: "transparent",
        regionStyle: {
            initial: {
                fill: a.gray[200],
                "fill-opacity": .8,
                stroke: "none",
                "stroke-width": 0,
                "stroke-opacity": 1
            },
            hover: {
                fill: a.gray[300],
                "fill-opacity": .8,
                cursor: "pointer"
            },              
        },
        markerStyle: {
            initial: {
                fill: "#fb6340",
                "stroke-width": 0
            },
            hover: {
                fill: "#5e72e4",
                "stroke-width": 0
            }
        },
        markers: location_marker,
        series: {
            regions: [{
                values: {
                    AU: 760,
                    BR: 550,
                    CA: 120,
                    DE: 1300,
                    FR: 540,
                    GB: 690,
                    GE: 200,
                    IN: 200,
                    RO: 600,
                    RU: 300,
                    US: 2920
                },
                scale: [a.gray[400], a.gray[500]],
                normalizeFunction: "polynomial"
            }]
        }
    });

    // if($('#revenue').val()){
    //     var ctx = document.getElementById('revenue-pie-chart');
    //     revenue = JSON.parse($('#revenue').val());        
    //     var label = [],data = [],color = [];
       
    //         for (let i = 0; i < revenue.length; i++) {
    //             label.push(revenue[i].name);  
    //             data.push(revenue[i].earning);  
    //             color.push( 'rgb(' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ',' + (Math.floor(Math.random() * 256)) + ')');  
    //         }                
    //         var isAllZero = data.reduce((a, b) => a + b) > 0 ? false : true;       
    //         if(isAllZero==true){
    //             var blank_data = [100];
    //             var myPieChart = new Chart(ctx, {
    //                 type: 'doughnut',
    //                 data:  {
    //                     labels: ['No data'],
    //                     datasets: [{
    //                         data: blank_data,
    //                         backgroundColor: ['#eee'],                  
    //                         label: "Restaurant Data",
    //                         borderWidth: 2
    //                     }]
    //                 },
    //                 options: {
    //                     cutoutPercentage:40,
    //                     responsive: !0,
    //                     legend: {
    //                         position: "center",                    
    //                     },                      
    //                     animation: {
    //                         duration:1000,
    //                         easing:"easeOutQuad",
    //                         animateScale: !0,
    //                         animateRotate: !0                    
    //                     },
                        
    //                 }
    //                 });
    //         }
    //         else{
    //             var myPieChart = new Chart(ctx, {
    //                 type: 'doughnut',
    //                 data:  {
    //                     labels: label,
    //                     datasets: [{
    //                         data: data,
    //                         backgroundColor: color,                  
    //                         label: "Restaurant Data",
    //                         borderWidth: 2
    //                     }]
    //             },
    //             options: {
    //                 cutoutPercentage:40,
    //                 percentageInnerCutout: 40,
    //                 responsive: 1,
    //                 legend: {                    
    //                     position: "bottom",                    
    //                 },
                    
    //                 animation: {
    //                     duration:1000,
    //                     easing:"easeOutQuad",
    //                     animateScale: !0,
    //                     animateRotate: !0                    
    //                 },                    
    //             }
    //             });
    //         }
    // }   

});