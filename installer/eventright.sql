-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 25, 2021 at 02:38 PM
-- Server version: 10.4.20-MariaDB
-- PHP Version: 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eventright_blank_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_user`
--

CREATE TABLE `app_user` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `remember_token` varchar(255) DEFAULT NULL,
  `email_verified_at` datetime DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `following` varchar(255) DEFAULT NULL,
  `favorite` varchar(255) DEFAULT NULL,
  `favorite_blog` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lang` varchar(255) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `provider_token` varchar(255) DEFAULT NULL,
  `device_token` varchar(255) DEFAULT NULL,
  `bio` text DEFAULT NULL,
  `language` text DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `banner`
--

CREATE TABLE `banner` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `image` varchar(255) NOT NULL,
  `tags` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `coupon`
--

CREATE TABLE `coupon` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `coupon_code` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `discount` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `start_date` varchar(255) NOT NULL,
  `end_date` varchar(255) NOT NULL,
  `max_use` int(11) NOT NULL,
  `use_count` int(11) NOT NULL DEFAULT 0,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `currency`
--

CREATE TABLE `currency` (
  `id` int(11) NOT NULL,
  `country` varchar(100) DEFAULT NULL,
  `currency` varchar(100) DEFAULT NULL,
  `code` varchar(100) DEFAULT NULL,
  `symbol` varchar(100) CHARACTER SET utf8 DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `currency`
--

INSERT INTO `currency` (`id`, `country`, `currency`, `code`, `symbol`) VALUES
(1, 'Albania', 'Leke', 'ALL', 'Lek'),
(2, 'America', 'Dollars', 'USD', '$'),
(3, 'Afghanistan', 'Afghanis', 'AFN', '؋'),
(4, 'Argentina', 'Pesos', 'ARS', '$'),
(5, 'Aruba', 'Guilders', 'AWG', 'Afl'),
(6, 'Australia', 'Dollars', 'AUD', '$'),
(7, 'Azerbaijan', 'New Manats', 'AZN', '₼'),
(8, 'Bahamas', 'Dollars', 'BSD', '$'),
(9, 'Barbados', 'Dollars', 'BBD', '$'),
(10, 'Belarus', 'Rubles', 'BYR', 'p.'),
(11, 'Belgium', 'Euro', 'EUR', '€'),
(12, 'Beliz', 'Dollars', 'BZD', 'BZ$'),
(13, 'Bermuda', 'Dollars', 'BMD', '$'),
(14, 'Bolivia', 'Bolivianos', 'BOB', '$b'),
(15, 'Bosnia and Herzegovina', 'Convertible Marka', 'BAM', 'KM'),
(16, 'Botswana', 'Pula', 'BWP', 'P'),
(17, 'Bulgaria', 'Leva', 'BGN', 'Лв.'),
(18, 'Brazil', 'Reais', 'BRL', 'R$'),
(19, 'Britain (United Kingdom)', 'Pounds', 'GBP', '£\r\n'),
(20, 'Brunei Darussalam', 'Dollars', 'BND', '$'),
(21, 'Cambodia', 'Riels', 'KHR', '៛'),
(22, 'Canada', 'Dollars', 'CAD', '$'),
(23, 'Cayman Islands', 'Dollars', 'KYD', '$'),
(24, 'Chile', 'Pesos', 'CLP', '$'),
(25, 'China', 'Yuan Renminbi', 'CNY', '¥'),
(26, 'Colombia', 'Pesos', 'COP', '$'),
(27, 'Costa Rica', 'Colón', 'CRC', '₡'),
(28, 'Croatia', 'Kuna', 'HRK', 'kn'),
(29, 'Cuba', 'Pesos', 'CUP', '₱'),
(30, 'Cyprus', 'Euro', 'EUR', '€'),
(31, 'Czech Republic', 'Koruny', 'CZK', 'Kč'),
(32, 'Denmark', 'Kroner', 'DKK', 'kr'),
(33, 'Dominican Republic', 'Pesos', 'DOP ', 'RD$'),
(34, 'East Caribbean', 'Dollars', 'XCD', '$'),
(35, 'Egypt', 'Pounds', 'EGP', '£'),
(36, 'El Salvador', 'Colones', 'SVC', '$'),
(37, 'England (United Kingdom)', 'Pounds', 'GBP', '£'),
(38, 'Euro', 'Euro', 'EUR', '€'),
(39, 'Falkland Islands', 'Pounds', 'FKP', '£'),
(40, 'Fiji', 'Dollars', 'FJD', '$'),
(41, 'France', 'Euro', 'EUR', '€'),
(42, 'Ghana', 'Cedis', 'GHC', 'GH₵'),
(43, 'Gibraltar', 'Pounds', 'GIP', '£'),
(44, 'Greece', 'Euro', 'EUR', '€'),
(45, 'Guatemala', 'Quetzales', 'GTQ', 'Q'),
(46, 'Guernsey', 'Pounds', 'GGP', '£'),
(47, 'Guyana', 'Dollars', 'GYD', '$'),
(48, 'Holland (Netherlands)', 'Euro', 'EUR', '€'),
(49, 'Honduras', 'Lempiras', 'HNL', 'L'),
(50, 'Hong Kong', 'Dollars', 'HKD', '$'),
(51, 'Hungary', 'Forint', 'HUF', 'Ft'),
(52, 'Iceland', 'Kronur', 'ISK', 'kr'),
(53, 'India', 'Rupees', 'INR', '₹'),
(54, 'Indonesia', 'Rupiahs', 'IDR', 'Rp'),
(55, 'Iran', 'Rials', 'IRR', '﷼'),
(56, 'Ireland', 'Euro', 'EUR', '€'),
(57, 'Isle of Man', 'Pounds', 'IMP', '£'),
(58, 'Israel', 'New Shekels', 'ILS', '₪'),
(59, 'Italy', 'Euro', 'EUR', '€'),
(60, 'Jamaica', 'Dollars', 'JMD', 'J$'),
(61, 'Japan', 'Yen', 'JPY', '¥'),
(62, 'Jersey', 'Pounds', 'JEP', '£'),
(63, 'Kazakhstan', 'Tenge', 'KZT', '₸'),
(64, 'Korea (North)', 'Won', 'KPW', '₩'),
(65, 'Korea (South)', 'Won', 'KRW', '₩'),
(66, 'Kyrgyzstan', 'Soms', 'KGS', 'Лв'),
(67, 'Laos', 'Kips', 'LAK', '	₭'),
(68, 'Latvia', 'Lati', 'LVL', 'Ls'),
(69, 'Lebanon', 'Pounds', 'LBP', '£'),
(70, 'Liberia', 'Dollars', 'LRD', '$'),
(71, 'Liechtenstein', 'Switzerland Francs', 'CHF', 'CHF'),
(72, 'Lithuania', 'Litai', 'LTL', 'Lt'),
(73, 'Luxembourg', 'Euro', 'EUR', '€'),
(74, 'Macedonia', 'Denars', 'MKD', 'Ден\r\n'),
(75, 'Malaysia', 'Ringgits', 'MYR', 'RM'),
(76, 'Malta', 'Euro', 'EUR', '€'),
(77, 'Mauritius', 'Rupees', 'MUR', '₹'),
(78, 'Mexico', 'Pesos', 'MXN', '$'),
(79, 'Mongolia', 'Tugriks', 'MNT', '₮'),
(80, 'Mozambique', 'Meticais', 'MZN', 'MT'),
(81, 'Namibia', 'Dollars', 'NAD', '$'),
(82, 'Nepal', 'Rupees', 'NPR', '₹'),
(83, 'Netherlands Antilles', 'Guilders', 'ANG', 'ƒ'),
(84, 'Netherlands', 'Euro', 'EUR', '€'),
(85, 'New Zealand', 'Dollars', 'NZD', '$'),
(86, 'Nicaragua', 'Cordobas', 'NIO', 'C$'),
(87, 'Nigeria', 'Nairas', 'NGN', '₦'),
(88, 'North Korea', 'Won', 'KPW', '₩'),
(89, 'Norway', 'Krone', 'NOK', 'kr'),
(90, 'Oman', 'Rials', 'OMR', '﷼'),
(91, 'Pakistan', 'Rupees', 'PKR', '₹'),
(92, 'Panama', 'Balboa', 'PAB', 'B/.'),
(93, 'Paraguay', 'Guarani', 'PYG', 'Gs'),
(94, 'Peru', 'Nuevos Soles', 'PEN', 'S/.'),
(95, 'Philippines', 'Pesos', 'PHP', 'Php'),
(96, 'Poland', 'Zlotych', 'PLN', 'zł'),
(97, 'Qatar', 'Rials', 'QAR', '﷼'),
(98, 'Romania', 'New Lei', 'RON', 'lei'),
(99, 'Russia', 'Rubles', 'RUB', '₽'),
(100, 'Saint Helena', 'Pounds', 'SHP', '£'),
(101, 'Saudi Arabia', 'Riyals', 'SAR', '﷼'),
(102, 'Serbia', 'Dinars', 'RSD', 'ع.د'),
(103, 'Seychelles', 'Rupees', 'SCR', '₹'),
(104, 'Singapore', 'Dollars', 'SGD', '$'),
(105, 'Slovenia', 'Euro', 'EUR', '€'),
(106, 'Solomon Islands', 'Dollars', 'SBD', '$'),
(107, 'Somalia', 'Shillings', 'SOS', 'S'),
(108, 'South Africa', 'Rand', 'ZAR', 'R'),
(109, 'South Korea', 'Won', 'KRW', '₩'),
(110, 'Spain', 'Euro', 'EUR', '€'),
(111, 'Sri Lanka', 'Rupees', 'LKR', '₹'),
(112, 'Sweden', 'Kronor', 'SEK', 'kr'),
(113, 'Switzerland', 'Francs', 'CHF', 'CHF'),
(114, 'Suriname', 'Dollars', 'SRD', '$'),
(115, 'Syria', 'Pounds', 'SYP', '£'),
(116, 'Taiwan', 'New Dollars', 'TWD', 'NT$'),
(117, 'Thailand', 'Baht', 'THB', '฿'),
(118, 'Trinidad and Tobago', 'Dollars', 'TTD', 'TT$'),
(119, 'Turkey', 'Lira', 'TRY', 'TL'),
(120, 'Turkey', 'Liras', 'TRL', '₺'),
(121, 'Tuvalu', 'Dollars', 'TVD', '$'),
(122, 'Ukraine', 'Hryvnia', 'UAH', '₴'),
(123, 'United Kingdom', 'Pounds', 'GBP', '£'),
(124, 'United States of America', 'Dollars', 'USD', '$'),
(125, 'Uruguay', 'Pesos', 'UYU', '$U'),
(126, 'Uzbekistan', 'Sums', 'UZS', 'so\'m'),
(127, 'Vatican City', 'Euro', 'EUR', '€'),
(128, 'Venezuela', 'Bolivares Fuertes', 'VEF', 'Bs'),
(129, 'Vietnam', 'Dong', 'VND', '₫\r\n'),
(130, 'Yemen', 'Rials', 'YER', '﷼'),
(131, 'Zimbabwe', 'Zimbabwe Dollars', 'ZWD', 'Z$');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `scanner_id` int(11) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `category_id` int(11) NOT NULL,
  `start_time` varchar(255) NOT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `gallery` varchar(255) DEFAULT NULL,
  `people` int(11) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `lang` varchar(255) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `security` int(11) NOT NULL DEFAULT 1,
  `tags` varchar(255) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `event_status` varchar(255) NOT NULL DEFAULT 'Pending',
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `event_report`
--

CREATE TABLE `event_report` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `message` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `faq`
--

CREATE TABLE `faq` (
  `id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` text DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `message` text NOT NULL,
  `rate` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `general_settng`
--

CREATE TABLE `general_settng` (
  `id` int(11) NOT NULL,
  `app_name` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `logo` varchar(255) DEFAULT NULL,
  `favicon` varchar(255) DEFAULT NULL,
  `app_version` varchar(255) DEFAULT NULL,
  `footer_copyright` varchar(255) DEFAULT NULL,
  `radius` int(11) DEFAULT NULL,
  `map_key` varchar(255) DEFAULT NULL,
  `currency` varchar(50) DEFAULT NULL,
  `timezone` varchar(255) DEFAULT NULL,
  `user_verify` int(11) DEFAULT NULL,
  `verify_by` varchar(255) DEFAULT NULL,
  `default_lat` varchar(255) DEFAULT NULL,
  `default_long` varchar(255) DEFAULT NULL,
  `org_commission_type` varchar(50) DEFAULT NULL,
  `org_commission` int(11) DEFAULT NULL,
  `push_notification` int(11) DEFAULT NULL,
  `onesignal_app_id` varchar(255) DEFAULT NULL,
  `onesignal_project_number` varchar(255) DEFAULT NULL,
  `onesignal_api_key` varchar(255) DEFAULT NULL,
  `onesignal_auth_key` varchar(255) DEFAULT NULL,
  `or_onesignal_app_id` varchar(255) DEFAULT NULL,
  `or_onesignal_project_number` varchar(255) DEFAULT NULL,
  `or_onesignal_api_key` varchar(255) DEFAULT NULL,
  `or_onesignal_auth_key` varchar(255) DEFAULT NULL,
  `mail_notification` int(11) NOT NULL,
  `mail_host` varchar(255) DEFAULT NULL,
  `mail_port` int(11) DEFAULT NULL,
  `mail_username` varchar(255) DEFAULT NULL,
  `mail_password` varchar(255) DEFAULT NULL,
  `sender_email` varchar(255) DEFAULT NULL,
  `sms_notification` int(11) NOT NULL,
  `twilio_account_id` varchar(255) DEFAULT NULL,
  `twilio_auth_token` varchar(255) DEFAULT NULL,
  `twilio_phone_number` varchar(255) DEFAULT NULL,
  `help_center` text DEFAULT NULL,
  `privacy_policy` text DEFAULT NULL,
  `cookie_policy` text DEFAULT NULL,
  `terms_services` text DEFAULT NULL,
  `privacy_policy_organizer` text DEFAULT NULL,
  `terms_use_organizer` text DEFAULT NULL,
  `acknowledgement` varchar(255) DEFAULT NULL,
  `primary_color` varchar(255) DEFAULT NULL,
  `license_key` varchar(255) DEFAULT NULL,
  `license_name` varchar(255) DEFAULT NULL,
  `license_status` int(11) NOT NULL DEFAULT 0,
  `language` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `general_settng`
--

INSERT INTO `general_settng` (`id`, `app_name`, `email`, `logo`, `favicon`, `app_version`, `footer_copyright`, `radius`, `map_key`, `currency`, `timezone`, `user_verify`, `verify_by`, `default_lat`, `default_long`, `org_commission_type`, `org_commission`, `push_notification`, `onesignal_app_id`, `onesignal_project_number`, `onesignal_api_key`, `onesignal_auth_key`, `or_onesignal_app_id`, `or_onesignal_project_number`, `or_onesignal_api_key`, `or_onesignal_auth_key`, `mail_notification`, `mail_host`, `mail_port`, `mail_username`, `mail_password`, `sender_email`, `sms_notification`, `twilio_account_id`, `twilio_auth_token`, `twilio_phone_number`, `help_center`, `privacy_policy`, `cookie_policy`, `terms_services`, `privacy_policy_organizer`, `terms_use_organizer`, `acknowledgement`, `primary_color`, `license_key`, `license_name`, `license_status`, `language`, `created_at`, `updated_at`) VALUES
(1, 'EventRight', 'eventright@gmail.com', '6124803bbe692.png', '60e548fc5c030.png', '1.0', 'Copyright © 2021', 50, 'errdgfc', 'INR', 'Asia/Hong_Kong', 0, 'email', '25.2048', '55.2708', 'amount', 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL, NULL, 0, 'AC0ca07e457c6147cbccccb4d7907b0a5f', '05d47c7fb6622c3c8a5ccb81f2e16290', '+15616091186', 'https://thirstydevs.com/', 'https://thirstydevs.com/', 'https://thirstydevs.com/', 'https://thirstydevs.com/', '<p style=\"-webkit-tap-highlight-color: transparent; outline: 0px; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.01) 0px 0px 1px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgba(0, 0, 0, 0.8); font-family: Avenirnextltpro, sans-serif; font-size: 16px;\">3.2 We may process data about your use of our website and services (“<span style=\"-webkit-tap-highlight-color: transparent; outline: 0px; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.01) 0px 0px 1px; font-weight: 700;\">usage data</span>“). The usage data may include your IP address, geographical location, browser type and version, operating system, referral source, length of visit, page views and website navigation paths, as well as information about the timing, frequency and pattern of your service use. The source of the usage data is Google Analytics. This usage data may be processed for the purposes of analysing the use of the website and services. The legal basis for this processing is consent.</p><p style=\"-webkit-tap-highlight-color: transparent; outline: 0px; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.01) 0px 0px 1px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgba(0, 0, 0, 0.8); font-family: Avenirnextltpro, sans-serif; font-size: 16px;\">3.3 We may process your account data (“<span style=\"-webkit-tap-highlight-color: transparent; outline: 0px; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.01) 0px 0px 1px; font-weight: 700;\">account data</span>“). The account data may include your name and email address. The account data may be processed for the purposes of operating our website, providing our services, ensuring the security of our website and services, maintaining back-ups of our databases and communicating with you. The legal basis for this processing is consent.</p>', '<p style=\"-webkit-tap-highlight-color: transparent; outline: 0px; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.01) 0px 0px 1px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgba(0, 0, 0, 0.8); font-family: Avenirnextltpro, sans-serif; font-size: 16px;\">The “Site”: The website store.tms-plugins.com, is owned by TMS Ltd. (“TMS”). It allows users to buy licenses to use certain software products as defined beneath. The website, its satellites wpdatatables.com, wpreportbuilde.com, wpamelia.com, and their subdomains, are mutually referred to as the Site.</p><p style=\"-webkit-tap-highlight-color: transparent; outline: 0px; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.01) 0px 0px 1px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgba(0, 0, 0, 0.8); font-family: Avenirnextltpro, sans-serif; font-size: 16px;\">&nbsp;</p><p style=\"-webkit-tap-highlight-color: transparent; outline: 0px; -webkit-font-smoothing: antialiased; text-shadow: rgba(0, 0, 0, 0.01) 0px 0px 1px; margin-right: 0px; margin-bottom: 16px; margin-left: 0px; color: rgba(0, 0, 0, 0.8); font-family: Avenirnextltpro, sans-serif; font-size: 16px;\">The “Software”: Amelia, wpDataTables, and the Powerful Filters, Gravity Forms ® integration, Formidable Forms ® integration, and Report Builder wpDataTables addons jointly define the Software which is available through their use. Free “Lite” versions of any of the above are also contained within in the Software.</p>', 'https://thirstydevs.com/', '#fec009', '3663-E8F6-8OAB-F641', 'saasmonks', 1, 'English', '2020-12-21 00:00:00', '2021-08-24 11:23:35');

-- --------------------------------------------------------

--
-- Table structure for table `language`
--

CREATE TABLE `language` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `json_file` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `direction` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `language`
--

INSERT INTO `language` (`id`, `name`, `json_file`, `image`, `direction`, `status`, `created_at`, `updated_at`) VALUES
(1, 'English', 'English.json', 'English.png', 'ltr', 1, '2021-08-24 01:09:46', '2021-08-24 01:09:46'),
(2, 'Arabic', 'Arabic.json', 'Arabic.png', 'rtl', 1, '2021-08-24 01:10:26', '2021-08-24 01:11:51'),
(3, 'French', 'French.json', 'French.png', 'ltr', 1, '2021-08-24 01:17:34', '2021-08-24 01:17:34');

-- --------------------------------------------------------

--
-- Table structure for table `location`
--

CREATE TABLE `location` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `address` text NOT NULL,
  `lat` varchar(255) NOT NULL,
  `lang` varchar(255) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `location`
--

INSERT INTO `location` (`id`, `name`, `address`, `lat`, `lang`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Rajkot', 'rajkot gujarat india', '22.3039', '70.8022', 1, '2020-12-16 05:08:40', '2020-12-16 05:13:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_12_15_112353_create_permission_tables', 2),
(5, '2016_06_01_000001_create_oauth_auth_codes_table', 3),
(6, '2016_06_01_000002_create_oauth_access_tokens_table', 3),
(7, '2016_06_01_000003_create_oauth_refresh_tokens_table', 3),
(8, '2016_06_01_000004_create_oauth_clients_table', 3),
(9, '2016_06_01_000005_create_oauth_personal_access_clients_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(1, 'App\\Models\\User', 1),
(2, 'App\\Models\\User', 2),
(2, 'App\\Models\\User', 3),
(2, 'App\\Models\\User', 4),
(2, 'App\\Models\\User', 5),
(2, 'App\\Models\\User', 8),
(2, 'App\\Models\\User', 13),
(2, 'App\\Models\\User', 15),
(2, 'App\\Models\\User', 16),
(2, 'App\\Models\\User', 17),
(2, 'App\\Models\\User', 18),
(2, 'App\\Models\\User', 19),
(2, 'App\\Models\\User', 20),
(2, 'App\\Models\\User', 21),
(2, 'App\\Models\\User', 22),
(2, 'App\\Models\\User', 23),
(2, 'App\\Models\\User', 24),
(2, 'App\\Models\\User', 25),
(2, 'App\\Models\\User', 26),
(2, 'App\\Models\\User', 27),
(2, 'App\\Models\\User', 28),
(2, 'App\\Models\\User', 30),
(2, 'App\\Models\\User', 31),
(2, 'App\\Models\\User', 32),
(2, 'App\\Models\\User', 33),
(2, 'App\\Models\\User', 35),
(2, 'App\\Models\\User', 36),
(2, 'App\\Models\\User', 38),
(2, 'App\\Models\\User', 39),
(2, 'App\\Models\\User', 40),
(2, 'App\\Models\\User', 41),
(2, 'App\\Models\\User', 43),
(2, 'App\\Models\\User', 44),
(2, 'App\\Models\\User', 45),
(2, 'App\\Models\\User', 46),
(2, 'App\\Models\\User', 48),
(2, 'App\\Models\\User', 49),
(2, 'App\\Models\\User', 50),
(2, 'App\\Models\\User', 51),
(2, 'App\\Models\\User', 52),
(2, 'App\\Models\\User', 56),
(2, 'App\\Models\\User', 57),
(2, 'App\\Models\\User', 58),
(2, 'App\\Models\\User', 59),
(2, 'App\\Models\\User', 62),
(2, 'App\\Models\\User', 64),
(2, 'App\\Models\\User', 65),
(2, 'App\\Models\\User', 66),
(2, 'App\\Models\\User', 67),
(2, 'App\\Models\\User', 68),
(2, 'App\\Models\\User', 70),
(2, 'App\\Models\\User', 71),
(2, 'App\\Models\\User', 74),
(2, 'App\\Models\\User', 75),
(3, 'App\\Models\\User', 3),
(3, 'App\\Models\\User', 10),
(3, 'App\\Models\\User', 11),
(3, 'App\\Models\\User', 12),
(3, 'App\\Models\\User', 34),
(3, 'App\\Models\\User', 42),
(3, 'App\\Models\\User', 47),
(3, 'App\\Models\\User', 53),
(3, 'App\\Models\\User', 54),
(3, 'App\\Models\\User', 55),
(3, 'App\\Models\\User', 63),
(3, 'App\\Models\\User', 69),
(3, 'App\\Models\\User', 72),
(3, 'App\\Models\\User', 73),
(4, 'App\\Models\\User', 14),
(6, 'App\\Models\\User', 15);

-- --------------------------------------------------------

--
-- Table structure for table `notification`
--

CREATE TABLE `notification` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `organizer_id` int(11) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `message` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `notification_template`
--

CREATE TABLE `notification_template` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subject` varchar(255) NOT NULL,
  `mail_content` text DEFAULT NULL,
  `message_content` text DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `notification_template`
--

INSERT INTO `notification_template` (`id`, `title`, `subject`, `mail_content`, `message_content`, `created_at`, `updated_at`) VALUES
(4, 'Book Ticket', 'Book Ticket', '<p>Hi {{user_name}},</p><p>your {{quantity}} ticket booked for event {{event_name}} on {{date}} successfully.</p><p>From {{app_name}}</p>', 'Hi {{user_name}}, your {{quantity}} ticket booked for event {{event_name}} on {{date}} successfully. From {{app_name}}', '2021-01-04 06:14:55', '2021-01-04 06:14:55'),
(5, 'Reset Password', 'Reset Password', '<p>Hi {{user_name}},</p><p>Your new password&nbsp; for {{app_name}} is <b>{{password}}</b>.</p><p>thank you.</p>', 'Hi {{user_name}}, Your new password  for {{app_name}} is {{password}}. thank you.', '2021-01-12 05:34:28', '2021-01-12 05:37:00'),
(6, 'Organizer Book Ticket', 'New Booking', '<p>Hi {{organizer_name}},</p><p>{{user_name}} has booked {{quantity}}&nbsp;<span style=\"background-color: transparent;\">tickets</span><span style=\"background-color: transparent;\">&nbsp;for event {{event_name}} on {{date}} successfully.</span></p><p>From {{app_name}}</p>', 'Hi {{organizer_name}}, {{user_name}} has booked {{quantity}} tickets for event {{event_name}} on {{date}} successfully. From {{app_name}}', '2021-01-18 04:38:58', '2021-01-18 04:38:58'),
(7, 'Sign up', 'Welcome', '<p>Hi {{user_name}},</p><p>Welcome to&nbsp;<span style=\"background-color: transparent;\">{{app_name}}</span><span style=\"background-color: transparent;\">&nbsp;on {{date}} successfully.</span></p><p>From {{app_name}}</p>', 'Hi {{user_name}},\r\n\r\nWelcome to {{app_name}} on {{date}} successfully.\r\n\r\nFrom {{app_name}}', '2021-07-13 03:23:04', '2021-07-13 03:23:04'),
(8, 'Gold User', 'This is a template', '<p>This is a template updated</p><p><br></p><p><br></p>', 'This is a text message', '2021-08-08 02:52:26', '2021-08-08 02:52:48'),
(9, 'Welcome', 'Welecome', 'bla bla&nbsp;', NULL, '2021-08-17 22:28:30', '2021-08-17 22:28:30');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('00bc8c1bb0cbfbbb551f72819657c6d366ca30c3ce11dfd36c11a5f2f7cdd5b513c57905c8b4507d', 10, 1, 'eventRight', '[]', 0, '2021-02-23 07:39:34', '2021-02-23 07:39:34', '2022-02-23 13:09:34'),
('02e5b107dbc1a3bc15359bcd08badf116c3be27e04969ae3d8d0936c5667731938980a7844276521', 10, 1, 'eventRight', '[]', 0, '2021-04-19 02:01:27', '2021-04-19 02:01:27', '2022-04-19 07:31:27'),
('043ea56998428c033a8628896b81dd090b36789452ef562bb357a75f22679dda18ea15e251b3991e', 5, 1, 'eventRight', '[]', 0, '2021-07-30 04:31:56', '2021-07-30 04:31:56', '2022-07-30 10:01:56'),
('05f7fbdbea2428db77caaff8bf0cdd943cdd8046b20d2edc29016f6c1131a2e84134e3c1816089dc', 43, 1, 'eventRight', '[]', 0, '2021-06-22 04:32:13', '2021-06-22 04:32:13', '2022-06-22 10:02:13'),
('086c9c368974f54d18840ec6b304b7101ec44bbfed2b53a62c43f02b7bac5213e35a99cc6414fd87', 32, 1, 'eventRight', '[]', 0, '2021-07-06 04:05:39', '2021-07-06 04:05:39', '2022-07-06 09:35:39'),
('0a255072e16f4d550f4e1fefbb49eb7ce5db580df155cc527fcb9b9d3fed41d9ee563539214826e4', 41, 1, 'eventRight', '[]', 0, '2021-07-07 01:14:34', '2021-07-07 01:14:34', '2022-07-07 06:44:34'),
('0a29d751e81b09e2df8aec78b699da03fed2c3bccfacce8929b556cc1f773714ba1fa7b98deb19c0', 10, 1, 'eventRight', '[]', 0, '2021-05-03 01:45:57', '2021-05-03 01:45:57', '2022-05-03 07:15:57'),
('0ac8f94c4b5011909ab130e85cb7c585300a13d746b60d6852ade9c9a1548a2720285400bf41b50f', 51, 1, 'eventRight', '[]', 0, '2021-07-10 06:25:32', '2021-07-10 06:25:32', '2022-07-10 11:55:32'),
('0ace4799c866c33e5bc4289ec07152a6c4585e814353418d24741461f49102f5ab79e975d61fd54d', 7, 1, 'eventRight', '[]', 0, '2021-06-09 05:49:51', '2021-06-09 05:49:51', '2022-06-09 11:19:51'),
('0d118211f9acad364abc5deac3f02b8a851a22181900a2f391e29ecc7e8eec70e4b3c9f359a38637', 7, 1, 'eventRight', '[]', 0, '2021-05-22 06:25:59', '2021-05-22 06:25:59', '2022-05-22 11:55:59'),
('101fd35f81c993ef69ae83ec0d7e30449c9e8add01913fc9d746983c3d4ca4fc5b00504f1443b668', 10, 1, 'eventRight', '[]', 0, '2021-08-06 22:11:21', '2021-08-06 22:11:21', '2022-08-07 03:41:21'),
('11555bd6ecb6a50278b5056bbe0777c71fcad06543d677085ccdef770d6447f56835504d1f7c01a5', 68, 1, 'eventRight', '[]', 0, '2021-08-08 12:34:58', '2021-08-08 12:34:58', '2022-08-08 18:04:58'),
('1176b3e663994a244b42b667b788f856719358c0a74f4992c0e28f52c1d151392367802cdfc36371', 32, 1, 'eventRight', '[]', 0, '2021-07-16 23:10:04', '2021-07-16 23:10:04', '2022-07-17 04:40:04'),
('129fbc48e5ab47f2fa6f4e0d4f122ee2e2e3d749c73e0da84212d6714ebb0ede38b13bdd50f82590', 7, 1, 'eventRight', '[]', 0, '2021-05-04 23:13:55', '2021-05-04 23:13:55', '2022-05-05 04:43:55'),
('134b53853daa9abf1d34d4444cdad6b00e821d6883d7ca6a6f8c383a991803bfdb4b8cfc54200959', 25, 1, 'eventRight', '[]', 0, '2021-04-22 16:11:22', '2021-04-22 16:11:22', '2022-04-22 21:41:22'),
('15b73aab381cfa1532816bdc693483a66c9c860c487cad587930c6744ec40e3291db5060619debeb', 10, 1, 'eventRight', '[]', 0, '2021-07-31 06:54:09', '2021-07-31 06:54:09', '2022-07-31 12:24:09'),
('15c382618926e52bcbe6bcfce7543bede6b2e4ac56d763e8a3b22c213bb8d3239d13d9fe2d01d93d', 48, 1, 'eventRight', '[]', 0, '2021-07-06 06:11:16', '2021-07-06 06:11:16', '2022-07-06 11:41:16'),
('182311fb001098fbe65024de203ac18a31dd63340fe8a8d90a69059bf581fbc4b917ea0128659ded', 74, 1, 'eventRight', '[]', 0, '2021-08-19 12:44:15', '2021-08-19 12:44:15', '2022-08-19 18:14:15'),
('18845fc12b4577863d8977abc9414e3a156d9852df35548d364279dc33d82795f3428f8577256ac2', 41, 1, 'eventRight', '[]', 0, '2021-07-08 05:56:24', '2021-07-08 05:56:24', '2022-07-08 11:26:24'),
('1a92dc8c2ee4519c4fa15bcd46d5c560f0d2c64f604710b9335c0c4d5ec8531db3d57c596b7a6ec4', 7, 1, 'eventRight', '[]', 0, '2021-06-19 23:54:01', '2021-06-19 23:54:01', '2022-06-20 05:24:01'),
('1ce91478083b7c8bab48c4fccacca6fda8a23018ec39c012d0414b14d65003166332c94a891b3e2a', 7, 1, 'eventRight', '[]', 0, '2021-08-01 15:01:36', '2021-08-01 15:01:36', '2022-08-01 20:31:36'),
('1d0a09b91f29c8ad9f98d34226202b764d14a1d8f31c3ad00dedf3f7f7d84a03cd4c3ba5cf228236', 7, 1, 'eventRight', '[]', 0, '2021-07-01 05:41:48', '2021-07-01 05:41:48', '2022-07-01 11:11:48'),
('1d93966e740d0a9c2c88c73f2c87a1a05607c662a31b366563f6dc04fa312da43fb1dab140304dba', 10, 1, 'eventRight', '[]', 0, '2021-06-23 14:38:12', '2021-06-23 14:38:12', '2022-06-23 20:08:12'),
('1f6de9603e0706dd7a7c4a3b66556e091c61a8b832f09334466dab4fc3d0203c6762c438cc71ed98', 70, 1, 'eventRight', '[]', 0, '2021-08-16 16:56:29', '2021-08-16 16:56:29', '2022-08-16 22:26:29'),
('20dea250070b4de8972bae804f338bddf1177c8f3842732491c9ea92c5d6270e2805861a1403bf8c', 5, 1, 'eventRight', '[]', 0, '2021-05-20 05:25:54', '2021-05-20 05:25:54', '2022-05-20 10:55:54'),
('21336fb4efce9862b7f94e85cdf671a9ad23f310bfdd6139c2a546b5e6a5440a3defc8c4403317af', 7, 1, 'eventRight', '[]', 0, '2021-06-09 06:11:54', '2021-06-09 06:11:54', '2022-06-09 11:41:54'),
('21a935b661742b9c605794b50974ee9122e583376d1f8fcd1d910886890c0b09c2a0a622969f0448', 5, 1, 'eventRight', '[]', 0, '2021-05-20 01:07:43', '2021-05-20 01:07:43', '2022-05-20 06:37:43'),
('21ab3cef35338a9b5d092399ed5d86bba6fa5b9bd5e1c973b071a25fac70fc553ba0fffb614e1f64', 10, 1, 'eventRight', '[]', 0, '2021-02-23 07:25:35', '2021-02-23 07:25:35', '2022-02-23 12:55:35'),
('21ce7dc8b52602e0e998cfb4ac2f685127a38430ca7c8a44008edb9f7b0ac891d64241cd35587329', 60, 1, 'eventRight', '[]', 0, '2021-08-12 18:38:38', '2021-08-12 18:38:38', '2022-08-13 00:08:38'),
('2244d6a04386217b779bde9dcf7adf91c4f6a7ce55d020ddd96053a344bb4ba06db37da77eaa053d', 7, 1, 'eventRight', '[]', 0, '2021-05-12 23:33:11', '2021-05-12 23:33:11', '2022-05-13 05:03:11'),
('22c40911947d4e0d0418d8e0dced264ace369cd588c9579ae758ff81df6b5cefe3896f19ced44ae2', 7, 1, 'eventRight', '[]', 0, '2021-07-09 08:49:53', '2021-07-09 08:49:53', '2022-07-09 14:19:53'),
('234496e18abbc09155a4c242a7b27874017aa2f8e21b7864c2290dce34ef9200a13b773480754fcb', 10, 1, 'eventRight', '[]', 0, '2021-07-17 18:13:40', '2021-07-17 18:13:40', '2022-07-17 23:43:40'),
('26dd15c0af27b5c84012a1fd6ce8a570f85992c7049fd0de60dd2805ccf01b72224dacc14e30955d', 19, 1, 'eventRight', '[]', 0, '2021-05-10 16:16:56', '2021-05-10 16:16:56', '2022-05-10 21:46:56'),
('28fc9e3187d5a762f9addaf6047e40eb0eb2bff1e7a663b8d8d99728faf152d9680ae29ef45dcba0', 7, 1, 'eventRight', '[]', 0, '2021-06-01 18:21:35', '2021-06-01 18:21:35', '2022-06-01 23:51:35'),
('294899f8ed2e9a0695ad7ebc0562794ead9a2ca9c82bb9710c7112432cd6dd62505c34576904f292', 10, 1, 'eventRight', '[]', 0, '2021-08-10 09:33:38', '2021-08-10 09:33:38', '2022-08-10 15:03:38'),
('2a76065ee927fb6bfcfd55649d54dbe9cb3ae7cf4b1fe222bb8cd1f54a6a487af40bcbe091f6d267', 31, 1, 'eventRight', '[]', 0, '2021-05-11 12:06:58', '2021-05-11 12:06:58', '2022-05-11 17:36:58'),
('2ba37192eda3df3038e784205aeedfc29f4c83d08ff53ef0d7346b2dd040e2cc9685270a8c185d98', 32, 1, 'eventRight', '[]', 0, '2021-07-06 01:54:25', '2021-07-06 01:54:25', '2022-07-06 07:24:25'),
('2c2bc87240aa2f28dfebeb5ed8cc54615089d955af6ece45f7f6c397f7b9fcd378b4ec6cfa0e2fc6', 10, 1, 'eventRight', '[]', 0, '2021-02-23 01:52:05', '2021-02-23 01:52:05', '2022-02-23 07:22:05'),
('2c963a2d73ad3e6fc0eb8b0a56598c8425b08eebfe798adc19e1a29ce53b59590680e273d2c8ba3c', 10, 1, 'eventRight', '[]', 0, '2021-05-04 23:21:21', '2021-05-04 23:21:21', '2022-05-05 04:51:21'),
('31c9252cbf1c147b1a59065fab798bdd3a00ebf495efeb0384064c25ecb3d9ebf76d4455e0afd565', 5, 1, 'eventRight', '[]', 0, '2021-05-10 02:26:23', '2021-05-10 02:26:23', '2022-05-10 07:56:23'),
('34e4f070ab896b394ae13acd6494f19efb784f0c616a0aae3cdea2b4754a71e569c0306c606e17da', 7, 1, 'eventRight', '[]', 0, '2021-05-22 23:52:26', '2021-05-22 23:52:26', '2022-05-23 05:22:26'),
('392c636a8e5a2ec83be058796e7d1436394d77d4c5fbab64dc93ec96fead4eaf44e48a8b5c1d21e6', 41, 1, 'eventRight', '[]', 0, '2021-07-07 06:18:26', '2021-07-07 06:18:26', '2022-07-07 11:48:26'),
('3bac4850678a7ef409edf6ba0b96ea0b8471b69a7b9d0aba3d5c7079db9d2d24d198b92837257dc6', 10, 1, 'eventRight', '[]', 0, '2021-02-23 01:00:51', '2021-02-23 01:00:51', '2022-02-23 06:30:51'),
('3d27b0511ec6ce21f8b30fac6e40d832245655c3d7d6b269ad0189811014bf5570452456c09c4f28', 10, 1, 'eventRight', '[]', 0, '2021-07-08 10:44:29', '2021-07-08 10:44:29', '2022-07-08 16:14:29'),
('404f2920e077c545202034c2077644db0bb724ae8489705260b5e59f51388a0711badd80e2298e0d', 10, 1, 'eventRight', '[]', 0, '2021-04-23 13:02:29', '2021-04-23 13:02:29', '2022-04-23 18:32:29'),
('42668dc15e7f78044348370d34fbc5efbaf34aaa8d19060855706d400c7621e364c788b822dd21bc', 7, 1, 'eventRight', '[]', 0, '2021-08-01 05:06:23', '2021-08-01 05:06:23', '2022-08-01 10:36:23'),
('42fc01b83d50b088ce1c6252b78ce89282798bf5412daca4882dbde2887650de53d6aa537ad3f86a', 7, 1, 'eventRight', '[]', 0, '2021-06-27 11:17:51', '2021-06-27 11:17:51', '2022-06-27 16:47:51'),
('437d1fde29dfaeabf0600b6693be11e6c7d651037a72e151728476860e8bf1f4ab2a781964835927', 7, 1, 'eventRight', '[]', 0, '2021-08-05 05:16:02', '2021-08-05 05:16:02', '2022-08-05 10:46:02'),
('44602fcd0732ba364bf385997741f86c74fb2da6bf177581862c8753175083eedeb78fe0b22a4b79', 7, 1, 'eventRight', '[]', 0, '2021-08-23 00:58:57', '2021-08-23 00:58:57', '2022-08-23 06:28:57'),
('45b1ba392081ac9b472ba7836347f97e030d9a6289d5e6168aee957de64f12df5a7bc46ebf2c72f4', 7, 1, 'eventRight', '[]', 0, '2021-05-21 23:27:32', '2021-05-21 23:27:32', '2022-05-22 04:57:32'),
('45c3591c3213aaa6942f0121724bafdf872127ff469d86d2307734a3fe3feda98eae92b0f491914d', 10, 1, 'eventRight', '[]', 0, '2021-06-05 05:13:05', '2021-06-05 05:13:05', '2022-06-05 10:43:05'),
('46b1ceb374ee38de076b6d51ae2f361085636322fdb9f568208eca97655876ff8d995859beddbc50', 7, 1, 'eventRight', '[]', 0, '2021-08-23 01:15:30', '2021-08-23 01:15:30', '2022-08-23 06:45:30'),
('4798381cd2a2c40500d679978545cfdb3ae4d419b1ba37b4eec6104d5a5d79a9ca233867a381397c', 7, 1, 'eventRight', '[]', 0, '2021-05-21 22:49:07', '2021-05-21 22:49:07', '2022-05-22 04:19:07'),
('480eafb634b366c6c69e13c235eaf3b88f8789fbf426f1c4492bfd7e85b3a8d9b9381e6ca5cbfd21', 10, 1, 'eventRight', '[]', 0, '2021-08-22 04:46:25', '2021-08-22 04:46:25', '2022-08-22 10:16:25'),
('49841ec69bd9cc6ac24697c431433c7d3824b192d6985d488736f9017783e714e5729c3a048f0550', 10, 1, 'eventRight', '[]', 0, '2021-05-15 09:43:46', '2021-05-15 09:43:46', '2022-05-15 15:13:46'),
('49a56aee21aa346de4803f38781fbf2b0292270c546f0a780c9431921ac5ddb68b76755c32881d88', 62, 1, 'eventRight', '[]', 0, '2021-07-29 09:50:32', '2021-07-29 09:50:32', '2022-07-29 15:20:32'),
('4ac46229b1508242fa76bc38f7516b269c48c43c21215901a48a3e8f5f3285053ee0fe84b4e688ff', 19, 1, 'eventRight', '[]', 0, '2021-04-12 22:37:00', '2021-04-12 22:37:00', '2022-04-13 04:07:00'),
('4bb249f18da9f03ae8b42054481979f2c2be22322a6b1d75a126810a43c0ff3d2e767e4b13cd4268', 16, 1, 'eventRight', '[]', 0, '2021-04-05 09:33:23', '2021-04-05 09:33:23', '2022-04-05 15:03:23'),
('4c6cc8f1d882b1c90dc8c730a9b8dc39fe0077d6ff1f2b8fafa0743d44e523fdb9e06d620410c809', 7, 1, 'eventRight', '[]', 0, '2021-05-21 23:53:14', '2021-05-21 23:53:14', '2022-05-22 05:23:14'),
('4d17ca7ddafaf67217f0a5bfad6f158c8a52ca5c6c8dfed65121bfcb4ea1b069dca24832bc5e3ddb', 7, 1, 'eventRight', '[]', 0, '2021-06-14 06:18:49', '2021-06-14 06:18:49', '2022-06-14 11:48:49'),
('4d19da103eaac4b7f28c7dbc6dd138c73c2a50171619605cc106de22f172995aa047f251734c1efe', 7, 1, 'eventRight', '[]', 0, '2021-05-22 06:24:30', '2021-05-22 06:24:30', '2022-05-22 11:54:30'),
('4e4500bf1800d6af98acd87833c1d90250107016795fc514e0564fdae4220be24795349e7aa785f0', 7, 1, 'eventRight', '[]', 0, '2021-08-23 00:54:19', '2021-08-23 00:54:19', '2022-08-23 06:24:19'),
('4ec28706847ee345b72b8a6f8dc79df9d6c87280184b919accc3518d6557334a1f2caefe0c8ccbdd', 7, 1, 'eventRight', '[]', 0, '2021-07-30 01:48:49', '2021-07-30 01:48:49', '2022-07-30 07:18:49'),
('4ed809b1513c341dc20f6fce2cae4b650bfebfffb9cb39a56d027b7809314957f10c155d07b9aceb', 7, 1, 'eventRight', '[]', 0, '2021-07-01 05:34:58', '2021-07-01 05:34:58', '2022-07-01 11:04:58'),
('4f6e37dc88b3a8a6fa12932d50fcbb36aae5d904f959f4ffefa92f33da0f43b60427440c4182ceeb', 10, 1, 'eventRight', '[]', 0, '2021-05-10 04:59:34', '2021-05-10 04:59:34', '2022-05-10 10:29:34'),
('4f98038ccb456a9e2773369e3565efa9d02512d8c9dbfb47ae3e05e11e586905833b922aad8e5db3', 53, 1, 'eventRight', '[]', 0, '2021-07-28 04:32:04', '2021-07-28 04:32:04', '2022-07-28 10:02:04'),
('4fbcc6bfd6259173fa1a5a83e6a0dd78d39902b91fab0106e00e9a2fb479700b7c1bfce76be8d1e6', 7, 1, 'eventRight', '[]', 0, '2021-03-28 19:33:21', '2021-03-28 19:33:21', '2022-03-29 01:03:21'),
('5015285e9289680fa7d1ad332212e498665094ad6b6967071a6b587bf5b7ccc8757aeb3ab7a5c5ce', 13, 1, 'eventRight', '[]', 0, '2021-02-22 00:33:21', '2021-02-22 00:33:21', '2022-02-22 06:03:21'),
('517d1eee76a1b7f135b60e204e299d085c2a2121f16d91235f3be788e3bda69027a5cab6fabf51e4', 30, 1, 'eventRight', '[]', 0, '2021-05-10 07:11:14', '2021-05-10 07:11:14', '2022-05-10 12:41:14'),
('523e04526a37706e7532f8bf6b570fab13267a785668e912005d3b0ca427523260fac50af858a74f', 44, 1, 'eventRight', '[]', 0, '2021-07-12 08:59:00', '2021-07-12 08:59:00', '2022-07-12 14:29:00'),
('538305d2bce7865d937822c94f52bea0edfb430d04fd7d75dc79d34275fa7d80209553687e451a3c', 7, 1, 'eventRight', '[]', 0, '2021-08-23 01:17:45', '2021-08-23 01:17:45', '2022-08-23 06:47:45'),
('549f7255177f69baf33c02db2143ec4a516e69574fc714654591af06baf2eb40c28a958c1098fe68', 28, 1, 'eventRight', '[]', 0, '2021-04-28 17:52:57', '2021-04-28 17:52:57', '2022-04-28 23:22:57'),
('54f89ff6eeceeab8d1b0107171fc260f7a02fb9bbb901a99a6526b899eae5d2f570858ab2c03961e', 32, 1, 'eventRight', '[]', 0, '2021-06-07 07:19:23', '2021-06-07 07:19:23', '2022-06-07 12:49:23'),
('5509c8ac9bed1b0a457cbfcbea9a656222fb378c92c76c6911f690e9d18ed7e26d6af92cf93b245f', 25, 1, 'eventRight', '[]', 0, '2021-05-26 12:17:26', '2021-05-26 12:17:26', '2022-05-26 17:47:26'),
('5852b825d2ab72b46756f4d5c80f0ba020f3373aa4813c5f3f7a749290f94e7274984baf3e5d16b6', 34, 1, 'eventRight', '[]', 0, '2021-06-25 03:49:11', '2021-06-25 03:49:11', '2022-06-25 09:19:11'),
('5ac528f470d94f7821b017a2b888efe499f91c6a2aa8db159913822a8ee47b731b14c2ce6d0cfce8', 28, 1, 'eventRight', '[]', 0, '2021-04-29 06:17:31', '2021-04-29 06:17:31', '2022-04-29 11:47:31'),
('5b501a0578c43038afc7ad30c9de6bd9e27d9bf9f8afb464c65fef46883e9e6d0715b5de75936044', 41, 1, 'eventRight', '[]', 0, '2021-07-07 23:33:31', '2021-07-07 23:33:31', '2022-07-08 05:03:31'),
('5b53e26a2aeb08b49adb11790c85a20f9af707f268f433be84f94430053556d4a4007fa92b250eda', 41, 1, 'eventRight', '[]', 0, '2021-06-17 01:52:14', '2021-06-17 01:52:14', '2022-06-17 07:22:14'),
('5d1885f8b65cb667278fad9566f2f96ffdd74867c6a112eba6b4fb295f438cfb6778e078273b65e2', 5, 1, 'eventRight', '[]', 0, '2021-07-31 06:56:33', '2021-07-31 06:56:33', '2022-07-31 12:26:33'),
('5db5bf962f96de450d4697847c9ccdcbe252eb05f491d86bf9db6b2772b0ae66ea5f0d658e05fcd5', 24, 1, 'eventRight', '[]', 0, '2021-04-22 09:48:42', '2021-04-22 09:48:42', '2022-04-22 15:18:42'),
('60d874e786cdd4c7ef8ea4ca36dec8882a5efcc378d7594030338ff41b00e664719fa61ed598f5a6', 7, 1, 'eventRight', '[]', 0, '2021-07-29 10:35:33', '2021-07-29 10:35:33', '2022-07-29 16:05:33'),
('6204af668b056a85affeb15a3a72d6a20425268d3b6d3e0c32bcb6ba5451d7024ef52b79f6f59d99', 5, 1, 'eventRight', '[]', 0, '2021-07-01 05:37:01', '2021-07-01 05:37:01', '2022-07-01 11:07:01'),
('63a3602879096160dec43863c749037aa9306a7e5887ef5803a58ae5f1d5db457c6e9eadd223ffdb', 7, 1, 'eventRight', '[]', 0, '2021-07-29 10:34:17', '2021-07-29 10:34:17', '2022-07-29 16:04:17'),
('6491e403ef4e52b9e79da61c70c7d7cf88a4f363dcfdd345c445c5ecf3f06764f45f32435083a38b', 5, 1, 'eventRight', '[]', 0, '2021-05-20 00:48:32', '2021-05-20 00:48:32', '2022-05-20 06:18:32'),
('65a8c75c9283bf881b198e6a94a386de75bf6241d869ecf549e9400f200b535af7958393a4d13d3f', 67, 1, 'eventRight', '[]', 0, '2021-08-07 12:58:13', '2021-08-07 12:58:13', '2022-08-07 18:28:13'),
('674165b93af203f7935c27ca954c6d5e95f44f470631f8f51e41618aa453dcf7b7ed0f6249f94109', 41, 1, 'eventRight', '[]', 0, '2021-07-08 00:10:52', '2021-07-08 00:10:52', '2022-07-08 05:40:52'),
('6894da5ba81922b37ced4397a9a92026eb192c1596f35a2c7138721d34ef37951562278b25176991', 10, 1, 'eventRight', '[]', 0, '2021-02-23 12:34:16', '2021-02-23 12:34:16', '2022-02-23 18:04:16'),
('69d2db7ef364fd4140bb55a64daf4f9161c8550a076933a5a3b19d4d70f10bc7917f38530e7a90ff', 5, 1, 'eventRight', '[]', 0, '2021-08-07 13:08:23', '2021-08-07 13:08:23', '2022-08-07 18:38:23'),
('6c20a1ebf9c3a2f823d3b90f6f4f39e59aae88b6f8d0b6d9e40b57681cfab39ea122acf2ac760c59', 18, 1, 'eventRight', '[]', 0, '2021-05-10 16:14:09', '2021-05-10 16:14:09', '2022-05-10 21:44:09'),
('6c4b43986f7d2ff1f9f5560cccd8b855d57a13b9a0efe9fd0216774cdc13ad20835d16f78f2d3743', 41, 1, 'eventRight', '[]', 0, '2021-07-08 00:21:34', '2021-07-08 00:21:34', '2022-07-08 05:51:34'),
('6c7d93e5a9a11563492f8980f4dda4a23d12118c0bb296ea0e7b33006e69181f3d2784b3569af64f', 10, 1, 'eventRight', '[]', 0, '2021-08-23 00:13:23', '2021-08-23 00:13:23', '2022-08-23 05:43:23'),
('6d0a85b153c89a3325db512b767b8e42591fdc725271578f796ecc8f9e974613a3655e88b8331d47', 5, 1, 'eventRight', '[]', 0, '2021-03-24 12:36:34', '2021-03-24 12:36:34', '2022-03-24 18:06:34'),
('6ee26f88bbea80754b718316e8e4f6a2bd52c9cf4e6665d6c26deec5fa08d52201e8bc1c9938b97d', 36, 1, 'eventRight', '[]', 0, '2021-06-10 08:54:19', '2021-06-10 08:54:19', '2022-06-10 14:24:19'),
('6fdd174baf33ec9551fa6ef52f32085c4cd00a963aafe4cb6c0e3556bb3ab187964ff6ccb83ee554', 15, 1, 'eventRight', '[]', 0, '2021-04-04 21:32:07', '2021-04-04 21:32:07', '2022-04-05 03:02:07'),
('71e9e71ea7dbb5c2c0f64856e933c8a52e7281b3380eb1f5d08949db2faae216d40c17e2913a8817', 7, 1, 'eventRight', '[]', 0, '2021-05-10 22:57:35', '2021-05-10 22:57:35', '2022-05-11 04:27:35'),
('746e54f1db1e8f9d1f32f9f80c27c6a03b2fd0aad24846e5f01e414e6ee9290b05d4dad087a560f6', 10, 1, 'eventRight', '[]', 0, '2021-02-23 22:05:37', '2021-02-23 22:05:37', '2022-02-24 03:35:37'),
('749bcf90b3ce49bd552e1a647a6618f4b4aa85631e04f3dad8a9a440e7687cd0ab8acbd483a94143', 10, 1, 'eventRight', '[]', 0, '2021-04-09 05:37:10', '2021-04-09 05:37:10', '2022-04-09 11:07:10'),
('75936c872d202fe9297de57fb6a2afc05f593368f4f6b82f4d9469cbdd27921e182053f1fbd065c4', 10, 1, 'eventRight', '[]', 0, '2021-08-10 09:37:41', '2021-08-10 09:37:41', '2022-08-10 15:07:41'),
('75d3b31cd1149c7d7143e5188938d0191d7f03b0d22ded14d53cedae1f252deaa83c9443fc748a08', 5, 1, 'eventRight', '[]', 0, '2021-05-28 18:39:59', '2021-05-28 18:39:59', '2022-05-29 00:09:59'),
('76639179c6b16e3976a81a58a1d98e8526988220b94cce54a16e219877aeffaefdc11b957cdb2784', 41, 1, 'eventRight', '[]', 0, '2021-07-08 01:09:06', '2021-07-08 01:09:06', '2022-07-08 06:39:06'),
('7773b2425d4e9b2c55bef70a5a0712eab5bd13e1691a5e430db8cc3bf8e38e5d144fc22f288ee93a', 51, 1, 'eventRight', '[]', 0, '2021-07-20 18:13:48', '2021-07-20 18:13:48', '2022-07-20 23:43:48'),
('79b6a78806d9341769206cd0d38fb731702724b694673c8b201552ff8aa3fe90f113f7a8091e42c9', 7, 1, 'eventRight', '[]', 0, '2021-04-30 16:21:33', '2021-04-30 16:21:33', '2022-04-30 21:51:33'),
('7a6d7c84b3a7e7e2cc41989decc1d4133c4b9ea905a38e30a484f17808145458848c3c3269b5af6c', 10, 1, 'eventRight', '[]', 0, '2021-05-05 22:44:43', '2021-05-05 22:44:43', '2022-05-06 04:14:43'),
('7b6fe6af03cfdf78b0e636770b86c8ef00f67e383a8d5fca94fab69df83c2f6ba58d68f5fb3cddea', 7, 1, 'eventRight', '[]', 0, '2021-06-13 12:08:30', '2021-06-13 12:08:30', '2022-06-13 17:38:30'),
('7c785a1ac14d742f548cba187f0b076e902fc8bd59e865ce7767be5c327995de1800d266a8ddc397', 5, 1, 'eventRight', '[]', 0, '2021-06-13 12:10:58', '2021-06-13 12:10:58', '2022-06-13 17:40:58'),
('7de245a93c41d0e84f4dc931326227f0757a21623aa772d38410a2f914a24f113976b69d177a28b0', 7, 1, 'eventRight', '[]', 0, '2021-06-30 00:37:01', '2021-06-30 00:37:01', '2022-06-30 06:07:01'),
('7e8afdada4b5d8aec7d34f3407d380a7c890274f11a814b8cdc48bc73ee65ce639a9280a5a4181f8', 55, 1, 'eventRight', '[]', 0, '2021-07-12 16:04:26', '2021-07-12 16:04:26', '2022-07-12 21:34:26'),
('8093a2450454c00175f5a3cfd9a8b7fa99b4434b28b254d4dc1fff4dcd2c0cf2a56e6fdeedbea1f2', 10, 1, 'eventRight', '[]', 0, '2021-05-10 02:38:53', '2021-05-10 02:38:53', '2022-05-10 08:08:53'),
('8128204d489e5a8dec1ddf25134473e52b9b56dd32fbed1b1f756eee00c3030424e2e23f10a4b491', 59, 1, 'eventRight', '[]', 0, '2021-07-28 18:00:08', '2021-07-28 18:00:08', '2022-07-28 23:30:08'),
('82a9dbe6ea55b903c9c332d7fe06809b21d08f3b64bff3a7752e1b76d4101cf6acabf61f5486220e', 42, 1, 'eventRight', '[]', 0, '2021-07-07 06:56:28', '2021-07-07 06:56:28', '2022-07-07 12:26:28'),
('82d341f0f14cf66d3a282297feecdc4cf8c2311240572475860095ae15b8c2744d31a9c9d38fd8f6', 7, 1, 'eventRight', '[]', 0, '2021-07-31 07:01:23', '2021-07-31 07:01:23', '2022-07-31 12:31:23'),
('830f4a84f3666ffe696827b27ea3320aca3b481d4d32550c0d426665ebb410ce3b39a006806f3f1d', 40, 1, 'eventRight', '[]', 0, '2021-06-15 10:50:49', '2021-06-15 10:50:49', '2022-06-15 16:20:49'),
('835542880643af57bead446c3cdf6708e6c572d72fb002b4bfead836eac74cb695cf202c4ad363e0', 7, 1, 'eventRight', '[]', 0, '2021-05-11 12:21:35', '2021-05-11 12:21:35', '2022-05-11 17:51:35'),
('841a030db31b74f55befdf69cc2dfe3b7658da05490dd108660f167ab0c07eb66c128525898889ec', 30, 1, 'eventRight', '[]', 0, '2021-05-31 03:28:11', '2021-05-31 03:28:11', '2022-05-31 08:58:11'),
('84fdddc6db01522a69163a67213a565e5c32256150fc889d3a80c76d5a128596ed7805030fcce4b9', 10, 1, 'eventRight', '[]', 0, '2021-08-16 16:43:26', '2021-08-16 16:43:26', '2022-08-16 22:13:26'),
('85229cd3254fa2f01d7f5e6c5ba522b1b350659e94165d345558a17fa17e23c2a7baefa40ca85862', 10, 1, 'eventRight', '[]', 0, '2021-02-23 07:36:07', '2021-02-23 07:36:07', '2022-02-23 13:06:07'),
('87f9e74f760d2923ed61678cd68e77f1755c4a9357ba8f37055aac39df36a2e5063b11b3c040919f', 7, 1, 'eventRight', '[]', 0, '2021-07-25 14:07:02', '2021-07-25 14:07:02', '2022-07-25 19:37:02'),
('8878f650e7b36959c4ad64b0cc8ccdec7c665e5ed1034081486337611e1af48db7880a076be6e58f', 54, 1, 'eventRight', '[]', 0, '2021-07-12 10:08:20', '2021-07-12 10:08:20', '2022-07-12 15:38:20'),
('8bf5281679cff8a6b944720ddb3047f78b35926eed01b9782c54cecefd6688544f0da4a97df016ed', 32, 1, 'eventRight', '[]', 0, '2021-07-06 03:27:23', '2021-07-06 03:27:23', '2022-07-06 08:57:23'),
('8db3b8c8bdda1f8b4b8d6e5dafd4b4711f576b271e61d0a0a9cf0ea1bea0e9e813de90761670ccaf', 5, 1, 'eventRight', '[]', 0, '2021-05-11 12:31:33', '2021-05-11 12:31:33', '2022-05-11 18:01:33'),
('923e9ff01039e6ae5f77ddc98c50b021eefb78bd15e65f3aed08158357413b23b0b86a50068e242a', 5, 1, 'eventRight', '[]', 0, '2021-07-25 14:15:04', '2021-07-25 14:15:04', '2022-07-25 19:45:04'),
('92ecd172aaacf0ca74b03f5c3ac784e4e8b4b7761e060b6cca2888a73184f4caaed99cbd8d5930ff', 10, 1, 'eventRight', '[]', 0, '2021-05-18 03:13:38', '2021-05-18 03:13:38', '2022-05-18 08:43:38'),
('93ce34fa8eb814ce8e95cfddd864fdedd7a083cd0a2687e11154b1612876071f882325d72cfb648c', 54, 1, 'eventRight', '[]', 0, '2021-07-12 08:55:37', '2021-07-12 08:55:37', '2022-07-12 14:25:37'),
('944ba278bf6b587fdc659bee0e25e6963a7be26229a6febfb91b83c5078c3efd8468132678819587', 7, 1, 'eventRight', '[]', 0, '2021-08-07 13:06:27', '2021-08-07 13:06:27', '2022-08-07 18:36:27'),
('95173ec27b48f7a485948faec01e09ff2a06640abe916006202d7a13da890f0632c6c3aaab170b59', 13, 1, 'eventRight', '[]', 0, '2021-04-12 00:44:10', '2021-04-12 00:44:10', '2022-04-12 06:14:10'),
('95e8d539d92b6d94e9117cc1face64e36acc011475cd3b9f837079c9ddc8a1755c796e08adeeb6e8', 7, 1, 'eventRight', '[]', 0, '2021-08-23 01:10:53', '2021-08-23 01:10:53', '2022-08-23 06:40:53'),
('965d8909bd5c14bf845aad99392f13abe1d2f3a08df8d5cdc70d7516dd16a0a6486de2dba90d3344', 7, 1, 'eventRight', '[]', 0, '2021-08-04 03:03:26', '2021-08-04 03:03:26', '2022-08-04 08:33:26'),
('98eff1153705098d48e26c129ebee4547e2b837c2ef1bcc503ebe4d58df3c1401c9c3ee4e637e679', 10, 1, 'eventRight', '[]', 0, '2021-02-23 07:49:43', '2021-02-23 07:49:43', '2022-02-23 13:19:43'),
('9a66580290037b544922aa76ef820b0d4e66458c9fc7efca9faabedb801076be36bc6d5ce3163a31', 20, 1, 'eventRight', '[]', 0, '2021-04-14 20:18:48', '2021-04-14 20:18:48', '2022-04-15 01:48:48'),
('9adc987f0ef1be1ce901af9a766821b7fba66c87b52c75be3500ddbaa2edb225023348b814546925', 7, 1, 'eventRight', '[]', 0, '2021-03-24 12:40:19', '2021-03-24 12:40:19', '2022-03-24 18:10:19'),
('9aef938edf276bd2fb62baed49fd3df6f7335366e59db0ef90275bb5df3ef83bcaa51bd9cba56477', 7, 1, 'eventRight', '[]', 0, '2021-05-22 06:27:35', '2021-05-22 06:27:35', '2022-05-22 11:57:35'),
('9b1d3b86c9b4950dee378c570ecb65f08aa341e6d121be32bf0656720a9d4568c756fa3a197f2e73', 64, 1, 'eventRight', '[]', 0, '2021-08-01 01:33:52', '2021-08-01 01:33:52', '2022-08-01 07:03:52'),
('9b57ca783d959d7555026e2b55c150d9adf709f05b03e8bf15c18b2e86c18e8da28f88046c976d8f', 28, 1, 'eventRight', '[]', 0, '2021-04-28 16:27:16', '2021-04-28 16:27:16', '2022-04-28 21:57:16'),
('9cbfe66ace8ea7a82b63a8791a98d8b5bc4ca434a1656ecfbb32015847bbd05fa23bab41462e6294', 7, 1, 'eventRight', '[]', 0, '2021-08-23 01:03:24', '2021-08-23 01:03:24', '2022-08-23 06:33:24'),
('9d20c18792fb58462d92cf808817fda5e4fc5e1664d1cd2eb2d28dd9937482068495157360e152ee', 7, 1, 'eventRight', '[]', 0, '2021-08-17 14:27:53', '2021-08-17 14:27:53', '2022-08-17 19:57:53'),
('9d28d47defd716393c50f359a5c01c6995019c4e98db8e4e41cc4219ed7362c971d63ab173e42ada', 10, 1, 'eventRight', '[]', 0, '2021-07-28 17:48:25', '2021-07-28 17:48:25', '2022-07-28 23:18:25'),
('9eb41b0c94cb005d935b0a28c61d825fb8a5bd11012e704940a4f9cf4767cc303057415545db1f6d', 41, 1, 'eventRight', '[]', 0, '2021-07-07 06:13:31', '2021-07-07 06:13:31', '2022-07-07 11:43:31'),
('9f19cc938c7fba153c2039902710a4892a18c92c75392ae8ab2dbe32ed7d2662dcf2cbfeec45cb25', 41, 1, 'eventRight', '[]', 0, '2021-07-07 06:16:48', '2021-07-07 06:16:48', '2022-07-07 11:46:48'),
('9f9406c508e5406dd197f0de815575aa6780889500bf587b9abfa391285a21853318e6fffa7694d2', 10, 1, 'eventRight', '[]', 0, '2021-02-24 06:28:49', '2021-02-24 06:28:49', '2022-02-24 11:58:49'),
('a25294faafeaf2df9afa33f4c5a544ceab6fb8c72615770d171869c5d7d1e9818625a6893550586c', 7, 1, 'eventRight', '[]', 0, '2021-05-03 01:28:40', '2021-05-03 01:28:40', '2022-05-03 06:58:40'),
('a35282e8f0129ff5d2d65fa3d0ec9e292f9fe74fcf87bee3459c8388b4c176de0c47ec6b88775bcf', 7, 1, 'eventRight', '[]', 0, '2021-08-07 13:06:41', '2021-08-07 13:06:41', '2022-08-07 18:36:41'),
('a3daa617e09eea8f84e029f6271942941c2c87d07dc726f4fdd3c0e0e7bc410a0068f7878e9cc600', 5, 1, 'eventRight', '[]', 0, '2021-08-05 20:15:31', '2021-08-05 20:15:31', '2022-08-06 01:45:31'),
('a4d770eea9d7cf5e6014b573d3178e2882ec225bfd9e5b602e85ff6687dc2741894044ff0df221e3', 41, 1, 'eventRight', '[]', 0, '2021-07-07 23:50:31', '2021-07-07 23:50:31', '2022-07-08 05:20:31'),
('a6cd3fe1343d01b85a6a3cc35ab2f28ac7806e474d0a978005a05fed1398c1869fe89bc80d579345', 45, 1, 'eventRight', '[]', 0, '2021-07-12 09:12:59', '2021-07-12 09:12:59', '2022-07-12 14:42:59'),
('a78608a7b181c6bcc59be43cd2cd886bebec86df9e4f9e9120e934bbdba36f265e31cfe2bb543b30', 35, 1, 'eventRight', '[]', 0, '2021-06-02 19:20:30', '2021-06-02 19:20:30', '2022-06-03 00:50:30'),
('a7ba0c673a4092db8e08dcab7a754324394cfcd9701f4b62678116709eca2f9c7ff2e935373a7e56', 59, 1, 'eventRight', '[]', 0, '2021-08-10 09:35:31', '2021-08-10 09:35:31', '2022-08-10 15:05:31'),
('a82bc09db96d6b4efa993df195d1783b4b40a8f7d2f955418f32147f096ff6e0c2754af7144771b0', 41, 1, 'eventRight', '[]', 0, '2021-07-08 01:05:17', '2021-07-08 01:05:17', '2022-07-08 06:35:17'),
('a8affa791bda3d0918291d5ff6f71f8ce8c33690b6569e8c69772b695a0a10ff8cf74f0a48261c4d', 22, 1, 'eventRight', '[]', 0, '2021-04-19 20:54:09', '2021-04-19 20:54:09', '2022-04-20 02:24:09'),
('a8cc0c878bb1f609aefd9890852a083c9305430f9155876fade682c189eaa68f94ec9bd515aaecf3', 7, 1, 'eventRight', '[]', 0, '2021-03-24 11:51:45', '2021-03-24 11:51:45', '2022-03-24 17:21:45'),
('a8e971ff8dcbc0402cba8350a8e2367a439f0206819895f0a76632a831b68f21893cba1edaca962d', 18, 1, 'eventRight', '[]', 0, '2021-04-11 15:54:08', '2021-04-11 15:54:08', '2022-04-11 21:24:08'),
('aa1f6b73e94a2f5d86a7261f98314b816d5d2b1019c6b70de7b0265c2070360008521e76f7a5c337', 5, 1, 'eventRight', '[]', 0, '2021-06-13 12:14:43', '2021-06-13 12:14:43', '2022-06-13 17:44:43'),
('ab2bf6dc4729bfbb37aa1c27d4e9f4d534a493cf0a4adfde33def3348f0baa7e370a29a8c7792bc6', 7, 1, 'eventRight', '[]', 0, '2021-05-03 01:24:23', '2021-05-03 01:24:23', '2022-05-03 06:54:23'),
('ac266b187e04db01de706cca451507bdab7b45efea070112f6d943a4dd6ed1d4b47309a195b976a5', 10, 1, 'eventRight', '[]', 0, '2021-05-25 01:14:03', '2021-05-25 01:14:03', '2022-05-25 06:44:03'),
('ad601326059813cd0f93cf03ea19cd6e610c4bfee7e21df97d6c1042025b41bb9fde4ce9107a6d1b', 10, 1, 'eventRight', '[]', 0, '2021-08-23 03:09:04', '2021-08-23 03:09:04', '2022-08-23 08:39:04'),
('ad99724007b8557020c3a43b5fec6d66fa90fafc67983a67cb9fb0ccd6d86e438db47dc216e04760', 7, 1, 'eventRight', '[]', 0, '2021-07-12 03:30:27', '2021-07-12 03:30:27', '2022-07-12 09:00:27'),
('adf91affac71c3ce1554c5be9b96d79a9cd074ccab11e486bb012da244fa73382f0a7521c231a34b', 10, 1, 'eventRight', '[]', 0, '2021-02-23 08:04:52', '2021-02-23 08:04:52', '2022-02-23 13:34:52'),
('ae03355a559a11dac994bb9f7bb0315e9b9889ed3a58b00c7726cfe6b437490bd4750382b08c7312', 7, 1, 'eventRight', '[]', 0, '2021-08-17 02:29:05', '2021-08-17 02:29:05', '2022-08-17 07:59:05'),
('ae550d588e591fb4bd42805b79deeaf4696e94bbf75216611b6713e3b565ffeb449aa4d2f517c043', 10, 1, 'eventRight', '[]', 0, '2021-02-23 12:30:32', '2021-02-23 12:30:32', '2022-02-23 18:00:32'),
('b087ddd3a4b0663a46601755652ed0ed7a57798d26b91f72ee8701374e0440b02899faac12e25d11', 10, 1, 'eventRight', '[]', 0, '2021-02-23 00:49:54', '2021-02-23 00:49:54', '2022-02-23 06:19:54'),
('b3fd01df835d7a1f1661b6d11138fac9d5ca09c727aa690a8b0eed855bff36288b90aa0973b51760', 27, 1, 'eventRight', '[]', 0, '2021-04-28 05:14:15', '2021-04-28 05:14:15', '2022-04-28 10:44:15'),
('b4dc7ed3306778bf7dd4bab051e8a739d23f5b1b034185ab39652cba284a567de92c63b9f961396c', 5, 1, 'eventRight', '[]', 0, '2021-05-03 02:31:02', '2021-05-03 02:31:02', '2022-05-03 08:01:02'),
('b5b25ec67b4bc17e706d7d2cc3c029dfeb910cd99afa22078e812543e3d27d6abcdf8e27f59e7b84', 42, 1, 'eventRight', '[]', 0, '2021-07-07 07:39:32', '2021-07-07 07:39:32', '2022-07-07 13:09:32'),
('b827275e115a0ad8389a8e5792b7881779a531a2edca845ec491529c87a93484632ef677a9d30723', 5, 1, 'eventRight', '[]', 0, '2021-06-07 10:08:06', '2021-06-07 10:08:06', '2022-06-07 15:38:06'),
('b832d6f337ba22e563aa63072434e73cfa0c2de1cbacb18073c79577690a092291e86e0f1447da05', 10, 1, 'eventRight', '[]', 0, '2021-08-23 01:57:04', '2021-08-23 01:57:04', '2022-08-23 07:27:04'),
('b982efed82e90ad29d35efc1084fd7be42df776d8730911268420b082de13e54a5f721bd80d8b96c', 41, 1, 'eventRight', '[]', 0, '2021-07-06 05:52:50', '2021-07-06 05:52:50', '2022-07-06 11:22:50'),
('bbbcc00abc1120bc882b9b2cc03ee064105cc903b8099ee324f3522f932f4254eb36ddf66f0fbd73', 75, 1, 'eventRight', '[]', 0, '2021-08-20 02:39:59', '2021-08-20 02:39:59', '2022-08-20 08:09:59'),
('bbff5ea6e1ef917502f3eb35686703d24cb73ee65c4b7beaf6d39ce6ada7398ee19f215f7b5f2850', 41, 1, 'eventRight', '[]', 0, '2021-08-19 03:17:12', '2021-08-19 03:17:12', '2022-08-19 08:47:12'),
('bdea3a01c9030b1a51e3e6666fc4faf8d4d47cbebc7f35f3c526a335d71ea758375cb007c70279ae', 21, 1, 'eventRight', '[]', 0, '2021-05-14 05:00:17', '2021-05-14 05:00:17', '2022-05-14 10:30:17'),
('be750d3f85be2a775e9c4cf2ec56740694a02232875d66e01f644b4d0255cbae4be58ad14516578a', 28, 1, 'eventRight', '[]', 0, '2021-04-28 16:48:49', '2021-04-28 16:48:49', '2022-04-28 22:18:49'),
('bf6d06d1520c8d7d184f9a118c92b2730d1bee54053bcd2f0640cdfef37b28f90b8324240c3ee841', 10, 1, 'eventRight', '[]', 0, '2021-07-27 13:21:15', '2021-07-27 13:21:15', '2022-07-27 18:51:15'),
('bf725eb59cebb87ac6245044619ee2d7d23c28ebde84077bb2c2eb3abcf8756c17115e08fe567aa0', 63, 1, 'eventRight', '[]', 0, '2021-07-29 09:53:17', '2021-07-29 09:53:17', '2022-07-29 15:23:17'),
('c08c971c0956cc920d92625a90a895bc8dd062e69827e2e50f4635567cb778db90b2e140d8b09560', 10, 1, 'eventRight', '[]', 0, '2021-06-09 06:10:26', '2021-06-09 06:10:26', '2022-06-09 11:40:26'),
('c0b84afd661877f6493a5b49e1c91f45972b001dd3a47c4520d57f95f4bb90077d6e633dc68db076', 45, 1, 'eventRight', '[]', 0, '2021-06-29 08:44:15', '2021-06-29 08:44:15', '2022-06-29 14:14:15'),
('c14b93511cea00707086aa2b562e5bb11fffe1ee3846dd88d7cf2410ee13a6c882f6c38963652091', 5, 1, 'eventRight', '[]', 0, '2021-05-22 06:36:46', '2021-05-22 06:36:46', '2022-05-22 12:06:46'),
('c15d84d1886e50564501fe6ab51f6a5f685501f32775d693e88c3c8be6f61d17464abdcf7d05eada', 7, 1, 'eventRight', '[]', 0, '2021-05-12 22:54:19', '2021-05-12 22:54:19', '2022-05-13 04:24:19'),
('c17473bf1d774abd292d538abd91957de2b9a68527c6ab5d13244121d75d80ff167bdc5a5f3d5b8c', 7, 1, 'eventRight', '[]', 0, '2021-08-05 20:59:11', '2021-08-05 20:59:11', '2022-08-06 02:29:11'),
('c1caeacefa0f3e5abbcba97522410abdc742ac7f204d3e701e497539a22515a8b07e6858462a388d', 7, 1, 'eventRight', '[]', 0, '2021-07-20 09:35:05', '2021-07-20 09:35:05', '2022-07-20 15:05:05'),
('c383c74ef3026cf9cf9b02f8365952f8f72b5696d7868633f3ebf8bb28f2d6c4dfc602987ae9b261', 7, 1, 'eventRight', '[]', 0, '2021-05-22 06:26:29', '2021-05-22 06:26:29', '2022-05-22 11:56:29'),
('c814dab4755a2dd52726a1d16f30c570c56afad0870a77b38e3a95f0afc5585fe73789317c234c22', 7, 1, 'eventRight', '[]', 0, '2021-07-27 12:59:56', '2021-07-27 12:59:56', '2022-07-27 18:29:56'),
('c8199d15e823cf3779486c897f16b8fb60ed2bfbe67e85c4ce02eb66f16b47c97558f48e103abfe9', 7, 1, 'eventRight', '[]', 0, '2021-05-22 06:27:19', '2021-05-22 06:27:19', '2022-05-22 11:57:19'),
('c903b37279a5e95d0564988fa00fb7b6a9009e54d8a301b91147be2d23a195a1dd472b2bea8edc7b', 7, 1, 'eventRight', '[]', 0, '2021-05-22 11:53:04', '2021-05-22 11:53:04', '2022-05-22 17:23:04'),
('c942c5d28dcf9ba8c73e4d914f925c05f0e96a3d7d1e66cacdf75aaa39aaa3f4fdea6f0b131ddfe6', 7, 1, 'eventRight', '[]', 0, '2021-08-23 00:52:52', '2021-08-23 00:52:52', '2022-08-23 06:22:52'),
('c992a6ff79ce1f38cf388bf139701e8cffea7edd00297979510d97848ce5f6f221c7d5d76bc0b8e7', 7, 1, 'eventRight', '[]', 0, '2021-05-21 23:03:54', '2021-05-21 23:03:54', '2022-05-22 04:33:54'),
('cb5fd6c0575583252fceaf277a8a9bdc639f62833d5e9583ff71a5a95b9a96b391d9b49b0bed2922', 39, 1, 'eventRight', '[]', 0, '2021-06-15 08:52:33', '2021-06-15 08:52:33', '2022-06-15 14:22:33'),
('ceca63686b3a0b67a35c2bd22075167d84c89e55a34529b0a138e5f64061d8727a16e862bf2c0bff', 10, 1, 'eventRight', '[]', 0, '2021-02-23 08:02:33', '2021-02-23 08:02:33', '2022-02-23 13:32:33'),
('cf71a31b840f791611628b5286f6bd2a8d8edff3908994d7cbfb768bca6cd3852739dfa8e763bd38', 7, 1, 'eventRight', '[]', 0, '2021-06-02 11:23:36', '2021-06-02 11:23:36', '2022-06-02 16:53:36'),
('d2f14edb89afb64ae3c7d20a85c08eca639c1f634f380c555c912af0ed05d4a818020f0536447dca', 45, 1, 'eventRight', '[]', 0, '2021-07-12 09:16:49', '2021-07-12 09:16:49', '2022-07-12 14:46:49'),
('d2f326478a8f4fa35b573b03a913f7278cacf715c6d6eeb33de1dcb7729b0d6b31cd5f064ce00ddb', 7, 1, 'eventRight', '[]', 0, '2021-08-05 05:17:34', '2021-08-05 05:17:34', '2022-08-05 10:47:34'),
('d386797a787399e43abe0c7eb1c603ba22c3f0ea9f6f1aca0a22031579fa68fc37cb59e17aaf9a9d', 7, 1, 'eventRight', '[]', 0, '2021-05-03 02:36:51', '2021-05-03 02:36:51', '2022-05-03 08:06:51'),
('d4705971fd30a45d40f76f4bd5ef250cf6ea52c1d9acbed144a8508566a9bf26945c776f4bd54f6d', 5, 1, 'eventRight', '[]', 0, '2021-03-24 11:54:07', '2021-03-24 11:54:07', '2022-03-24 17:24:07'),
('d50941f406ad9e5add8adc9a21564eae6ec102b25aed047e22aedaa3009837404ea79f4a4e6bcff0', 41, 1, 'eventRight', '[]', 0, '2021-07-07 05:45:32', '2021-07-07 05:45:32', '2022-07-07 11:15:32'),
('d5be49e14242b70d4f2b8271655a7c6944e58cabe48cef7afd560f91757cb8829bca718286a623a0', 10, 1, 'eventRight', '[]', 0, '2021-02-23 08:05:09', '2021-02-23 08:05:09', '2022-02-23 13:35:09'),
('d5e2d9ca1d24f1a16683ad1e7d7bd3b98ac8f90da611bdfefacebaae0d7bf4c57019fc5fa6536a8f', 33, 1, 'eventRight', '[]', 0, '2021-05-12 14:48:20', '2021-05-12 14:48:20', '2022-05-12 20:18:20'),
('d6156249b23073588361421f77b1cb239375ae940c2e57cadd18320019035c8d624f481867f16f3f', 66, 1, 'eventRight', '[]', 0, '2021-08-06 10:21:15', '2021-08-06 10:21:15', '2022-08-06 15:51:15'),
('d61743ffa4a5228aa7d9668be7fd9553feb4c54fa0cf80b2719d0cf4333398422da378d1bc2f9182', 7, 1, 'eventRight', '[]', 0, '2021-06-01 18:23:24', '2021-06-01 18:23:24', '2022-06-01 23:53:24'),
('d6739c8f7ff6fa21bdfb9285b024dfa900e6ff00a41afeddab1fbc2d02225c8c026321b6773a4780', 26, 1, 'eventRight', '[]', 0, '2021-04-28 03:24:00', '2021-04-28 03:24:00', '2022-04-28 08:54:00'),
('d7b97c57edfcec89cc731d39ae98afe8e5f075ef6223a115d44985a97d56ee2163c624c185fba830', 7, 1, 'eventRight', '[]', 0, '2021-05-23 05:53:20', '2021-05-23 05:53:20', '2022-05-23 11:23:20'),
('d97c641bb8b5c01fdcfc75b840e6632e5bb693c312d8403b3627d3457d2cd92493b5e6f5425701b3', 28, 1, 'eventRight', '[]', 0, '2021-05-28 18:31:06', '2021-05-28 18:31:06', '2022-05-29 00:01:06'),
('d990eec40669a43e628922eb52749f2e63ac85383648f80a6c6c5f7d27c57b617944a3efa08fef8e', 37, 1, 'eventRight', '[]', 0, '2021-06-29 08:41:06', '2021-06-29 08:41:06', '2022-06-29 14:11:06'),
('da5fd153a41bd49a56fa53c72e86594ec6c5825f1d9265fb76aa6b7dfde02fd2f112de674e8efdf7', 7, 1, 'eventRight', '[]', 0, '2021-05-27 00:33:52', '2021-05-27 00:33:52', '2022-05-27 06:03:52'),
('da7ee3d603b039c20d1c7963643067a68cf8d7533438242e5bfaa76b3bc71f6a7fbd5a602e769e36', 33, 1, 'eventRight', '[]', 0, '2021-05-12 14:00:45', '2021-05-12 14:00:45', '2022-05-12 19:30:45'),
('df6a5c5dc59c11e89635cb8ff7e0366b5de704844b4666211f37960b2b70bc13bf44a943e28b44df', 7, 1, 'eventRight', '[]', 0, '2021-06-01 19:08:55', '2021-06-01 19:08:55', '2022-06-02 00:38:55'),
('e05f8656ed3b9d92f04572af0ba866a8425de5283e31773a08696bd752d70a0147cf59e676e29c14', 7, 1, 'eventRight', '[]', 0, '2021-08-23 00:49:10', '2021-08-23 00:49:10', '2022-08-23 06:19:10'),
('e19b8934e0a83a8d151cd05b09927526da697695f6ac81ac99e101e4699ce662a424ddd058edafc4', 7, 1, 'eventRight', '[]', 0, '2021-07-29 10:35:11', '2021-07-29 10:35:11', '2022-07-29 16:05:11'),
('e19c82f512847b418f14f5e7cd849a559561eb99b685647adb90356b8bf430e8c38c3c6a9d85aa80', 32, 1, 'eventRight', '[]', 0, '2021-07-06 01:07:22', '2021-07-06 01:07:22', '2022-07-06 06:37:22'),
('e24de30876ba11f03a3a739fe9822dc028d8bc4929ba3a36f45a9d6893f959d5e260a08ff8a04815', 10, 1, 'eventRight', '[]', 0, '2021-08-08 07:30:15', '2021-08-08 07:30:15', '2022-08-08 13:00:15'),
('e27d7ab42ec4ef4b96f46503e8041a68c363b9bb476931324db9d25a5d50742ce17e2a2ea212de35', 7, 1, 'eventRight', '[]', 0, '2021-07-30 04:30:19', '2021-07-30 04:30:19', '2022-07-30 10:00:19'),
('e324aa8d5d056685fa1b9b703035240eb6fd19c9aa5ba5e4435a6b5936aeb6b5910ce63b1c7303ca', 10, 1, 'eventRight', '[]', 0, '2021-04-21 00:58:35', '2021-04-21 00:58:35', '2022-04-21 06:28:35'),
('e53ef2abf7ada5a7e40499ca36595e1a874a550fe094d8739ebbd9eff99e9de17746ed423ac57fbe', 58, 1, 'eventRight', '[]', 0, '2021-08-06 10:19:16', '2021-08-06 10:19:16', '2022-08-06 15:49:16'),
('e65f73c641a2dc13d0172c152196d7e2fa06772a208a93f9f145d8343ccc02d20648abb496d5f3e9', 17, 1, 'eventRight', '[]', 0, '2021-04-08 02:07:42', '2021-04-08 02:07:42', '2022-04-08 07:37:42'),
('e6b292997910147215e90d0829792f4c3909996c01acfc4923612eccee9d12a383381e42e29eedb7', 52, 1, 'eventRight', '[]', 0, '2021-07-10 10:31:52', '2021-07-10 10:31:52', '2022-07-10 16:01:52'),
('ea90426c665d1b834b830268662aa4b81990087075d8721ded1d5a4ea942efb3a2e7aae3419a8d5d', 57, 1, 'eventRight', '[]', 0, '2021-07-20 22:54:17', '2021-07-20 22:54:17', '2022-07-21 04:24:17'),
('ec17ef3a466ae53109035a74b2a8c9d41a119368c9a02d009a05d7156a15eb67a680707681271538', 16, 1, 'eventRight', '[]', 0, '2021-04-16 05:23:56', '2021-04-16 05:23:56', '2022-04-16 10:53:56'),
('ecbcf40556072089b4552c5bf5d56918a0530a747353eb64b36608cb37d5aad7e8e7f179c2a7760b', 7, 1, 'eventRight', '[]', 0, '2021-05-22 06:24:24', '2021-05-22 06:24:24', '2022-05-22 11:54:24'),
('f01ed83c00b5232392c3c0be378083ba3f3d679c6cf9998ab50573f27b2f3f52bad3cc93937e43bf', 7, 1, 'eventRight', '[]', 0, '2021-08-07 13:07:32', '2021-08-07 13:07:32', '2022-08-07 18:37:32'),
('f191f5435850396e1033bf020df4230659b40910dab9771be32791c20715b6dfb33b06ce7c327019', 65, 1, 'eventRight', '[]', 0, '2021-08-05 05:11:29', '2021-08-05 05:11:29', '2022-08-05 10:41:29'),
('f362ee287ff1abe8eb59feca7233a1488ee3c5a5e0cf8366b27a0c5737eaf9827afb08e50739f716', 13, 1, 'eventRight', '[]', 0, '2021-02-23 00:24:08', '2021-02-23 00:24:08', '2022-02-23 05:54:08'),
('f5eac5af574e578759ca0ede346eda03fa940e55304ec916638cfac0cdf7742e75409fa6cd87e1c2', 10, 1, 'eventRight', '[]', 0, '2021-03-25 09:09:17', '2021-03-25 09:09:17', '2022-03-25 14:39:17'),
('f725ec18cbad47fefb5e83a0e1a04b6405aa56e01a8618534ede021e56279bf4090e07f1e233dd76', 10, 1, 'eventRight', '[]', 0, '2021-03-24 00:27:34', '2021-03-24 00:27:34', '2022-03-24 05:57:34'),
('f7f9fa3f5df6743d556dab33f809080b3dd92710e4c2dbb222ace46ce35d62a699be1f7821cbb2a8', 10, 1, 'eventRight', '[]', 0, '2021-05-10 03:08:36', '2021-05-10 03:08:36', '2022-05-10 08:38:36'),
('f8a9407c521ca978f4d05bc14ec73f4c9e7c86177fce84b015aa87b8c1fe6c2ba88e9a526d3bf216', 7, 1, 'eventRight', '[]', 0, '2021-05-21 22:47:02', '2021-05-21 22:47:02', '2022-05-22 04:17:02'),
('f9d73dfe48f78a660b3d36143037d5e388b556113e52fd13ff64ba9d47446cc892647a99b973519e', 5, 1, 'eventRight', '[]', 0, '2021-07-27 13:39:19', '2021-07-27 13:39:19', '2022-07-27 19:09:19'),
('fb1692393ee87c04a51ee0f5fc7ef05331c12930f279b7b79ec2639271eaaa8cb79f205d13a751fe', 7, 1, 'eventRight', '[]', 0, '2021-06-11 08:11:41', '2021-06-11 08:11:41', '2022-06-11 13:41:41');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `provider` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `provider`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'Laravel Personal Access Client', 'JNn1oX9FfYTcxBK568gREkOUz7QB5JD6y53YreAV', NULL, 'http://localhost', 1, 0, 0, '2020-12-25 00:28:20', '2020-12-25 00:28:20'),
(2, NULL, 'Laravel Password Grant Client', 'duBl5iiKc701YU0oiskSb8thELzsB6uN8ozneNwH', 'users', 'http://localhost', 0, 1, 0, '2020-12-25 00:28:20', '2020-12-25 00:28:20');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `client_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2020-12-25 00:28:20', '2020-12-25 00:28:20');

-- --------------------------------------------------------

--
-- Table structure for table `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(255) DEFAULT NULL,
  `customer_id` int(11) NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `event_id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `coupon_id` int(11) DEFAULT NULL,
  `quantity` int(11) NOT NULL,
  `coupon_discount` int(11) NOT NULL DEFAULT 0,
  `tax` int(11) DEFAULT 0,
  `org_commission` int(11) DEFAULT NULL,
  `payment` int(11) NOT NULL,
  `payment_type` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL DEFAULT 0,
  `payment_token` varchar(255) DEFAULT NULL,
  `order_status` varchar(255) NOT NULL DEFAULT 'Pending',
  `org_pay_status` int(11) DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_child`
--

CREATE TABLE `order_child` (
  `id` int(11) NOT NULL,
  `ticket_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `customer_id` int(11) DEFAULT NULL,
  `ticket_number` varchar(255) NOT NULL,
  `status` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_tax`
--

CREATE TABLE `order_tax` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `tax_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_setting`
--

CREATE TABLE `payment_setting` (
  `id` int(11) NOT NULL,
  `stripe` int(11) NOT NULL,
  `cod` int(11) NOT NULL,
  `paypal` int(11) DEFAULT NULL,
  `razor` int(11) DEFAULT NULL,
  `flutterwave` int(11) DEFAULT NULL,
  `stripeSecretKey` varchar(255) DEFAULT NULL,
  `stripePublicKey` varchar(255) DEFAULT NULL,
  `paypalClientId` varchar(255) DEFAULT NULL,
  `paypalSecret` varchar(255) DEFAULT NULL,
  `razorPublishKey` varchar(255) DEFAULT NULL,
  `razorSecretKey` varchar(255) DEFAULT NULL,
  `ravePublicKey` varchar(255) DEFAULT NULL,
  `raveSecretKey` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment_setting`
--

INSERT INTO `payment_setting` (`id`, `stripe`, `cod`, `paypal`, `razor`, `flutterwave`, `stripeSecretKey`, `stripePublicKey`, `paypalClientId`, `paypalSecret`, `razorPublishKey`, `razorSecretKey`, `ravePublicKey`, `raveSecretKey`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 1, 0, 0, 'pk_test_4IAuwNLPW5juObBEnd5WUNgH00kaQsilTH', 'sk_test_zWj8f67UiYMjYe5kwsHzPGr400fJhUdHqB', 'sb-d71jk54885_api1.business.example.com', 'AFPkWEaesrA1Pp2BBuH8mRnoTWO1A4I635SZzhD4sUra3MkjM0wUDUI6', NULL, NULL, NULL, NULL, '2020-12-23 00:00:00', '2021-08-18 08:24:02');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin_dashboard', 'web', '2020-12-15 05:59:59', '2020-12-15 05:59:59'),
(2, 'user_access', 'web', '2020-12-15 06:00:45', '2020-12-15 06:00:45'),
(3, 'user_create', 'web', '2020-12-15 06:31:28', '2020-12-15 06:31:28'),
(4, 'user_edit', 'web', '2020-12-15 06:31:28', '2020-12-15 06:31:28'),
(5, 'user_delete', 'web', '2020-12-15 06:31:28', '2020-12-15 06:31:28'),
(6, 'role_access', 'web', '2020-12-15 06:32:27', '2020-12-15 06:32:27'),
(7, 'role_create', 'web', '2020-12-15 06:32:27', '2020-12-15 06:32:27'),
(8, 'role_edit', 'web', '2020-12-15 06:32:27', '2020-12-15 06:32:27'),
(9, 'role_delete', 'web', '2020-12-15 06:32:27', '2020-12-15 06:32:27'),
(10, 'admin_setting', 'web', '2020-12-15 06:38:19', '2020-12-15 06:38:19'),
(11, 'event_access', 'web', '2020-12-15 07:10:01', '2020-12-15 07:10:01'),
(12, 'event_create', 'web', '2020-12-15 07:10:01', '2020-12-15 07:10:01'),
(13, 'event_edit', 'web', '2020-12-15 07:10:01', '2020-12-15 07:10:01'),
(14, 'event_delete', 'web', '2020-12-15 07:10:02', '2020-12-15 07:10:02'),
(15, 'category_access', 'web', '2020-12-15 07:10:42', '2020-12-15 07:10:42'),
(16, 'category_create', 'web', '2020-12-15 07:10:43', '2020-12-15 07:10:43'),
(17, 'category_edit', 'web', '2020-12-15 07:10:43', '2020-12-15 07:10:43'),
(18, 'category_delete', 'web', '2020-12-15 07:10:43', '2020-12-15 07:10:43'),
(19, 'location_access', 'web', '2020-12-15 07:11:16', '2020-12-15 07:11:16'),
(20, 'location_create', 'web', '2020-12-15 07:11:16', '2020-12-15 07:11:16'),
(21, 'location_edit', 'web', '2020-12-15 07:11:16', '2020-12-15 07:11:16'),
(22, 'location_delete', 'web', '2020-12-15 07:11:16', '2020-12-15 07:11:16'),
(23, 'ticket_access', 'web', '2020-12-16 05:33:31', '2020-12-16 05:33:31'),
(24, 'ticket_create', 'web', '2020-12-16 05:33:32', '2020-12-16 05:33:32'),
(25, 'ticket_edit', 'web', '2020-12-16 05:33:32', '2020-12-16 05:33:32'),
(26, 'ticket_delete', 'web', '2020-12-16 05:33:32', '2020-12-16 05:33:32'),
(27, 'organization_dashboard', 'web', '2020-12-16 05:56:02', '2020-12-16 05:56:02'),
(28, 'organization_setting', 'web', '2020-12-16 05:56:36', '2020-12-16 05:56:36'),
(29, 'organization_report', 'web', '2020-12-16 05:58:03', '2020-12-16 05:58:03'),
(30, 'admin_report', 'web', '2020-12-16 05:58:38', '2020-12-16 05:58:38'),
(31, 'block_app_user', 'web', '2020-12-21 01:23:26', '2020-12-21 01:23:26'),
(32, 'app_user_create', 'web', '2020-12-21 01:34:34', '2020-12-21 01:34:34'),
(33, 'notification_template_access', 'web', '2020-12-23 05:46:44', '2020-12-23 05:46:44'),
(34, 'notification_template_create', 'web', '2020-12-23 05:46:45', '2020-12-23 05:46:45'),
(35, 'notification_template_edit', 'web', '2020-12-23 05:46:45', '2020-12-23 05:46:45'),
(36, 'coupon_access', 'web', '2020-12-26 00:29:32', '2020-12-26 00:29:32'),
(37, 'coupon_create', 'web', '2020-12-26 00:29:33', '2020-12-26 00:29:33'),
(38, 'coupon_edit', 'web', '2020-12-26 00:29:33', '2020-12-26 00:29:33'),
(39, 'coupon_delete', 'web', '2020-12-26 00:29:33', '2020-12-26 00:29:33'),
(40, 'tax_access', 'web', '2021-01-09 00:13:19', '2021-01-09 00:13:19'),
(41, 'tax_create', 'web', '2021-01-09 00:13:19', '2021-01-09 00:13:19'),
(42, 'tax_edit', 'web', '2021-01-09 00:13:19', '2021-01-09 00:13:19'),
(43, 'tax_delete', 'web', '2021-01-09 00:13:19', '2021-01-09 00:13:19'),
(44, 'faq_access', 'web', '2021-01-11 03:21:55', '2021-01-11 03:21:55'),
(45, 'faq_create', 'web', '2021-01-11 03:21:55', '2021-01-11 03:21:55'),
(46, 'faq_edit', 'web', '2021-01-11 03:21:55', '2021-01-11 03:21:55'),
(47, 'faq_delete', 'web', '2021-01-11 03:21:55', '2021-01-11 03:21:55'),
(48, 'feedback_access', 'web', '2021-01-12 06:55:01', '2021-01-12 06:55:01'),
(49, 'feedback_create', 'web', '2021-01-12 06:55:01', '2021-01-12 06:55:01'),
(50, 'feedback_edit', 'web', '2021-01-12 06:55:01', '2021-01-12 06:55:01'),
(51, 'feedback_delete', 'web', '2021-01-12 06:55:01', '2021-01-12 06:55:01'),
(52, 'banner_access', 'web', '2021-02-04 22:06:33', '2021-02-04 22:06:33'),
(53, 'banner_create', 'web', '2021-02-04 22:06:33', '2021-02-04 22:06:33'),
(54, 'banner_edit', 'web', '2021-02-04 22:06:34', '2021-02-04 22:06:34'),
(55, 'banner_delete', 'web', '2021-02-04 22:06:34', '2021-02-04 22:06:34'),
(56, 'blog_access', 'web', '2021-02-07 22:15:36', '2021-02-07 22:15:36'),
(57, 'blog_create', 'web', '2021-02-07 22:15:36', '2021-02-07 22:15:36'),
(58, 'blog_edit', 'web', '2021-02-07 22:15:36', '2021-02-07 22:15:36'),
(59, 'blog_delete', 'web', '2021-02-07 22:15:36', '2021-02-07 22:15:36'),
(60, 'scanner_access', 'web', '2021-02-18 04:30:03', '2021-02-18 04:30:03'),
(61, 'scanner_create', 'web', '2021-02-18 04:30:03', '2021-02-18 04:30:03'),
(62, 'scanner_delete', 'web', '2021-02-18 04:30:03', '2021-02-18 04:30:03'),
(63, 'language_access', 'web', '2021-08-23 23:56:58', '2021-08-23 23:56:58'),
(64, 'language_add', 'web', '2021-08-23 23:56:58', '2021-08-23 23:56:58'),
(65, 'language_edit', 'web', '2021-08-23 23:56:59', '2021-08-23 23:56:59');

-- --------------------------------------------------------

--
-- Table structure for table `review`
--

CREATE TABLE `review` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `organization_id` int(11) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `message` varchar(255) NOT NULL,
  `rate` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'web', '2020-12-15 06:06:10', '2020-12-15 06:06:10'),
(2, 'organization', 'web', '2020-12-15 06:35:40', '2020-12-15 06:35:40'),
(3, 'scanner', 'web', '2021-02-18 04:10:12', '2021-02-18 04:10:12'),
(4, 'Ballon', 'web', '2021-03-19 03:33:15', '2021-03-19 03:33:15'),
(5, 'Annie Hy', 'web', '2021-03-27 04:01:00', '2021-03-27 04:01:00'),
(6, 'Abdul', 'web', '2021-06-07 02:56:25', '2021-06-07 02:56:25'),
(7, 'Gu', 'web', '2021-06-09 14:39:24', '2021-06-09 14:39:24'),
(8, 'Mohamed Kassim', 'web', '2021-06-19 22:10:19', '2021-06-19 22:10:19');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 5),
(2, 1),
(2, 3),
(2, 4),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(11, 2),
(12, 1),
(12, 2),
(13, 1),
(13, 2),
(14, 1),
(14, 2),
(15, 1),
(15, 4),
(16, 1),
(17, 1),
(18, 1),
(19, 2),
(20, 2),
(21, 2),
(22, 2),
(23, 1),
(23, 2),
(23, 8),
(24, 1),
(24, 2),
(24, 7),
(25, 1),
(25, 2),
(26, 1),
(26, 2),
(27, 2),
(30, 1),
(31, 1),
(32, 1),
(32, 2),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(40, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(51, 1),
(52, 1),
(52, 2),
(53, 1),
(53, 2),
(54, 1),
(54, 2),
(55, 1),
(55, 2),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 2),
(61, 2),
(62, 2),
(63, 1),
(64, 1),
(65, 1);

-- --------------------------------------------------------

--
-- Table structure for table `settlement`
--

CREATE TABLE `settlement` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `payment` int(11) NOT NULL,
  `payment_token` varchar(255) DEFAULT NULL,
  `payment_type` varchar(255) NOT NULL,
  `payment_status` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tax`
--

CREATE TABLE `tax` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` int(11) NOT NULL,
  `allow_all_bill` int(11) NOT NULL DEFAULT 1,
  `status` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tax`
--

INSERT INTO `tax` (`id`, `user_id`, `name`, `price`, `allow_all_bill`, `status`, `created_at`, `updated_at`) VALUES
(1, 2, 'Service tax', 30, 0, 1, '2021-01-09 07:09:59', '2021-01-09 07:30:13'),
(6, 5, 'Road Tax', 10, 1, 1, '2021-01-15 06:24:28', '2021-06-24 03:02:28'),
(7, 20, 'Gratuito', 0, 1, 1, '2021-04-15 01:52:20', '2021-04-15 01:52:20'),
(8, 74, 'main', 15, 1, 1, '2021-08-19 18:15:56', '2021-08-19 18:15:56');

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `ticket_number` varchar(50) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `type` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `ticket_per_order` int(11) DEFAULT 1,
  `start_time` varchar(255) DEFAULT NULL,
  `end_time` varchar(255) DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `description` text DEFAULT NULL,
  `status` int(11) NOT NULL,
  `is_deleted` int(11) NOT NULL DEFAULT 0,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `timezones`
--

CREATE TABLE `timezones` (
  `CountryCode` char(2) NOT NULL,
  `Coordinates` char(15) NOT NULL,
  `TimeZone` char(32) NOT NULL,
  `Comments` varchar(85) NOT NULL,
  `UTC_offset` char(8) NOT NULL,
  `UTC_DST_offset` char(8) NOT NULL,
  `Notes` varchar(79) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `timezones`
--

INSERT INTO `timezones` (`CountryCode`, `Coordinates`, `TimeZone`, `Comments`, `UTC_offset`, `UTC_DST_offset`, `Notes`) VALUES
('CI', '+0519-00402', 'Africa/Abidjan', '', '+00:00', '+00:00', ''),
('GH', '+0533-00013', 'Africa/Accra', '', '+00:00', '+00:00', ''),
('ET', '+0902+03842', 'Africa/Addis_Ababa', '', '+03:00', '+03:00', ''),
('DZ', '+3647+00303', 'Africa/Algiers', '', '+01:00', '+01:00', ''),
('ER', '+1520+03853', 'Africa/Asmara', '', '+03:00', '+03:00', ''),
('', '', 'Africa/Asmera', '', '+03:00', '+03:00', 'Link to Africa/Asmara'),
('ML', '+1239-00800', 'Africa/Bamako', '', '+00:00', '+00:00', ''),
('CF', '+0422+01835', 'Africa/Bangui', '', '+01:00', '+01:00', ''),
('GM', '+1328-01639', 'Africa/Banjul', '', '+00:00', '+00:00', ''),
('GW', '+1151-01535', 'Africa/Bissau', '', '+00:00', '+00:00', ''),
('MW', '-1547+03500', 'Africa/Blantyre', '', '+02:00', '+02:00', ''),
('CG', '-0416+01517', 'Africa/Brazzaville', '', '+01:00', '+01:00', ''),
('BI', '-0323+02922', 'Africa/Bujumbura', '', '+02:00', '+02:00', ''),
('EG', '+3003+03115', 'Africa/Cairo', '', '+02:00', '+02:00', 'DST has been canceled since 2011'),
('MA', '+3339-00735', 'Africa/Casablanca', '', '+00:00', '+01:00', ''),
('ES', '+3553-00519', 'Africa/Ceuta', 'Ceuta & Melilla', '+01:00', '+02:00', ''),
('GN', '+0931-01343', 'Africa/Conakry', '', '+00:00', '+00:00', ''),
('SN', '+1440-01726', 'Africa/Dakar', '', '+00:00', '+00:00', ''),
('TZ', '-0648+03917', 'Africa/Dar_es_Salaam', '', '+03:00', '+03:00', ''),
('DJ', '+1136+04309', 'Africa/Djibouti', '', '+03:00', '+03:00', ''),
('CM', '+0403+00942', 'Africa/Douala', '', '+01:00', '+01:00', ''),
('EH', '+2709-01312', 'Africa/El_Aaiun', '', '+00:00', '+00:00', ''),
('SL', '+0830-01315', 'Africa/Freetown', '', '+00:00', '+00:00', ''),
('BW', '-2439+02555', 'Africa/Gaborone', '', '+02:00', '+02:00', ''),
('ZW', '-1750+03103', 'Africa/Harare', '', '+02:00', '+02:00', ''),
('ZA', '-2615+02800', 'Africa/Johannesburg', '', '+02:00', '+02:00', ''),
('SS', '+0451+03136', 'Africa/Juba', '', '+03:00', '+03:00', ''),
('UG', '+0019+03225', 'Africa/Kampala', '', '+03:00', '+03:00', ''),
('SD', '+1536+03232', 'Africa/Khartoum', '', '+03:00', '+03:00', ''),
('RW', '-0157+03004', 'Africa/Kigali', '', '+02:00', '+02:00', ''),
('CD', '-0418+01518', 'Africa/Kinshasa', 'west Dem. Rep. of Congo', '+01:00', '+01:00', ''),
('NG', '+0627+00324', 'Africa/Lagos', '', '+01:00', '+01:00', ''),
('GA', '+0023+00927', 'Africa/Libreville', '', '+01:00', '+01:00', ''),
('TG', '+0608+00113', 'Africa/Lome', '', '+00:00', '+00:00', ''),
('AO', '-0848+01314', 'Africa/Luanda', '', '+01:00', '+01:00', ''),
('CD', '-1140+02728', 'Africa/Lubumbashi', 'east Dem. Rep. of Congo', '+02:00', '+02:00', ''),
('ZM', '-1525+02817', 'Africa/Lusaka', '', '+02:00', '+02:00', ''),
('GQ', '+0345+00847', 'Africa/Malabo', '', '+01:00', '+01:00', ''),
('MZ', '-2558+03235', 'Africa/Maputo', '', '+02:00', '+02:00', ''),
('LS', '-2928+02730', 'Africa/Maseru', '', '+02:00', '+02:00', ''),
('SZ', '-2618+03106', 'Africa/Mbabane', '', '+02:00', '+02:00', ''),
('SO', '+0204+04522', 'Africa/Mogadishu', '', '+03:00', '+03:00', ''),
('LR', '+0618-01047', 'Africa/Monrovia', '', '+00:00', '+00:00', ''),
('KE', '-0117+03649', 'Africa/Nairobi', '', '+03:00', '+03:00', ''),
('TD', '+1207+01503', 'Africa/Ndjamena', '', '+01:00', '+01:00', ''),
('NE', '+1331+00207', 'Africa/Niamey', '', '+01:00', '+01:00', ''),
('MR', '+1806-01557', 'Africa/Nouakchott', '', '+00:00', '+00:00', ''),
('BF', '+1222-00131', 'Africa/Ouagadougou', '', '+00:00', '+00:00', ''),
('BJ', '+0629+00237', 'Africa/Porto-Novo', '', '+01:00', '+01:00', ''),
('ST', '+0020+00644', 'Africa/Sao_Tome', '', '+00:00', '+00:00', ''),
('', '', 'Africa/Timbuktu', '', '+00:00', '+00:00', 'Link to Africa/Bamako'),
('LY', '+3254+01311', 'Africa/Tripoli', '', '+01:00', '+02:00', ''),
('TN', '+3648+01011', 'Africa/Tunis', '', '+01:00', '+01:00', ''),
('NA', '-2234+01706', 'Africa/Windhoek', '', '+01:00', '+02:00', ''),
('', '', 'AKST9AKDT', '', 'âˆ’09:00', 'âˆ’08:00', 'Link to America/Anchorage'),
('US', '+515248-1763929', 'America/Adak', 'Aleutian Islands', 'âˆ’10:00', 'âˆ’09:00', ''),
('US', '+611305-1495401', 'America/Anchorage', 'Alaska Time', 'âˆ’09:00', 'âˆ’08:00', ''),
('AI', '+1812-06304', 'America/Anguilla', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('AG', '+1703-06148', 'America/Antigua', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('BR', '-0712-04812', 'America/Araguaina', 'Tocantins', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-3436-05827', 'America/Argentina/Buenos_Aires', 'Buenos Aires (BA, CF)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-2828-06547', 'America/Argentina/Catamarca', 'Catamarca (CT), Chubut (CH)', 'âˆ’03:00', 'âˆ’03:00', ''),
('', '', 'America/Argentina/ComodRivadavia', '', 'âˆ’03:00', 'âˆ’03:00', 'Link to America/Argentina/Catamarca'),
('AR', '-3124-06411', 'America/Argentina/Cordoba', 'most locations (CB, CC, CN, ER, FM, MN, SE, SF)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-2411-06518', 'America/Argentina/Jujuy', 'Jujuy (JY)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-2926-06651', 'America/Argentina/La_Rioja', 'La Rioja (LR)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-3253-06849', 'America/Argentina/Mendoza', 'Mendoza (MZ)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-5138-06913', 'America/Argentina/Rio_Gallegos', 'Santa Cruz (SC)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-2447-06525', 'America/Argentina/Salta', '(SA, LP, NQ, RN)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-3132-06831', 'America/Argentina/San_Juan', 'San Juan (SJ)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-3319-06621', 'America/Argentina/San_Luis', 'San Luis (SL)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-2649-06513', 'America/Argentina/Tucuman', 'Tucuman (TM)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AR', '-5448-06818', 'America/Argentina/Ushuaia', 'Tierra del Fuego (TF)', 'âˆ’03:00', 'âˆ’03:00', ''),
('AW', '+1230-06958', 'America/Aruba', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('PY', '-2516-05740', 'America/Asuncion', '', 'âˆ’04:00', 'âˆ’03:00', ''),
('CA', '+484531-0913718', 'America/Atikokan', 'Eastern Standard Time - Atikokan, Ontario and Southampton I, Nunavut', 'âˆ’05:00', 'âˆ’05:00', ''),
('', '', 'America/Atka', '', 'âˆ’10:00', 'âˆ’09:00', 'Link to America/Adak'),
('BR', '-1259-03831', 'America/Bahia', 'Bahia', 'âˆ’03:00', 'âˆ’03:00', ''),
('MX', '+2048-10515', 'America/Bahia_Banderas', 'Mexican Central Time - Bahia de Banderas', 'âˆ’06:00', 'âˆ’05:00', ''),
('BB', '+1306-05937', 'America/Barbados', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('BR', '-0127-04829', 'America/Belem', 'Amapa, E Para', 'âˆ’03:00', 'âˆ’03:00', ''),
('BZ', '+1730-08812', 'America/Belize', '', 'âˆ’06:00', 'âˆ’06:00', ''),
('CA', '+5125-05707', 'America/Blanc-Sablon', 'Atlantic Standard Time - Quebec - Lower North Shore', 'âˆ’04:00', 'âˆ’04:00', ''),
('BR', '+0249-06040', 'America/Boa_Vista', 'Roraima', 'âˆ’04:00', 'âˆ’04:00', ''),
('CO', '+0436-07405', 'America/Bogota', '', 'âˆ’05:00', 'âˆ’05:00', ''),
('US', '+433649-1161209', 'America/Boise', 'Mountain Time - south Idaho & east Oregon', 'âˆ’07:00', 'âˆ’06:00', ''),
('', '', 'America/Buenos_Aires', '', 'âˆ’03:00', 'âˆ’03:00', 'Link to America/Argentina/Buenos_Aires'),
('CA', '+690650-1050310', 'America/Cambridge_Bay', 'Mountain Time - west Nunavut', 'âˆ’07:00', 'âˆ’06:00', ''),
('BR', '-2027-05437', 'America/Campo_Grande', 'Mato Grosso do Sul', 'âˆ’04:00', 'âˆ’03:00', ''),
('MX', '+2105-08646', 'America/Cancun', 'Central Time - Quintana Roo', 'âˆ’06:00', 'âˆ’05:00', ''),
('VE', '+1030-06656', 'America/Caracas', '', 'âˆ’04:30', 'âˆ’04:30', ''),
('', '', 'America/Catamarca', '', 'âˆ’03:00', 'âˆ’03:00', 'Link to America/Argentina/Catamarca'),
('GF', '+0456-05220', 'America/Cayenne', '', 'âˆ’03:00', 'âˆ’03:00', ''),
('KY', '+1918-08123', 'America/Cayman', '', 'âˆ’05:00', 'âˆ’05:00', ''),
('US', '+415100-0873900', 'America/Chicago', 'Central Time', 'âˆ’06:00', 'âˆ’05:00', ''),
('MX', '+2838-10605', 'America/Chihuahua', 'Mexican Mountain Time - Chihuahua away from US border', 'âˆ’07:00', 'âˆ’06:00', ''),
('', '', 'America/Coral_Harbour', '', 'âˆ’05:00', 'âˆ’05:00', 'Link to America/Atikokan'),
('', '', 'America/Cordoba', '', 'âˆ’03:00', 'âˆ’03:00', 'Link to America/Argentina/Cordoba'),
('CR', '+0956-08405', 'America/Costa_Rica', '', 'âˆ’06:00', 'âˆ’06:00', ''),
('CA', '+4906-11631', 'America/Creston', 'Mountain Standard Time - Creston, British Columbia', 'âˆ’07:00', 'âˆ’07:00', ''),
('BR', '-1535-05605', 'America/Cuiaba', 'Mato Grosso', 'âˆ’04:00', 'âˆ’03:00', ''),
('CW', '+1211-06900', 'America/Curacao', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('GL', '+7646-01840', 'America/Danmarkshavn', 'east coast, north of Scoresbysund', '+00:00', '+00:00', ''),
('CA', '+6404-13925', 'America/Dawson', 'Pacific Time - north Yukon', 'âˆ’08:00', 'âˆ’07:00', ''),
('CA', '+5946-12014', 'America/Dawson_Creek', 'Mountain Standard Time - Dawson Creek & Fort Saint John, British Columbia', 'âˆ’07:00', 'âˆ’07:00', ''),
('US', '+394421-1045903', 'America/Denver', 'Mountain Time', 'âˆ’07:00', 'âˆ’06:00', ''),
('US', '+421953-0830245', 'America/Detroit', 'Eastern Time - Michigan - most locations', 'âˆ’05:00', 'âˆ’04:00', ''),
('DM', '+1518-06124', 'America/Dominica', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('CA', '+5333-11328', 'America/Edmonton', 'Mountain Time - Alberta, east British Columbia & west Saskatchewan', 'âˆ’07:00', 'âˆ’06:00', ''),
('BR', '-0640-06952', 'America/Eirunepe', 'W Amazonas', 'âˆ’04:00', 'âˆ’04:00', ''),
('SV', '+1342-08912', 'America/El_Salvador', '', 'âˆ’06:00', 'âˆ’06:00', ''),
('', '', 'America/Ensenada', '', 'âˆ’08:00', 'âˆ’07:00', 'Link to America/Tijuana'),
('BR', '-0343-03830', 'America/Fortaleza', 'NE Brazil (MA, PI, CE, RN, PB)', 'âˆ’03:00', 'âˆ’03:00', ''),
('', '', 'America/Fort_Wayne', '', 'âˆ’05:00', 'âˆ’04:00', 'Link to America/Indiana/Indianapolis'),
('CA', '+4612-05957', 'America/Glace_Bay', 'Atlantic Time - Nova Scotia - places that did not observe DST 1966-1971', 'âˆ’04:00', 'âˆ’03:00', ''),
('GL', '+6411-05144', 'America/Godthab', 'most locations', 'âˆ’03:00', 'âˆ’02:00', ''),
('CA', '+5320-06025', 'America/Goose_Bay', 'Atlantic Time - Labrador - most locations', 'âˆ’04:00', 'âˆ’03:00', ''),
('TC', '+2128-07108', 'America/Grand_Turk', '', 'âˆ’05:00', 'âˆ’04:00', ''),
('GD', '+1203-06145', 'America/Grenada', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('GP', '+1614-06132', 'America/Guadeloupe', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('GT', '+1438-09031', 'America/Guatemala', '', 'âˆ’06:00', 'âˆ’06:00', ''),
('EC', '-0210-07950', 'America/Guayaquil', 'mainland', 'âˆ’05:00', 'âˆ’05:00', ''),
('GY', '+0648-05810', 'America/Guyana', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('CA', '+4439-06336', 'America/Halifax', 'Atlantic Time - Nova Scotia (most places), PEI', 'âˆ’04:00', 'âˆ’03:00', ''),
('CU', '+2308-08222', 'America/Havana', '', 'âˆ’05:00', 'âˆ’04:00', ''),
('MX', '+2904-11058', 'America/Hermosillo', 'Mountain Standard Time - Sonora', 'âˆ’07:00', 'âˆ’07:00', ''),
('US', '+394606-0860929', 'America/Indiana/Indianapolis', 'Eastern Time - Indiana - most locations', 'âˆ’05:00', 'âˆ’04:00', ''),
('US', '+411745-0863730', 'America/Indiana/Knox', 'Central Time - Indiana - Starke County', 'âˆ’06:00', 'âˆ’05:00', ''),
('US', '+382232-0862041', 'America/Indiana/Marengo', 'Eastern Time - Indiana - Crawford County', 'âˆ’05:00', 'âˆ’04:00', ''),
('US', '+382931-0871643', 'America/Indiana/Petersburg', 'Eastern Time - Indiana - Pike County', 'âˆ’05:00', 'âˆ’04:00', ''),
('US', '+375711-0864541', 'America/Indiana/Tell_City', 'Central Time - Indiana - Perry County', 'âˆ’06:00', 'âˆ’05:00', ''),
('US', '+384452-0850402', 'America/Indiana/Vevay', 'Eastern Time - Indiana - Switzerland County', 'âˆ’05:00', 'âˆ’04:00', ''),
('US', '+384038-0873143', 'America/Indiana/Vincennes', 'Eastern Time - Indiana - Daviess, Dubois, Knox & Martin Counties', 'âˆ’05:00', 'âˆ’04:00', ''),
('US', '+410305-0863611', 'America/Indiana/Winamac', 'Eastern Time - Indiana - Pulaski County', 'âˆ’05:00', 'âˆ’04:00', ''),
('', '', 'America/Indianapolis', '', 'âˆ’05:00', 'âˆ’04:00', 'Link to America/Indiana/Indianapolis'),
('CA', '+682059-1334300', 'America/Inuvik', 'Mountain Time - west Northwest Territories', 'âˆ’07:00', 'âˆ’06:00', ''),
('CA', '+6344-06828', 'America/Iqaluit', 'Eastern Time - east Nunavut - most locations', 'âˆ’05:00', 'âˆ’04:00', ''),
('JM', '+1800-07648', 'America/Jamaica', '', 'âˆ’05:00', 'âˆ’05:00', ''),
('', '', 'America/Jujuy', '', 'âˆ’03:00', 'âˆ’03:00', 'Link to America/Argentina/Jujuy'),
('US', '+581807-1342511', 'America/Juneau', 'Alaska Time - Alaska panhandle', 'âˆ’09:00', 'âˆ’08:00', ''),
('US', '+381515-0854534', 'America/Kentucky/Louisville', 'Eastern Time - Kentucky - Louisville area', 'âˆ’05:00', 'âˆ’04:00', ''),
('US', '+364947-0845057', 'America/Kentucky/Monticello', 'Eastern Time - Kentucky - Wayne County', 'âˆ’05:00', 'âˆ’04:00', ''),
('', '', 'America/Knox_IN', '', 'âˆ’06:00', 'âˆ’05:00', 'Link to America/Indiana/Knox'),
('BQ', '+120903-0681636', 'America/Kralendijk', '', 'âˆ’04:00', 'âˆ’04:00', 'Link to America/Curacao'),
('BO', '-1630-06809', 'America/La_Paz', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('PE', '-1203-07703', 'America/Lima', '', 'âˆ’05:00', 'âˆ’05:00', ''),
('US', '+340308-1181434', 'America/Los_Angeles', 'Pacific Time', 'âˆ’08:00', 'âˆ’07:00', ''),
('', '', 'America/Louisville', '', 'âˆ’05:00', 'âˆ’04:00', 'Link to America/Kentucky/Louisville'),
('SX', '+180305-0630250', 'America/Lower_Princes', '', 'âˆ’04:00', 'âˆ’04:00', 'Link to America/Curacao'),
('BR', '-0940-03543', 'America/Maceio', 'Alagoas, Sergipe', 'âˆ’03:00', 'âˆ’03:00', ''),
('NI', '+1209-08617', 'America/Managua', '', 'âˆ’06:00', 'âˆ’06:00', ''),
('BR', '-0308-06001', 'America/Manaus', 'E Amazonas', 'âˆ’04:00', 'âˆ’04:00', ''),
('MF', '+1804-06305', 'America/Marigot', '', 'âˆ’04:00', 'âˆ’04:00', 'Link to America/Guadeloupe'),
('MQ', '+1436-06105', 'America/Martinique', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('MX', '+2550-09730', 'America/Matamoros', 'US Central Time - Coahuila, Durango, Nuevo LeÃ³n, Tamaulipas near US border', 'âˆ’06:00', 'âˆ’05:00', ''),
('MX', '+2313-10625', 'America/Mazatlan', 'Mountain Time - S Baja, Nayarit, Sinaloa', 'âˆ’07:00', 'âˆ’06:00', ''),
('', '', 'America/Mendoza', '', 'âˆ’03:00', 'âˆ’03:00', 'Link to America/Argentina/Mendoza'),
('US', '+450628-0873651', 'America/Menominee', 'Central Time - Michigan - Dickinson, Gogebic, Iron & Menominee Counties', 'âˆ’06:00', 'âˆ’05:00', ''),
('MX', '+2058-08937', 'America/Merida', 'Central Time - Campeche, YucatÃ¡n', 'âˆ’06:00', 'âˆ’05:00', ''),
('US', '+550737-1313435', 'America/Metlakatla', 'Metlakatla Time - Annette Island', 'âˆ’08:00', 'âˆ’08:00', ''),
('MX', '+1924-09909', 'America/Mexico_City', 'Central Time - most locations', 'âˆ’06:00', 'âˆ’05:00', ''),
('PM', '+4703-05620', 'America/Miquelon', '', 'âˆ’03:00', 'âˆ’02:00', ''),
('CA', '+4606-06447', 'America/Moncton', 'Atlantic Time - New Brunswick', 'âˆ’04:00', 'âˆ’03:00', ''),
('MX', '+2540-10019', 'America/Monterrey', 'Mexican Central Time - Coahuila, Durango, Nuevo LeÃ³n, Tamaulipas away from US border', 'âˆ’06:00', 'âˆ’05:00', ''),
('UY', '-3453-05611', 'America/Montevideo', '', 'âˆ’03:00', 'âˆ’02:00', ''),
('CA', '+4531-07334', 'America/Montreal', 'Eastern Time - Quebec - most locations', 'âˆ’05:00', 'âˆ’04:00', ''),
('MS', '+1643-06213', 'America/Montserrat', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('BS', '+2505-07721', 'America/Nassau', '', 'âˆ’05:00', 'âˆ’04:00', ''),
('US', '+404251-0740023', 'America/New_York', 'Eastern Time', 'âˆ’05:00', 'âˆ’04:00', ''),
('CA', '+4901-08816', 'America/Nipigon', 'Eastern Time - Ontario & Quebec - places that did not observe DST 1967-1973', 'âˆ’05:00', 'âˆ’04:00', ''),
('US', '+643004-1652423', 'America/Nome', 'Alaska Time - west Alaska', 'âˆ’09:00', 'âˆ’08:00', ''),
('BR', '-0351-03225', 'America/Noronha', 'Atlantic islands', 'âˆ’02:00', 'âˆ’02:00', ''),
('US', '+471551-1014640', 'America/North_Dakota/Beulah', 'Central Time - North Dakota - Mercer County', 'âˆ’06:00', 'âˆ’05:00', ''),
('US', '+470659-1011757', 'America/North_Dakota/Center', 'Central Time - North Dakota - Oliver County', 'âˆ’06:00', 'âˆ’05:00', ''),
('US', '+465042-1012439', 'America/North_Dakota/New_Salem', 'Central Time - North Dakota - Morton County (except Mandan area)', 'âˆ’06:00', 'âˆ’05:00', ''),
('MX', '+2934-10425', 'America/Ojinaga', 'US Mountain Time - Chihuahua near US border', 'âˆ’07:00', 'âˆ’06:00', ''),
('PA', '+0858-07932', 'America/Panama', '', 'âˆ’05:00', 'âˆ’05:00', ''),
('CA', '+6608-06544', 'America/Pangnirtung', 'Eastern Time - Pangnirtung, Nunavut', 'âˆ’05:00', 'âˆ’04:00', ''),
('SR', '+0550-05510', 'America/Paramaribo', '', 'âˆ’03:00', 'âˆ’03:00', ''),
('US', '+332654-1120424', 'America/Phoenix', 'Mountain Standard Time - Arizona', 'âˆ’07:00', 'âˆ’07:00', ''),
('HT', '+1832-07220', 'America/Port-au-Prince', '', 'âˆ’05:00', 'âˆ’04:00', ''),
('', '', 'America/Porto_Acre', '', 'âˆ’04:00', 'âˆ’04:00', 'Link to America/Rio_Branco'),
('BR', '-0846-06354', 'America/Porto_Velho', 'Rondonia', 'âˆ’04:00', 'âˆ’04:00', ''),
('TT', '+1039-06131', 'America/Port_of_Spain', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('PR', '+182806-0660622', 'America/Puerto_Rico', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('CA', '+4843-09434', 'America/Rainy_River', 'Central Time - Rainy River & Fort Frances, Ontario', 'âˆ’06:00', 'âˆ’05:00', ''),
('CA', '+624900-0920459', 'America/Rankin_Inlet', 'Central Time - central Nunavut', 'âˆ’06:00', 'âˆ’05:00', ''),
('BR', '-0803-03454', 'America/Recife', 'Pernambuco', 'âˆ’03:00', 'âˆ’03:00', ''),
('CA', '+5024-10439', 'America/Regina', 'Central Standard Time - Saskatchewan - most locations', 'âˆ’06:00', 'âˆ’06:00', ''),
('CA', '+744144-0944945', 'America/Resolute', 'Central Standard Time - Resolute, Nunavut', 'âˆ’06:00', 'âˆ’05:00', ''),
('BR', '-0958-06748', 'America/Rio_Branco', 'Acre', 'âˆ’04:00', 'âˆ’04:00', ''),
('', '', 'America/Rosario', '', 'âˆ’03:00', 'âˆ’03:00', 'Link to America/Argentina/Cordoba'),
('BR', '-0226-05452', 'America/Santarem', 'W Para', 'âˆ’03:00', 'âˆ’03:00', ''),
('MX', '+3018-11452', 'America/Santa_Isabel', 'Mexican Pacific Time - Baja California away from US border', 'âˆ’08:00', 'âˆ’07:00', ''),
('CL', '-3327-07040', 'America/Santiago', 'most locations', 'âˆ’04:00', 'âˆ’03:00', ''),
('DO', '+1828-06954', 'America/Santo_Domingo', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('BR', '-2332-04637', 'America/Sao_Paulo', 'S & SE Brazil (GO, DF, MG, ES, RJ, SP, PR, SC, RS)', 'âˆ’03:00', 'âˆ’02:00', ''),
('GL', '+7029-02158', 'America/Scoresbysund', 'Scoresbysund / Ittoqqortoormiit', 'âˆ’01:00', '+00:00', ''),
('US', '+364708-1084111', 'America/Shiprock', 'Mountain Time - Navajo', 'âˆ’07:00', 'âˆ’06:00', 'Link to America/Denver'),
('US', '+571035-1351807', 'America/Sitka', 'Alaska Time - southeast Alaska panhandle', 'âˆ’09:00', 'âˆ’08:00', ''),
('BL', '+1753-06251', 'America/St_Barthelemy', '', 'âˆ’04:00', 'âˆ’04:00', 'Link to America/Guadeloupe'),
('CA', '+4734-05243', 'America/St_Johns', 'Newfoundland Time, including SE Labrador', 'âˆ’03:30', 'âˆ’02:30', ''),
('KN', '+1718-06243', 'America/St_Kitts', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('LC', '+1401-06100', 'America/St_Lucia', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('VI', '+1821-06456', 'America/St_Thomas', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('VC', '+1309-06114', 'America/St_Vincent', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('CA', '+5017-10750', 'America/Swift_Current', 'Central Standard Time - Saskatchewan - midwest', 'âˆ’06:00', 'âˆ’06:00', ''),
('HN', '+1406-08713', 'America/Tegucigalpa', '', 'âˆ’06:00', 'âˆ’06:00', ''),
('GL', '+7634-06847', 'America/Thule', 'Thule / Pituffik', 'âˆ’04:00', 'âˆ’03:00', ''),
('CA', '+4823-08915', 'America/Thunder_Bay', 'Eastern Time - Thunder Bay, Ontario', 'âˆ’05:00', 'âˆ’04:00', ''),
('MX', '+3232-11701', 'America/Tijuana', 'US Pacific Time - Baja California near US border', 'âˆ’08:00', 'âˆ’07:00', ''),
('CA', '+4339-07923', 'America/Toronto', 'Eastern Time - Ontario - most locations', 'âˆ’05:00', 'âˆ’04:00', ''),
('VG', '+1827-06437', 'America/Tortola', '', 'âˆ’04:00', 'âˆ’04:00', ''),
('CA', '+4916-12307', 'America/Vancouver', 'Pacific Time - west British Columbia', 'âˆ’08:00', 'âˆ’07:00', ''),
('', '', 'America/Virgin', '', 'âˆ’04:00', 'âˆ’04:00', 'Link to America/St_Thomas'),
('CA', '+6043-13503', 'America/Whitehorse', 'Pacific Time - south Yukon', 'âˆ’08:00', 'âˆ’07:00', ''),
('CA', '+4953-09709', 'America/Winnipeg', 'Central Time - Manitoba & west Ontario', 'âˆ’06:00', 'âˆ’05:00', ''),
('US', '+593249-1394338', 'America/Yakutat', 'Alaska Time - Alaska panhandle neck', 'âˆ’09:00', 'âˆ’08:00', ''),
('CA', '+6227-11421', 'America/Yellowknife', 'Mountain Time - central Northwest Territories', 'âˆ’07:00', 'âˆ’06:00', ''),
('AQ', '-6617+11031', 'Antarctica/Casey', 'Casey Station, Bailey Peninsula', '+11:00', '+08:00', ''),
('AQ', '-6835+07758', 'Antarctica/Davis', 'Davis Station, Vestfold Hills', '+05:00', '+07:00', ''),
('AQ', '-6640+14001', 'Antarctica/DumontDUrville', 'Dumont-d\'Urville Station, Terre Adelie', '+10:00', '+10:00', ''),
('AQ', '-5430+15857', 'Antarctica/Macquarie', 'Macquarie Island Station, Macquarie Island', '+11:00', '+11:00', ''),
('AQ', '-6736+06253', 'Antarctica/Mawson', 'Mawson Station, Holme Bay', '+05:00', '+05:00', ''),
('AQ', '-7750+16636', 'Antarctica/McMurdo', 'McMurdo Station, Ross Island', '+12:00', '+13:00', ''),
('AQ', '-6448-06406', 'Antarctica/Palmer', 'Palmer Station, Anvers Island', 'âˆ’04:00', 'âˆ’03:00', ''),
('AQ', '-6734-06808', 'Antarctica/Rothera', 'Rothera Station, Adelaide Island', 'âˆ’03:00', 'âˆ’03:00', ''),
('AQ', '-9000+00000', 'Antarctica/South_Pole', 'Amundsen-Scott Station, South Pole', '+12:00', '+13:00', 'Link to Antarctica/McMurdo'),
('AQ', '-690022+0393524', 'Antarctica/Syowa', 'Syowa Station, E Ongul I', '+03:00', '+03:00', ''),
('AQ', '-7824+10654', 'Antarctica/Vostok', 'Vostok Station, Lake Vostok', '+06:00', '+06:00', ''),
('SJ', '+7800+01600', 'Arctic/Longyearbyen', '', '+01:00', '+02:00', 'Link to Europe/Oslo'),
('YE', '+1245+04512', 'Asia/Aden', '', '+03:00', '+03:00', ''),
('KZ', '+4315+07657', 'Asia/Almaty', 'most locations', '+06:00', '+06:00', ''),
('JO', '+3157+03556', 'Asia/Amman', '', '+03:00', '+03:00', ''),
('RU', '+6445+17729', 'Asia/Anadyr', 'Moscow+08 - Bering Sea', '+12:00', '+12:00', ''),
('KZ', '+4431+05016', 'Asia/Aqtau', 'Atyrau (Atirau, Gur\'yev), Mangghystau (Mankistau)', '+05:00', '+05:00', ''),
('KZ', '+5017+05710', 'Asia/Aqtobe', 'Aqtobe (Aktobe)', '+05:00', '+05:00', ''),
('TM', '+3757+05823', 'Asia/Ashgabat', '', '+05:00', '+05:00', ''),
('', '', 'Asia/Ashkhabad', '', '+05:00', '+05:00', 'Link to Asia/Ashgabat'),
('IQ', '+3321+04425', 'Asia/Baghdad', '', '+03:00', '+03:00', ''),
('BH', '+2623+05035', 'Asia/Bahrain', '', '+03:00', '+03:00', ''),
('AZ', '+4023+04951', 'Asia/Baku', '', '+04:00', '+05:00', ''),
('TH', '+1345+10031', 'Asia/Bangkok', '', '+07:00', '+07:00', ''),
('LB', '+3353+03530', 'Asia/Beirut', '', '+02:00', '+03:00', ''),
('KG', '+4254+07436', 'Asia/Bishkek', '', '+06:00', '+06:00', ''),
('BN', '+0456+11455', 'Asia/Brunei', '', '+08:00', '+08:00', ''),
('', '', 'Asia/Calcutta', '', '+05:30', '+05:30', 'Link to Asia/Kolkata'),
('MN', '+4804+11430', 'Asia/Choibalsan', 'Dornod, Sukhbaatar', '+08:00', '+08:00', ''),
('CN', '+2934+10635', 'Asia/Chongqing', 'central China - Sichuan, Yunnan, Guangxi, Shaanxi, Guizhou, etc.', '+08:00', '+08:00', 'Covering historic Kansu-Szechuan time zone.'),
('', '', 'Asia/Chungking', '', '+08:00', '+08:00', 'Link to Asia/Chongqing'),
('LK', '+0656+07951', 'Asia/Colombo', '', '+05:30', '+05:30', ''),
('', '', 'Asia/Dacca', '', '+06:00', '+06:00', 'Link to Asia/Dhaka'),
('SY', '+3330+03618', 'Asia/Damascus', '', '+02:00', '+03:00', ''),
('BD', '+2343+09025', 'Asia/Dhaka', '', '+06:00', '+06:00', ''),
('TL', '-0833+12535', 'Asia/Dili', '', '+09:00', '+09:00', ''),
('AE', '+2518+05518', 'Asia/Dubai', '', '+04:00', '+04:00', ''),
('TJ', '+3835+06848', 'Asia/Dushanbe', '', '+05:00', '+05:00', ''),
('PS', '+3130+03428', 'Asia/Gaza', 'Gaza Strip', '+02:00', '+03:00', ''),
('CN', '+4545+12641', 'Asia/Harbin', 'Heilongjiang (except Mohe), Jilin', '+08:00', '+08:00', 'Covering historic Changpai time zone.'),
('PS', '+313200+0350542', 'Asia/Hebron', 'West Bank', '+02:00', '+03:00', ''),
('HK', '+2217+11409', 'Asia/Hong_Kong', '', '+08:00', '+08:00', ''),
('MN', '+4801+09139', 'Asia/Hovd', 'Bayan-Olgiy, Govi-Altai, Hovd, Uvs, Zavkhan', '+07:00', '+07:00', ''),
('VN', '+1045+10640', 'Asia/Ho_Chi_Minh', '', '+07:00', '+07:00', ''),
('RU', '+5216+10420', 'Asia/Irkutsk', 'Moscow+05 - Lake Baikal', '+09:00', '+09:00', ''),
('', '', 'Asia/Istanbul', '', '+02:00', '+03:00', 'Link to Europe/Istanbul'),
('ID', '-0610+10648', 'Asia/Jakarta', 'Java & Sumatra', '+07:00', '+07:00', ''),
('ID', '-0232+14042', 'Asia/Jayapura', 'west New Guinea (Irian Jaya) & Malukus (Moluccas)', '+09:00', '+09:00', ''),
('IL', '+3146+03514', 'Asia/Jerusalem', '', '+02:00', '+03:00', ''),
('AF', '+3431+06912', 'Asia/Kabul', '', '+04:30', '+04:30', ''),
('RU', '+5301+15839', 'Asia/Kamchatka', 'Moscow+08 - Kamchatka', '+12:00', '+12:00', ''),
('PK', '+2452+06703', 'Asia/Karachi', '', '+05:00', '+05:00', ''),
('CN', '+3929+07559', 'Asia/Kashgar', 'west Tibet & Xinjiang', '+08:00', '+08:00', 'Covering historic Kunlun time zone.'),
('NP', '+2743+08519', 'Asia/Kathmandu', '', '+05:45', '+05:45', ''),
('', '', 'Asia/Katmandu', '', '+05:45', '+05:45', 'Link to Asia/Kathmandu'),
('IN', '+2232+08822', 'Asia/Kolkata', '', '+05:30', '+05:30', 'Note: Different zones in history, see Time in India.'),
('RU', '+5601+09250', 'Asia/Krasnoyarsk', 'Moscow+04 - Yenisei River', '+08:00', '+08:00', ''),
('MY', '+0310+10142', 'Asia/Kuala_Lumpur', 'peninsular Malaysia', '+08:00', '+08:00', ''),
('MY', '+0133+11020', 'Asia/Kuching', 'Sabah & Sarawak', '+08:00', '+08:00', ''),
('KW', '+2920+04759', 'Asia/Kuwait', '', '+03:00', '+03:00', ''),
('', '', 'Asia/Macao', '', '+08:00', '+08:00', 'Link to Asia/Macau'),
('MO', '+2214+11335', 'Asia/Macau', '', '+08:00', '+08:00', ''),
('RU', '+5934+15048', 'Asia/Magadan', 'Moscow+08 - Magadan', '+12:00', '+12:00', ''),
('ID', '-0507+11924', 'Asia/Makassar', 'east & south Borneo, Sulawesi (Celebes), Bali, Nusa Tenggara, west Timor', '+08:00', '+08:00', ''),
('PH', '+1435+12100', 'Asia/Manila', '', '+08:00', '+08:00', ''),
('OM', '+2336+05835', 'Asia/Muscat', '', '+04:00', '+04:00', ''),
('CY', '+3510+03322', 'Asia/Nicosia', '', '+02:00', '+03:00', ''),
('RU', '+5345+08707', 'Asia/Novokuznetsk', 'Moscow+03 - Novokuznetsk', '+07:00', '+07:00', ''),
('RU', '+5502+08255', 'Asia/Novosibirsk', 'Moscow+03 - Novosibirsk', '+07:00', '+07:00', ''),
('RU', '+5500+07324', 'Asia/Omsk', 'Moscow+03 - west Siberia', '+07:00', '+07:00', ''),
('KZ', '+5113+05121', 'Asia/Oral', 'West Kazakhstan', '+05:00', '+05:00', ''),
('KH', '+1133+10455', 'Asia/Phnom_Penh', '', '+07:00', '+07:00', ''),
('ID', '-0002+10920', 'Asia/Pontianak', 'west & central Borneo', '+07:00', '+07:00', ''),
('KP', '+3901+12545', 'Asia/Pyongyang', '', '+09:00', '+09:00', ''),
('QA', '+2517+05132', 'Asia/Qatar', '', '+03:00', '+03:00', ''),
('KZ', '+4448+06528', 'Asia/Qyzylorda', 'Qyzylorda (Kyzylorda, Kzyl-Orda)', '+06:00', '+06:00', ''),
('MM', '+1647+09610', 'Asia/Rangoon', '', '+06:30', '+06:30', ''),
('SA', '+2438+04643', 'Asia/Riyadh', '', '+03:00', '+03:00', ''),
('', '', 'Asia/Saigon', '', '+07:00', '+07:00', 'Link to Asia/Ho_Chi_Minh'),
('RU', '+4658+14242', 'Asia/Sakhalin', 'Moscow+07 - Sakhalin Island', '+11:00', '+11:00', ''),
('UZ', '+3940+06648', 'Asia/Samarkand', 'west Uzbekistan', '+05:00', '+05:00', ''),
('KR', '+3733+12658', 'Asia/Seoul', '', '+09:00', '+09:00', ''),
('CN', '+3114+12128', 'Asia/Shanghai', 'east China - Beijing, Guangdong, Shanghai, etc.', '+08:00', '+08:00', 'Covering historic Chungyuan time zone.'),
('SG', '+0117+10351', 'Asia/Singapore', '', '+08:00', '+08:00', ''),
('TW', '+2503+12130', 'Asia/Taipei', '', '+08:00', '+08:00', ''),
('UZ', '+4120+06918', 'Asia/Tashkent', 'east Uzbekistan', '+05:00', '+05:00', ''),
('GE', '+4143+04449', 'Asia/Tbilisi', '', '+04:00', '+04:00', ''),
('IR', '+3540+05126', 'Asia/Tehran', '', '+03:30', '+04:30', ''),
('', '', 'Asia/Tel_Aviv', '', '+02:00', '+03:00', 'Link to Asia/Jerusalem'),
('', '', 'Asia/Thimbu', '', '+06:00', '+06:00', 'Link to Asia/Thimphu'),
('BT', '+2728+08939', 'Asia/Thimphu', '', '+06:00', '+06:00', ''),
('JP', '+353916+1394441', 'Asia/Tokyo', '', '+09:00', '+09:00', ''),
('', '', 'Asia/Ujung_Pandang', '', '+08:00', '+08:00', 'Link to Asia/Makassar'),
('MN', '+4755+10653', 'Asia/Ulaanbaatar', 'most locations', '+08:00', '+08:00', ''),
('', '', 'Asia/Ulan_Bator', '', '+08:00', '+08:00', 'Link to Asia/Ulaanbaatar'),
('CN', '+4348+08735', 'Asia/Urumqi', 'most of Tibet & Xinjiang', '+08:00', '+08:00', 'Covering historic Sinkiang-Tibet time zone.'),
('LA', '+1758+10236', 'Asia/Vientiane', '', '+07:00', '+07:00', ''),
('RU', '+4310+13156', 'Asia/Vladivostok', 'Moscow+07 - Amur River', '+11:00', '+11:00', ''),
('RU', '+6200+12940', 'Asia/Yakutsk', 'Moscow+06 - Lena River', '+10:00', '+10:00', ''),
('RU', '+5651+06036', 'Asia/Yekaterinburg', 'Moscow+02 - Urals', '+06:00', '+06:00', ''),
('AM', '+4011+04430', 'Asia/Yerevan', '', '+04:00', '+04:00', ''),
('PT', '+3744-02540', 'Atlantic/Azores', 'Azores', 'âˆ’01:00', '+00:00', ''),
('BM', '+3217-06446', 'Atlantic/Bermuda', '', 'âˆ’04:00', 'âˆ’03:00', ''),
('ES', '+2806-01524', 'Atlantic/Canary', 'Canary Islands', '+00:00', '+01:00', ''),
('CV', '+1455-02331', 'Atlantic/Cape_Verde', '', 'âˆ’01:00', 'âˆ’01:00', ''),
('', '', 'Atlantic/Faeroe', '', '+00:00', '+01:00', 'Link to Atlantic/Faroe'),
('FO', '+6201-00646', 'Atlantic/Faroe', '', '+00:00', '+01:00', ''),
('', '', 'Atlantic/Jan_Mayen', '', '+01:00', '+02:00', 'Link to Europe/Oslo'),
('PT', '+3238-01654', 'Atlantic/Madeira', 'Madeira Islands', '+00:00', '+01:00', ''),
('IS', '+6409-02151', 'Atlantic/Reykjavik', '', '+00:00', '+00:00', ''),
('GS', '-5416-03632', 'Atlantic/South_Georgia', '', 'âˆ’02:00', 'âˆ’02:00', ''),
('FK', '-5142-05751', 'Atlantic/Stanley', '', 'âˆ’03:00', 'âˆ’03:00', ''),
('SH', '-1555-00542', 'Atlantic/St_Helena', '', '+00:00', '+00:00', ''),
('', '', 'Australia/ACT', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
('AU', '-3455+13835', 'Australia/Adelaide', 'South Australia', '+09:30', '+10:30', ''),
('AU', '-2728+15302', 'Australia/Brisbane', 'Queensland - most locations', '+10:00', '+10:00', ''),
('AU', '-3157+14127', 'Australia/Broken_Hill', 'New South Wales - Yancowinna', '+09:30', '+10:30', ''),
('', '', 'Australia/Canberra', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
('AU', '-3956+14352', 'Australia/Currie', 'Tasmania - King Island', '+10:00', '+11:00', ''),
('AU', '-1228+13050', 'Australia/Darwin', 'Northern Territory', '+09:30', '+09:30', ''),
('AU', '-3143+12852', 'Australia/Eucla', 'Western Australia - Eucla area', '+08:45', '+08:45', ''),
('AU', '-4253+14719', 'Australia/Hobart', 'Tasmania - most locations', '+10:00', '+11:00', ''),
('', '', 'Australia/LHI', '', '+10:30', '+11:00', 'Link to Australia/Lord_Howe'),
('AU', '-2016+14900', 'Australia/Lindeman', 'Queensland - Holiday Islands', '+10:00', '+10:00', ''),
('AU', '-3133+15905', 'Australia/Lord_Howe', 'Lord Howe Island', '+10:30', '+11:00', ''),
('AU', '-3749+14458', 'Australia/Melbourne', 'Victoria', '+10:00', '+11:00', ''),
('', '', 'Australia/North', '', '+09:30', '+09:30', 'Link to Australia/Darwin'),
('', '', 'Australia/NSW', '', '+10:00', '+11:00', 'Link to Australia/Sydney'),
('AU', '-3157+11551', 'Australia/Perth', 'Western Australia - most locations', '+08:00', '+08:00', ''),
('', '', 'Australia/Queensland', '', '+10:00', '+10:00', 'Link to Australia/Brisbane'),
('', '', 'Australia/South', '', '+09:30', '+10:30', 'Link to Australia/Adelaide'),
('AU', '-3352+15113', 'Australia/Sydney', 'New South Wales - most locations', '+10:00', '+11:00', ''),
('', '', 'Australia/Tasmania', '', '+10:00', '+11:00', 'Link to Australia/Hobart'),
('', '', 'Australia/Victoria', '', '+10:00', '+11:00', 'Link to Australia/Melbourne'),
('', '', 'Australia/West', '', '+08:00', '+08:00', 'Link to Australia/Perth'),
('', '', 'Australia/Yancowinna', '', '+09:30', '+10:30', 'Link to Australia/Broken_Hill'),
('', '', 'Brazil/Acre', '', 'âˆ’04:00', 'âˆ’04:00', 'Link to America/Rio_Branco'),
('', '', 'Brazil/DeNoronha', '', 'âˆ’02:00', 'âˆ’02:00', 'Link to America/Noronha'),
('', '', 'Brazil/East', '', 'âˆ’03:00', 'âˆ’02:00', 'Link to America/Sao_Paulo'),
('', '', 'Brazil/West', '', 'âˆ’04:00', 'âˆ’04:00', 'Link to America/Manaus'),
('', '', 'Canada/Atlantic', '', 'âˆ’04:00', 'âˆ’03:00', 'Link to America/Halifax'),
('', '', 'Canada/Central', '', 'âˆ’06:00', 'âˆ’05:00', 'Link to America/Winnipeg'),
('', '', 'Canada/East-Saskatchewan', '', 'âˆ’06:00', 'âˆ’06:00', 'Link to America/Regina'),
('', '', 'Canada/Eastern', '', 'âˆ’05:00', 'âˆ’04:00', 'Link to America/Toronto'),
('', '', 'Canada/Mountain', '', 'âˆ’07:00', 'âˆ’06:00', 'Link to America/Edmonton'),
('', '', 'Canada/Newfoundland', '', 'âˆ’03:30', 'âˆ’02:30', 'Link to America/St_Johns'),
('', '', 'Canada/Pacific', '', 'âˆ’08:00', 'âˆ’07:00', 'Link to America/Vancouver'),
('', '', 'Canada/Saskatchewan', '', 'âˆ’06:00', 'âˆ’06:00', 'Link to America/Regina'),
('', '', 'Canada/Yukon', '', 'âˆ’08:00', 'âˆ’07:00', 'Link to America/Whitehorse'),
('', '', 'CET', '', '+01:00', '+02:00', ''),
('', '', 'Chile/Continental', '', 'âˆ’04:00', 'âˆ’03:00', 'Link to America/Santiago'),
('', '', 'Chile/EasterIsland', '', 'âˆ’06:00', 'âˆ’05:00', 'Link to Pacific/Easter'),
('', '', 'CST6CDT', '', 'âˆ’06:00', 'âˆ’05:00', ''),
('', '', 'Cuba', '', 'âˆ’05:00', 'âˆ’04:00', 'Link to America/Havana'),
('', '', 'EET', '', '+02:00', '+03:00', ''),
('', '', 'Egypt', '', '+02:00', '+02:00', 'Link to Africa/Cairo'),
('', '', 'Eire', '', '+00:00', '+01:00', 'Link to Europe/Dublin'),
('', '', 'EST', '', 'âˆ’05:00', 'âˆ’05:00', ''),
('', '', 'EST5EDT', '', 'âˆ’05:00', 'âˆ’04:00', ''),
('', '', 'Etc./GMT', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'Etc./GMT+0', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'Etc./UCT', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'Etc./Universal', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'Etc./UTC', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'Etc./Zulu', '', '+00:00', '+00:00', 'Link to UTC'),
('NL', '+5222+00454', 'Europe/Amsterdam', '', '+01:00', '+02:00', ''),
('AD', '+4230+00131', 'Europe/Andorra', '', '+01:00', '+02:00', ''),
('GR', '+3758+02343', 'Europe/Athens', '', '+02:00', '+03:00', ''),
('', '', 'Europe/Belfast', '', '+00:00', '+01:00', 'Link to Europe/London'),
('RS', '+4450+02030', 'Europe/Belgrade', '', '+01:00', '+02:00', ''),
('DE', '+5230+01322', 'Europe/Berlin', '', '+01:00', '+02:00', 'In 1945, the Trizone did not follow Berlin\'s switch to DST, see Time in Germany'),
('SK', '+4809+01707', 'Europe/Bratislava', '', '+01:00', '+02:00', 'Link to Europe/Prague'),
('BE', '+5050+00420', 'Europe/Brussels', '', '+01:00', '+02:00', ''),
('RO', '+4426+02606', 'Europe/Bucharest', '', '+02:00', '+03:00', ''),
('HU', '+4730+01905', 'Europe/Budapest', '', '+01:00', '+02:00', ''),
('MD', '+4700+02850', 'Europe/Chisinau', '', '+02:00', '+03:00', ''),
('DK', '+5540+01235', 'Europe/Copenhagen', '', '+01:00', '+02:00', ''),
('IE', '+5320-00615', 'Europe/Dublin', '', '+00:00', '+01:00', ''),
('GI', '+3608-00521', 'Europe/Gibraltar', '', '+01:00', '+02:00', ''),
('GG', '+4927-00232', 'Europe/Guernsey', '', '+00:00', '+01:00', 'Link to Europe/London'),
('FI', '+6010+02458', 'Europe/Helsinki', '', '+02:00', '+03:00', ''),
('IM', '+5409-00428', 'Europe/Isle_of_Man', '', '+00:00', '+01:00', 'Link to Europe/London'),
('TR', '+4101+02858', 'Europe/Istanbul', '', '+02:00', '+03:00', ''),
('JE', '+4912-00207', 'Europe/Jersey', '', '+00:00', '+01:00', 'Link to Europe/London'),
('RU', '+5443+02030', 'Europe/Kaliningrad', 'Moscow-01 - Kaliningrad', '+03:00', '+03:00', ''),
('UA', '+5026+03031', 'Europe/Kiev', 'most locations', '+02:00', '+03:00', ''),
('PT', '+3843-00908', 'Europe/Lisbon', 'mainland', '+00:00', '+01:00', ''),
('SI', '+4603+01431', 'Europe/Ljubljana', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
('GB', '+513030-0000731', 'Europe/London', '', '+00:00', '+01:00', ''),
('LU', '+4936+00609', 'Europe/Luxembourg', '', '+01:00', '+02:00', ''),
('ES', '+4024-00341', 'Europe/Madrid', 'mainland', '+01:00', '+02:00', ''),
('MT', '+3554+01431', 'Europe/Malta', '', '+01:00', '+02:00', ''),
('AX', '+6006+01957', 'Europe/Mariehamn', '', '+02:00', '+03:00', 'Link to Europe/Helsinki'),
('BY', '+5354+02734', 'Europe/Minsk', '', '+03:00', '+03:00', ''),
('MC', '+4342+00723', 'Europe/Monaco', '', '+01:00', '+02:00', ''),
('RU', '+5545+03735', 'Europe/Moscow', 'Moscow+00 - west Russia', '+04:00', '+04:00', ''),
('', '', 'Europe/Nicosia', '', '+02:00', '+03:00', 'Link to Asia/Nicosia'),
('NO', '+5955+01045', 'Europe/Oslo', '', '+01:00', '+02:00', ''),
('FR', '+4852+00220', 'Europe/Paris', '', '+01:00', '+02:00', ''),
('ME', '+4226+01916', 'Europe/Podgorica', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
('CZ', '+5005+01426', 'Europe/Prague', '', '+01:00', '+02:00', ''),
('LV', '+5657+02406', 'Europe/Riga', '', '+02:00', '+03:00', ''),
('IT', '+4154+01229', 'Europe/Rome', '', '+01:00', '+02:00', ''),
('RU', '+5312+05009', 'Europe/Samara', 'Moscow+00 - Samara, Udmurtia', '+04:00', '+04:00', ''),
('SM', '+4355+01228', 'Europe/San_Marino', '', '+01:00', '+02:00', 'Link to Europe/Rome'),
('BA', '+4352+01825', 'Europe/Sarajevo', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
('UA', '+4457+03406', 'Europe/Simferopol', 'central Crimea', '+02:00', '+03:00', ''),
('MK', '+4159+02126', 'Europe/Skopje', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
('BG', '+4241+02319', 'Europe/Sofia', '', '+02:00', '+03:00', ''),
('SE', '+5920+01803', 'Europe/Stockholm', '', '+01:00', '+02:00', ''),
('EE', '+5925+02445', 'Europe/Tallinn', '', '+02:00', '+03:00', ''),
('AL', '+4120+01950', 'Europe/Tirane', '', '+01:00', '+02:00', ''),
('', '', 'Europe/Tiraspol', '', '+02:00', '+03:00', 'Link to Europe/Chisinau'),
('UA', '+4837+02218', 'Europe/Uzhgorod', 'Ruthenia', '+02:00', '+03:00', ''),
('LI', '+4709+00931', 'Europe/Vaduz', '', '+01:00', '+02:00', ''),
('VA', '+415408+0122711', 'Europe/Vatican', '', '+01:00', '+02:00', 'Link to Europe/Rome'),
('AT', '+4813+01620', 'Europe/Vienna', '', '+01:00', '+02:00', ''),
('LT', '+5441+02519', 'Europe/Vilnius', '', '+02:00', '+03:00', ''),
('RU', '+4844+04425', 'Europe/Volgograd', 'Moscow+00 - Caspian Sea', '+04:00', '+04:00', ''),
('PL', '+5215+02100', 'Europe/Warsaw', '', '+01:00', '+02:00', ''),
('HR', '+4548+01558', 'Europe/Zagreb', '', '+01:00', '+02:00', 'Link to Europe/Belgrade'),
('UA', '+4750+03510', 'Europe/Zaporozhye', 'Zaporozh\'ye, E Lugansk / Zaporizhia, E Luhansk', '+02:00', '+03:00', ''),
('CH', '+4723+00832', 'Europe/Zurich', '', '+01:00', '+02:00', ''),
('', '', 'GB', '', '+00:00', '+01:00', 'Link to Europe/London'),
('', '', 'GB-Eire', '', '+00:00', '+01:00', 'Link to Europe/London'),
('', '', 'GMT', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'GMT+0', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'GMT-0', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'GMT0', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'Greenwich', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'Hong Kong', '', '+08:00', '+08:00', 'Link to Asia/Hong_Kong'),
('', '', 'HST', '', 'âˆ’10:00', 'âˆ’10:00', ''),
('', '', 'Iceland', '', '+00:00', '+00:00', 'Link to Atlantic/Reykjavik'),
('MG', '-1855+04731', 'Indian/Antananarivo', '', '+03:00', '+03:00', ''),
('IO', '-0720+07225', 'Indian/Chagos', '', '+06:00', '+06:00', ''),
('CX', '-1025+10543', 'Indian/Christmas', '', '+07:00', '+07:00', ''),
('CC', '-1210+09655', 'Indian/Cocos', '', '+06:30', '+06:30', ''),
('KM', '-1141+04316', 'Indian/Comoro', '', '+03:00', '+03:00', ''),
('TF', '-492110+0701303', 'Indian/Kerguelen', '', '+05:00', '+05:00', ''),
('SC', '-0440+05528', 'Indian/Mahe', '', '+04:00', '+04:00', ''),
('MV', '+0410+07330', 'Indian/Maldives', '', '+05:00', '+05:00', ''),
('MU', '-2010+05730', 'Indian/Mauritius', '', '+04:00', '+04:00', ''),
('YT', '-1247+04514', 'Indian/Mayotte', '', '+03:00', '+03:00', ''),
('RE', '-2052+05528', 'Indian/Reunion', '', '+04:00', '+04:00', ''),
('', '', 'Iran', '', '+03:30', '+04:30', 'Link to Asia/Tehran'),
('', '', 'Israel', '', '+02:00', '+03:00', 'Link to Asia/Jerusalem'),
('', '', 'Jamaica', '', 'âˆ’05:00', 'âˆ’05:00', 'Link to America/Jamaica'),
('', '', 'Japan', '', '+09:00', '+09:00', 'Link to Asia/Tokyo'),
('', '', 'JST-9', '', '+09:00', '+09:00', 'Link to Asia/Tokyo'),
('', '', 'Kwajalein', '', '+12:00', '+12:00', 'Link to Pacific/Kwajalein'),
('', '', 'Libya', '', '+02:00', '+02:00', 'Link to Africa/Tripoli'),
('', '', 'MET', '', '+01:00', '+02:00', ''),
('', '', 'Mexico/BajaNorte', '', 'âˆ’08:00', 'âˆ’07:00', 'Link to America/Tijuana'),
('', '', 'Mexico/BajaSur', '', 'âˆ’07:00', 'âˆ’06:00', 'Link to America/Mazatlan'),
('', '', 'Mexico/General', '', 'âˆ’06:00', 'âˆ’05:00', 'Link to America/Mexico_City'),
('', '', 'MST', '', 'âˆ’07:00', 'âˆ’07:00', ''),
('', '', 'MST7MDT', '', 'âˆ’07:00', 'âˆ’06:00', ''),
('', '', 'Navajo', '', 'âˆ’07:00', 'âˆ’06:00', 'Link to America/Denver'),
('', '', 'NZ', '', '+12:00', '+13:00', 'Link to Pacific/Auckland'),
('', '', 'NZ-CHAT', '', '+12:45', '+13:45', 'Link to Pacific/Chatham'),
('WS', '-1350-17144', 'Pacific/Apia', '', '+13:00', '+14:00', ''),
('NZ', '-3652+17446', 'Pacific/Auckland', 'most locations', '+12:00', '+13:00', ''),
('NZ', '-4357-17633', 'Pacific/Chatham', 'Chatham Islands', '+12:45', '+13:45', ''),
('FM', '+0725+15147', 'Pacific/Chuuk', 'Chuuk (Truk) and Yap', '+10:00', '+10:00', ''),
('CL', '-2709-10926', 'Pacific/Easter', 'Easter Island & Sala y Gomez', 'âˆ’06:00', 'âˆ’05:00', ''),
('VU', '-1740+16825', 'Pacific/Efate', '', '+11:00', '+11:00', ''),
('KI', '-0308-17105', 'Pacific/Enderbury', 'Phoenix Islands', '+13:00', '+13:00', ''),
('TK', '-0922-17114', 'Pacific/Fakaofo', '', '+13:00', '+13:00', ''),
('FJ', '-1808+17825', 'Pacific/Fiji', '', '+12:00', '+13:00', ''),
('TV', '-0831+17913', 'Pacific/Funafuti', '', '+12:00', '+12:00', ''),
('EC', '-0054-08936', 'Pacific/Galapagos', 'Galapagos Islands', 'âˆ’06:00', 'âˆ’06:00', ''),
('PF', '-2308-13457', 'Pacific/Gambier', 'Gambier Islands', 'âˆ’09:00', 'âˆ’09:00', ''),
('SB', '-0932+16012', 'Pacific/Guadalcanal', '', '+11:00', '+11:00', ''),
('GU', '+1328+14445', 'Pacific/Guam', '', '+10:00', '+10:00', ''),
('US', '+211825-1575130', 'Pacific/Honolulu', 'Hawaii', 'âˆ’10:00', 'âˆ’10:00', ''),
('UM', '+1645-16931', 'Pacific/Johnston', 'Johnston Atoll', 'âˆ’10:00', 'âˆ’10:00', ''),
('KI', '+0152-15720', 'Pacific/Kiritimati', 'Line Islands', '+14:00', '+14:00', ''),
('FM', '+0519+16259', 'Pacific/Kosrae', 'Kosrae', '+11:00', '+11:00', ''),
('MH', '+0905+16720', 'Pacific/Kwajalein', 'Kwajalein', '+12:00', '+12:00', ''),
('MH', '+0709+17112', 'Pacific/Majuro', 'most locations', '+12:00', '+12:00', ''),
('PF', '-0900-13930', 'Pacific/Marquesas', 'Marquesas Islands', 'âˆ’09:30', 'âˆ’09:30', ''),
('UM', '+2813-17722', 'Pacific/Midway', 'Midway Islands', 'âˆ’11:00', 'âˆ’11:00', ''),
('NR', '-0031+16655', 'Pacific/Nauru', '', '+12:00', '+12:00', ''),
('NU', '-1901-16955', 'Pacific/Niue', '', 'âˆ’11:00', 'âˆ’11:00', ''),
('NF', '-2903+16758', 'Pacific/Norfolk', '', '+11:30', '+11:30', ''),
('NC', '-2216+16627', 'Pacific/Noumea', '', '+11:00', '+11:00', ''),
('AS', '-1416-17042', 'Pacific/Pago_Pago', '', 'âˆ’11:00', 'âˆ’11:00', ''),
('PW', '+0720+13429', 'Pacific/Palau', '', '+09:00', '+09:00', ''),
('PN', '-2504-13005', 'Pacific/Pitcairn', '', 'âˆ’08:00', 'âˆ’08:00', ''),
('FM', '+0658+15813', 'Pacific/Pohnpei', 'Pohnpei (Ponape)', '+11:00', '+11:00', ''),
('', '', 'Pacific/Ponape', '', '+11:00', '+11:00', 'Link to Pacific/Pohnpei'),
('PG', '-0930+14710', 'Pacific/Port_Moresby', '', '+10:00', '+10:00', ''),
('CK', '-2114-15946', 'Pacific/Rarotonga', '', 'âˆ’10:00', 'âˆ’10:00', ''),
('MP', '+1512+14545', 'Pacific/Saipan', '', '+10:00', '+10:00', ''),
('', '', 'Pacific/Samoa', '', 'âˆ’11:00', 'âˆ’11:00', 'Link to Pacific/Pago_Pago'),
('PF', '-1732-14934', 'Pacific/Tahiti', 'Society Islands', 'âˆ’10:00', 'âˆ’10:00', ''),
('KI', '+0125+17300', 'Pacific/Tarawa', 'Gilbert Islands', '+12:00', '+12:00', ''),
('TO', '-2110-17510', 'Pacific/Tongatapu', '', '+13:00', '+13:00', ''),
('', '', 'Pacific/Truk', '', '+10:00', '+10:00', 'Link to Pacific/Chuuk'),
('UM', '+1917+16637', 'Pacific/Wake', 'Wake Island', '+12:00', '+12:00', ''),
('WF', '-1318-17610', 'Pacific/Wallis', '', '+12:00', '+12:00', ''),
('', '', 'Pacific/Yap', '', '+10:00', '+10:00', 'Link to Pacific/Chuuk'),
('', '', 'Poland', '', '+01:00', '+02:00', 'Link to Europe/Warsaw'),
('', '', 'Portugal', '', '+00:00', '+01:00', 'Link to Europe/Lisbon'),
('', '', 'PRC', '', '+08:00', '+08:00', 'Link to Asia/Shanghai'),
('', '', 'PST8PDT', '', 'âˆ’08:00', 'âˆ’07:00', ''),
('', '', 'ROC', '', '+08:00', '+08:00', 'Link to Asia/Taipei'),
('', '', 'ROK', '', '+09:00', '+09:00', 'Link to Asia/Seoul'),
('', '', 'Singapore', '', '+08:00', '+08:00', 'Link to Asia/Singapore'),
('', '', 'Turkey', '', '+02:00', '+03:00', 'Link to Europe/Istanbul'),
('', '', 'UCT', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'Universal', '', '+00:00', '+00:00', 'Link to UTC'),
('', '', 'US/Alaska', '', 'âˆ’09:00', 'âˆ’08:00', 'Link to America/Anchorage'),
('', '', 'US/Aleutian', '', 'âˆ’10:00', 'âˆ’09:00', 'Link to America/Adak'),
('', '', 'US/Arizona', '', 'âˆ’07:00', 'âˆ’07:00', 'Link to America/Phoenix'),
('', '', 'US/Central', '', 'âˆ’06:00', 'âˆ’05:00', 'Link to America/Chicago'),
('', '', 'US/East-Indiana', '', 'âˆ’05:00', 'âˆ’04:00', 'Link to America/Indiana/Indianapolis'),
('', '', 'US/Eastern', '', 'âˆ’05:00', 'âˆ’04:00', 'Link to America/New_York'),
('', '', 'US/Hawaii', '', 'âˆ’10:00', 'âˆ’10:00', 'Link to Pacific/Honolulu'),
('', '', 'US/Indiana-Starke', '', 'âˆ’06:00', 'âˆ’05:00', 'Link to America/Indiana/Knox'),
('', '', 'US/Michigan', '', 'âˆ’05:00', 'âˆ’04:00', 'Link to America/Detroit'),
('', '', 'US/Mountain', '', 'âˆ’07:00', 'âˆ’06:00', 'Link to America/Denver'),
('', '', 'US/Pacific', '', 'âˆ’08:00', 'âˆ’07:00', 'Link to America/Los_Angeles'),
('', '', 'US/Pacific-New', '', 'âˆ’08:00', 'âˆ’07:00', 'Link to America/Los_Angeles'),
('', '', 'US/Samoa', '', 'âˆ’11:00', 'âˆ’11:00', 'Link to Pacific/Pago_Pago'),
('', '', 'UTC', '', '+00:00', '+00:00', ''),
('', '', 'W-SU', '', '+04:00', '+04:00', 'Link to Europe/Moscow'),
('', '', 'WET', '', '+00:00', '+01:00', ''),
('', '', 'Zulu', '', '+00:00', '+00:00', 'Link to UTC');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `device_token` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'defaultuser.png',
  `phone` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bio` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `org_id` int(11) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT 1,
  `language` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `first_name`, `last_name`, `email`, `email_verified_at`, `password`, `remember_token`, `device_token`, `image`, `phone`, `bio`, `country`, `org_id`, `status`, `language`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 'admin', 'admin@admin.com', NULL, '$2y$10$l84Xfp4x8hyNPwK.RqBzRuNbItCJXf7FRzHoJ.WU/VIwLQY4rNDuu', 'vsErv3yZwfFW6px6HtDssTe5hoauSnT5HuVMXj02CUnoVW9SiZmBgb4SnVs9', NULL, 'defaultuser.png', NULL, NULL, NULL, NULL, 1, NULL, '2020-12-14 18:30:00', '2021-02-22 22:20:32');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_user`
--
ALTER TABLE `app_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `banner`
--
ALTER TABLE `banner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `currency`
--
ALTER TABLE `currency`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `event_report`
--
ALTER TABLE `event_report`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `general_settng`
--
ALTER TABLE `general_settng`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `language`
--
ALTER TABLE `language`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `location`
--
ALTER TABLE `location`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_id_model_type_index` (`model_id`,`model_type`);

--
-- Indexes for table `notification`
--
ALTER TABLE `notification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notification_template`
--
ALTER TABLE `notification_template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_auth_codes_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indexes for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_child`
--
ALTER TABLE `order_child`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_tax`
--
ALTER TABLE `order_tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `payment_setting`
--
ALTER TABLE `payment_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `review`
--
ALTER TABLE `review`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `settlement`
--
ALTER TABLE `settlement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tax`
--
ALTER TABLE `tax`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `timezones`
--
ALTER TABLE `timezones`
  ADD PRIMARY KEY (`TimeZone`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_user`
--
ALTER TABLE `app_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `banner`
--
ALTER TABLE `banner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `currency`
--
ALTER TABLE `currency`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=132;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `event_report`
--
ALTER TABLE `event_report`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_settng`
--
ALTER TABLE `general_settng`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `language`
--
ALTER TABLE `language`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `location`
--
ALTER TABLE `location`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `notification`
--
ALTER TABLE `notification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `notification_template`
--
ALTER TABLE `notification_template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_child`
--
ALTER TABLE `order_child`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_tax`
--
ALTER TABLE `order_tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_setting`
--
ALTER TABLE `payment_setting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=66;

--
-- AUTO_INCREMENT for table `review`
--
ALTER TABLE `review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `settlement`
--
ALTER TABLE `settlement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tax`
--
ALTER TABLE `tax`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=76;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
