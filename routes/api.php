<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;
use App\Http\Controllers\OrganizationApiController;
use App\Http\Controllers\ScannerApiController;
use App\Http\Controllers\BoatsController;
use App\Http\Controllers\EventController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/user/login',[ApiController::class, 'userLogin']);
Route::post('/user/send-otp',[ApiController::class, 'sendOTP']);
Route::post('/user/register-step1',[ApiController::class, 'userRegister']);
Route::post('/user/register-step2',[ApiController::class, 'userRegisterStep2']);
Route::post('/user/forgot-password',[ApiController::class, 'forgotPassword']);
Route::get('/user/setting',[ApiController::class, 'allSetting']);
Route::post('/user/events',[ApiController::class, 'events']);
Route::get('/user/event-detail/{id}',[ApiController::class, 'eventDetail']);
Route::get('/user/ticket-detail/{id}',[ApiController::class, 'ticketDetail']);
Route::get('/user/event-tickets/{id}',[ApiController::class, 'eventTickets']);
Route::post('/user/event-from-category',[ApiController::class, 'EventFrmCategory']);
Route::post('/user/search-free-event',[ApiController::class, 'searchFreeEvent']);
Route::post('/user/report-event',[ApiController::class, 'reportEvent']);
Route::get('/user/all-coupon',[ApiController::class, 'allCoupon']);   
Route::post('/user/search-event',[ApiController::class, 'searchEvent']); 
Route::get('/user/category',[ApiController::class, 'category']);
Route::post('/user/home',[ApiController::class, 'homepage']);
Route::post('/user/listing',[ApiController::class, 'eventsListing']);
Route::get('/event-locations',[ApiController::class, 'eventsLocations']);

Route::group(['prefix' => 'user','middleware' => ['auth:userApi']], function () {

    Route::get('/organization',[ApiController::class, 'organization']);       
    Route::get('/organization-detail/{id}',[ApiController::class, 'organizationDetail']);    
    Route::get('/category-event',[ApiController::class, 'categoryEvent']);
    Route::get('/profile',[ApiController::class, 'userProfile']);
    Route::get('/user-likes',[ApiController::class, 'userLikes']);
    Route::get('/user-following',[ApiController::class, 'userFollowing']);
    Route::post('/update/profile',[ApiController::class, 'editUserProfile']);
    Route::post('/change-profile-image',[ApiController::class, 'editImage']);
    Route::post('/add-favorite',[ApiController::class, 'addFavorite']);
    Route::get('/list-favorite',[ApiController::class, 'listFavorite']);
    Route::post('/add-following-list',[ApiController::class, 'addFollowing']);
    Route::post('/check-code',[ApiController::class, 'checkCode']);
    Route::get('/user-notification',[ApiController::class, 'userNotification']);   
    Route::get('/order-tax/{event_id}',[ApiController::class, 'orderTax']); 

    Route::post('/create-order',[ApiController::class, 'createOrder']);

    Route::post('/splitpayments',[ApiController::class, 'splitpayments']); 

    Route::post('/startchat',[ApiController::class, 'startchat']);

    Route::post('/storechat',[ApiController::class, 'storechat']); 

    Route::get('/mychatlist',[ApiController::class, 'mychatlist']); 

    Route::get('/chatmessage',[ApiController::class, 'chatmessage']); 
    
    Route::get('/user-order',[ApiController::class, 'userOrder']);   
    Route::get('/view-all-tickets',[ApiController::class, 'viewUserOrder']);              
    Route::get('/view-single-order/{id}',[ApiController::class, 'viewSingleOrder']); 
    Route::post('/add-review',[ApiController::class, 'addReview']); 
    Route::post('/change-password',[ApiController::class, 'changePassword']); 
    Route::get('/clear-notification',[ApiController::class, 'clearNotification']); 
    Route::post('/logout',[ApiController::class, 'logout']);
    Route::get('/dashboard',[ApiController::class, 'dashboard']);
    Route::post('/order-status',[ApiController::class, 'orderStatus']);
});
Route::get('/event-dates/{id}',[ApiController::class, 'eventAvailableDates']);
Route::get('/event-cancellation-policies/{id}',[ApiController::class, 'eventCancellationPolicies']);
Route::get('/country-codes/{type}',[ApiController::class, 'countryCodes']);
Route::get('/split-payment-commission',[OrganizationApiController::class, 'splitPaymentCommission']);
Route::get('/pending-payment-links/{order_id}',[ApiController::class, 'pendingPaymentLinks']);

// organization
Route::post('/organization/login',[OrganizationApiController::class, 'organizationLogin']);
Route::post('/organization/app/send_login_otp',[OrganizationApiController::class, 'organizationAppSendLoginOtp']);
Route::post('/organization/app/login',[OrganizationApiController::class, 'organizationAppLogin']);
Route::post('/organization/forget-password',[OrganizationApiController::class, 'forgetPassword']);
Route::post('/organization/register',[OrganizationApiController::class, 'organizationRegister']);
Route::post('/organization/app/register',[OrganizationApiController::class, 'organizationAppRegister']);

Route::get('/organization/getcountry',[OrganizationApiController::class, 'getcountry']);

Route::get('/organization/setting',[OrganizationApiController::class, 'organizationSetting']);

Route::group(['prefix' => 'organization','middleware' => ['auth:api']], function () {
    Route::post('/set-profile',[OrganizationApiController::class, 'setProfile']);
    Route::get('/profile',[OrganizationApiController::class, 'profile']);
    Route::get('/all-events',[OrganizationApiController::class, 'events']);    
    Route::get('/search-events',[OrganizationApiController::class, 'searchEvents']);
    Route::get('/all-scanner',[OrganizationApiController::class, 'scanner']);
    Route::post('/add-scanner',[OrganizationApiController::class, 'addScanner']);
    Route::post('/add-event',[OrganizationApiController::class, 'addEvent']);  
    Route::post('/edit-event',[OrganizationApiController::class, 'editEvent']);  
    Route::get('/cancel-event/{id}',[OrganizationApiController::class, 'cancelEvent']); 
    Route::get('/delete-event/{id}',[OrganizationApiController::class, 'deleteEvent']);  
    Route::get('/eventDetail/{id}',[OrganizationApiController::class, 'eventDetail']);  
    Route::get('/event-guestList/{id}',[OrganizationApiController::class, 'eventGuestList']);  
    Route::get('/orderDetail/{id}',[OrganizationApiController::class, 'orderDetail']);    
    Route::get('/event-tickets/{id}',[OrganizationApiController::class, 'eventTickets']);  
    Route::post('/add-ticket',[OrganizationApiController::class, 'addTicket']);
    Route::post('/edit-ticket',[OrganizationApiController::class, 'editTicket']); 
    Route::get('/delete-ticket/{id}',[OrganizationApiController::class, 'deleteTicket']);  
    Route::get('/ticketDetail/{id}',[OrganizationApiController::class, 'ticketDetail']);      

    Route::post('/storechat',[OrganizationApiController::class, 'storechat']); 

    Route::get('/mychatlist',[OrganizationApiController::class, 'mychatlist']); 

    Route::get('/chatmessage',[OrganizationApiController::class, 'chatmessage']); 

    Route::get('/gettaxs',[OrganizationApiController::class, 'gettaxs']); 
    
    Route::get('/category',[OrganizationApiController::class, 'allCategory']);  
    Route::get('/view-tax',[OrganizationApiController::class, 'viewTax']);  
    Route::post('/add-tax',[OrganizationApiController::class, 'addTax']);  
    Route::post('/edit-tax',[OrganizationApiController::class, 'editTax']);  
    Route::get('/delete-tax/{id}',[OrganizationApiController::class, 'deleteTax']);  
    Route::get('/change-status-tax/{id}/{status}',[OrganizationApiController::class, 'changeStatusTax']);      
    Route::get('/taxDetail/{id}',[OrganizationApiController::class, 'taxDetail']);      
    Route::get('/faq',[OrganizationApiController::class, 'viewFaq']);  
    Route::post('/add-feedback',[OrganizationApiController::class, 'addFeedback']);  
    Route::post('/change-password',[OrganizationApiController::class, 'changePassword']);  
    Route::post('/edit-profile',[OrganizationApiController::class, 'editProfile']);  
    Route::get('/followers',[OrganizationApiController::class, 'followers']);  
    Route::get('/notifications',[OrganizationApiController::class, 'notifications']);  
    Route::get('/order-delete/{id}',[OrganizationApiController::class, 'deleteOrder']);  
    Route::post('/change-profile-image',[OrganizationApiController::class, 'editImage']);
    Route::get('/coupon-event',[OrganizationApiController::class, 'couponEvent']);
    Route::get('/coupons',[OrganizationApiController::class, 'coupons']);
    Route::post('/add-coupon',[OrganizationApiController::class, 'addCoupon']);
    Route::post('/edit-coupon',[OrganizationApiController::class, 'editCoupon']);
    Route::get('/couponDetail/{id}',[OrganizationApiController::class, 'couponDetail']);
    Route::get('/delete-coupon/{id}',[OrganizationApiController::class, 'deleteCoupon']);
    Route::get('/clear-notification',[OrganizationApiController::class, 'clearNotification']);
    Route::post('/remove-gallery',[OrganizationApiController::class, 'removeGalleryImage']);

    Route::get('/prerequisites',[OrganizationApiController::class, 'prerequisites']);
    Route::post('/add/boat-details',[BoatsController::class, 'store']);
    Route::post('/update/service-images',[BoatsController::class, 'updateImages']);
    Route::post('/add/games-events-details',[EventController::class, 'store_app']);
    Route::get('/dashboard',[OrganizationApiController::class, 'dashboard']);
    Route::get('/listing',[OrganizationApiController::class, 'listing']);
    Route::get('/bookings',[OrganizationApiController::class, 'bookings']);
    Route::post('/add/ticket',[OrganizationApiController::class, 'addTickets']);
    Route::get('/list/tickets',[OrganizationApiController::class, 'listTickets']);
    Route::get('/get/ticket-details',[OrganizationApiController::class, 'listTicketDetails']);
    Route::post('/update/ticket-details',[OrganizationApiController::class, 'updateTicketDetails']);
    Route::post('/update/tax',[OrganizationApiController::class, 'updateOrganisationTax']);
    Route::post('/update/profile',[OrganizationApiController::class, 'profileUpdate']);
    Route::post('/update/bank',[OrganizationApiController::class, 'updateBankAccounts']);

    Route::get('/bank/details',[OrganizationApiController::class, 'getBankDetails']);
    
    Route::post('/update/bank-details',[OrganizationApiController::class, 'updateBankDetails']);
    Route::post('/update/tax_form',[OrganizationApiController::class, 'updateTaxForms']);
    Route::post('/update/ticket/status',[OrganizationApiController::class, 'updateTicketStatus']);
    Route::post('/remove/image',[OrganizationApiController::class, 'removeEventImage']);
    Route::post('/logout',[OrganizationApiController::class, 'logout']);

    Route::get('/get/boat_make',[OrganizationApiController::class, 'boat_make']);
    Route::get('/get/boat_category',[OrganizationApiController::class, 'boat_category']);
    Route::get('/get/boat_model',[OrganizationApiController::class, 'boat_model']);
});
Route::get('/event-availability',[OrganizationApiController::class, 'eventAvailability']);
Route::get('/get/terms-privacy',[OrganizationApiController::class, 'termsPrivacyLinks']);

// scanner

Route::post('/scanner/login',[ScannerApiController::class, 'scannerLogin']);
Route::post('/scanner/forget-password',[ScannerApiController::class, 'forgetPassword']);
Route::get('/scanner/setting',[ScannerApiController::class, 'scannerSetting']);
Route::group(['prefix' => 'scanner','middleware' => ['auth:api']], function () {
    Route::get('/events',[ScannerApiController::class, 'events']);
    Route::get('/event-detail/{id}',[ScannerApiController::class, 'eventDetail']);
    Route::get('/event-users/{id}',[ScannerApiController::class, 'eventUsers']);
    Route::get('/profile',[ScannerApiController::class, 'profile']);
    Route::post('/edit-profile',[ScannerApiController::class, 'editProfile']);
    Route::get('/scan-ticket/{id}/{event_id}',[ScannerApiController::class, 'scanTicket']);
    Route::post('/change-password',[ScannerApiController::class, 'changePassword']);
});

