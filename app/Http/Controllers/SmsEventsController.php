<?php

namespace App\Http\Controllers;

use App\Models\SmsEvents;
use Illuminate\Http\Request;

class SmsEventsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sms_events = SmsEvents::all();
        return view('admin.sms_events.index',compact('sms_events'));
    }
//
//    /**
//     * Show the form for creating a new resource.
//     *
//     * @return \Illuminate\Http\Response
//     */
//    public function create()
//    {
//        //
//    }
//
//    /**
//     * Store a newly created resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @return \Illuminate\Http\Response
//     */
//    public function store(Request $request)
//    {
//        //
//    }
//
//    /**
//     * Display the specified resource.
//     *
//     * @param  \App\Models\SmsEvents  $smsEvents
//     * @return \Illuminate\Http\Response
//     */
//    public function show(SmsEvents $smsEvents)
//    {
//        //
//    }
//
//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  \App\Models\SmsEvents  $smsEvents
//     * @return \Illuminate\Http\Response
//     */
//    public function edit(SmsEvents $smsEvents)
//    {
//        //
//    }
//
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  \App\Models\SmsEvents  $smsEvents
//     * @return \Illuminate\Http\Response
//     */
//    public function update(Request $request, SmsEvents $smsEvents)
//    {
//        //
//    }
//
//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  \App\Models\SmsEvents  $smsEvents
//     * @return \Illuminate\Http\Response
//     */
//    public function destroy(SmsEvents $smsEvents)
//    {
//        //
//    }
}
