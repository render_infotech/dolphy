<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCountryCodeRequest;
use App\Http\Requests\UpdateCountryCodeRequest;
use App\Models\CountryCode;

class CountryCodeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreCountryCodeRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCountryCodeRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CountryCode  $countryCode
     * @return \Illuminate\Http\Response
     */
    public function show(CountryCode $countryCode)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CountryCode  $countryCode
     * @return \Illuminate\Http\Response
     */
    public function edit(CountryCode $countryCode)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateCountryCodeRequest  $request
     * @param  \App\Models\CountryCode  $countryCode
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCountryCodeRequest $request, CountryCode $countryCode)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CountryCode  $countryCode
     * @return \Illuminate\Http\Response
     */
    public function destroy(CountryCode $countryCode)
    {
        //
    }
}
