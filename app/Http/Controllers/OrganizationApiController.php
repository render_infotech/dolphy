<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\AppUser;
use App\Models\User;
use App\Models\Event;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\PaymentSetting;
use App\Models\Tax;
use App\Models\Feedback;
use App\Models\Currency;
use App\Models\Faq;
use App\Models\Ticket;
use App\Models\Setting;
use App\Mail\ResetPassword;
use App\Http\Controllers\AppHelper;
use App\Models\NotificationTemplate;
use App\Models\Notification;
use App\Models\EventReport;
use Carbon\Carbon;
use DB;    
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;
use App\Models\SmsEvents;
use App\Models\PayoutType;
use App\Models\Chatroom;
use App\Models\Chatmessages;
use App\Models\Maina;
use App\Models\DockType;
use App\Models\CancellationPolicy;
use App\Models\BoatOperator;
use App\Models\BoatModel;
use App\Models\BoatMakeCompany;
use App\Models\AvailabilityType;
use App\Models\ApplicablePhotos;
use App\Models\Applicable;
use App\Models\AllowedItems;
use App\Models\AdvanceNotice;
use App\Models\BankAccounts;
use App\Models\TaxForms;
use App\Models\BankDetails;
use Stripe;

class OrganizationApiController extends Controller
{
    public function organizationLogin(Request $request){
        $validation = Validator::make($request->all(),[
            'email' => 'bail|required|email',            
            'password' => 'bail|required',                            
            'device_token' => 'bail|required',                            
        ]);

        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid Username or password', 'data' => null,'success'=>false], 400);
        }
        $userdata = array( 'email' => $request->email, 'password' => $request->password );
        if(Auth::attempt($userdata)){
            $user = Auth::user();
            User::find($user->id)->update(['device_token'=>$request->device_token]);
            $user['token'] = $user->createToken('eventRight')->accessToken;
            $user['prerequisites'] = $this->prerequisitesDetails();
            return response()->json(['msg' => 'Login successfully', 'data' => $user,'success'=>true], 200);
        }
        else{
            return response()->json(['msg' => 'Invalid Username or password', 'data' => null,'success'=>false], 400);
        }

    }

    public function organizationAppSendLoginOtp(Request $request) {
        $validation = Validator::make($request->all(),[
            'mobile' => 'bail|required',
            'code' => 'bail|required',
        ]);
        $generateOTP = false;
        $otp = "";
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid Username or password', 'data' => null,'status'=>400], 400);
        }
        $mobile = $request->mobile;
        $mobile = str_ireplace(" ","", $mobile);
//        $mobile = str_ireplace("-","", $mobile);
        $request->merge(['mobile' => $mobile]);
        $code = $request->code;
        $mobileData = $code.$mobile;
        $status = ['msg' => 'OTP sent successfully', 'status'=>200];
        $dbRecords = SmsEvents::where('mobile',$mobileData)->where('verified_at',NULL)->orderBy('updated_at','desc')->first();
        if($dbRecords) {
            if($dbRecords->resend_attempts >= (int)env('OTP_RESEND_ATTEMPTS') && strtotime($dbRecords->valid_till) > strtotime('now')) {
                $status = ['msg' => 'You have tried maximum number of resends. Try again after '.timeDiffInMins(date("Y-m-d H:i:s"), date("d M Y H:i:s",strtotime($dbRecords->valid_till))), 'status'=>400];
            } else if ($dbRecords->resend_attempts >= (int)env('OTP_RESEND_ATTEMPTS') && strtotime($dbRecords->valid_till) <= strtotime('now')) {
                goto generateLoginOTP;
            } else if ($dbRecords->resend_attempts > 0 && $dbRecords->resend_attempts < (int)env('OTP_RESEND_ATTEMPTS') && strtotime($dbRecords->valid_till) > strtotime('now')) {
                $otp = $dbRecords->otp;
                $generateOTP = true;
            } else if ($dbRecords->resend_attempts > 0 && $dbRecords->resend_attempts < (int)(env('OTP_RESEND_ATTEMPTS') - 1) && strtotime($dbRecords->valid_till) <= strtotime('now')) {
                goto generateLoginOTP;
            } else {
                goto generateLoginOTP;
            }
        } else {
            generateLoginOTP:
            $otp = generateOTP((int)env('OTP_LENGTH'));
            if($mobile == '9999999999') {   //For Play store and App store verification
                $otp = '1234';
            }
            $generateOTP = true;
        }
        if($generateOTP) {
            $smsContent = trans("auth.otp_login_sms");
            $smsContent = str_replace("#OTP",$otp,$smsContent);
            $validTill = date("Y-m-d H:i:00",strtotime(env("OTP_VALIDITY","+15 minutes")));
            if($dbRecords) {
                if(strtotime($dbRecords->valid_till) < strtotime('now')) {
                    $dbRecords = NULL;
                    goto generateLoginOTP;
                }
                $dbRecords->resend_attempts = $dbRecords->resend_attempts+1;
                $dbRecords->valid_till = $validTill;
                $smsContent = str_replace("#TIME",date("d M Y h:i A", strtotime($validTill)),$smsContent);
                $dbRecords->sms_content = $smsContent;
                $dbRecords->otp = $otp;
                $dbRecords->updated_at = date("Y-m-d H:i:s");
                $dbRecords->save();
                $status["msg"] = "OTP sent successfully. You are left with ".((int)env('OTP_RESEND_ATTEMPTS') -$dbRecords->resend_attempts)." resend attempts";
            } else {
                $dbRecords = new SmsEvents();
                $dbRecords->mobile = $mobileData;
                $dbRecords->otp = $otp;
                $dbRecords->is_otp = '1';
                $dbRecords->attempts = 0;
                $dbRecords->resend_attempts = 0;
                $dbRecords->verified_at = NULL;
                $dbRecords->valid_till = $validTill;
                $smsContent = str_replace("#TIME",date("d M Y h:i", strtotime($validTill)),$smsContent);
                $dbRecords->sms_content = $smsContent;
                $dbRecords->created_at = date("Y-m-d H:i:s");
                $dbRecords->updated_at = date("Y-m-d H:i:s");
                $dbRecords->save();
            }
            $smsStatus = sendSMS($dbRecords->mobile, $smsContent);
            if(!$smsStatus) {
                if($dbRecords->resend_attempts > 0) {
                    $dbRecords->resend_attempts = $dbRecords->resend_attempts-1;
                    $dbRecords->updated_at = date("Y-m-d H:i:s");
                    $dbRecords->save();
                }
                $status["msg"] = "Something went wrong, try again later!!!";
                $status["status"] = 400;
            }
        }
        return response()->json($status,$status['status']);
    }

    public function organizationAppLogin(Request $request){
        $validation = Validator::make($request->all(),[
            'code' => 'required',
            'mobile' => 'bail|required',
            'otp' => 'bail|required|digits:'. (int)env('OTP_LENGTH'),
            'device_token' => 'bail|required'
        ]);

        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Kindly try again!!!', 'data' => null,'status'=>400], 400);
        }
        $mobile = $request->mobile;
        $mobile = str_ireplace(" ","", $mobile);
//        $mobile = str_ireplace("-","", $mobile);
        $request->merge(['mobile' => $mobile]);
        $code = $request->code;
        $mobileData = $code.$mobile;
        $isOTPVerified = false;
        $status = ['msg' => 'Invalid credentials, Kindly try again!!!', 'status'=>400, 'data' => null];
        $dbRecords = SmsEvents::where('mobile',$mobileData)->where('verified_at',NULL)->orderBy('updated_at','desc')->first();
        if($dbRecords) {
            if($dbRecords->attempts >= (int)env('OTP_VERIFY_ATTEMPTS')) {
                $status = ['msg' => 'Your account is locked due to maximum number of login attempts. Try again after '.timeDiffInMins(date("Y-m-d H:i:s"),date("d M Y H:i:s",strtotime($dbRecords->valid_till))), 'status'=>400, 'data' => null];
            } else if (strtotime($dbRecords->valid_till) < strtotime('now')) {
                $status = ['msg' => 'Your OTP is expired, Kindly try with new OTP.', 'status'=>400, 'data' => null];
            } else if ((int)$dbRecords->otp === (int)$request->otp) {
                $isOTPVerified = true;
            }
            $dbRecords->attempts = $dbRecords->attempts+1;
            $dbRecords->updated_at = date("Y-m-d H:i:s");
            if($isOTPVerified) {
                $dbRecords->verified_at = date("Y-m-d H:i:s");
            }
            $dbRecords->save();
        }
        if(!$isOTPVerified) {
            return response()->json($status, 400);
        }
        $userRecord = User::where('phone',$request->mobile)->orderBy('updated_at','desc')->first();
        if(!$userRecord) {
            return response()->json(['msg' => 'No Records found with the provided Phone number. Kindly register!!!', 'status'=>200, 'data' => null],200);
        } else {
            Auth::login($userRecord,true);
        }
        $user = Auth::user();
        User::find($user->id)->update(['device_token'=>$request->device_token]);
        $user['token'] = $user->createToken('eventRight')->accessToken;
        $user['prerequisites'] = $this->prerequisitesDetails();
        return response()->json(['msg' => 'Login successfully', 'data' => $user,'status'=>200], 200);

    }

    public function organizationRegister(Request $request){
        $data = $request->all();
        $validation = Validator::make($data,[
            'email' => 'bail|required|email|unique:users',
            'confirm_email' => 'bail|required|email|same:email',
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'phone' => 'bail|required',
            'password' => 'bail|required|min:6',
            'confirm_password' => 'bail|required|min:6|same:password',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => $validation->errors()->first(), 'data' => null,'success'=>false], 400);
        }
        $data['image'] = "defaultuser.png";
        $data['password'] =  Hash::make($request->password);
        $user = User::create($data);
        $user->assignRole('organization');
        $user['token'] = $user->createToken('eventRight')->accessToken;
        $user['prerequisites'] = $this->prerequisitesDetails();
        return response()->json(['msg' => null, 'data' => $user,'success'=>true], 200);
    }

    public function organizationAppRegister(Request $request){
        $data = $request->all();
        $validation = Validator::make($data,[
            'first_name' => 'bail|required',
            'email' => 'bail|required|email|unique:users',
            'code' => 'bail|required',
            'mobile' => 'bail|required|unique:users,phone',
            'password' => 'bail|required|min:6',
            'confirm_password' => 'bail|required|min:6|same:password',
            'address_1' => 'bail|required|max:255',
            'pincode' => 'bail|required',
            'device_token' => 'bail|required'
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => $validation->errors()->first(), 'data' => null,'status'=>400], 400);
        }
        $mobile = $request->mobile;
        $mobile = str_ireplace(" ","", $mobile);
//        $mobile = str_ireplace("-","", $mobile);
        $request->merge(['phone' => $mobile]);
        if(!$request->hasFile('image')) {
            $data['image'] = env('APP_URL')."/images/upload/defaultuser.png";
        } else {
            $imageName = $request->phone.'.'.$request->image->extension();
            $request->image->storeAs('images',$imageName);
            $request->image->move(public_path('images/upload'),$imageName);
            $data['image'] = env('APP_URL')."/images/upload/".$imageName;
        }
        $data['password'] =  Hash::make($request->password);
        $data['name'] =  $request->first_name;
        $data['phone'] = $mobile;
        $user = User::create($data);
        $user->assignRole('organization');
        Auth::login($user,true);
        $user['token'] = $user->createToken('eventRight')->accessToken;
        $user['prerequisites'] = $this->prerequisitesDetails(false,false,false, true);
        return response()->json(['msg' => null, 'data' => $user,'status'=>200], 200);
    }

    public function setProfile(Request $request){
        $data= $request->all();
        if(isset($request->image)){                  
            $data['image'] = (new AppHelper)->saveApiImage($request);                                      
        }
        
        User::find(Auth::user()->id)->update($data);
        return response()->json(['msg' => 'profile set successfully.','success'=>true], 200);
    }

    public function forgetPassword(Request $request){
        $request->validate([
            'email' => 'bail|required|email',          
        ]);
        $user = User::where('email',$request->email)->first();
      
        $password=rand(100000,999999);
        if($user){          
            $content = NotificationTemplate::where('title','Reset Password')->first()->mail_content;           
            $detail['user_name'] = $user->name;
            $detail['password'] = $password;
            $detail['app_name'] = Setting::find(1)->app_name;            
            try{
                Mail::to($user)->send(new ResetPassword($content,$detail));
            } catch (\Throwable $th) {  }
            User::find($user->id)->update(['password'=>Hash::make($password)]);            
            return response()->json(['success'=>true,'msg'=>'Please check your email new password will send on it.','data' =>null ], 200);
        }
        else{
            return response()->json(['success'=>false,'msg'=>'Invalid email ID','data' =>null ], 200);
        }
    }

    public function profile(){
        $data = Auth::user()->makeHidden(['created_at','updated_at']);
        return response()->json(['data' => $data,'success'=>true], 200);
    }

    public function events(){
        $timezone = Setting::find(1)->timezone;       
        $date = Carbon::now($timezone);                  
        $data['past'] =  Event::with(['ticket'])
        ->where([['status',1],['is_deleted',0],['user_id',Auth::user()->id],['start_time', '<=',$date->format('Y-m-d H:i:s')]])       
        ->orWhere([['status',1],['is_deleted',1],['user_id',Auth::user()->id],['start_time', '<=',$date->format('Y-m-d H:i:s')]])      
        ->orWhere([['status',1],['event_status','Cancel'],['user_id',Auth::user()->id],['start_time', '<=',$date->format('Y-m-d H:i:s')]])       
        ->orderBy('start_time','ASC')->get();  

        $data['draft'] =  Event::with(['ticket'])
        ->where([['status',0],['event_status','Pending'],['is_deleted',0],['user_id',Auth::user()->id]])
        ->orderBy('start_time','ASC')->get();  

        $data['upcoming'] =  Event::with(['ticket'])
        ->where([['status',1],['is_deleted',0],['user_id',Auth::user()->id],['start_time', '>=',$date->format('Y-m-d H:i:s')]])       
        ->where([['status',1],['event_status','Pending'],['user_id',Auth::user()->id],['start_time', '>=',$date->format('Y-m-d H:i:s')]])       
        ->orderBy('start_time','ASC')->get();   
        
      
        return response()->json(['data' => $data,'success'=>true], 200);
    }
    public function searchEvents(){        
        $data =  Event::with(['ticket'])
        ->where([['user_id',Auth::user()->id],['is_deleted',0]])       
        ->orderBy('start_time','ASC')->get();   
        return response()->json(['data' => $data,'success'=>true], 200);
        
    }

    public function scanner(){
        $data = User::role('scanner')->where('org_id',Auth::user()->id)->orderBy('id','DESC')->get();
        return response()->json(['data' => $data,'success'=>true], 200);
    }

    public function addScanner(Request $request){
        $request->validate([
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'email' => 'bail|required|email|unique:users',
            'phone' => 'bail|required',
            'password' => 'bail|required|min:6',
        ]);
        $data = $request->all();
        $data['org_id'] = Auth::user()->id;
        $data['password'] =  Hash::make($request->password); 
       
        $user = User::create($data);            
        $user->assignRole('scanner');
        return response()->json(['data' => $user,'success'=>true], 200);
    }

    public function addEvent(Request $request){
              
        $request->validate([
            'name' => 'bail|required',
            'image' => 'bail|required',
            'start_date' => 'bail|required|date_format:Y-m-d',
            'end_date' => 'bail|required|date_format:Y-m-d',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required',
            'scanner_id' => 'bail|required',
            'category_id' => 'bail|required|numeric',
            'type' => 'bail|required',  // offline & online
            'address' => 'bail|required_if:type,offline',           
            'lat' => 'bail|required_if:type,offline',
            'lang' => 'bail|required_if:type,offline',
            'status' => 'bail|required|numeric',
            'description' => 'bail|required',
            'people' => 'bail|required|numeric',                                     
        ]);         
        $data = $request->all();            
        $data['user_id'] = Auth::user()->id; 
        if($request->tags != null){
            $data['tags'] = array();
            foreach ($request->tags as $key) {          
                array_push( $data['tags'],$key['value']);
            }    
        }  
        if (isset($request->tags)) {
            if(count($data['tags']) > 0) {
                $data['tags'] = implode(',',$data['tags']);
            }
        }
        if (isset($request->image)) {
            // $data['image'] = 'default.png';  // for postmaan
            $data['image'] = (new AppHelper)->saveApiImage($request);  // for api
        } 
        
        $data['start_time'] = $request->start_date.' '.$request->start_time;     
        $data['end_time']  = $request->end_date.' '.$request->end_time;     
     
        $event = Event::create($data);
        return response()->json(['data' => $event,'success'=>true], 200);
    }

    public function editEvent(Request $request){
        $request->validate([
            'name' => 'bail|required',
            'id' => 'bail|required|numeric',           
            'start_date' => 'bail|required|date_format:Y-m-d',
            'end_date' => 'bail|required|date_format:Y-m-d',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required', 
            'scanner_id' => 'bail|required',
            'category_id' => 'bail|required|numeric',
            'type' => 'bail|required',  // offline & online
            'address' => 'bail|required_if:type,offline',           
            'lat' => 'bail|required_if:type,offline',
            'lang' => 'bail|required_if:type,offline',
            'status' => 'bail|required|numeric',
            'description' => 'bail|required',
            'people' => 'bail|required|numeric',                                     
        ]); 
        $data = $request->all();                    
        if (isset($request->image)) {
            // $data['image'] = 'default.png';  // for postmaan
            $data['image'] = (new AppHelper)->saveApiImage($request);  // for api
        } 
        $data['start_time'] = $request->start_date.' '.$request->start_time;     
        $data['end_time']  = $request->end_date.' '.$request->end_time;   

        
        if(isset($request->gallery)){
            $gallery = array();
            foreach (json_decode($request->gallery) as $value) {
                $img = $value;
                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                $img_code = base64_decode($img);
                $Iname = uniqid();
                $file = public_path('/images/upload/') . $Iname . ".png";
                $success = file_put_contents($file, $img_code);
                $image_name=$Iname . ".png";
                array_push($gallery,$image_name);
            }
            $data['gallery'] = implode(',',$gallery);
        }
        $event = Event::find($request->id)->update($data);

        return response()->json(['data' => $event,'success'=>true], 200);       
    }

    public function cancelEvent($id){
        Event::find($id)->update(['event_status'=>'Cancel']);
        return response()->json(['msg' => 'Event Canceled successfully.','success'=>true], 200);       
    }

    public function deleteEvent($id){
        Event::find($id)->update(['is_deleted'=>1,'event_status'=>'Deleted']);
        $ticket = Ticket::where('event_id',$id)->update(['is_deleted'=>1]);        
        return response()->json(['msg' => 'Event deleted successfully.','success'=>true], 200);   
    }

    public function eventDetail($id){
        $data = Event::with(['category:id,name'])->find($id);    
        $data->startTime = $data->start_time->format('h:i a');
        $data->endTime = $data->end_time->format('h:i a');  
        $data->tags = array_filter(explode(',',$data->tags));  
        $data->start__time = $data->start_time->format('Y-m-d\TH:i:s'.'.000000');
        $data->end__time =  $data->end_time->format('Y-m-d\TH:i:s'.'.000000');
        $gallery = array_filter(explode(',',$data->gallery)); 
        $g=array(); 
        if(count($gallery)>0){
            foreach ($gallery as  $value) {
                array_push($g,url('/').'/images/upload/'.$value);
            }
            $data->gallery = $g;
        }            
        return response()->json(['data' => $data,'success'=>true], 200);  
    }

    public function eventGuestList($id){
        $data = Order::with(['customer:name,id,email,phone','ticket:id,ticket_number'])->where('event_id',$id)->orderBy('id','DESC')->get()->each->setAppends([])->makeHidden(['payment_type','updated_at','payment_token','coupon_discount','coupon_id','order_status']);
        return response()->json(['data' => $data,'success'=>true], 200);  
    }

    public function orderDetail($id){
        $data = Order::with(['customer:name,id,email,phone','ticket:id,ticket_number'])->find($id)->setAppends([])->makeHidden(['updated_at','payment_token','coupon_discount','coupon_id','order_status']);
        return response()->json(['data' => $data,'success'=>true], 200);  
    }

    public function eventTickets($id){
        $data = Ticket::where('event_id',$id)->orderBy('id','DESC')->get();
        foreach ($data as $value) {
            $value->use_ticket = Order::where('ticket_id',$value->id)->sum('quantity');
        }
        return response()->json(['data' => $data,'success'=>true], 200);  
    }

    public function editImage(Request $request){
        $request->validate([
            'image' => 'bail|required',               
        ]);          
        if(isset($request->image))
        {           
            $image_name = (new AppHelper)->saveApiImage($request);
            User::find(Auth::user()->id)->update(['image'=>$image_name]);    
            return response()->json(['msg' => null, 'data' => null,'success'=>true], 200);
        }
        else{
            return response()->json(['msg' => null, 'data' => null,'success'=>false], 200);
        }    
        
    }

    public function ticketDetail($id){
        $data = Ticket::find($id);
        $data->use_ticket = Order::where('ticket_id',$data->id)->sum('quantity');
        $data->startTime = $data->start_time->format('Y-m-d\TH:i:s'.'.000000');
        $data->endTime =  $data->end_time->format('Y-m-d\TH:i:s'.'.000000');
    
        return response()->json(['data' => $data,'success'=>true], 200);  
    }

    public function addTicket(Request $request){
        $request->validate([
            'name' => 'bail|required',
            'event_id'=> 'bail|required|numeric',
            'quantity' => 'bail|required',
            // 'start_date' => 'bail|required|date_format:Y-m-d',
            // 'end_date' => 'bail|required|date_format:Y-m-d',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required',
            'type' => 'bail|required',
            'ticket_per_order'=> 'bail|required',
            'description'=> 'bail|required',
            'price' =>  'bail|required_if:type,paid',   
            'status' =>  'bail|required',            
        ]);   
        $data = $request->all();       
        if($request->type=="free"){
            $data['price'] = 0;
        } 
        // $data['start_time'] = $request->start_date.' '.$request->start_time;     
        // $data['end_time']  = $request->end_date.' '.$request->end_time;    
        $data['ticket_number'] = chr(rand(65,90)).chr(rand(65,90)).'-'.rand(999,10000);
        $event = Event::find($request->event_id); 
        $data['user_id'] = $event->user_id; 
        $ticket = Ticket::create($data);  
        return response()->json(['data' => $ticket,'success'=>true], 200);  
    }

   
    public function editTicket(Request $request){
        $request->validate([
            'name' => 'bail|required',
            'event_id'=> 'bail|required|numeric',
            'id' => 'bail|required',
            'quantity' => 'bail|required',   
            'start_date' => 'bail|required|date_format:Y-m-d',
            'end_date' => 'bail|required|date_format:Y-m-d',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required',
            'type' => 'bail|required',
            'ticket_per_order'=> 'bail|required',
            'description'=> 'bail|required',
            'status' =>  'bail|required',
            'price' =>  'bail|required_if:type,paid',            
        ]);   
        $data = $request->all();       
        if($request->type=="free"){
            $data['price'] = 0;
        }      
        $data['start_time'] = $request->start_date.' '.$request->start_time;     
        $data['end_time']  = $request->end_date.' '.$request->end_time;                
        Ticket::find($request->id)->update($data);  
        $ticket = Ticket::find($request->id);
        return response()->json(['data' => $ticket,'success'=>true], 200);  
    }

    public function deleteTicket($id){
        Ticket::find($id)->delete();
        return response()->json(['msg' => 'Ticket deleted successfully.','success'=>true], 200);  
    }
    
    public function allCategory(){
        $data = Category::where('status',1)->orderBy('id','DESC')->get();  
        return response()->json(['data' => $data,'success'=>true], 200);  
    }

    public function organizationSetting(){
        $data = Setting::find(1,['currency','default_lat','default_long','privacy_policy_organizer','terms_use_organizer','app_version','or_onesignal_app_id','or_onesignal_project_number','footer_copyright']);        
        $data->currency_symbol = Currency::where('code',$data->currency)->first()->symbol;
        return response()->json(['data' => $data,'success'=>true], 200);    
    }
    
    public function viewTax(){
        $data = Tax::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
        return response()->json(['data' => $data,'success'=>true], 200);    
    }

    public function taxDetail($id){
        $data = Tax::find($id);
        return response()->json(['data' => $data,'success'=>true], 200);    
    }

    public function deleteOrder($id){
        $data = Order::find($id);
        $data->delete();
        return response()->json(['msg' => 'Order deleted successfully.','success'=>true], 200);    
    }
    
    public function addTax(Request $request){
        $request->validate([
            'name' => 'bail|required',
            'price' => 'bail|required|numeric',
            'allow_all_bill'=> 'bail|required|numeric',            
        ]);   
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $tax = Tax::create($data);
        return response()->json(['data' => $tax,'success'=>true], 200);    
    }

    public function editTax(Request $request){
        $request->validate([
            'name' => 'bail|required',
            'id' => 'bail|required',
            'price' => 'bail|required|numeric',
            'allow_all_bill'=> 'bail|required|numeric',           
        ]);   
        $data = $request->all();        
        Tax::find($request->id)->update($data);
        $tax =Tax::find($request->id);
        return response()->json(['data' => $tax,'success'=>true], 200);    
    }

    public function deleteTax($id){
        Tax::find($id)->delete();
        return response()->json(['msg' =>'Tax deleted successfully.' ,'success'=>true], 200);    

    }

    public function changeStatusTax($id,$status){
        Tax::find($id)->update(['status'=>$status]);
        $data = Tax::find($id);
        return response()->json(['data' =>$data ,'success'=>true], 200);    

    }

    public function viewFaq(){
        $data = Faq::where('status',1)->orderBy('id','DESC')->get()->makeHidden(['created_at','updated_at']);
        return response()->json(['data' => $data,'success'=>true], 200);    
    }

    public function addFeedback(Request $request){
        $request->validate([
            'email' => 'bail|required|email',
            'message' => 'bail|required',
            'rate' => 'bail|required|numeric',          
        ]);   
        $data = $request->all();        
        $data['user_id'] = Auth::user()->id;
        $feedback = Feedback::create($data);
        return response()->json(['data' => $feedback,'success'=>true], 200);   
    }

    public function changePassword(Request $request){
        $request->validate([
            'old_password' => 'bail|required',
            'password' => 'bail|required|min:6',
            'password_confirmation' => 'bail|required|same:password|min:6'
        ]);
        if (Hash::check($request->old_password, Auth::user()->password)){
            User::find(Auth::user()->id)->update(['password'=>Hash::make($request->password)]);            
            return response()->json(['success'=>true,'msg'=>'Your password is change successfully','data' =>null ], 200);
        }
        else{
            return response()->json(['success'=>false,'msg'=>'Current Password is wrong!','data' =>null ], 200);
        }
    }

    public function editProfile(Request $request){
        User::find(Auth::user()->id)->update($request->all());
        $user = User::find(Auth::user()->id);
        return response()->json(['success'=>true,'msg'=>null,'data' =>$user ], 200);
    }

    public function followers(){
        $user_id = Auth::user()->followers;
        $user  = AppUser::whereIn('id',$user_id)->get()->makeHidden(['created_at','updated_at','provider_token','provider','lang','lat','favorite','email_verified_at']);
        return response()->json(['success'=>true,'msg'=>null,'data' =>$user ], 200);        
    }

    public function notifications(){
        $data = Notification::where('organizer_id',Auth::user()->id)->orderBy('id','DESC')->get()->each->setAppends(['event']);
        return response()->json(['success'=>true,'msg'=>null,'data' =>$data ], 200);
    }

    public function clearNotification(){
        $noti = Notification::where('organizer_id',Auth::user()->id)->get();        
        foreach ($noti as $value) {
            $value->delete();
        }
        return response()->json(['success'=>true,'msg'=>'Notification deleted successfully.' ], 200);
    }

    public function couponEvent(){
        $data = Event::where([['status',1],['is_deleted',0],['event_status','Pending'],['user_id',Auth::user()->id]])->orderBy('id','DESC')->get(['id','name'])->each->setAppends([]);
        return response()->json(['success'=>true,'msg'=>null,'data' =>$data ], 200); 
    }

    public function coupons(){
        $coupon = Coupon::with(['event:id,name,image'])->where('user_id',Auth::user()->id)->OrderBy('id','DESC')->get()->makeHidden(['created_at','updated_at']);            
        return response()->json(['success'=>true,'msg'=>null,'data' =>$coupon ], 200);
    }

    public function addCoupon(Request $request){
        $request->validate([
            'name' => 'bail|required',
            'event_id' => 'bail|required',
            'discount' => 'bail|required',
            'start_date' => 'bail|required|date_format:Y-m-d',
            'end_date' => 'bail|required|date_format:Y-m-d',
            'max_use' => 'bail|required',
            'description' => 'bail|required',        
        ]);   
        $data = $request->all(); 
        $data['user_id'] = Auth::user()->id;
        $data['status'] = 1;
        $data['coupon_code'] =  chr(rand(65,90)).chr(rand(65,90)).'-'.rand(9999,100000);
        $coupon =  Coupon::create( $data);
        return response()->json(['success'=>true,'msg'=>null,'data' =>$coupon ], 200);
    }

    public function editCoupon(Request $request){
        $request->validate([
            'name' => 'bail|required',
            'id' => 'bail|required',
            'event_id' => 'bail|required',
            'discount' => 'bail|required',
            'start_date' => 'bail|required|date_format:Y-m-d',
            'end_date' => 'bail|required|date_format:Y-m-d',
            'max_use' => 'bail|required',
            'description' => 'bail|required',        
        ]);   
        $data = $request->all();                         
        $coupon =  Coupon::find($request->id)->update($data);        
        return response()->json(['success'=>true,'msg'=>null,'data' =>$coupon ], 200);
    }

    public function couponDetail($id){
        $data = Coupon::find($id)->makeHidden(['created_at','updated_at']);
        $data->event_name = Event::find($data->event_id)->name;
        return response()->json(['success'=>true,'msg'=>null,'data' =>$data ], 200);
    }

    public function deleteCoupon($id){
        $data = Coupon::find($id);
        $data->delete();
        return response()->json(['success'=>true,'msg'=>'Coupon deleted successfully.' ], 200);
    }

    public function  removeGalleryImage(Request $request){
        $request->validate([
            'image' => 'bail|required',   
            'id' => 'bail|required',          
        ]);
        $gallery = array_filter(explode(',',Event::find($request->id)->gallery)); 
        if(count(array_keys($gallery,$request->image))>0){
            if (($key = array_search($request->image, $gallery)) !== false) {
                unset($gallery[$key]);
            }
        }
        $aa =implode(',',$gallery);
        $data= Event::find($request->id);
        $data->gallery =$aa;
        $data->update();    
        return response()->json(['success'=>true,'msg'=>null ], 200);
    }

    public function prerequisites(Request $request) {
        $records = $this->prerequisitesDetails();
        return response()->json(['msg' => null, 'data' => $records,'status'=>200], 200);
    }

    public function prerequisitesDetails($isDashboard = false, $isListing = false, $isBooking = false, $isRegister = false) {
        $scanner = [];
        if(Auth::user()->hasRole('admin')){
            $scanner = User::role('scanner')->orderBy('id','DESC')->get(['id','name']);
        }
        else if(Auth::user()->hasRole('organization')){
            $scanner = User::role('scanner')->where('org_id',Auth::user()->id)->orderBy('id','DESC')->get(['id','name']);
        }
        $boatTax = Tax::where('name','commission')->where('user_id',1)->first('price');
        $eventTax = $boatTax;
        $gameTax = $boatTax;
        $records = [];
        if(!$isDashboard && !$isListing && !$isBooking) {
            $records = [
                'pending_entry' => [
                    'description' => '',
                    'data' => (new Event)->getPendingEntries(Auth::user()->id),
                ],
                'advance_notice' => [
                    'description' => '',
                    'data' => AdvanceNotice::where('status','1')->get(['id','time']),
                ],
                'allowed_items' => [
                    'description' => trans('content.allowed_item_heading'),
                    'data' => AllowedItems::where('status','1')->get(['id','name','image'])
                ],
                'categories' => [
                    'description' => trans('content.category_heading'),
                    'data' => Applicable::where('status','1')->get(['id','name','image'])
                ],
                'availability_type' => [
                    'description' => trans('content.availability_heading'),
                    'data' => trans('content.availability_data'),
                ],
                'boat_operator' => [
                    'description' => '',
                    'data' => BoatOperator::where('status','1')->get(['id','name']),
                ],
                'cancellation_policy' => [
                    'description' => '',
                    'data' => CancellationPolicy::where('status','1')->where('applicable_for',1)->get(['id','name','policy_description']),
                ],
                'dock_type' => [
                    'description' => '',
                    'data' => DockType::where('status','1')->get(['id','name']),
                ],
                'payout_type' => [
                    'description' => '',
                    'data' => PayoutType::where('status','1')->get(['id','name']),
                ],
                'maina_name' => [
                    'description' => '',
                    'data' => Maina::where('status','1')->get(['id','name']),
                ],
                'country' => [
                    'description' => '',
                    'data' => Currency::get(['id','country']),
                ],
                'type' => [
                    'description' => '',
                    'data' => [["order" => 1,"key" => "offline","data" =>"Venue"],["order" => 2,"key" => "online","data" =>"Online"]],
                ],
                'scanners' => [
                    'description' => '',
                    'data' => $scanner,
                ],
                'event_category' => [
                    'description' => '',
                    'data' => Category::where('status',1)->get(['id','name']),
                ],
                'terms_n_condition' => [
                    'description' => trans('content.terms_n_condition_heading'),
                    'data' => trans('content.terms_n_condition_body'),
                ],
                'tax_details' => [
                    'description' => '',
                    'data' => [
                        'boat' => is_null($boatTax) ? 0.00 : (float)$boatTax->price,
                        'event' => is_null($eventTax) ? 0.00 : (float)$eventTax->price,
                        'game' => is_null($gameTax) ? 0.00 : (float)$gameTax->price,
                    ],
                ],
                'dashboard' => $this->getDashboardData(),
                'bookings' => $this->getBookingData(),
                'listing' => $this->getListingData(),
            ];
            if($isRegister) {
                unset($records['pending_entry']);
                unset($records['tax_details']);
                unset($records['dashboard']);
                unset($records['bookings']);
                unset($records['listing']);
            }
        } else {
            if($isDashboard) {
                $records['dashboard'] = $this->getDashboardData();
            }
            if($isListing) {
                $records['listing'] = $this->getListingData();
            }
            if($isBooking) {
                $records['bookings'] = $this->getBookingData();
            }
        }
        return $records;
    }

    public function dashboard() {
        $records = $this->getDashboardData();
        return response()->json(['status' => 200, 'msg' => $records],200);
    }

    public function bookings() {
        $records = $this->getBookingData();
        return response()->json(['status' => 200, 'msg' => $records],200);
    }

    public function listing() {
        $records = $this->getListingData();
        return response()->json(['status' => 200, 'msg' => $records],200);
    }

    public function updateOrganisationTax(Request $request) {
        $validator = Validator::make($request->all(), [
            'boat' => 'bail|required_without_all:event,game|numeric',
            'event' => 'bail|required_without_all:boat,game|numeric',
            'game' => 'bail|required_without_all:boat,event|numeric',
        ]);
        $inputRecords = $request->all();
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'msg' => $validator->errors()],400);
        }
        $userId = Auth::user()->id;
        $boatTax = Tax::where('name','boat')->where('user_id',$userId)->first();
        $eventTax = Tax::where('name','event')->where('user_id',$userId)->first();
        $gameTax = Tax::where('name','game')->where('user_id',$userId)->first();
        if(array_key_exists('boat',$inputRecords)) {
            if(is_null($boatTax)) {
                $boatTax = new Tax;
                $boatTax->name = 'boat';
                $boatTax->user_id = $userId;
                $boatTax->price = $inputRecords['boat'];
                $boatTax->save();
            } else {
                $boatTax->price = $inputRecords['boat'];
                $boatTax->save();
            }
        }
        if(array_key_exists('event',$inputRecords)) {
            if(is_null($eventTax)) {
                $eventTax = new Tax;
                $eventTax->name = 'event';
                $eventTax->user_id = $userId;
                $eventTax->price = $inputRecords['event'];
                $eventTax->save();
            } else {
                $eventTax->price = $inputRecords['event'];
                $eventTax->save();
            }
        }
        if(array_key_exists('game',$inputRecords)) {
            if(is_null($gameTax)) {
                $gameTax = new Tax;
                $gameTax->name = 'game';
                $gameTax->user_id = $userId;
                $gameTax->price = $inputRecords['game'];
                $gameTax->save();
            } else {
                $gameTax->price = $inputRecords['game'];
                $gameTax->save();
            }
        }
        $taxDetails = Tax::where('status',1)->where('user_id',Auth::user()->id)->whereIn('name',['boat','game','event'])->get(['name','price']);
        $taxData = [
            'boat' => is_null($taxDetails->where('name','boat')->first()) ? 0.00 : (float)$taxDetails->where('name','boat')->first()->price,
            'event' => is_null($taxDetails->where('name','event')->first()) ? 0.00 : (float)$taxDetails->where('name','event')->first()->price,
            'game' => is_null($taxDetails->where('name','game')->first()) ? 0.00 : (float)$taxDetails->where('name','game')->first()->price,
        ];
        return response()->json(['status' => 200, 'msg' => 'success', 'payment_settings' => ['tax' => $taxData]],200);
    }
    public function profileUpdate(Request $request) {
        $validator = Validator::make($request->all(), [
            'address_1' => 'bail|required_without_all:image,address_2,city,state,pincode,country',
            'address_2' => 'bail|required_without_all:image,address_1,city,state,pincode,country',
            'city' => 'bail|required_without_all:image,address_1,address_2,state,pincode,country',
            'state' => 'bail|required_without_all:image,address_1,address_2,city,pincode,country',
            'pincode' => 'bail|required_without_all:image,address_1,address_2,city,state,country',
            'country' => 'bail|required_without_all:image,address_1,address_2,city,state,pincode',
            'image' => 'bail|required_without_all:address_1,address_2,city,state,pincode,country'
        ]);
        $inputRecords = $request->all();
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'msg' => $validator->errors()],400);
        }
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $hasChange = 0;
        if(array_key_exists('address_1',$inputRecords) && strlen(trim($inputRecords['address_1'])) > 0) {
            $hasChange = 1;
            $user->address_1 = $inputRecords['address_1'];
        }
        if(array_key_exists('address_2',$inputRecords) && strlen(trim($inputRecords['address_2'])) > 0) {
            $hasChange = 1;
            $user->address_2 = $inputRecords['address_2'];
        }
        if(array_key_exists('city',$inputRecords) && strlen(trim($inputRecords['city'])) > 0) {
            $hasChange = 1;
            $user->city = $inputRecords['city'];
        }
        if(array_key_exists('state',$inputRecords) && strlen(trim($inputRecords['state'])) > 0) {
            $hasChange = 1;
            $user->state = $inputRecords['state'];
        }
        if(array_key_exists('pincode',$inputRecords) && strlen(trim($inputRecords['pincode'])) > 0) {
            $hasChange = 1;
            $user->pincode = $inputRecords['pincode'];
        }
        if(array_key_exists('country',$inputRecords) && strlen(trim($inputRecords['country'])) > 0) {
            $hasChange = 1;
            $user->country = $inputRecords['country'];
        }
        if($request->hasFile('image')) {
            $imageName = $request->phone.'.'.$request->image->extension();
            $request->image->storeAs('images',$imageName);
            $request->image->move(public_path('images/upload'),$imageName);
            $user->image = env('APP_URL')."/images/upload/".$imageName;
            $hasChange = 1;
        }
        if($hasChange) {
            $user->save();
            $profile = [
                'image' => $user->image,
                'phone' => $user->phone,
                'email' => $user->email,
                'first_name' => $user->first_name,
                'last_name' => $user->last_name,
                'full_name' => $user->first_name.' '.$user->last_name,
                'address' => $user->address_1.', '.$user->address_2.', '.$user->city.', '.$user->state.', '.$user->pincode,
                'address_1' => $user->address_1,
                'address_2' => $user->address_2,
                'city' => $user->city,
                'state' => $user->state,
                'pincode' => $user->pincode,
                'country' => $user->country,
            ];
            return response()->json(['status' => 200, 'msg' => 'success','profile' => $profile],200);
        }
        return response()->json(['status' => 400, 'msg' => 'No changes found'],400);
    }

    public function updateBankAccounts(Request $request){
        $validator = Validator::make($request->all(), [
            'column1' => 'bail|required_without_all:column2,column3,column4,column5,column6,column7,column8,column9,column10',
            'column2' => 'bail|required_without_all:column1,column3,column4,column5,column6,column7,column8,column9,column10',
            'column3' => 'bail|required_without_all:column1,column2,column4,column5,column6,column7,column8,column9,column10',
            'column4' => 'bail|required_without_all:column1,column2,column3,column5,column6,column7,column8,column9,column10',
            'column5' => 'bail|required_without_all:column1,column2,column3,column4,column6,column7,column8,column9,column10',
            'column6' => 'bail|required_without_all:column1,column2,column3,column4,column5,column7,column8,column9,column10',
            'column7' => 'bail|required_without_all:column1,column2,column3,column4,column5,column6,column8,column9,column10',
            'column8' => 'bail|required_without_all:column1,column2,column3,column4,column5,column6,column7,column9,column10',
            'column9' => 'bail|required_without_all:column1,column2,column3,column4,column5,column6,column7,column8,column10',
            'column10' => 'bail|required_without_all:column1,column2,column3,column4,column5,column6,column7,column8,column9',
        ]);


        $inputRecords = $request->all();
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'msg' => $validator->errors()],400);
        }


        $userId = Auth::user()->id;
        $user = BankAccounts::where('user_id',$userId)->get()->first();

        if(!isset($user->id)){
           $user = BankAccounts::create(['user_id'=>$userId]);
        }

        if(strlen($user->stripe_account_id)>0){
             return response()->json(['status' => 400, 'msg' => 'No changes found'],400);
        }

        $hasChange = 0;
        if(array_key_exists('column1',$inputRecords) && strlen(trim($inputRecords['column1'])) > 0) {
            $hasChange = 1;
            $user->account_holder_name = $inputRecords['column1'];
        }
        if(array_key_exists('column2',$inputRecords) && strlen(trim($inputRecords['column2'])) > 0) {
            $hasChange = 1;
            $user->account_holder_type = $inputRecords['column2'];
        }
        if(array_key_exists('column3',$inputRecords) && strlen(trim($inputRecords['column3'])) > 0) {
            $hasChange = 1;
            $user->currency = $inputRecords['column3'];
        }
        if(array_key_exists('column4',$inputRecords) && strlen(trim($inputRecords['column4'])) > 0) {
            $hasChange = 1;
            $user->bic = $inputRecords['column4'];
        }
        if(array_key_exists('column5',$inputRecords) && strlen(trim($inputRecords['column5'])) > 0) {
            $hasChange = 1;
            $user->account_number = $inputRecords['column5'];
        }
        if(array_key_exists('column6',$inputRecords) && strlen(trim($inputRecords['column6'])) > 0) {
            $hasChange = 1;
            $user->country = $inputRecords['column6'];
        }
        if(array_key_exists('column7',$inputRecords) && strlen(trim($inputRecords['column7'])) > 0) {
            $hasChange = 1;
            $user->document_file = $inputRecords['column7'];
        }
        if(array_key_exists('column8',$inputRecords) && strlen(trim($inputRecords['column8'])) > 0) {
            $hasChange = 1;
            $user->column8 = $inputRecords['column8'];
        }
        if(array_key_exists('column9',$inputRecords) && strlen(trim($inputRecords['column9'])) > 0) {
            $hasChange = 1;
            $user->column9 = $inputRecords['column9'];
        }
        if(array_key_exists('column10',$inputRecords) && strlen(trim($inputRecords['column10'])) > 0) {
            $hasChange = 1;
            $user->column10 = $inputRecords['column10'];
        }
        if($hasChange) {
            $user->save();

            $currency = Setting::find(1)->currency;
            $stripe_secret = PaymentSetting::find(1)->stripeSecretKey;  
            Stripe\Stripe::setApiKey($stripe_secret);

             $account = Stripe\Account::create(array('type' => 'custom',"country" => "US","email" => Auth::user()->email,"external_account" => array("object" => "bank_account", "account_number" => $user->account_number, "country" => "US","currency" => $user->currency, "routing_number" => $user->bic, "account_holder_type" => "individual", "account_holder_name" => $user->account_holder_name ),'capabilities' => [
                'card_payments' => ['requested' => true],
                'transfers' => ['requested' => true],
              ]));
             $user->stripe_account_id=$account->id;

             $user->save();
            $banks_details = BankAccounts::where('user_id',Auth::user()->id)->get(['stripe_account_id','account_holder_name','account_holder_type','account_number','currency','bic','country','column7','column8','column9','column10'])->first();
            return response()->json(['status' => 200, 'msg' => 'success','banks_details' => $banks_details],200);
        }
        return response()->json(['status' => 400, 'msg' => 'No changes found'],400);
    }

    public function getBankDetails(Request $request) {

        $userId = Auth::user()->id;
        $bankUser = BankDetails::where('user_id',$userId)->where('user_type','0')->get()->first();

         return response()->json(['status' => 200, 'msg' => 'success','banks_details' => $bankUser],200);

    }


    public function getcountry(Request $request) {

        $countrylist = Currency::where('country_code','IS NOT',null)->orWhere('country_code','!=','')->get();

     return response()->json(['status' => 200, 'msg' => 'success','countrylist' => $countrylist],200);

    }


    public function gettaxs(Request $request) {

     
     $userId = Auth::user()->id;

     $taxFormS = TaxForms::where('user_id',$userId)->get();

     return response()->json(['status' => 200, 'msg' => 'success','data' => $taxFormS],200);

    }

    
    // Depricating the  updateBankAccounts function
    public function updateBankDetails(Request $request) {
        $validator = Validator::make($request->all(), [
            'account_number' => 'bail|required',
            'routing_number' => 'bail|required|digits:9',
            'account_holder_type' => 'nullable',
            'account_holder_name' => 'nullable',
            'country' => 'bail|required',
            'currency' => 'bail|required',
            'email' => 'bail|nullable|email',
            'managed' => 'bail|nullable|in:0,1',
        ]);
        $inputRecords = $request->all();
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'msg' => $validator->errors()],400);
        }
        $userId = Auth::user()->id;
        $bankUser = BankDetails::where('user_id',$userId)->where('user_type','0')->get()->first();
        if($bankUser && !is_null($bankUser->stripe_id)) {
            return response()->json(['status' => 400, 'msg' => 'You have already added bank details'],400);
        }
        if(!$bankUser) {
            $bankUser = new BankDetails();
        }
        $bankUser->user_id = $userId;
        if(array_key_exists('email',$inputRecords) && strlen(trim($inputRecords['email'])) > 0) {
            $bankUser->email = $inputRecords['email'];
        } else {
            $bankUser->email = Auth::user()->email;
        }
        if(array_key_exists('managed',$inputRecords) && strlen(trim($inputRecords['managed'])) > 0) {
            $bankUser->managed = $inputRecords['managed'].'';
        } else {
            $bankUser->managed = '0';
        }
        if(array_key_exists('account_holder_name',$inputRecords) && strlen(trim($inputRecords['account_holder_name'])) > 0) {
            $bankUser->account_holder_name = $inputRecords['account_holder_name'];
        } else {
            $bankUser->account_holder_name = Auth::user()->first_name.' '.Auth::user()->last_name;
        }
        if(array_key_exists('account_holder_type',$inputRecords) && strlen(trim($inputRecords['account_holder_type'])) > 0) {
            $bankUser->account_holder_type = $inputRecords['account_holder_type'];
        } else {
            $bankUser->account_holder_type = 'individual';
        }
        $bankUser->account_number = $inputRecords['account_number'];
        $bankUser->routing_number = $inputRecords['routing_number'];
        $bankUser->country = $inputRecords['country'];
        $bankUser->currency = $inputRecords['currency'];
        $bankUser->save();

        // Create Stripe account for the bank details provided
        try {
            $stripe_secret = env('STRIPE_SECRET');
            $stripe = new Stripe\StripeClient(
                $stripe_secret
            );
            $stripeData = [
                'type' => 'custom',
                'country' => $bankUser->country,
                'email' => $bankUser->email,
                'capabilities' => [
                  'card_payments' => ['requested' => true],
                  'transfers' => ['requested' => true],
                ],
                'business_type' => $bankUser->account_holder_type,
                'external_account' => [
                    'object' => 'bank_account',
                    'account_number' => $bankUser->account_number, 
                    'country' => $bankUser->country,
                    'currency' => $bankUser->currency, 
                    'routing_number' => $bankUser->routing_number, 
                    'account_holder_type' => $bankUser->account_holder_type, 
                    'account_holder_name' => $bankUser->account_holder_name,
                ]
            ];
            $stripeResponse = $stripe->accounts->create($stripeData);
            $bankUser->stripe_id = $stripeResponse['id'];
            $bankUser->stripe_response = json_encode($stripeResponse);
            $bankUser->save();
        } catch(\Exception $e) {
            \Log::info("Stripe Account creation Exception");
            \Log::info($e->getMessage());
        }
        $banks_details = BankDetails::where('user_id',Auth::user()->id)->get(['account_number','routing_number','account_holder_type','account_holder_name','country','currency','email','managed'])->first();
        return response()->json(['status' => 200, 'msg' => 'success','banks_details' => $banks_details],200);
    }

    public function updateTaxForms(Request $request) {

        \Log::info($request->all());
        
        $validator = Validator::make($request->all(), [
            'month' => 'bail|required|digits_between:1,12',
            'year' => 'bail|required|digits:4|integer|min:'.(date('Y')-2).'|max:'.(date('Y')),
            'file' => 'bail|required|file'
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'msg' => $validator->errors()],400);
        }
        $userId = Auth::user()->id;
        $taxForm = TaxForms::where('user_id',$userId)->where('month',$request->month)->where('year',$request->year)->get()->first();
        $imageName = $userId.'_'.$request->month.'_'.$request->year.'.'.$request->file->extension();
        $request->file->storeAs('images',$imageName);
        $request->file->move(public_path('images/upload'),$imageName);
        if(is_null($taxForm)) {
            $taxForm = new TaxForms;
            $taxForm->user_id = $userId;
            $taxForm->created_at = date("Y-m-d H:i:s");
        }
        $taxForm->file_path = env('APP_URL')."/images/upload/".$imageName;
        $taxForm->month = $request->month;
        $taxForm->year = $request->year;
        $taxForm->updated_at = date("Y-m-d H:i:s");
        $taxForm->save();
        $tax_forms = TaxForms::where('user_id',Auth::user()->id)->orderBy('year','desc')->orderBy('month','desc')->get(['month','year','file_path']);
        return response()->json(['status' => 200, 'msg' => 'success','tax_forms' => $tax_forms],200);
    }

    public function removeEventImage(Request $request) {
        $validator = Validator::make($request->all(), [
            'event_id' => 'bail|required|integer|exists:events,id',
            'id' => 'bail|required|integer|exists:pictures,id',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'msg' => $validator->errors()],400);
        }
        $picture = ApplicablePhotos::where('id',$request->id)->where('event_id',$request->event_id)->get()->first();
        if(!is_null($picture)) {
            try{
                if(file_exists(public_path($picture->image_path))) {
                    unlink(public_path($picture->image_path));
                }
            } catch (\Exception $e) {
            }
            $picture->delete();
            return response()->json(['status' => 200, 'msg' => 'success'],200);
        }
        return response()->json(['status' => 400, 'msg' => 'Record not found'],400);
    }

    public function updateTicketStatus(Request $request) {
        $validator = Validator::make($request->all(), [
            'booking_id' => 'bail|required|integer|exists:orders,id',
            'status' => 'bail|required|in:approved,declined',
        ]);
        if ($validator->fails()) {
            return response()->json(['status' => 400, 'msg' => $validator->errors()],400);
        }
        $order = Order::where('id',$request->booking_id)->get()->first();
        $order->order_status = $request->status;
        $order->updated_at = date("Y-m-d H:i:s");
        $order->save();
        return response()->json(['status' => 200, 'msg' => 'success'],200);
    }

    public function addTickets(Request $request) {
        $request->validate([
            'name' => 'bail|required',
            'quantity' => 'bail|required|numeric|min:1',
            'start_time' => 'nullable|date_format:Y-m-d H:i:s',
            'end_time' => 'nullable|date_format:Y-m-d H:i:s',
            'type' => 'bail|required|in:free,paid',
            'ticket_per_order'=> 'bail|required|numeric|max:'.$request->quantity.'|min:1',
            'price' =>  'bail|required_if:type,paid|numeric',
            'service_id' => 'bail|required|integer|exists:events,id',
        ]);
        if(isset($request->start_time) && isset($request->end_time) && strtotime($request->start_time) > strtotime($request->end_time)) {
            return response()->json(['status' => 400, 'msg' => 'Event Ticket end date and time can not be lesser than start date & time'],400);
        }
        $event = Event::find($request->service_id);
        if(!isset($request->start_time) || !isset($request->end_time)) {
            $request->merge(['start_time' => $event->start_date, 'end_time' => $event->end_date]);
        }
        $data = $request->all();
        if(strtolower(trim($request->type))==="free"){
            $data['price'] = 0;
        }
        $data['ticket_number'] = chr(rand(65,90)).chr(rand(65,90)).'-'.rand(999,10000);
        $data['user_id'] = $event->user_id;
        $data['event_id'] = $event->id;
        $data['status'] = 1;
        $data['created_at'] = date("Y-m-d H:i:s");
        $data['updated_at'] = date("Y-m-d H:i:s");
        Ticket::create($data);
        return response()->json(['status' => 200, 'msg' => 'success'],200);
    }

    public function listTickets(Request $request) {
        $validation = Validator::make($request->all(),[
            'service_id' => 'bail|required|integer|exists:events,id',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Record not found.', 'tickets' => null,'status'=>400], 400);
        }
        $event = Event::where('id',$request->service_id)->where('user_id',Auth::user()->id)->get()->first();
        if(is_null($event)) {
            return response()->json(['status' => 400, 'tickets' => null, 'msg' => 'Record not found'],400);
        }
        $tickets = ['completed' => [], 'upcoming' => []];
        $tickets['completed'] = Ticket::where('event_id',$request->service_id)->where('status',1)->orderBy('start_time','desc')->get();
        $tickets['upcoming'] = Ticket::where('event_id',$request->service_id)->where('status',1)->orderBy('start_time','desc')->get();
        return response()->json(['status' => 200, 'msg' => 'success', 'tickets' => $tickets],200);
    }

    public function listTicketDetails(Request $request) {
        $validation = Validator::make($request->all(),[
            'service_id' => 'bail|required|integer|exists:events,id',
            'ticket_id' => 'bail|required|integer|exists:tickets,id',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Record not found.', 'tickets' => null,'status'=>400], 400);
        }
        $event = Event::where('id',$request->service_id)->where('user_id',Auth::user()->id)->get()->first();
        if(is_null($event)) {
            return response()->json(['status' => 400,'tickets' => null, 'msg' => 'Record not found'],400);
        }
        $ticket = Ticket::where('event_id',$request->service_id)->where('id',$request->ticket_id)->get()->first();
        return response()->json(['status' => 200, 'msg' => 'success', 'tickets' => $ticket],200);
    }

    public function updateTicketDetails(Request $request) {
        $validation = Validator::make($request->all(),[
            'service_id' => 'bail|required|integer|exists:events,id',
            'ticket_id' => 'bail|required|integer|exists:tickets,id',
            'name' => 'bail|required',
            'quantity' => 'bail|required|numeric|min:1',
            'start_time' => 'nullable|date_format:Y-m-d H:i:s',
            'end_time' => 'nullable|date_format:Y-m-d H:i:s',
            'type' => 'bail|required|in:free,paid',
            'ticket_per_order'=> 'bail|required|numeric|max:'.$request->quantity.'|min:1',
            'price' =>  'bail|required_if:type,paid|numeric',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Record not found.', 'tickets' => null,'status'=>400], 400);
        }
        $event = Event::where('id',$request->service_id)->where('user_id',Auth::user()->id)->get()->first();
        if(is_null($event)) {
            return response()->json(['status' => 400,'tickets' => null, 'msg' => 'Record not found'],400);
        }
        if(strtolower(trim($request->type))==="free"){
            $request->merge(['price' => 0]);
        }
        $ticket = Ticket::where('event_id',$request->service_id)->where('id',$request->ticket_id)->get()->first();
        if(strtotime('now') > strtotime($ticket->start_time)) {
            return response()->json(['status' => 400, 'msg' => 'Event Ticket is already expired'],400);
        }
        if(isset($request->start_time) && strtotime('now') > strtotime($request->start_time)) {
            return response()->json(['status' => 400, 'msg' => 'Event Ticket start date and time can not be past date & time'],400);
        }
        if(isset($request->start_time) && isset($request->end_time) && strtotime($request->start_time) > strtotime($request->end_time)) {
            return response()->json(['status' => 400, 'msg' => 'Event Ticket end date and time can not be lesser than start date & time'],400);
        }
        if(!$request->start_time) {
            $request->merge(['start_time' => null]);
        }
        if(!$request->end_time) {
            $request->merge(['end_time' => null]);
        }
        $ticket = Ticket::find($request->ticket_id)->update(['name'=>$request->name, 'quantity'=>$request->quantity, 'start_time'=>$request->start_time, 'end_time'=>$request->end_time, 'type'=>$request->type, 'ticket_per_order'=>$request->ticket_per_order, 'price'=>$request->price]);
        $ticket = Ticket::find($request->ticket_id);
        return response()->json(['status' => 200, 'msg' => 'success', 'tickets' => $ticket],200);
    }

    public function logout() {
        $user = Auth::user()->token();
        $user->revoke();
        return response()->json(['status' => 200, 'msg' => 'success'],200);
    }

    private function getDashboardData() {
        $taxDetails = Tax::where('status',1)->where('user_id',Auth::user()->id)->whereIn('name',['boat','game','event'])->get(['name','price']);
        $taxData = [
            'boat' => is_null($taxDetails->where('name','boat')->first()) ? 0.00 : (float)$taxDetails->where('name','boat')->first()->price,
            'event' => is_null($taxDetails->where('name','event')->first()) ? 0.00 : (float)$taxDetails->where('name','event')->first()->price,
            'game' => is_null($taxDetails->where('name','game')->first()) ? 0.00 : (float)$taxDetails->where('name','game')->first()->price,
        ];
        return [
            'description' => '',
            'data' => [
                'profile' => [
                    'image' => Auth::user()->image,
                    'phone' => Auth::user()->phone,
                    'email' => Auth::user()->email,
                    'first_name' => Auth::user()->first_name,
                    'last_name' => Auth::user()->last_name,
                    'full_name' => Auth::user()->first_name.' '.Auth::user()->last_name,
                    'address' => Auth::user()->address_1.', '.Auth::user()->address_2.', '.Auth::user()->city.', '.Auth::user()->state.', '.Auth::user()->pincode,
                    'address_1' => Auth::user()->address_1,
                    'address_2' => Auth::user()->address_2,
                    'city' => Auth::user()->city,
                    'state' => Auth::user()->state,
                    'pincode' => Auth::user()->pincode,
                    'country' => Auth::user()->country,
                    'code' => Auth::user()->code,
                ],
                'total_earnings' => Order::getOrganisationTotalEarnings().'',
                'total_bookings' => Order::getOrganisationTotalBookings().'',
                'banks_details' => BankAccounts::where('user_id',Auth::user()->id)->get(['column1','column2','column3','column4','column5','column6','column7','column8','column9','column10'])->first(),
                'payment_settings' => [
                    'tax' => $taxData,
                ],
                'payment_history' => Ticket::paymentHistory(),
                'tax_forms' => TaxForms::where('user_id',Auth::user()->id)->orderBy('year','desc')->orderBy('month','desc')->get(['month','year','file_path']),
            ],
        ];
    }

    private function getListingData() {
        $data = [
            'description' => '',
            'data' => [
                'boats' => [
                    'active_count' => Event::where('user_id',Auth::user()->id)->where('applicable_type',1)->where('status',1)->count(),
                    'inactive_count' => Event::where('user_id',Auth::user()->id)->where('applicable_type',1)->where('status',0)->count(),
                    'total' => Event::where('user_id',Auth::user()->id)->where('applicable_type',1)->count(),
                    'active' => Event::where('events.user_id',Auth::user()->id)
                        ->where('events.applicable_type',1)
                        ->where('events.status',1)
                        ->with('pictures')
                        ->with('dock_type')
                        ->with('maina_name')
                        ->with('country')
                        ->with('availability_type')
                        ->with('boat_operator')
                        ->with('cancellation_policy')
                        ->with('payout_type')
                        ->with('allowed_items')
                        ->with('boat_prices')
                        ->orderBy('created_at','desc')
                        ->get()
                        ->makeHidden(['rate','applicable_type','type','user_id','scanner_id','start_time','end_time','image','gallery','security','tags','event_status','is_deleted','created_at','updated_at','venue_title','is_public','totalTickets','soldTickets'])
                        ->toArray(),
                    'inactive' => Event::where('events.user_id',Auth::user()->id)
                        ->where('events.applicable_type',1)
                        ->where('events.status',0)
                        ->with('pictures')
                        ->with('dock_type')
                        ->with('maina_name')
                        ->with('country')
                        ->with('availability_type')
                        ->with('boat_operator')
                        ->with('cancellation_policy')
                        ->with('payout_type')
                        ->with('allowed_items')
                        ->with('boat_prices')
                        ->orderBy('created_at','desc')
                        ->get()
                        ->makeHidden(['rate','applicable_type','type','user_id','scanner_id','start_time','end_time','image','gallery','security','tags','event_status','is_deleted','created_at','updated_at','venue_title','is_public','totalTickets','soldTickets'])
                        ->toArray(),
                ],
                'events' => [
                    'active_count' => Event::where('user_id',Auth::user()->id)->where('applicable_type',2)->where('status',1)->count(),
                    'inactive_count' => Event::where('user_id',Auth::user()->id)->where('applicable_type',2)->where('status',0)->count(),
                    'total' => Event::where('user_id',Auth::user()->id)->where('applicable_type',2)->count(),
                    'active' => Event::where('events.user_id',Auth::user()->id)
                        ->where('events.applicable_type',2)
                        ->where('events.status',1)
                        ->with('pictures')
                        ->with('availability_type')
                        ->orderBy('created_at','desc')
                        ->get()
                        ->makeHidden(['rate','maina_name','slip_number','dock_type','advance_notice','boat_operator','multiple_booking_allowed','time_gap','payout_type','make_company','boat_model','applicable_type','type','user_id','scanner_id','image','gallery','security','tags','event_status','is_deleted','created_at','updated_at']),
                    'inactive' => Event::where('events.user_id',Auth::user()->id)
                        ->where('events.applicable_type',2)
                        ->where('events.status',0)
                        ->with('pictures')
                        ->with('availability_type')
                        ->orderBy('created_at','desc')
                        ->get()
                        ->makeHidden(['rate','maina_name','slip_number','dock_type','advance_notice','boat_operator','multiple_booking_allowed','time_gap','payout_type','make_company','boat_model','applicable_type','type','user_id','scanner_id','image','gallery','security','tags','event_status','is_deleted','created_at','updated_at']),
                ],
                'games' => [
                    'active_count' => Event::where('user_id',Auth::user()->id)->where('applicable_type',3)->where('status',1)->count(),
                    'inactive_count' => Event::where('user_id',Auth::user()->id)->where('applicable_type',3)->where('status',0)->count(),
                    'total' => Event::where('user_id',Auth::user()->id)->where('applicable_type',3)->count(),
                    'active' => Event::where('events.user_id',Auth::user()->id)
                        ->where('events.applicable_type',3)
                        ->where('events.status',1)
                        ->with('pictures')
                        ->with('availability_type')
                        ->orderBy('created_at','desc')
                        ->get()
                        ->makeHidden(['rate','maina_name','slip_number','dock_type','advance_notice','boat_operator','multiple_booking_allowed','time_gap','payout_type','make_company','boat_model','applicable_type','type','user_id','scanner_id','image','gallery','security','tags','event_status','is_deleted','created_at','updated_at']),
                    'inactive' => Event::where('events.user_id',Auth::user()->id)
                        ->where('events.applicable_type',3)
                        ->where('events.status',0)
                        ->with('pictures')
                        ->with('availability_type')
                        ->orderBy('created_at','desc')
                        ->get()
                        ->makeHidden(['rate','maina_name','slip_number','dock_type','advance_notice','boat_operator','multiple_booking_allowed','time_gap','payout_type','make_company','boat_model','applicable_type','type','user_id','scanner_id','image','gallery','security','tags','event_status','is_deleted','created_at','updated_at']),
                ],
            ]
        ];
        foreach($data['data']['boats']['active'] as $key => $value){
            if($value['allowed_items']){
                $data['data']['boats']['active'][$key]['allowed_items'] = array_column($value['allowed_items'], 'allowed_items');
            }
            if($value['availability_slots']){
                try{
                    $data['data']['boats']['active'][$key]['availability_slots'] = json_decode($value['availability_slots'],true);
                } catch (\Exception $e){
                    $data['data']['boats']['active'][$key]['availability_slots'] = [];
                }
            }
            if($value['advance_notice']){
                try{
                    $data['data']['boats']['active'][$key]['advance_notice'] = json_decode($value['advance_notice'],true);
                } catch (\Exception $e){
                    $data['data']['boats']['active'][$key]['advance_notice'] = [];
                }
            }
        }
        foreach($data['data']['boats']['inactive'] as $key => $value){
            if($value['allowed_items']){
                $data['data']['boats']['inactive'][$key]['allowed_items'] = array_column($value['allowed_items'], 'allowed_items');
            }
            if($value['availability_slots']){
                try{
                    $data['data']['boats']['inactive'][$key]['availability_slots'] = json_decode($value['availability_slots'],true);
                } catch (\Exception $e){
                    $data['data']['boats']['inactive'][$key]['availability_slots'] = [];
                }
            }
            if($value['advance_notice']){
                try{
                    $data['data']['boats']['inactive'][$key]['advance_notice'] = json_decode($value['advance_notice'],true);
                } catch (\Exception $e){
                    $data['data']['boats']['inactive'][$key]['advance_notice'] = [];
                }
            }
        }
        foreach($data['data']['events']['active'] as $key => $value){
            if($value['availability_slots']){
                try{
                    $data['data']['events']['active'][$key]['availability_slots'] = json_decode($value['availability_slots'],true);
                } catch (\Exception $e){
                    $data['data']['events']['active'][$key]['availability_slots'] = [];
                }
            }
            if($value['advance_notice']){
                try{
                    $data['data']['events']['active'][$key]['advance_notice'] = json_decode($value['advance_notice'],true);
                } catch (\Exception $e){
                    $data['data']['events']['active'][$key]['advance_notice'] = [];
                }
            }
        }
        foreach($data['data']['events']['inactive'] as $key => $value){
            if($value['availability_slots']){
                try{
                    $data['data']['events']['inactive'][$key]['availability_slots'] = json_decode($value['availability_slots'],true);
                } catch (\Exception $e){
                    $data['data']['events']['inactive'][$key]['availability_slots'] = [];
                }
            }
            if($value['advance_notice']){
                try{
                    $data['data']['events']['inactive'][$key]['advance_notice'] = json_decode($value['advance_notice'],true);
                } catch (\Exception $e){
                    $data['data']['events']['inactive'][$key]['advance_notice'] = [];
                }
            }
        }
        foreach($data['data']['games']['active'] as $key => $value){
            if($value['availability_slots']){
                try{
                    $data['data']['games']['active'][$key]['availability_slots'] = json_decode($value['availability_slots'],true);
                } catch (\Exception $e){
                    $data['data']['games']['active'][$key]['availability_slots'] = [];
                }
            }
            if($value['advance_notice']){
                try{
                    $data['data']['games']['active'][$key]['advance_notice'] = json_decode($value['advance_notice'],true);
                } catch (\Exception $e){
                    $data['data']['games']['active'][$key]['advance_notice'] = [];
                }
            }
        }
        foreach($data['data']['games']['inactive'] as $key => $value){
            if($value['availability_slots']){
                try{
                    $data['data']['games']['inactive'][$key]['availability_slots'] = json_decode($value['availability_slots'],true);
                } catch (\Exception $e){
                    $data['data']['games']['inactive'][$key]['availability_slots'] = [];
                }
            }
            if($value['advance_notice']){
                try{
                    $data['data']['games']['inactive'][$key]['advance_notice'] = json_decode($value['advance_notice'],true);
                } catch (\Exception $e){
                    $data['data']['games']['inactive'][$key]['advance_notice'] = [];
                }
            }
        }

        return $data;
    }



    public function storechat(Request $request){
        $validation = Validator::make($request->all(),[      
            'chat_id' => 'required',
            'message' => 'required', 
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => $validation->errors(),'status'=>400], 400);
        }


        $authuser = Auth::user();
        

        $chat = Chatroom::with(['userdata'])->where(['vendor_id'=>$authuser->id,'id'=>$request->chat_id])->first();


        $message = $request->message;
        $emailPattern = '/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i';
        $replaceEmail = "*********@email.com";
        $message = preg_replace($emailPattern, $replaceEmail, $message);
        $mobileNumberPattern = '/[0-9\-+]{8,}/';
        $replaceMobile = "8**8****88";
        $message = preg_replace($mobileNumberPattern, $replaceMobile, $message);
        $message = strip_tags($message);
        if(isset($chat->id)){

            $messages = Chatmessages::create(['user_id'=>$chat->user_id,'vendor_id'=>$chat->vendor_id,'message' => $message,"action"=>"text",'action_value'=>0,'chat_id'=>$chat->id,'is_vendor_replied'=>1]);


            $chat->messages;
        }

        
        return response()->json(['success'=>true,'msg'=>"created chat" ,'data' =>$chat], 200);


    }


    public function mychatlist(Request $request){
       

        $authuser = Auth::user();
        

        $chatlist = Chatroom::with(['userdata','vendor'])->where(['vendor_id'=>$authuser->id])->get();


        
        return response()->json(['success'=>true,'msg'=>"created chat" ,'data' =>$chatlist], 200);


    }


    public function chatmessage(Request $request){
        $validation = Validator::make($request->all(),[      
            'chat_id' => 'required'
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => $validation->errors(),'status'=>400], 400);
        }


        $authuser = Auth::user();
        

        $chat = Chatroom::with(['userdata'])->where(['vendor_id'=>$authuser->id,'id'=>$request->chat_id])->first();


        if(isset($chat->id)){


            $chat->messages;
        }

        
        return response()->json(['success'=>true,'msg'=>"created chat" ,'data' =>$chat], 200);


    }


    private function getBookingData() {
        $bookingDetails = Order::where('organization_id',Auth::user()->id)->with('ticket')->with('customer')->with('event')->orderBy('created_at','desc')->get();
        if(count($bookingDetails) > 0) {
            $bookingDetails = $bookingDetails->makeHidden(['org_commission','order_id','payment','payment_type','payment_status','payment_token','org_pay_status','review'])->toArray();
        }
        $pending = [];
        $completed = [];
        $allData = [];
        if(count($bookingDetails) > 0) {
            foreach ($bookingDetails as $key => $booking) {
                $booking['ticket']['start_time'] = date("jS M Y, h:i A",strtotime($booking['ticket']['start_time']));
                $bookingDetails[$key]['ticket']['start_time'] = $booking['ticket']['start_time'];
                $tempData = [
                    'booking_id' => $booking['id'],
                    'event_date' => $booking['ticket']['start_time'],
                    'name' => $booking['event']['name'],
                    'customer_name' => $booking['customer']['name'],
                    'no_of_customers' => $booking['quantity'],
                    'status' => is_null($booking['event']['cancellation_policy']) ? 'Accepted' : (((int)$booking['event']['cancellation_policy']['id'] === 3 && strtolower(trim($booking['order_status'])) === 'pending') ? 'Pending' : (($booking['event']['cancellation_policy']['id'] === 3 && strtolower(trim($booking['order_status'])) === 'declined') ? 'Declined' : 'Accepted')),
                    'cancellation_policy' => $booking['event']['cancellation_policy'],
                    'pictures' => $booking['event']['pictures'],
                ];
                if(strtotime($booking['ticket']['start_time']) > strtotime('now')) {
                    $pending[] = $tempData;
                } else {
                    $completed[] = $tempData;
                }
                $allData[] = $tempData;
            }
        }
        return [
            'description' => '',
            'data' => [
                'all' => [
                    'count' => count($allData),
                    'data' => $allData
                ],
                'pending' => [
                    'count' => count($pending),
                    'data' => $pending
                ],
                'completed' => [
                    'count' => count($completed),
                    'data' => $completed,
                ]
            ]
        ];
    }

    public function boat_make() {
        $boat_make = BoatModel::where('status','1')->select('make_company AS name')->groupBy('make_company')->get()->pluck('name')->toArray();
        return response()->json(['status' => 200, 'data' => $boat_make, 'msg' => 'success'],200);
    }

    public function boat_category(Request $request) {

        $validation = Validator::make($request->all(),[
            'make' => 'bail|required'
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid details provided.', 'data' => implode(",",$validation->errors()->all()),'status'=>400], 400);
        }
        $boat_category = BoatModel::where('status','1')->where('make_company',$request->make)->select('boat_category AS name')->groupBy('boat_category')->get()->pluck('name')->toArray();
        return response()->json(['status' => 200, 'msg' => 'success', 'data' => $boat_category],200);
    }

    public function boat_model(Request $request) {

        $validation = Validator::make($request->all(),[
            'make' => 'bail|required',
            'category' => 'bail|required',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid details provided.', 'data' => implode(",",$validation->errors()->all()),'status'=>400], 400);
        }
        $boat_model = BoatModel::where('status','1')->where('make_company',$request->make)->where('boat_category',$request->category)->select('name')->groupBy('name')->get()->pluck('name')->toArray();
        return response()->json(['status' => 200, 'msg' => 'success', 'data' => $boat_model],200);
    }

    public function eventAvailability(Request $request) {
        $validation = Validator::make($request->all(),[
            'event_id' => 'bail|required|integer|exists:events,id',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid details provided.', 'data' => implode(",",$validation->errors()->all()),'status'=>400], 400);
        }
        $record = Event::with('availabledays')->where('id',$request->event_id)->first()->toArray();
        $record['availability_slots'] = $record['availability_slots'] ? json_decode($record['availability_slots']) : [];
        return response()->json(['status' => 200, 'msg' => 'success', 'data' => $record],200);
    }

    public function splitPaymentCommission(Request $request) {
        $record = $splitPaymentCommission = Tax::where(['name' => 'split_payment_commission'])->first();
        $record = $record ? $record->price.'' : '0';
        return response()->json(['status' => 200, 'msg' => 'success', 'data' => $record],200);
    }

    public function termsPrivacyLinks() {
        $data = [
            'terms' => [
                'title' => 'Terms & Conditions',
                'url' => env('APP_URL').'/tnc',
            ],
            'privacy' => [
                'title' => 'Privacy Policy',
                'url' => env('APP_URL').'/privacy-policy',
            ],
        ];
        return response()->json(['status' => 200, 'msg' => 'success', 'data' => $data],200);
    }
}