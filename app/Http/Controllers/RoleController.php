<?php

namespace App\Http\Controllers;

use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Gate;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  
        abort_if(Gate::denies('role_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        
        // $permission1 = Permission::create(['name' => 'scanner_access']);
        // $permission1 = Permission::create(['name' => 'scanner_create']);
        // $permission1 = Permission::create(['name' => 'scanner_edit']);
        // $permission1 = Permission::create(['name' => 'scanner_delete']);
        $roles = Role::with(['permissions:id,name'])->get();              
        return view('admin.role.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        abort_if(Gate::denies('role_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $permissions = Permission::all();
        return view('admin.role.create', compact('permissions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required|unique:roles|max:255',                       
        ]);        
        $role = Role::create($request->all());  
        if($request->permissions != null){     
            $per =  Permission::whereIn('id', $request->permissions)->get();
            $role->syncPermissions($per);
        }
        return redirect()->route('roles.index')->withStatus(__('Role is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        //

        return view('admin.roles.show', compact('role'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //
        abort_if(Gate::denies('role_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $permissions = Permission::all();    
        return view('admin.role.edit', compact('permissions', 'role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Role $role)
    {
        //
        $request->validate([
            'name' => 'bail|required|unique:roles,name,' . $role->id . ',id',            
        ]);
        if($request->permissions != null){
            $per =  Permission::whereIn('id', $request->permissions)->get();
            $role->syncPermissions($per);
        }
         else {
            $role->syncPermissions([]);
        }
        return redirect()->route('roles.index')->withStatus(__('Role is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
        // abort_if(Gate::denies('role_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');      
        // $role->syncPermissions([]);
        // $role->delete();
        // return response()->json(['success' => true, 'msg' => __('Role Deleted Successfully.')], 200);
    }
}
