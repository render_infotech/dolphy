<?php

namespace App\Http\Controllers;

use App\Models\NotificationTemplate;
use App\Models\Notification;
use Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;

class NotificationTemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        abort_if(Gate::denies('notification_template_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $data = NotificationTemplate::OrderBy('id','DESC')->get();
        return view('admin.template.index', compact('data')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        abort_if(Gate::denies('notification_template_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.template.create'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'title' => 'bail|required',
            'subject' => 'bail|required',            
        ]);       
        $data = $request->all();             
        NotificationTemplate::create($data);
        return redirect()->route('notification-template.index')->withStatus(__('Template is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NotificationTemplate  $notificationTemplate
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NotificationTemplate  $notificationTemplate
     * @return \Illuminate\Http\Response
     */
    public function edit(NotificationTemplate $notificationTemplate)
    {
        //
        abort_if(Gate::denies('notification_template_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return response()->json(['success'=>true ,'data' =>$notificationTemplate ], 200);

        // return view('admin..edit', compact( 'category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\NotificationTemplate  $notificationTemplate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        
        $request->validate([
            'subject' => 'bail|required',            
        ]);       
        $data = $request->all();                   
        NotificationTemplate::find($id)->update($data);
        return redirect()->route('notification-template.index')->withStatus(__('Template is update successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NotificationTemplate  $notificationTemplate
     * @return \Illuminate\Http\Response
     */
    public function destroy(NotificationTemplate $notificationTemplate)
    {
        //
    }

    public function notification(){
        $notification = Notification::where('organizer_id',Auth::user()->id)->orderBy('id','DESC')->get();
        return view('admin.notification',compact('notification'));
    }

    public function deleteNotification($id){
        $data = Notification::find($id);        
        $data->delete();
        return redirect()->back();
    }
}
