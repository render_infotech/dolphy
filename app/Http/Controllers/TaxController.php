<?php

namespace App\Http\Controllers;

use App\Models\Tax;
use App\Models\User;
use App\Models\TaxForms;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Gate;

class TaxController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        abort_if(Gate::denies('tax_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $taxes = Tax::orderBy('id','DESC')->get();
        return view('admin.tax.index', compact('taxes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        abort_if(Gate::denies('tax_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.tax.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'bail|required',
            'price' => 'bail|required',
        ]);
        $data = $request->all();
        if(!Auth::user()->hasRole('admin')){
            $data['user_id'] = Auth::user()->id;
        }
        if(!isset($request->allow_all_bill)){ $data['allow_all_bill'] = 0; }
        $Tax = Tax::create($data);
        return redirect()->route('tax.index')->withStatus(__('Tax is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function show(Tax $tax)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function edit(Tax $tax)
    {
        //
        abort_if(Gate::denies('tax_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.tax.edit', compact('tax'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tax $tax)
    {
        //

        $request->validate([
            'name' => 'bail|required',
            'price' => 'bail|required',
        ]);
        $data = $request->all();
        if(!isset($request->allow_all_bill)){ $data['allow_all_bill'] = 0; }
        $Tax = Tax::find($tax->id)->update($data);
        return redirect()->route('tax.index')->withStatus(__('Tax is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tax  $tax
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tax $tax)
    {
        //
    }

    public function viewTaxForms(Request $request)
    {
        //
        $organisers = User::role('organization')->where('status',1)->orderBy('id','DESC')->get()->makeHidden(['created_at','updated_at']);
        $taxes = new TaxForms();
        if(!Auth::user()->hasRole('admin')){
            $organisers = $organisers = User::role('organization')->where('id',Auth::user()->id)->where('status',1)->orderBy('id','DESC')->get()->makeHidden(['created_at','updated_at']);
            $taxes = $taxes->where('user_id',Auth::user()->id);
        } else {
            if($request->has('organiser') && strlen(trim($request->organiser)) > 0){
                $taxes = $taxes->where('user_id',$request->organiser);
            }
            if($request->has('year') && strlen(trim($request->year)) > 0){
                $taxes = $taxes->where('year',$request->year);
            }
            if($request->has('month') && strlen(trim($request->month)) > 0){
                $taxes = $taxes->where('month',$request->month);
            }
        }
        $taxes = $taxes->with('organization')->orderBy('created_at','DESC')->paginate(50);
        return view('admin.tax.view_tax_forms', compact('taxes','organisers','request'));
    }
}
