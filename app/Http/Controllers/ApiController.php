<?php

namespace App\Http\Controllers;

use App\Models\CancellationPolicy;
use App\Models\CountryCode;
use App\Models\SmsEvents;
use Auth;
use App\Models\AppUser;
use App\Models\User;
use App\Models\Event;
use App\Models\Category;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\OrderTax;
use App\Models\Tax;
use App\Models\Review;
use App\Models\OrderChild;
use App\Http\Controllers\AppHelper;
use App\Models\PaymentSetting;
use App\Models\Ticket;
use App\Models\Currency;
use App\Models\Setting;
use App\Mail\ResetPassword;
use App\Mail\TicketBook;
use App\Mail\TicketBookOrg;
use App\Models\NotificationTemplate;
use App\Models\Notification;
use App\Models\EventReport;
use App\Models\Splitpayment;
use App\Models\Chatroom;
use App\Models\Chatmessages;
use Carbon\Carbon;
use OneSignal;
use Config;
use Stripe;
use DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;
use Validator;


class ApiController extends Controller
{
    public $radius = 5000000;
    //
    public function userLogin(Request $request){
        $data = $request->all();
        if(array_key_exists('email',$data) && is_numeric($data['email'])){
                $data['phone'] = $data['email'];
                unset($data['email']);
        }
        $request->merge($data);
        $validation = Validator::make($data,[
            'email' => 'bail|required_without:phone|email|exists:app_user,email',
            'phone' => 'bail|required_without:email|exists:app_user,phone',
            'code' => 'bail|required_without:email',
            'password' => 'bail|required',
            'device_token' => 'bail|required',                      
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => null,'status'=>400], 400);
        }
        $userdata = array( 'email' => $request->email, 'status' => 1, 'password' => $request->password );

        if(!empty($request->phone)){
            $userdata = array( 'phone' => $request->phone, 'code' => $request->code, 'status' => 1, 'password' => $request->password );
        }

        if(Auth::guard('appuser')->attempt($userdata)){
            $user = Auth::guard('appuser')->user();
            AppUser::find($user->id)->update(['device_token'=>$request->device_token]);
            $user['token'] = $user->createToken('eventRight')->accessToken;
            return response()->json(['msg' => 'Login successfully', 'data' => $user,'status'=>200], 200);
        }
        return response()->json(['msg' => 'Invalid credentials or account is not active. Try again later or contact admin.', 'data' => null,'status'=>400], 400);
    }

    public function logout() {
        $user = Auth::guard('userApi')->user()->token();
        $user->revoke();
        return response()->json(['status' => 200, 'msg' => 'success'],200);
    }

    public function userRegister(Request $request){
        $validation = Validator::make($request->all(),[
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'email' => 'bail|required|email|unique:app_user,email',
            'password' => 'bail|required|min:6',
            'confirm_password' => 'bail|required|min:6|same:password',
            'phone' => 'bail|required',
            'code' => 'bail|required',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => $validation->errors()->first(), 'stattus'=>400], 400);
        }
        $data = $request->all();
        $data['password'] =  Hash::make($request->password);
        $data['image'] ="defaultuser.png";
        $data['status'] =1;
        $data['name'] =$request->first_name;
        $data['provider'] ="LOCAL";
        $user = AppUser::create($data);
        $user['token'] = $user->createToken('eventRight')->accessToken;
        return $this->sendOTP($request,1);
    }

    public function userRegisterStep2(Request $request) {
        $validation = Validator::make($request->all(),[
            'code' => 'bail|required',
            'phone' => 'bail|required_without:email|exists:app_user,phone',
            'otp' => 'bail|required|digits:'. (int)env('OTP_LENGTH'),
            'device_token' => 'bail|required'
        ]);

        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid details provided, Kindly try again!!!', 'status'=>400], 400);
        }
        $mobile = $request->phone;
        $mobile = str_ireplace(" ","", $mobile);
//        $mobile = str_ireplace("-","", $mobile);
        $request->merge(['mobile' => $mobile]);
        $mobileData = $request->code.$mobile;
        $isOTPVerified = false;
        $status = ['msg' => 'Invalid OTP, Kindly try again!!!', 'status'=>400, 'data' => null];
        $dbRecords = SmsEvents::where('mobile',$mobileData)->where('verified_at',NULL)->orderBy('updated_at','desc')->first();
        if($dbRecords) {
            if($dbRecords->attempts >= (int)env('OTP_VERIFY_ATTEMPTS')) {
                $status = ['msg' => 'Your account is locked due to maximum number of login attempts. Try again after '.timeDiffInMins(date("Y-m-d H:i:s"),date("d M Y H:i:s",strtotime($dbRecords->valid_till))), 'status'=>400, 'data' => null];
            } else if (strtotime($dbRecords->valid_till) < strtotime('now')) {
                $status = ['msg' => 'Your OTP is expired, Kindly try with new OTP.', 'status'=>400, 'data' => null];
            } else if ((int)$dbRecords->otp === (int)$request->otp) {
                $isOTPVerified = true;
            }
            $dbRecords->attempts = $dbRecords->attempts+1;
            $dbRecords->updated_at = date("Y-m-d H:i:s");
            if($isOTPVerified) {
                $dbRecords->verified_at = date("Y-m-d H:i:s");
            }
            $dbRecords->save();
        }
        if(!$isOTPVerified) {
            return response()->json($status, 400);
        }
        $userRecord = AppUser::where('phone',$request->mobile)->orderBy('updated_at','desc')->first();
        if(!$userRecord) {
            return response()->json(['msg' => 'No Records found with the provided Phone number. Kindly register!!!', 'status'=>200, 'data' => null],200);
        } else {
            Auth::guard('appuser')->login($userRecord,true);
        }
        $user = Auth::guard('appuser')->user();
        AppUser::find($user->id)->update(['device_token'=>$request->device_token]);
        $user['token'] = $user->createToken('eventRight')->accessToken;
        return response()->json(['msg' => 'Registration successful', 'data' => $user,'status'=>200], 200);
    }

    public function sendOTP(Request $request,$isInternal=0){
        $validation = Validator::make($request->all(),[
            'phone' => 'bail|required_without:email|exists:app_user,phone',
            'email' => 'bail|required_without:phone|email|exists:app_user,email',
        ]);
        $mobile = $request->phone;
        $mobile = str_ireplace(" ","", $mobile);
//        $mobile = str_ireplace("-","", $mobile);
        $request->merge(['mobile' => $mobile]);
        $mobileData = $mobile;
        if($isInternal) {
            $validation = Validator::make($request->all(),[
                'code' => 'bail|required',
                'phone' => 'bail|required|exists:app_user,phone',
            ]);
            $mobileData = $request->code.$mobile;
        }else{
            if(isset($request->code) && $request->code != "") {
                $mobile = $request->code.$mobile;
            }
        }
        $generateOTP = false;
        $otp = "";
        if($validation->fails() && !$isInternal) {
            return response()->json(['msg' => 'Invalid details provided', 'data' => null,'status'=>400], 400);
        }
        if(!$isInternal) {
            $mobileData = $mobile;
        }
        $status = ['msg' => 'OTP sent successfully', 'status'=>200];
        $dbRecords = SmsEvents::where('mobile',$mobileData)->where('verified_at',NULL)->orderBy('updated_at','desc')->first();
        if($dbRecords) {
            if($dbRecords->resend_attempts >= (int)env('OTP_RESEND_ATTEMPTS') && strtotime($dbRecords->valid_till) > strtotime('now')) {
                $status = ['msg' => 'You have tried maximum number of resends. Try again after '.timeDiffInMins(date("Y-m-d H:i:s"), date("d M Y H:i:s",strtotime($dbRecords->valid_till))), 'status'=>400];
            } else if ($dbRecords->resend_attempts >= (int)env('OTP_RESEND_ATTEMPTS') && strtotime($dbRecords->valid_till) <= strtotime('now')) {
                goto generateLoginOTP;
            } else if ($dbRecords->resend_attempts > 0 && $dbRecords->resend_attempts < (int)env('OTP_RESEND_ATTEMPTS') && strtotime($dbRecords->valid_till) > strtotime('now')) {
                $otp = $dbRecords->otp;
                $generateOTP = true;
            } else if ($dbRecords->resend_attempts > 0 && $dbRecords->resend_attempts < (int)(env('OTP_RESEND_ATTEMPTS') - 1) && strtotime($dbRecords->valid_till) <= strtotime('now')) {
                goto generateLoginOTP;
            } else {
                goto generateLoginOTP;
            }
        } else {
            generateLoginOTP:
            $otp = generateOTP((int)env('OTP_LENGTH'));
            if($mobile == '9999999999') {   //For Play store and App store verification
                $otp = '1234';
            }
            $generateOTP = true;
        }
        if($generateOTP) {
            $smsContent = trans("auth.otp_login_sms");
            $smsContent = str_replace("#OTP",$otp,$smsContent);
            $validTill = date("Y-m-d H:i:00",strtotime(env("OTP_VALIDITY","+15 minutes")));
            if($dbRecords) {
                if(strtotime($dbRecords->valid_till) < strtotime('now')) {
                    $dbRecords = NULL;
                    goto generateLoginOTP;
                }
                $dbRecords->resend_attempts = $dbRecords->resend_attempts+1;
                $dbRecords->valid_till = $validTill;
                $smsContent = str_replace("#TIME",date("d M Y h:i A", strtotime($validTill)),$smsContent);
                $dbRecords->sms_content = $smsContent;
                $dbRecords->otp = $otp;
                $dbRecords->updated_at = date("Y-m-d H:i:s");
                $dbRecords->save();
                $status["msg"] = "OTP sent successfully. You are left with ".((int)env('OTP_RESEND_ATTEMPTS') -$dbRecords->resend_attempts)." resend attempts";
            } else {
                $dbRecords = new SmsEvents();
                $dbRecords->mobile = $mobileData;
                $dbRecords->otp = $otp;
                $dbRecords->is_otp = '1';
                $dbRecords->attempts = 0;
                $dbRecords->resend_attempts = 0;
                $dbRecords->verified_at = NULL;
                $dbRecords->valid_till = $validTill;
                $smsContent = str_replace("#TIME",date("d M Y h:i", strtotime($validTill)),$smsContent);
                $dbRecords->sms_content = $smsContent;
                $dbRecords->created_at = date("Y-m-d H:i:s");
                $dbRecords->updated_at = date("Y-m-d H:i:s");
                $dbRecords->save();
            }
            $smsStatus = sendSMS($dbRecords->mobile, $smsContent);
            if(!$smsStatus) {
                if($dbRecords->resend_attempts > 0) {
                    $dbRecords->resend_attempts = $dbRecords->resend_attempts-1;
                    $dbRecords->updated_at = date("Y-m-d H:i:s");
                    $dbRecords->save();
                }
                $status["msg"] = "Something went wrong, try again later!!!";
                $status["status"] = 400;
            }
        }
        return response()->json($status,$status['status']);
    }

    public function organization(){        
        $users = User::role('organization')->where('status',1)->orderBy('id','DESC')->get()->makeHidden(['created_at','updated_at']);

        foreach ($users as $value) {
           if(Auth::check()){
                if(in_array(Auth::user()->id,$value->followers)){
                    $value->isFollow = true;  
                }
                else{
                    $value->isFollow = false;  
                }
           } 
           else{
                $value->isFollow = false;  
           }          
        }      
        return response()->json(['msg' => null, 'data' => $users,'success'=>true], 200); 
    }
    
    public function events(Request $request){ 
        $request->validate([
            'lat' => 'bail|required',
            'lang' => 'bail|required',                                      
        ]);    
        $lat = $request->lat;
        $lang = $request->lang;
        $event = array();
        $results = DB::select(DB::raw('SELECT id,name, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lang ) - radians(' . $lang . ') ) + sin( radians(' . $lat . ') ) * sin( radians(lat) ) ) ) AS distance FROM events HAVING distance < '.$this->radius.'  ORDER BY distance'));       
        if(count($results)>0){
            foreach ($results as $q) {
                array_push($event, $q->id);
            }
        }
        
        $timezone = Setting::find(1)->timezone;
        $date = Carbon::now($timezone);
        
        $online = Event::where([['status',1],['is_deleted',0],['event_status','Pending'],['type','online'],['end_time', '>',$date->format('Y-m-d H:i:s')]])->get();
        foreach ($online as $value) {
            array_push($event, $value->id);
        }      
        
        $data['events'] = Event::with(['ticket'])
        ->where([['status',1],['is_deleted',0],['event_status','Pending'],['end_time', '>',$date->format('Y-m-d H:i:s')]])        
        ->whereIn('id',$event)
        ->orderBy('start_time','ASC')->get()->makeHidden(['created_at','updated_at']);    
         
        foreach ($data['events'] as $value) {
            $value->boat_prices;
            $value->description =  str_replace("&nbsp;", " ", strip_tags($value->description));           
            $value->time = $value->start_time->format('d F Y h:i a');
            if(Auth::guard('userApi')->check()){
                if(in_array($value->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                    $value->isLike = true;  
                }
                else{
                    $value->isLike = false; 
                }
            }
            else{
                $value->isLike = false;  
            } 
        }          
        $data['top'] = Event::with(['ticket'])->whereIn('id',$event)->where([['status',1],['is_deleted',0],['start_time', '>=',$date->format('Y-m-d H:i:s')]])->orderBy('start_time','ASC')->first();  
        if($data['top']!=null){
            $data['top']->description =  str_replace("&nbsp;", " ", strip_tags($data['top']->description));           
            $data['top']->time = $data['top']->start_time->format('d F Y h:i a');
            if(Auth::guard('userApi')->check()){
                if(in_array($data['top']->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                    $data['top']->isLike = true;  
                }
                else{
                    $data['top']->isLike = false; 
                }
            }
            else{
                $data['top']->isLike = false;  
            }
        }
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200); 
    }



    public function EventFrmCategory(Request $request){
        $request->validate([           
            'category_id' => 'bail|required',                        
        ]);
        $timezone = Setting::find(1)->timezone;       
        $date = Carbon::now($timezone); 
        $data =  Event::where([['status',1],['is_deleted',0],['category_id',$request->category_id],['start_time', '>=',$date->format('Y-m-d H:i:s')]]);
        if($request->lat!=null &&  $request->lang!=null){               
            $lat = $request->lat;
            $lang = $request->lang;
            $event = array();
            $results = DB::select(DB::raw('SELECT id,name, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lang ) - radians(' . $lang . ') ) + sin( radians(' . $lat . ') ) * sin( radians(lat) ) ) ) AS distance FROM events HAVING distance < '.$this->radius.'  ORDER BY distance'));                 
            if(count($results)>0){
                foreach ($results as $q) {
                    array_push($event, $q->id);
                }
            }           
            $data = $data->whereIn('id',$event);            
        }

        $data= $data->orderBy('id','DESC')->get();       
        foreach ($data as $value) {
            $value->boat_prices;
            $value->description =  str_replace("&nbsp;", " ", strip_tags($value->description));           
            $value->time = $value->start_time->format('d F Y h:i a');
            if(Auth::guard('userApi')->check()){
                if(in_array($value->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                    $value->isLike = true;  
                }
                else{
                    $value->isLike = false; 
                }
            }
            else{
                $value->isLike = false;  
            }
        }  
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200);   
    }

    public function searchFreeEvent(Request $request){
        $request->validate([           
            'free_event' => 'bail|required|numeric',
            'category_id' => 'bail|required|numeric',
        ]); 
        $timezone = Setting::find(1)->timezone;       
        $date = Carbon::now($timezone); 
        $data = Event::where([['status',1],['is_deleted',0],['category_id',$request->category_id],['start_time', '>=',$date->format('Y-m-d H:i:s')]]);
        if($request->free_event==1){
            $ar_event = array();
            $ar = Event::where([['status',1],['is_deleted',0],['start_time', '>=',$date->format('Y-m-d H:i:s')]])->get(); 
            foreach ($ar as $value) {
                $ticket = Ticket::where([['status',1],['is_deleted',0],['event_id',$value->id],['type','free']])->get();
                if(count($ticket)>0){
                    array_push($ar_event, $value->id);
                }
            }
            $data = $data->whereIn('id',$ar_event);
        }
        if($request->lat!=null &&  $request->lang!=null){               
            $lat = $request->lat;
            $lang = $request->lang;
            $event = array();
            $results = DB::select(DB::raw('SELECT id,name, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lang ) - radians(' . $lang . ') ) + sin( radians(' . $lat . ') ) * sin( radians(lat) ) ) ) AS distance FROM events HAVING distance < '.$this->radius.'  ORDER BY distance'));                 
            if(count($results)>0){
                foreach ($results as $q) {
                    array_push($event, $q->id);
                }
            }           
            $data = $data->whereIn('id',$event);            
        }
        $data= $data->orderBy('id','DESC')->get(); 
        foreach ($data as $value) {
            $value->boat_prices;
            $value->description =  str_replace("&nbsp;", " ", strip_tags($value->description));           
            $value->time = $value->start_time->format('d F Y h:i a');
            if(Auth::guard('userApi')->check()){
                if(in_array($value->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                    $value->isLike = true;  
                }
                else{
                    $value->isLike = false; 
                }
            }
            else{
                $value->isLike = false;  
            }
        }  
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200); 
    }

    public function category(){        
        $data = Category::where('status',1)->orderBy('id','DESC')->get();
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200); 
    }
    
    public function organizationDetail($id){
        $data = User::find($id);
        $data->event = Event::where([['user_id',$id],['is_deleted',0],['status',1]])->orderBy('id','DESC')->get();
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200); 
    }

    public function eventDetail($id){
        
        $data = Event::with(['ticket','pictures','dock_type','maina_name','country','availability_type','boat_operator','cancellation_policy','payout_type','allowed_items']);
        $data = $data->find($id)->makeHidden(['created_at','updated_at']);
        $data->hasTag = explode(',',$data->tags);
        $data->description =  str_replace("&nbsp;", " ", strip_tags($data->description));           
        $data->recent_event = Event::where([['category_id',$data->category_id],['is_deleted',0],['status',0]])->orderBy('start_time','ASC')->get();
        $data->date = "";
        $data->endDate = "";
        $data->startTime = ""; 
        $data->endTime = "";
        if($data->applicable_type != 1) {
            $data->date = $data->start_time->format('d F Y');
            $data->endDate = $data->end_time->format('d F Y');
            $data->startTime = $data->start_time->format('h:i a'); 
            $data->endTime = $data->end_time->format('h:i a');
        }
        $data->gallery = array_filter(explode(',',$data->gallery));
        foreach ( $data->recent_event  as $value) {
            $value->boat_prices;
            $value->time = "";
            if($data->applicable_type != 1) {
                $value->time = $value->start_time->format('d F Y h:i a');
            }
            if(Auth::guard('userApi')->check()){                
                if(in_array($value->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                    $value->isLike = true;  
                }
                else{
                    $value->isLike = false; 
                }
            }
            else{
                $value->isLike = false;  
            }
        }
        if(Auth::guard('userApi')->check()){
            if(in_array(Auth::guard('userApi')->user()->id,$data->organization->followers)){
                $data->organization->isFollow = true;  
            }
            else{
                $data->organization->isFollow = false;  
            }
            if(in_array($id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                $data->isLike = true;  
            }
            else{
                $data->isLike = false; 
            }
        }
        else{
            $data->organization->isFollow = false;  
            $data->isLike = false; 
        }
        $all_ticket = Ticket::where([['event_id',$id],['is_deleted',0],['status',1]])->sum('quantity');
        $use_ticket = Order::where('event_id',$id)->sum('quantity');
        if($all_ticket == 0){
            $data->sold_out =false;
        }
        else{
            if($all_ticket == $use_ticket){
                $data->sold_out = true;
            }
            else{
                $data->sold_out =false;
            }
        }
        $ticket = $data->ticket;
        $updatedTicket = [];
        foreach ($ticket as $key => $value) {
            if(strtotime($value->start_time) > strtotime(Carbon::now())){
                $updatedTickets = $value->toArray();
                $updatedTickets['start_time'] = $value->start_time->format('d F Y h:i a');
                $updatedTickets['end_time'] = $value->end_time->format('d F Y h:i a');
                array_push($updatedTicket, $updatedTickets);
            }
        }
        $data = $data->toArray();
        $data['ticket'] = $updatedTicket;
       
        return response()->json(['msg' => 'success', 'data' => $data,'status'=>200], 200); 
    }

    public function ticketDetail($id){
        
        $data = Ticket::find($id)->makeHidden(['created_at','updated_at']);
        $event = Event::find($data->event_id);
        $data->event_name = $event->name;
        $data->organization = User::find($event->user_id)->name;     
        $data->use_ticket = (int)Order::where('ticket_id',$id)->sum('quantity');   
        $data->startTime = $data->start_time->format('Y-m-d h:i a');            
        $data->endTime = $data->end_time->format('Y-m-d h:i a'); 
        if($data->quantity <= $data->use_ticket ){
            $data->sold_out = true; 
        }
        else{
            $data->sold_out = false; 
        }
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200); 
    }

    public function eventTickets($id){
        $event = Event::find($id);
        $data['event_name'] = $event->name;
    
        $data['organization'] = User::find($event->user_id)->name;
        $timezone = Setting::find(1)->timezone;       
        $date = Carbon::now($timezone);                     
        
        // ['start_time', '>=',$date->format('Y-m-d H:i:s')],['end_time', '>=',$date->format('Y-m-d H:i:s')]    
        $data['ticket'] = Ticket::where([['event_id',$id],['is_deleted',0],['status',1],['end_time', '>=',$date->format('Y-m-d H:i:s')],['start_time', '<=',$date->format('Y-m-d H:i:s')]])
        ->orderBy('id','DESC')->get();

        foreach ($data['ticket'] as $value) {
            $value->use_ticket = Order::where('ticket_id',$value->id)->sum('quantity');               
            $value->startTime = $value->start_time->format('Y-m-d h:i a');            
            $value->endTime = $value->end_time->format('Y-m-d h:i a');  
            if($value->use_ticket==$value->quantity){
                $value->sold_out = true;
            }          
            else{
                $value->sold_out = false;
            }
        }        
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200); 
    }

    

    public function reportEvent(Request $request){
        $request->validate([
            'event_id' => 'bail|required',            
            'email' => 'bail|required|email',
            'reason' => 'bail|required',                              
            'message' => 'bail|required',                              
        ]);              
        $report = EventReport::create($request->all());
        return response()->json(['msg' => null, 'data' => $report,'success'=>true], 200);
    }

    public function categoryEvent(){
        $data = Category::where('status',1)->orderBy('id','DESC')->get();
        foreach ($data as $value) {
            $value->events = Event::where([['status',1],['is_deleted',0],['category_id',$value->id]])->orderBy('id','DESC')->get();
        }
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200);
    }

    public function userProfile(){
        $data = Auth::guard('userApi')->user();
        $data->likeCount = count(array_filter(explode(',',$data->favorite)));
        $data->totalTicket = Order::where('customer_id',$data->id)->count(); 
        $data->followingCount = count(array_filter(explode(',',$data->following)));
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200);
    }

    public function userLikes(){
        $data = Event::whereIn('id',array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))->where([['status',1],['is_deleted',0]])->orderBy('id','DESC')->get();
        foreach ($data as $value) {
            $value->description =  str_replace("&nbsp;", " ", strip_tags($value->description));           
            $value->time = $value->start_time->format('d F Y h:i a');   
        }
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200);
    }

    public function userFollowing(){
        $data = User::whereIn('id',array_filter(explode(',',Auth::guard('userApi')->user()->following)))->orderBy('id','DESC')->get();
        foreach ($data as $value) {
            if(Auth::check()){
                 if(in_array(Auth::guard('userApi')->user()->id,$value->followers)){
                     $value->isFollow = true;  
                 }
                 else{
                     $value->isFollow = false;  
                 }
            } 
            else{
                 $value->isFollow = false;  
            }
           
         }
        return response()->json(['msg' => null, 'data' => $data,'success'=>true], 200);
    }

    public function editUserProfile(Request $request){
        $validation = Validator::make($request->all(),[
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',  
            'address' => 'nullable',
            'image' => 'nullable'
        ]); 
        if($validation->fails()) {
            return response()->json(['msg' => $validation->errors()->first() , 'data' => null,'status'=>400], 400);
        }
        $user = AppUser::find(Auth::guard('userApi')->user()->id);
        $data = [
            'name' => $request->first_name,
            'last_name' => $request->last_name,
            'address' => strlen(trim($request->address))>0?$request->address:$user->address,
        ];
        if($request->hasFile('image')) {
            $imageName = $user->id.'_app_user.'.$request->image->extension();
            $request->image->storeAs('images',$imageName);
            $request->image->move(public_path('images/upload'),$imageName);
            $data['image'] = env('APP_URL')."/images/upload/".$imageName;
        }
        $user->update($data);
        return response()->json(['msg' => "Profile updated successfully", 'status'=>200], 200);
    }

    public function editImage(Request $request){
        $request->validate([
            'image' => 'bail|required',               
        ]);  
        
        if(isset($request->image))
        {           
            $image_name = (new AppHelper)->saveApiImage($request);
            AppUser::find(Auth::user()->id)->update(['image'=>$image_name]);    
            return response()->json(['msg' => null, 'data' => null,'success'=>true], 200);
        }
        else{
            return response()->json(['msg' => null, 'data' => null,'success'=>false], 200);
        }    
        
    }

    public function addFavorite(Request $request){
        $request->validate([      
            'event_id' => 'bail|required',          
        ]);
        $users = AppUser::find(Auth::user()->id); 
        $likes=array_filter(explode(',',$users->favorite));    
        $msg = "Added to Favorites List!";  
        if(count(array_keys($likes,$request->event_id))>0){
            if (($key = array_search($request->event_id, $likes)) !== false) {
                unset($likes[$key]);
            }
            $msg = "Removed from Favorites!";
        }
        else{
            array_push($likes,$request->event_id);
        }        
        $client = AppUser::find(Auth::user()->id);
        $client->favorite =implode(',',$likes);
        $client->update();
        $data = Event::with(['ticket','pictures','dock_type','maina_name','country','availability_type','boat_operator','cancellation_policy','payout_type','allowed_items'])->whereIn('id',$likes)->where([['status',1],['is_deleted',0]])->orderBy('id','DESC')->get();

        return response()->json(['msg' => $msg, 'data' => $data,'status'=>200], 200);
    }

    public function listFavorite(Request $request){
        $users = AppUser::find(Auth::user()->id); 
        $likes=array_filter(explode(',',$users->favorite));      
        $data = Event::with(['availabledays','ticket','pictures','payout_type','cancellation_policy','boat_operator','availability_type','maina_name','dock_type','organization','category','allowed_items','country'])->whereIn('id',$likes)->where([['status',1],['is_deleted',0]])->orderBy('id','DESC')->get();


        foreach ($data as $value) {
            $value->boat_prices;
            $value->description =  str_replace("&nbsp;", " ", strip_tags($value->description));           
            $value->time = $value->start_time->format('d F Y h:i a');
            if(Auth::guard('userApi')->check()){
                if(in_array($value->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                    $value->isLike = true;  
                }
                else{
                    $value->isLike = false; 
                }
            }
            else{
                $value->isLike = false;  
            }  
        }


        return response()->json(['msg' => 'List of favorites', 'data' => $data,'status'=>200], 200);
    }

    public function addFollowing(Request $request){
        $request->validate([      
            'user_id' => 'bail|required',          
        ]); 
        $users = AppUser::find(Auth::user()->id); 
        $likes=array_filter(explode(',',$users->following));      
        if(count(array_keys($likes,$request->user_id))>0){
            if (($key = array_search($request->user_id, $likes)) !== false) {
                unset($likes[$key]);
            }
            $msg = "Remove from following list!";
        }
        else{
            array_push($likes,$request->user_id);
            $msg = "Add in following!";
        }        
        $client = AppUser::find(Auth::user()->id);
        $client->following =implode(',',$likes);
        $client->update();
        return response()->json(['msg' => $msg, 'data' => null,'success'=>true], 200);
    }

    public function checkCode(Request $request){
        $request->validate([      
            'coupon_code' => 'bail|required',          
            'event_id' => 'bail|required',          
        ]);   
        $date = Carbon::now()->format('Y-m-d');
       
        $data = Coupon::where([['coupon_code',$request->coupon_code],['status',1],['event_id',$request->event_id]])->first();
       
        if($data){            
            if (Carbon::parse($date)->between(Carbon::parse($data->start_date),Carbon::parse($data->end_date))){                
                if($data->max_use<=$data->use_count){
                    return response()->json(['success'=>false,'msg'=>'This coupon is expire!' ,'data' =>null ], 200);
                }
                else{
                    return response()->json(['success'=>true,'msg'=>null ,'data' =>$data ], 200);
                }
            }
            else{
                return response()->json(['success'=>false,'msg'=>'This coupon is expire!' ,'data' =>null ], 200);
            }
        }
        else{
            return response()->json(['success'=>false,'msg'=>'Invalid Coupon code for this event!' ,'data' =>null ], 200);
        }
    }

    public function  allCoupon(){
        $data = Coupon::where('status',1)->orderBy('id','DESC')->get();
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$data ], 200);
    }

    public function userNotification(){
        $data = Notification::where('user_id',Auth::user()->id)->orderBy('id','DESC')->get();
        foreach ($data as $value) {
            $order = Order::find($value->order_id);
            if($order){
                $event = $order->event_id;
                $value->event_image = url('images/upload').'/'.Event::find($event)->image;
            }
            else{
                $value->event_image =null;
            }          
        }
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$data ], 200);
    }
    
    public function addReview(Request $request){
        $request->validate([      
            'event_id' => 'bail|required',          
            'order_id' => 'bail|required',    
            'message' => 'bail|required',          
            'rate' => 'bail|required|numeric',                      
        ]);
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $data['organization_id'] = Event::find($request->event_id)->user_id;
        $data['status'] = 0;
        $review = Review::create($data);
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$review ], 200);
    }

    public function orderTax($id){
        $organizer = Event::find($id)->user_id;
        $data = Tax::where([['user_id',$organizer],['status',1],['allow_all_bill',1]])->orderBy('id','DESC')->get()->makeHidden(['created_at','updated_at']);
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$data ], 200);
    }

    public function createOrder(Request $request){  
        

        

        $validation = Validator::make($request->all(),[      
            'event_id' => 'required',          
            'ticket_id' => 'required', 
            'quantity' => 'required',       
            'payment' => 'required|numeric',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => $validation->errors(),'status'=>400], 400);
        }


        $event = Event::find($request->event_id);
        $data = $request->all();
        $data['order_id'] = rand(9999,100000);
        $data['organization_id'] = $event->user_id;        
        $data['customer_id'] = Auth::guard('userApi')->user()->id;
        
        $data['payment_status'] = 0;
        
        $com = Setting::find(1,['org_commission_type','org_commission']);
        $p =   $request->payment- $request->tax; 

        $request->merge(['payment_type'=>'STRIPE']);

        $data['payment_type'] = 'STRIPE';
        if($request->payment_type=="FREE"){
            $data['org_commission']  = 0;
        }
        else{
            if($com->org_commission_type == "percentage"){
                $data['org_commission'] =  $p*$com->org_commission/100;
            }
            else if($com->org_commission_type=="amount"){
                $data['org_commission']  = $com->org_commission;
            }
        }

        $data['advance_deposite']  = $p*0.3;

        $data['total_payment']  = $p+($p*0.3);


        $data['min_payment'] = ($p+($p*0.3))*0.3;
                        
        if($request->coupon_id != null){      
            $count = Coupon::find($request->coupon_id)->use_count;
            $count = $count +1;           
            Coupon::find($request->coupon_id)->update(['use_count'=>$count]);
        } 
        // if($request->payment_type=="STRIPE"){
        //     $currency = Setting::find(1)->currency;
        //     $stripe_secret = PaymentSetting::find(1)->stripeSecretKey;  
        //     Stripe\Stripe::setApiKey($stripe_secret);
        //     $stripeDetail =  Stripe\Charge::create ([
        //         "amount" => intval($request->payment) * 100,
        //         "currency" => $currency,
        //         "source" => "DOLPHI",
        //     ]);
        //     $data['payment_token'] = $stripeDetail->id;
        // }
        $data = Order::create($data);
      
        for ($i=1; $i <= $request->quantity; $i++) { 
            $child['ticket_number'] = uniqid('', true);
            $child['ticket_id'] = $request->ticket_id;
            $child['order_id'] = $data->id;
            $child['customer_id'] = Auth::guard('userApi')->user()->id;
            OrderChild::create($child);         
        } 

        if(isset($request->tax_data)){
            foreach (json_decode($request->tax_data) as $value) {
                $tax['order_id'] = $data->id;
                $tax['tax_id'] = $value->tax_id;
                $tax['price'] = $value->price;
                OrderTax::create($tax);
            }
        }
              
        $user = AppUser::find($data->customer_id);
        $setting = Setting::find(1);
        
        // for user notification
        $message = NotificationTemplate::where('title','Book Ticket')->first()->message_content;
        $detail['user_name'] = $user->name;
        $detail['quantity'] = $request->quantity;
        $detail['event_name'] = Event::find($request->event_id)->name;
        $dateForHandle = Carbon::parse($request->date.' '.$request->time.':00')->format('d F Y h:i a');
        $detail['date'] = $dateForHandle;
        $detail['app_name'] =$setting->app_name;
        $noti_data = ["{{user_name}}", "{{quantity}}","{{event_name}}","{{date}}","{{app_name}}"];
        $message1 = str_replace($noti_data, $detail, $message);
        $notification = array();
        $notification['organizer_id']= null;
        $notification['user_id']= $user->id;
        $notification['order_id']= $data->id;
        $notification['title']= 'Ticket Booked';
        $notification['message']= $message1;      
        Notification::create($notification);
        if($setting->push_notification==1){
            if($user->device_token!=null){
                Config::set('onesignal.app_id', env('APP_ID'));
                Config::set('onesignal.rest_api_key', env('REST_API_KEY'));
                Config::set('onesignal.user_auth_key', env('USER_AUTH_KEY'));
                try{
                    OneSignal::sendNotificationToUser(
                        $message1,
                        $user->device_token,
                        $url = null,
                        $data1 = null,
                        $buttons = null,
                        $schedule = null
                    );
                }catch (\Throwable $th) { }
            }
        }
        // for user mail
        $ticket_book = NotificationTemplate::where('title','Book Ticket')->first();
        $details['user_name']= $user->name.' '.$user->last_name;
        $details['quantity']= $request->quantity;
        $details['event_name']= Event::find($request->event_id)->name;
        $details['date']= $dateForHandle;
        $details['app_name'] = $setting->app_name;
        if($setting->mail_notification == 1)
        {
            try{
                Mail::to($user->email)->send(new TicketBook($ticket_book->mail_content, $details, $ticket_book->subject));
            }
            catch (\Throwable $th) {}
        }

        // for Organizer notification
        $org =  User::find($data->organization_id);
        $or_message = NotificationTemplate::where('title','Organizer Book Ticket')->first()->message_content;
        $or_detail['organizer_name'] = @$org->first_name .' '. @$org->last_name;
        $or_detail['user_name'] = @$user->name.' '.@$user->last_name;
        $or_detail['quantity'] = $request->quantity;
        $or_detail['event_name'] = Event::find($request->event_id)->name;
        $or_detail['date'] = $dateForHandle;
        $or_detail['app_name'] =$setting->app_name;
        $or_noti_data = ["{{organizer_name}}", "{{user_name}}", "{{quantity}}","{{event_name}}","{{date}}","{{app_name}}"];
        $or_message1 = str_replace($or_noti_data, $or_detail, $or_message);
        $or_notification = array();
        $or_notification['organizer_id']=  $data->organization_id;
        $or_notification['user_id']= null;
        $or_notification['order_id']= $data->id;
        $or_notification['title']= 'New Ticket Booked';
        $or_notification['message']= $or_message1;      
        Notification::create($or_notification);
        if($setting->push_notification==1){
            if($org->device_token!=null){
                Config::set('onesignal.app_id', env('ORG_APP_ID'));
                Config::set('onesignal.rest_api_key', env('ORG_REST_API_KEY'));
                Config::set('onesignal.user_auth_key', env('ORG_USER_AUTH_KEY'));
                try{
                    OneSignal::sendNotificationToUser(
                        $or_message1,
                        $org->device_token,
                        $url = null,
                        $data1 = null,
                        $buttons = null,
                        $schedule = null
                    );
                }catch (\Throwable $th) { }
            }
        }
        // for Organizer mail
        $new_ticket = NotificationTemplate::where('title','Organizer Book Ticket')->first();
        $details1['organizer_name']= @$org->first_name .' '. @$org->last_name;
        $details1['user_name'] = @$user->name.' '.@$user->last_name;
        $details1['quantity']= $request->quantity;
        $details1['event_name']= Event::find($request->event_id)->name;
        $details1['date']= $dateForHandle;
        $details1['app_name'] = $setting->app_name;
        if($setting->mail_notification == 1)
        {
            try{
                Mail::to($user->email)->send(new TicketBookOrg($new_ticket->mail_content, $details1, $new_ticket->subject));
            }
            catch (\Throwable $th) {}
        }

        if($request->payment_type == "FLUTTERWAVE"){
            $data['redirect_url'] = url('/').'/create-payment/'.$data->id;
        }


        // $a= number_format((float)(100), 5);

        $data['advance_deposite']  = number_format((float)($data['advance_deposite']), 2);

        $data['total_payment']  = number_format((float)($data['total_payment']), 2);


        $data['min_payment'] = number_format((float)($data['min_payment']), 2);
        $resData = [
            "event_id" => $data->event_id.'',
            "ticket_id" => $data->ticket_id.'',
            "quantity" => $data->quantity.'',
            "payment" => $data->payment.'',
            "order_id" => $data->order_id.'',
            "organization_id" => $data->organization_id.'',
            "customer_id" => $data->customer_id.'',
            "payment_status" => $data->payment_status.'',
            "payment_type" => $data->payment_type.'',
            "org_commission" => $data->org_commission.'',
            "advance_deposite" => $data->advance_deposite.'',
            "total_payment" => $data->total_payment.'',
            "min_payment" => $data->min_payment.'',
            "updated_at" => $data->updated_at.'',
            "created_at" => $data->created_at.'',
            "id" => $data->id.'',
            "review" => $data->review,
        ];
        return response()->json(['success'=>true,'msg'=>'order created successfully' ,'data' =>$resData ], 200);
    }

    public function splitpayments(Request $request){
        $validation = Validator::make($request->all(),[      
            'order_id' => 'required',
            'payee_amount' => 'required',          
            'split_payments' => 'nullable|array',
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => $validation->errors(),'status'=>400], 400);
        }

        Splitpayment::where('order_id','=',$request->order_id)->delete();


        $authuser = Auth::guard('userApi')->user();

        
        $currency = Setting::find(1)->currency;
        $stripe_secret = PaymentSetting::find(1)->stripeSecretKey;
        $stripe = new \Stripe\StripeClient(
            $stripe_secret
        );
        $commission = Tax::where('name','split_payment_commission')->get();
        if($commission->count() > 0){
            $commission = $commission[0]->price;
        }else{
            $commission = 0;
        }
        $splitAmount = $request->payee_amount;
        if($commission > 0){
            $commission = ($splitAmount*$commission/100);
            $splitAmount = $splitAmount - $commission;
        }
        if(count($request->split_payments) > 0){
            $splitAmount = $splitAmount/(count($request->split_payments)+1);
        }

        $product = $stripe->products->create([
            'name' => 'Dolphi Split Payment '.$request->order_id,
        ]);
        $paymentarray=[];
        foreach($request->split_payments as $dataset){
            $user = AppUser::where('phone','=',$dataset['phone'])->first();

            if(!isset($user->id)){
                $data = $dataset;
                $data['password'] =  Hash::make('dolphi4321');
                $data['image'] ="defaultuser.png";
                $data['status'] =1;
                $data['name'] =$dataset['first_name'];
                $data['email'] ="splituser@dolphibooking.com";
                $data['provider'] ="LOCAL";
                $user = AppUser::create($data);
            }
            $meta = [
                'order_id' => $request->order_id,
                'user_id' => $user->id,
                'split_payment' => 1,
            ];
            $price = $stripe->prices->create([
                'product' => $product->id,
                'unit_amount' => $splitAmount*100,
                'currency' => 'usd',
                'metadata' => $meta,
            ]);
            $linkData = [
                'line_items' => [
                    [
                        'price' => $price->id,
                        'quantity' => 1,
                    ],
                ],
                'metadata' => $meta,
                'after_completion' => [
                    'type' => 'redirect',
                    'redirect' => [
                        'url' => env('APP_URL').'/payment/'.$request->order_id.'/'.$user->id.'/response',
                    ],
                ],
            ];
            $paymentLink = $this->curlStripePaymentLink($linkData,$stripe_secret);
            \Log::info(json_encode($paymentLink));
            $stripe_payment_id= $paymentLink['id'];
            $paymentUrl = $paymentLink['url'];

            $paymentobj = Splitpayment::create(['order_id'=>$request->order_id,'user_id' =>$user->id,'pay_amount'=>$splitAmount,'stripe_payment_id'=>$stripe_payment_id,'is_main_user' => 0, 'payment_url'=>$paymentUrl]);

            $paymentobj->pay_amount = number_format((float)($paymentobj->pay_amount), 2);
            $paymentobj->phone = $dataset['phone'];
            $paymentobj->first_name = $dataset['first_name'];

            array_push($paymentarray, $paymentobj);
        }

        $authuser = Auth::guard('userApi')->user();

        
        $proposerAmount = ($splitAmount+$commission);
        $charge = $stripe->paymentIntents->create(array(
            "amount" => $proposerAmount*100,
            "currency" => "usd")
        );

        $stripe_payment_id= $charge->id;
        $paymentobj = Splitpayment::create(['order_id'=>$request->order_id,'user_id' =>$authuser->id,'pay_amount'=>$proposerAmount,'stripe_payment_id'=>$stripe_payment_id,'is_main_user' => 1]);
        $paymentobj->payment_url = "";
        $paymentobj->pay_amount = number_format((float)($paymentobj->pay_amount), 2);
        $paymentobj->phone = $authuser->phone;
        $paymentobj->first_name = $authuser->name;

        // $paymentobj->pay_amount = number_format((float)($paymentobj->pay_amount), 2);

        $check =  [$paymentobj];
        $check = array_merge($check, $paymentarray);
        $dataDetails = [
            'split_details' => $check,
            'stripe_keys' => [
                'sk' => env('STRIPE_SECRET'),
                'pk' => env('STRIPE_PUBLIC'),
                'aid' => env('STRIPE_ACCOUNT_ID'),
                'mid' => env('STRIPE_MERCHANT_ID'),
            ]
        ];


        return response()->json(['success'=>true,'msg'=>"created split payments" ,'data' =>$dataDetails ], 200);
    }

    public function curlStripePaymentLink($request, $key, $method = 'POST') {
        $ch = curl_init("https://api.stripe.com/v1/payment_links");
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer ".$key]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($request));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        return json_decode($output, true);
    }

    public function curlStripeGetPaymentStatus($request, $key) {
        $ch = curl_init("https://api.stripe.com/v1/payment_links/".$request);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer ".$key]);
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        return json_decode($output, true);
    }

    public function linkResponse($order_id,$user_id){
        $data = [
            'order_id' => $order_id,
            'user_id' => $user_id,
        ];
        $payment = Splitpayment::where($data)->first();
        return view('frontend.payment', compact('payment'));
    }
    public function startchat(Request $request){
        $validation = Validator::make($request->all(),[      
            'event_id' => 'required', 
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => $validation->errors(),'status'=>400], 400);
        }


        $authuser = Auth::guard('userApi')->user();
        $data = Event::where('id','=',$request->event_id)->first();

        $chat = Chatroom::with(['vendor'])->where(['user_id'=>$authuser->id,'vendor_id'=>$data->user_id])->first();


        if(!isset($chat->id)){

            $chat = Chatroom::create(['user_id'=>$authuser->id,'vendor_id'=>$data->user_id]);


            $chat->vendor;
        }


        $chat->messages;

        
        return response()->json(['success'=>true,'msg'=>"created chat" ,'data' =>$chat], 200);


    }


    public function storechat(Request $request){
        $validation = Validator::make($request->all(),[      
            'chat_id' => 'required',
            'message' => 'required', 
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => $validation->errors(),'status'=>400], 400);
        }


        $authuser = Auth::guard('userApi')->user();
        

        $chat = Chatroom::with(['vendor'])->where(['user_id'=>$authuser->id,'id'=>$request->chat_id])->first();


        $message = $request->message;
        $emailPattern = '/[\._a-zA-Z0-9-]+@[\._a-zA-Z0-9-]+/i';
        $replaceEmail = "*********@email.com";
        $message = preg_replace($emailPattern, $replaceEmail, $message);
        $mobileNumberPattern = '/[0-9\-+]{8,}/';
        $replaceMobile = "8**8****88";
        $message = preg_replace($mobileNumberPattern, $replaceMobile, $message);
        $message = strip_tags($message);
        if(isset($chat->id)){

            $messages = Chatmessages::create(['user_id'=>$chat->user_id,'vendor_id'=>$chat->vendor_id,'message' => $message,"action"=>"text",'action_value'=>0,'chat_id'=>$chat->id]);


            $chat->messages;
        }

        
        return response()->json(['success'=>true,'msg'=>"created chat" ,'data' =>$chat], 200);


    }


    public function mychatlist(Request $request){
       

        $authuser = Auth::guard('userApi')->user();
        

        $chatlist = Chatroom::with(['vendor','userdata'])->where(['user_id'=>$authuser->id])->get();


        
        return response()->json(['success'=>true,'msg'=>"created chat" ,'data' =>$chatlist], 200);


    }


    public function chatmessage(Request $request){
        $validation = Validator::make($request->all(),[      
            'chat_id' => 'required'
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => $validation->errors(),'status'=>400], 400);
        }


        $authuser = Auth::guard('userApi')->user();
        

        $chat = Chatroom::with(['vendor'])->where(['user_id'=>$authuser->id,'id'=>$request->chat_id])->first();


        if(isset($chat->id)){


            $chat->messages;
        }

        
        return response()->json(['success'=>true,'msg'=>"created chat" ,'data' =>$chat], 200);


    }

    public function viewUserOrder(){
        $data = Order::with(['event','ticket','organization'])->where('customer_id',Auth::user()->id)->orderBy('id','DESC')->get();
        // Order ID, Payment ID, Amount, Time, Status, Event Name, Ticket Name
        $order = [];
        foreach($data as $key => $value){
            $value = $value->toArray();
            $order[$key]['order_id'] = $value['id']."";
            $order[$key]['payment_id'] = @$value['payment_token']."";
            $order[$key]['amount'] = @$value['total_payment']."";
            $order[$key]['time'] = $value['created_at']."";
            $order[$key]['status'] = $value['order_status']."";
            $order[$key]['event_name'] = @$value['event']['name']."";
            $order[$key]['ticket_name'] = $value['order_status'] == "Completed" ? (isset($value['ticket']['name']) ? $value['ticket']['name']."" : "BOAT-".$value['id']) : "";
        }
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$order ], 200);
    }

    public function viewSingleOrder($id){
        $data = Order::with(['event','ticket','organization'])->find($id);
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$data ], 200);  
    }

    public function allSetting(){
        $general = Setting::find(1,['app_name','app_version','logo','map_key','currency','onesignal_app_id','onesignal_project_number','onesignal_api_key','onesignal_auth_key','help_center','privacy_policy','cookie_policy','terms_services','acknowledgement','primary_color']);     
        $general->currency_symbol = Currency::where('code',$general->currency)->first()->symbol;
        $general->stripe = PaymentSetting::find(1)->stripe;
        $general->cod = PaymentSetting::find(1)->cod;
        $general->paypal = PaymentSetting::find(1)->paypal;
        $general->razor = PaymentSetting::find(1)->razor;
        $general->flutterwave = PaymentSetting::find(1)->flutterwave;
        $general->stripeSecretKey = PaymentSetting::find(1)->stripeSecretKey;
        $general->stripePublicKey = PaymentSetting::find(1)->stripePublicKey;
        $general->paypalClientId = PaymentSetting::find(1)->paypalClientId;
        // $general->paypalProduction = PaymentSetting::find(1)->paypalProduction;
        $general->razorPublishKey = PaymentSetting::find(1)->razorPublishKey;
        $general->razorSecretKey = PaymentSetting::find(1)->razorSecretKey;
      
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$general ], 200);  
    }

    public function userOrder(){
        
        $data['upcoming'] = Order::with(['event','ticket'])->where([['customer_id',Auth::user()->id],['order_status','Pending']])->orderBy('id','DESC')->get();
        $data['past'] = Order::with(['event','ticket'])
        ->where([['customer_id',Auth::user()->id],['order_status','Complete']])
        ->orWhere([['customer_id',Auth::user()->id],['order_status','Cancel']])
        ->orderBy('id','DESC')->get();
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$data ], 200);  
    }

    public function singleOrder($id){
        $data = Order::with(['event','ticket'])->find($id);
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$data ], 200); 
    }

    public function searchEvent(Request $request){
        $timezone = Setting::find(1)->timezone;       
        $date = Carbon::now($timezone); 
        $data = Event::where([['status',1],['is_deleted',0],['start_time', '>=',$date->format('Y-m-d H:i:s')]]);        
        if($request->lat != null && $request->lang != null){
            $lat = $request->lat;
            $lang = $request->lang;
            $event = array();
            $results = DB::select(DB::raw('SELECT id,name, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lang ) - radians(' . $lang . ') ) + sin( radians(' . $lat . ') ) * sin( radians(lat) ) ) ) AS distance FROM events HAVING distance < '.$this->radius.'  ORDER BY distance'));       
            if(count($results)>0){
                foreach ($results as $q) {
                    array_push($event, $q->id);
                }
            }
            $data = $data->whereIn('id',$event);
        }
        if($request->category != "All"){
           $data = $data->where([['category_id',$request->category],['start_time', '>=',$date->format('Y-m-d H:i:s')]]);
        }    
        if($request->date != "All"){             
            if($request->date == "Today"){
                $start_date = Carbon::now()->format('Y-m-d').' 00:00:00';
                $end_date = Carbon::now()->format('Y-m-d').' 23:59:59';                
            }
            elseif($request->date == "Tomorrow"){
                $start_date = Carbon::now()->addDays(1)->format('Y-m-d').' 00:00:00';
                $end_date = Carbon::now()->addDays(1)->format('Y-m-d').' 23:59:59';                
            }
            elseif($request->date == "This Week"){
                $start_date = Carbon::now()->modify('this week')->format('Y-m-d').' 00:00:00';
                $end_date = Carbon::now()->modify('this week +6 days')->format('Y-m-d').' 23:59:59';
            }   
            else{
                $start_date = carbon::parse($request->date)->format('Y-m-d').' 00:00:00';
                $end_date = carbon::parse($request->date)->format('Y-m-d').' 23:59:59';
            }         
            $data = $data->durationData($start_date,$end_date);
        }

        $data= $data->get();
        foreach ($data as $value) {
            $value->boat_prices;
            $value->description =  str_replace("&nbsp;", " ", strip_tags($value->description));           
            $value->time = $value->start_time->format('d F Y h:i a');
            if(Auth::guard('userApi')->check()){
                if(in_array($value->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                    $value->isLike = true;  
                }
                else{
                    $value->isLike = false; 
                }
            }
            else{
                $value->isLike = false;  
            }  
        }
        return response()->json(['success'=>true,'msg'=>null ,'data' =>$data ], 200);  
    }

    public function changePassword(Request $request){
        $validation = Validator::make($request->all(),[
            'old_password' => 'bail|required',
            'password' => 'bail|required|min:6',
            'password_confirmation' => 'bail|required|same:password|min:6'
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid credentials, Try again later.', 'data' => null,'status'=>400], 400);
        }
        if (Hash::check($request->old_password, Auth::guard('userApi')->user()->password)){
            AppUser::find(Auth::guard('userApi')->user()->id)->update(['password'=>Hash::make($request->password)]);
            return response()->json(['status'=>200,'msg'=>'Your password is change successfully' ], 200);
        }
        else{
            return response()->json(['status'=>400,'msg'=>'Current Password is wrong!' ], 400);
        }
    }

    public function forgotPassword(Request $request){
      
        $request->validate([
            'email' => 'bail|required|email|exists:app_user,email'
        ]);
        $user = AppUser::where('email',$request->email)->first();
        $password=rand(100000,999999);
       
        if($user){
            $content = NotificationTemplate::where('title','Reset Password')->first()->mail_content;            
            $detail['user_name'] = $user->name;
            $detail['password'] = $password;
            $detail['app_name'] = Setting::find(1)->app_name;
            
            try{
                Mail::to($user)->send(new ResetPassword($content,$detail));
            } catch (\Throwable $th) {  }
            AppUser::find($user->id)->update(['password'=>Hash::make($password)]);
            return response()->json(['status'=>200,'msg'=>'New password send in your email' ], 200);
        }
        return response()->json(['status'=>400,'msg'=>'Invalid email ID'], 400);
    }

    public function clearNotification(){
        $noti = Notification::where('user_id',Auth::user()->id)->get();        
        foreach ($noti as $value) {
            $value->delete();
        }
        return response()->json(['success'=>true,'msg'=>'Notification deleted successfully.' ], 200);
    }

   public function homepage(Request $request) {
       $request->validate([
           'lat' => 'bail|required',
           'lang' => 'bail|required',
       ]);
       $lat = $request->lat;
       $lang = $request->lang;
       $event = array();
       $timezone = Setting::find(1)->timezone;
       $date = Carbon::now($timezone);
       $results = DB::select(DB::raw('SELECT id,name, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lang ) - radians(' . $lang . ') ) + sin( radians(' . $lat . ') ) * sin( radians(lat) ) ) ) AS distance FROM events HAVING distance < '.$this->radius.'  ORDER BY distance'));
       if(count($results)>0){
           foreach ($results as $q) {
               array_push($event, $q->id);
           }
       }

       //->whereIn('id',$event)  removed from where for testing

       $data['popular'] = Event::with(['availabledays','ticket','pictures','payout_type','cancellation_policy','boat_operator','availability_type','maina_name','dock_type','organization','category','allowed_items','country'])->where([['is_popular',1],['status',1],['is_deleted',0],['start_time', '>=',$date->format('Y-m-d H:i:s')]])->orderBy('start_time','ASC')->get();

       if(count($data['popular']) == 0){
           $data['popular'] = Event::with(['availabledays','ticket','pictures','payout_type','cancellation_policy','boat_operator','availability_type','maina_name','dock_type','organization','category','allowed_items','country'])->where([['status',1],['is_deleted',0],['start_time', '>=',$date->format('Y-m-d H:i:s')]])->orderBy('start_time','ASC')->get();
       }
       if(count($data['popular'])>0){
           foreach($data['popular'] as $key => $value) {
               $value->boat_prices;
               $data['popular'][$key]->description =  str_replace("&nbsp;", " ", strip_tags($value->description));
               $data['popular'][$key]->time = $value->start_time->format('d F Y h:i a');
               if(Auth::guard('userApi')->check()){
                   if(in_array($value->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                       $data['popular'][$key]->isLike = true;
                   }
                   else{
                       $data['popular'][$key]->isLike = false;
                   }
               }
               else{
                   $data['popular'][$key]->isLike = false;
               }
           }
       }
       $data['footer'] = [
           'title' => 'DOLPHI',
           'description' => 'Yacht chartering is the practice of renting or chartering, a sail boat or motor yacht and travelling to various coastal or Island destinations. This is usually a vacation activity, but it also can be a business event. There are two main kinds of charters. Bareboat and skippered.',
           'brand' => '',
       ];
       return response()->json(['msg' => null, 'data' => $data,'status'=>200], 200);
   }

   public function eventsListing(Request $request) {
       $validation = Validator::make($request->all(),[
           'lat' => 'bail|required',
           'lang' => 'bail|required',
           'applicable_type' => 'nullable|numeric|between:1,3',
           'search' => 'nullable|string',// Minimum 3 characters are required 
       ]);
       if(isset($request->search) && strlen(trim($request->search))<3) {
            $request->merge(['search'=>'']);
       } 
       if($validation->fails()) {
           return response()->json(['msg' => $validation->errors()->first() , 'data' => null,'status'=>400], 400);
       }
       $lat = $request->lat;
       $lang = $request->lang;
       $timezone = Setting::find(1)->timezone;
       $date = Carbon::now($timezone);
       $event = [];
       $applicable_type = null;
       if(isset($request->applicable_type) && strlen(trim($request->applicable_type))>0){
           $applicable_type = $request->applicable_type;
           $results = DB::select(DB::raw('SELECT id,name, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lang ) - radians(' . $lang . ') ) + sin( radians(' . $lat . ') ) * sin( radians(lat) ) ) ) AS distance FROM events WHERE applicable_type=\''.$applicable_type.'\' HAVING distance < '.$this->radius.' ORDER BY distance'));
           if((int)$applicable_type !== 1) {
               $online = Event::where([['applicable_type',$applicable_type],['status',1],['is_deleted',0],['event_status','Pending'],['type','online'],['end_time', '>',$date->format('Y-m-d H:i:s')]]);
           } else {
               $online = Event::where([['applicable_type',$applicable_type],['status',1],['is_deleted',0],['event_status','Pending']]);
           }
       }
       else{
           $results = DB::select(DB::raw('SELECT id,name, ( 3959 * acos( cos( radians(' . $lat . ') ) * cos( radians( lat ) ) * cos( radians( lang ) - radians(' . $lang . ') ) + sin( radians(' . $lat . ') ) * sin( radians(lat) ) ) ) AS distance FROM events HAVING distance < '.$this->radius.'  ORDER BY distance'));
           $online = Event::where([['status',1],['is_deleted',0],['event_status','Pending']]);
       }
       if(isset($request->search) && strlen(trim($request->search))>0){
            $online = $online->where('name','like','%'.$request->search.'%');
       }
    //    echo $online->toSql();exit;
       $online = $online->get();
       if(!isset($request->search) || (isset($request->search) && strlen(trim($request->search))==0)){
            if(count($results)>0) {
                foreach ($results as $evnt) {
                    array_push($event, $evnt->id);
                }
            }
        }
       if(count($online)>0) {
           foreach ($online as $evnt) {
               array_push($event, $evnt->id);
           }
       }

       $data = [];
       if(count($event) > 0) {
            $data = Event::with(['availabledays','ticket','pictures','payout_type','cancellation_policy','boat_operator','availability_type','maina_name','dock_type','organization','category','allowed_items','country'])
            ->where([['status',1],['is_deleted',0],['event_status','Pending']])
            ->whereIn('id',$event);
            if(!is_null($applicable_type) && $applicable_type !== 1){
                $data = $data->where([['end_time', '>',$date->format('Y-m-d H:i:s')]])->orderBy('start_time','ASC');
            }
            $data = $data->get()->makeHidden(['created_at','updated_at']);
        }

        foreach ($data as $key => $value) {
            $value->boat_prices;
            $value->description =  str_replace("&nbsp;", " ", strip_tags($value->description));
            if(!is_null($applicable_type) && $applicable_type !== 1){
                $value->time = $value->start_time->format('d F Y h:i a');
            } else {
                $value->time = null;
            }
            $value->isLike = false;
            if(Auth::guard('userApi')->check()){
                if(in_array($value->id,array_filter(explode(',',Auth::guard('userApi')->user()->favorite)))){
                    $value->isLike = true;
                }
            }
        }
       return response()->json(['msg' => 'success', 'data' => $data,'status'=>200], 200);
   }

   public function eventsLocations() {
        $data = \DB::select('SELECT DISTINCT(e.city),e.lat,e.lang,c.country FROM events e LEFT JOIN currency c ON e.country = c.id WHERE e.status = 1 AND e.is_deleted = 0 AND e.city IS NOT NULL AND c.country IS NOT NULL AND e.lat IS NOT NULL AND e.lang IS NOT NULL ORDER BY e.city');
        return response()->json(['msg' => 'success', 'data' => $data,'status'=>200], 200);
   }

   public function dashboard() {
        $user = Auth::guard('userApi')->user();

        $totalTicket = Order::where('customer_id',$user->id)->count(); 
        $followingCount = count(array_filter(explode(',',$user->following)));
        $favoritesCount = count(array_filter(explode(',',$user->favorite)));

        $user = AppUser::find($user->id);
        $data = [
            'about_us' => env('APP_URL').'/',
            'privacy_policy' => env('APP_URL').'/privacy-policy',
            't_n_c' => env('APP_URL').'/tnc',
            'contact_us' => env('APP_URL').'/',
            'account' => [
                'first_name' => $user->name,
                'last_name' => $user->last_name,
                'email' => $user->email,
                'phone' => $user->phone,
                'code' => $user->code,
                'image' => $user->image,
                'address' => is_null($user->address) ? '' : $user->address,
                'active_since' => $user->created_at->format('d F Y'),
                'following_count' => is_null($followingCount) ? 0 : $followingCount,
                'total_tickets' => is_null($totalTicket) ? 0 : $totalTicket,
                'favorites_count' => is_null($favoritesCount) ? 0 : $favoritesCount,
            ]
        ];
        return response()->json(['status' => 200, 'msg' => $data],200);
   }

    public function bookingHistory(Request $request) {
        $validation = Validator::make($request->all(),[
            'offset' => 'nullable|numeric',
            'month' => 'nullable|numeric|between:1,12',
            'year' => 'nullable|numeric|max:'.date('Y'),
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => $validation->errors()->first() , 'data' => null,'status'=>400], 400);
        }
    }

    public function eventAvailableDates($id) {
        $commonError = 'No tickets available/sold out.';
        $response = ['status' => 400, 'msg' => 'Invalid Event Details', 'data' => null];
        if(is_numeric($id)) {
            $eventRecords = Event::with(['availabledays','ticket','boat_prices'])->where([['id',$id],['status',1],['is_deleted',0]])->orderBy('start_time','ASC')->first();
            if($eventRecords) {
                $eventRecords = $eventRecords->only(['id','start_time','end_time','applicable_type','availability_type','availability_slots','ticket','recurring','multiple_booking_allowed','time_gap','boat_prices','people','description','name']);
                $availableDates = [
                    'tickets' => [],
                    'stripe_keys' => [
                        'sk' => env('STRIPE_SECRET'),
                        'pk' => env('STRIPE_PUBLIC'),
                        'aid' => env('STRIPE_ACCOUNT_ID'),
                        'mid' => env('STRIPE_MERCHANT_ID'),
                    ]
                ];
                if(count($eventRecords['ticket']) > 0 && $eventRecords['applicable_type'] != 1) {
                    foreach ($eventRecords['ticket'] as $key => $value) {
                        $orderForThisTicket = Order::where([['ticket_id',$value->id],['event_id',$id],['payment_status',1]])->get();
                        $totalSoldTickets = 0;
                        if($orderForThisTicket) {
                            $totalSoldTickets = array_sum(array_column($orderForThisTicket->toArray(),'quantity'));
                        }
                        if($totalSoldTickets < $value->quantity) {
                            $availableTickets = $value->quantity - $totalSoldTickets;
                            $startDate = '';
                            $endDate = '';
                            if(!empty($value->start_time) && !empty($value->end_time)) {
                                $startDate = date('Y-m-d',strtotime($value->start_time));
                                $endDate = date('Y-m-d',strtotime($value->end_time));
                            } else if (!empty($eventRecords['start_time'])) {
                                $startDate = date('Y-m-d',strtotime($eventRecords['start_time']));
                                $endDate = date('Y-m-d',strtotime($eventRecords['end_time']));
                            } else {
                                continue;
                            }
                            if(strlen(trim($startDate)) ==0 || strlen(trim($endDate)) == 0) {
                                continue;
                            }
                            $manipulatedDates = $this->getAllAvailableDates($startDate,$endDate,$eventRecords['recurring'] ,$eventRecords['availability_type'],$eventRecords['availability_slots']);
                            if(!count($manipulatedDates)) {
                                continue;
                            }
                            $availableDates['tickets'][] = [
                                'ticket_id' => $value->id,
                                'event_id' => $id,
                                'available_tickets' => $availableTickets,
                                'total_tickets' => $value->quantity,
                                'max_quantity_per_order' => $value->ticket_per_order,
                                'ticket_price' => $value->price,
                                'ticket_description' => $value->description,
                                'ticket_name' => $value->name,
                                'ticket_type' => $value->type,
                                'available_dates' => $manipulatedDates,
                            ];
                        }
                    }
                    if(count($availableDates['tickets']) > 0) {
                        $response = ['status' => 200, 'msg' => 'success', 'data' => $availableDates];
                    } else {
                        $response = ['status' => 400, 'msg' => $commonError, 'data' => null];
                    }
                } else if($eventRecords['applicable_type'] == 1) {
                        $timeGap = -1;
                        if($eventRecords['multiple_booking_allowed'] == 1 && $eventRecords['availability_type'] == 1) {
                            $timeGap = $eventRecords['time_gap'];
                        }
                        $startDate = date('Y-m-d',strtotime('tomorrow'));
                        $endDate = date('Y-m-d',strtotime('+30 days'));
                        $manipulatedDates = $this->getAllAvailableDates($startDate,$endDate,$eventRecords['recurring'] ,$eventRecords['availability_type'],$eventRecords['availability_slots'], $timeGap);
                        $prices = collect($eventRecords['boat_prices'])->where('price','>',0)->toArray();
                        foreach($prices as $key => $item) {
                            foreach($item as $key1 => $item1) {
                                if (in_array($key1, ['id', 'event_id', 'created_at', 'updated_at'])) {
                                    unset($prices[$key][$key1]);
                                }
                                $prices[$key]['price'] = (double)$prices[$key]['price'];
                            }
                        }
                        $availableDates['tickets'][] = [
                            'ticket_id' => 0,
                            'event_id' => $id,
                            'available_tickets' => $eventRecords['people'],
                            'total_tickets' => $eventRecords['people'],
                            'max_quantity_per_order' => $eventRecords['people'],
                            'ticket_price' => array_values($prices),
                            'ticket_description' => $eventRecords['description'],
                            'ticket_name' => $eventRecords['name'],
                            'ticket_type' => "",
                            'available_dates' => $manipulatedDates,
                        ];
                        if(count($availableDates['tickets'][0]['available_dates']) > 0) {
                            $response = ['status' => 200, 'msg' => 'success', 'data' => $availableDates];
                        } else {
                            $response = ['status' => 400, 'msg' => $commonError, 'data' => null];
                        }
                } else {
                    $response = ['status' => 400, 'msg' => $commonError, 'data' => null];
                }

            }
        }
        return response()->json($response, $response['status']);
    }

    /*
     * Function to fetch all dates falling on Mondays within the date range
     * Params:
     *      start date
     *      end date
     *      recurring
     *      availability type
     *      availability slots
     *      timegap (For boat)
    */
    public function getAllAvailableDates($startDate, $endDate, $recurring = 0, $availabilityType = 1, $availabilitySlots = [], $timegap = -1) {
        $startDate = new \DateTime($startDate);
        $endDate = new \DateTime($endDate);
        $interval = \DateInterval::createFromDateString('1 day');
        $period = new \DatePeriod($startDate, $interval, $endDate);
        $dates = [];
        $times = ['Sunday' => [], 'Monday' => [], 'Tuesday' => [], 'Wednesday' => [], 'Thursday' => [], 'Friday' => [], 'Saturday' => []];

        //SCENARIOS for Time Slots:
        // recurring            1     1       0       0
        // availability type    1     0(2)    1       0(2)
        // Value for time       8-6    AS     8-6      AS
        // 8-6 = 8:00-6:00 (Default Time)
        // AS = Availability Slots to be considered

        $defaultSlot = [
            'id' => '08:00',
            'name' => '08:00 AM',
            'status' => true,
        ];

        if(($recurring && $availabilityType == 1) || (!$recurring && $availabilityType == 1)) {
            foreach($times as $key => $value) {
                $times[$key][] = $defaultSlot;
                if($timegap > 0) {
                    $currentTime = strtotime('08:00');
                    for($i = strtotime('+'.$timegap.' minutes', $currentTime) ; $i <= strtotime('18:00'); $i = strtotime('+'.$timegap.' minutes', $i)) {
                        $times[$key][] = [
                            'id' => date('H:i', $i),
                            'name' => date('H:i A', $i),
                            'status' => true,
                        ];
                    }
                }
            }
        } else {
            if(is_string($availabilitySlots)) {
                $availabilitySlots = json_decode($availabilitySlots,true);
            }
            if($availabilitySlots && count($availabilitySlots) > 0) {
                foreach($availabilitySlots as $key => $value) {
                    $availableSlotForaDay = collect($value['data'])->where('status','=' ,true)->toArray();
                    if(count($availableSlotForaDay) > 0) {
                        $times[$value['title']] = array_values($availableSlotForaDay);
                    }
                }
            }
        }

        foreach ($period as $dt) {
            $day = $dt->format("l");
            if(count($times[$day]) == 0) {
                continue;
            }
            $dates[] = [
                'id' => $dt->format('d-m-Y'),
                'value' => $dt->format('d M Y'),
                'time_slots' => $times[$day],
            ];
        }
        return $dates;
    }

    public function orderStatus(Request $request) {
        $validation = Validator::make($request->all(),[
            'order_id' => 'bail|exists:orders,id',
            'payment_intents' => 'bail'
        ]);
        if($validation->fails()) {
            return response()->json(['msg' => $validation->errors()->first() , 'data' => null,'status'=>400], 400);
        }
        try{
            $stripe = new \Stripe\StripeClient(
                env('STRIPE_SECRET')
            );
            $paymentStatus = $stripe->paymentIntents->retrieve(
                $request->payment_intents,
                []
            );
            if(strtolower(trim($paymentStatus->status)) == 'succeeded') {
                $order = Order::find($request->order_id);
                $isActualAmount = false;
                if($order->price <= ($paymentStatus->amount_received/100)) {
                    $isActualAmount = true;
                }
                $msg = 'Payment Successful';
                $isSplitPaymentSuccessful = 1;
                $splitPaymentAmount = 0;
                $mainUser = Splitpayment::with('user')->where('order_id',$order->id)->where('is_main_user',1)->first();
                if($order->payment_status == 0 && $isActualAmount) {
                    $status = 'Completed';
                    $split = Splitpayment::where('order_id',$order->id)->get();
                    if(count($split) > 0) {
                        $status = 'Pending';
                        Splitpayment::where('order_id',$order->id)->where('is_main_user',1)->update(['paid_status' => '1','paid_date' => date('Y-m-d H:i:s'),'paid_payment_id' => $paymentStatus->id]);
                        $split = Splitpayment::with('user')->where('order_id',$order->id)->where('is_main_user',0)->where('paid_status',0)->get();
                        if(count($split) > 0) {
                            $isSplitPaymentSuccessful = 0;
                            $splitPaymentAmount = collect(array_column($split->toArray(),'pay_amount'))->sum();
                            $msg = 'Payment Successful, but split payment is pending';
                            $this->sendSplitPaymentSMS($mainUser->user->name, $split);
                        } else {
                            $split = Splitpayment::where('order_id',$order->id)->where('is_main_user',0)->where('paid_status',1)->get();
                            $splitPaymentAmount = collect(array_column($split->toArray(),'pay_amount'))->sum();
                        }
                    }
                    $order->payment_status = 1;
                    $order->order_status = $status;
                    $order->payment_token = $paymentStatus->id;
                    $order->save();
                } else if($order->payment_status == 0 && !$isActualAmount) {
                    $order->payment_status = 0;
                    $order->order_status = 'InvalidAmount';
                    $order->payment_token = $paymentStatus->id;
                    $order->save();
                } else {
                    $split = Splitpayment::where('order_id',$order->id)->get();
                    if(count($split) > 0) {
                        Splitpayment::where('order_id',$order->id)->where('is_main_user',1)->update(['paid_status' => '1','paid_date' => date('Y-m-d H:i:s'),'paid_payment_id' => $paymentStatus->id]);
                        $split = Splitpayment::with('user')->where('order_id',$order->id)->where('is_main_user',0)->where('paid_status',0)->get();
                        if(count($split) > 0) {
                            $isSplitPaymentSuccessful = 0;
                            $splitPaymentAmount = collect(array_column($split->toArray(),'pay_amount'))->sum();
                            $msg = 'Payment Successful, but split payment is pending';
                            $this->sendSplitPaymentSMS($mainUser->user->name, $split);
                        } else {
                            $split = Splitpayment::where('order_id',$order->id)->where('is_main_user',0)->where('paid_status',1)->get();
                            $splitPaymentAmount = collect(array_column($split->toArray(),'pay_amount'))->sum();
                        }
                    }
                }
                $orderDetails = [];
                if($isActualAmount) {
                    $ticket_number = "BOAT-".$order->id;
                    if($order->ticket_id != 0) {
                        $ticket_number = Ticket::find($order->ticket_id)->ticket_number.'-'.$order->id;
                    }
                    $orderDetails = [
                        'id' => $order->id,
                        'order_id' => $order->order_id,
                        'ticket_number' => $ticket_number,
                        'order_amount' => ($order->total_payment+$splitPaymentAmount).'',
                        'transaction_number' => $paymentStatus->id,
                    ];
                    if(!$isSplitPaymentSuccessful) {
                        $orderDetails['ticket_number'] = "";
                    }
                } else {
                    $orderDetails = null;
                    $msg = 'Payment Failed';
                }
                return response()->json(['msg' => $msg,'data' => $orderDetails,'status'=>200], 200);
            } else {
                return response()->json(['msg' => 'Payment Failed','data' => null,'status'=>400], 400);
            }
        }catch (Stripe\Exception\InvalidRequestException $e) {
            \Log::error("Payment status stripe exception");
            \Log::error($e->getMessage());
            return response()->json(['msg' => 'Unable to process the request', 'data' => null,'status'=>400], 400);
        }catch(\Exception $e) {
            \Log::error("Payment status exception");
            \Log::error($e->getMessage());
            return response()->json(['msg' => 'Unable to process the request', 'data' => null,'status'=>400], 400);
        }
    }

    private function sendSplitPaymentSMS($name="", $details = []) {
        try {
            $sms_content = trans('content.split_payment_sms_content');
            $sms_content = str_ireplace("#NAME#", $name, $sms_content);
            foreach ($details as $key => $value) {
                $smsData = $sms_content." ".$value->payment_url;
                sendSMS($value->user->code.$value->user->phone, $smsData);
            }
        } catch(\Exception $e) {
            \Log::error("Send split payment URL sms exception");
            \Log::error($e->getMessage());
        }
    }

    public function eventCancellationPolicies($id) {
        $cancellationPolicy = [];
        if(is_numeric($id)) {
            $cancellationPolicy = CancellationPolicy::where('status','1')->where('applicable_for',$id)->get(['id','name','policy_description']);
        }
        return response()->json(['msg' => 'Success','data' => $cancellationPolicy,'status'=>200], 200);
    }

    public function countryCodes($type=1) {
        if($type == 1) {
            $countryCodes = array_values(collect(CountryCode::where('vendor_status','1')->get(['code','country'])->toArray())->sortBy('country')->toArray());
        } else {
            $countryCodes = array_values(collect(CountryCode::where('customer_status','1')->get(['code','country'])->toArray())->sortBy('country')->toArray());
        }
        return response()->json(['msg' => 'Success','data' => $countryCodes,'status'=>200], 200);
    }

    public function pendingPaymentLinks($order_id) {
        $order = Splitpayment::with('user')->where('order_id',$order_id)->where('paid_status',0)->where('is_main_user',0)->get();
        if(count($order) == 0) {
            $order = [];
        }
        $order1 = Splitpayment::with('user')->where('order_id',$order_id)->where('paid_status',1)->where('is_main_user',0)->get();
        if(count($order1) == 0) {
            $order1 = [];
        }
        $data = [
            'pending' => $order,
            'completed' => $order1,
        ];
        return response()->json(['msg' => 'Success','data' => $data,'status'=>200], 200);
    }
}
