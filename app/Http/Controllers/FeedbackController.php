<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Feedback;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Gate;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        abort_if(Gate::denies('feedback_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if(Auth::user()->hasRole('organization')){
            $feedback = Feedback::where('user_id',Auth::user()->id)->with(['user'])->orderBy('id','DESC')->get();
        }
        elseif(Auth::user()->hasRole('admin')){
            $feedback = Feedback::with(['user'])->orderBy('id','DESC')->get();
        }

        return view('admin.feedback.index', compact('feedback'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        abort_if(Gate::denies('feedback_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.feedback.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'email' => 'bail|required',
            'message' => 'bail|required',
            'rate' => 'bail|required',
        ]);
        $data = $request->all();
        $data['user_id'] = Auth::user()->id;
        $feedback = Feedback::create($data);
        return redirect()->route('feedback.index')->withStatus(__('Feedback is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function show(Feedback $feedback)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function edit(Feedback $feedback)
    {
        //
        abort_if(Gate::denies('feedback_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        return view('admin.feedback.edit', compact( 'feedback'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Feedback $feedback)
    {
        //
        $request->validate([
            'user_id' => 'bail|required',
            'email' => 'bail|required',
            'message' => 'bail|required',
            'rate' => 'bail|required',
        ]);

        $feedback = Feedback::find($feedback->id)->update($request->all());
        return redirect()->route('feedback.index')->withStatus(__('Feedback is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Feedback  $feedback
     * @return \Illuminate\Http\Response
     */
    public function destroy(Feedback $feedback)
    {
        //
        try{
            $feedback->delete();
            return true;
        }catch(Throwable $th){
            return response('Data is Connected with other Data', 400);
        }
    }
}
