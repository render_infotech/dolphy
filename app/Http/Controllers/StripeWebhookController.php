<?php

namespace App\Http\Controllers;

use App\Models\PaymentSetting;
use App\Models\Splitpayment;
use Illuminate\Http\Request;

class StripeWebhookController extends Controller
{
    public function index(Request $request) {
        \Log::info('Stripe webhook call: ');
        \Log::info(json_encode($request->all()));
        $data = $request->all();
        $paymentEventId = $data['id'];
        if(array_key_exists('data',$data)) {
            $object = $data['data'];
            if(array_key_exists('object',$object)) {
                $object = $object['object'];
                if(array_key_exists('metadata',$object)) {
                    $metadata = $object['metadata'];
                    if(array_key_exists('order_id',$metadata) && array_key_exists('split_payment',$metadata) && array_key_exists('user_id',$metadata) && intval($metadata['split_payment']) == 1) {
                        $order_id = $metadata['order_id'];
                        $user_id = $metadata['user_id'];
                        $paymentStatus = $object['payment_status'];
                        if($paymentStatus == "paid") {
                            $record = Splitpayment::where('order_id',$order_id)->where('user_id',$user_id)->get()->first();
                            if(!empty($record)) {
                                $plink = $record->stripe_payment_id;
                                $record->stripe_payment_id = $object['payment_intent'];
                                $record->paid_payment_id = $paymentEventId;
                                $record->paid_status = 1;
                                $record->paid_date = date('Y-m-d H:i:s');
                                $record->save();
                                $this->curlStripePaymentLink($plink);
                                $record = Splitpayment::where('order_id',$order_id)->where('paid_status',0)->get();
                                if(count($record) == 0) {
                                    $order = \App\Models\Order::find($order_id);
                                    $order->order_status = 'Completed';
                                    $order->payment_status = 1;
                                    $order->save();
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public function curlStripePaymentLink($request, $method = 'POST') {
        $stripe_secret = PaymentSetting::find(1)->stripeSecretKey;
        $ch = curl_init("https://api.stripe.com/v1/payment_links/".$request);
        curl_setopt($ch, CURLOPT_HTTPHEADER, ["Authorization: Bearer ".$stripe_secret]);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query(['active' => "false"]));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        return json_decode($output, true);
    }
}
