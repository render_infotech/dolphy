<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Event;
use App\Models\Ticket;
use App\Models\AppUser;
use App\Models\Order;
use Carbon\Carbon;
use Redirect;
use App;
use App\Models\Language;
use Rave;
use Auth;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Gate;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Hash;
use DB;
use Illuminate\Http\Request;
use App\Models\Tax;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //                        
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $users = User::with(['roles:id,name'])->get();      
        return view('admin.user.index', compact('users')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $roles = Role::all();
        $orgs = User::role('organization')->orderBy('id','DESC')->get();  
        return view('admin.user.create', compact('roles','orgs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'email' => 'bail|required|email|unique:users',
            'phone' => 'bail|required',
            'password' => 'bail|required|min:6',
        ]);
        $data = $request->all();
        $data['password'] =  Hash::make($request->password); 
        $data['org_id'] = $request->organization;
        $user = User::create($data);
        $user->assignRole($request->input('roles', []));
        $taxData = [
            ['name' => 'boat', 'price' => $data['boat'], 'user_id' => $user->id,'status' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            ['name' => 'event', 'price' => $data['event'], 'user_id' => $user->id,'status' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
            ['name' => 'game', 'price' => $data['game'], 'user_id' => $user->id,'status' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")],
        ];
        Tax::insert($taxData);
        return redirect()->route('users.index')->withStatus(__('User is added successfully.'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
        // $user->load('roles');
        $tax = Tax::where('status',1)->where('user_id', $user->id)->get();
        if (count($tax)) {
            $taxData = [];
            foreach ($tax as $key => $value) {
                $taxData[$value->name] = (float)$value->price;
            }
            if(!array_key_exists('boat', $taxData)){
                $taxData['boat'] = 0;
            }
            if(!array_key_exists('game', $taxData)){
                $taxData['game'] = 0;
            }
            if(!array_key_exists('event', $taxData)){
                $taxData['event'] = 0;
            }
            $tax = $taxData;
        } else {
            $tax = [
                'boat' => 0,
                'game' => 0,
                'event' => 0,
            ];
        }
        return view('admin.user.show', compact('user','tax'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $roles = Role::all();
        $user->load('roles');
        $orgs = User::role('organization')->orderBy('id','DESC')->get();  
        $taxData = Tax::where('status',1)->where('user_id', $user->id)->get();
        $tax = [];
        if (count($taxData)) {
            foreach ($taxData as $key => $value) {
                $tax[$value->name] = (float)$value->price;
            }
            if(!array_key_exists('commission', $tax)){
                $tax['commission'] = 0;
            }
        } else {
            $tax = [
                'commission' => 0,
            ];
        }
        return view('admin.user.edit', compact('roles', 'user','orgs','tax'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
        $request->validate([
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'phone' => 'bail|required',
            'email' => 'bail|required|unique:users,email,' . $user->id . ',id',
        ]);
        $data = $request->all();
        $data['org_id'] = $request->organization;
        $user->update($data);        
        $user->syncRoles($request->input('roles', []));
        $updateData = Tax::where('status',1)->where('user_id', $user->id)->where('name' ,'commission')->update(['price' => $data['commission']]);
        if(!$updateData) {
            Tax::insert(['user_id' => $user->id, 'name' => 'commission', 'price' => $data['commission'], 'status' => 1, 'created_at' => date("Y-m-d H:i:s"), 'updated_at' => date("Y-m-d H:i:s")]);
        }
        return redirect()->route('users.index')->withStatus(__('User is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
        // $user->delete();
        // $user->syncRoles([]);

    }
        public function adminLogin(Request $request)
        {                
            $request->validate([
                'email' => 'bail|required|email',
                'password' => 'bail|required',
            ]);
            
            $userdata = array(
                'email' => $request->email,
                'password' => $request->password,          
            );
            $remember = $request->get('remember');
            if (Auth::attempt($userdata, $remember)) {
                // $user = Auth::user()->load('roles');  
                if (Auth::user()->hasRole('admin')) {
                    return redirect('admin/home');
                } elseif (Auth::user()->hasRole('organization')) {
                    return redirect('organization/home');
                } else {
                    return Redirect::back()->with('error_msg', 'Only authorized person can login.');
                }
            } else {
                return Redirect::back()->with('error_msg', 'Invalid Username or Password');
            }            
        }
    public function adminDashboard(){  
        
        $master['organizations'] = User::role('organization')->count();
        $master['users'] = AppUser::count();
        $master['total_order'] = Order::count();
        $master['pending_order'] = Order::where('order_status','Pending')->count();
        $master['complete_order'] = Order::where('order_status','Complete')->count();
        $master['cancel_order'] = Order::where('order_status','Cancel')->count();
        $master['eventDate'] = array();
        $events = Event::where([['status',1],['is_deleted',0]])->orderBy('id','DESC')->get();              
        $day = Carbon::parse(Carbon::now()->year.'-'.Carbon::now()->month.'-01')->daysInMonth;                       
        $monthEvent = Event::whereBetween('start_time', [ date('Y')."-".date('m')."-01 00:00:00",  date('Y')."-".date('m')."-".$day." 23:59:59"])
        ->where([['status',1],['is_deleted',0]])
        ->orderBy('id','DESC')->get();
      
        foreach ($monthEvent as $value) {
            $value->tickets = Ticket::where('event_id',$value->id)->sum('quantity');
            $value->sold_ticket = Order::where('event_id',$value->id)->sum('quantity');         
            $value->average = $value->tickets==0? 0: $value->sold_ticket*100/$value->tickets;           
        }
        foreach ($events as $value) {
            $tickets = Ticket::where('event_id',$value->id)->sum('quantity');
            $sold_ticket = Order::where('event_id',$value->id)->sum('quantity'); 
            $value->avaliable = $tickets - $sold_ticket;
            array_push( $master['eventDate'], strlen(trim($value->start_time)) ? $value->start_time->format('Y-m-d') : "");
        }
    
        return view('admin.dashboard', compact('events','monthEvent','master'));
    }


    public function organizationDashboard(){        
        $master['total_tickets'] = Ticket::where('user_id',Auth::user()->id)->sum('quantity');
        $master['used_tickets'] = Order::where('organization_id',Auth::user()->id)->sum('quantity');
        $master['events'] = Event::where([['user_id',Auth::user()->id],['is_deleted',0]])->count();
        $master['total_order'] = Order::where('organization_id',Auth::user()->id)->count();
        $master['pending_order'] = Order::where([['order_status','Pending'],['organization_id',Auth::user()->id]])->count();
        $master['complete_order'] = Order::where([['order_status','Complete'],['organization_id',Auth::user()->id]])->count();
        $master['cancel_order'] = Order::where([['order_status','Cancel'],['organization_id',Auth::user()->id]])->count();
        $day = Carbon::parse(Carbon::now()->year.'-'.Carbon::now()->month.'-01')->daysInMonth; 
        $monthEvent = Event::whereBetween('start_time', [ date('Y')."-".date('m')."-01 00:00:00",  date('Y')."-".date('m')."-".$day." 23:59:59"])
        ->where([['status',1],['user_id',Auth::user()->id],['is_deleted',0]])
        ->orderBy('id','DESC')->get();      

        foreach ($monthEvent as $value) {
            $value->tickets = Ticket::where('event_id',$value->id)->sum('quantity');
            $value->sold_ticket = Order::where('event_id',$value->id)->sum('quantity');         
            $value->average = $value->tickets==0? 0: $value->sold_ticket*100/$value->tickets;           
        }

        $events = Event::where([['status',1],['user_id',Auth::user()->id],['is_deleted',0]])->orderBy('id','DESC')->get(); 
        $master['eventDate'] = array();
        foreach ($events as $value) {
            $tickets = Ticket::where('event_id',$value->id)->sum('quantity');
            $sold_ticket = Order::where('event_id',$value->id)->sum('quantity'); 
            $value->avaliable = $tickets - $sold_ticket;
            array_push( $master['eventDate'],$value->start_time->format('Y-m-d'));
        }
    
        return view('admin.org_dashboard', compact('events','monthEvent','master'));
    }

    public function viewProfile(){
        return view('admin.profile');
    }

    public function editProfile(Request $request){        
        User::find(Auth::user()->id)->update($request->all());
        return redirect('profile')->withStatus(__('Profile is updated successfully.'));
    }

    public function changePassword(Request $request){
        $request->validate([
            'current_password' => 'bail|required',
            'password' => 'bail|required|min:6',
            'confirm_password' => 'bail|required|same:password|min:6'
        ]);

        if (Hash::check($request->current_password, Auth::user()->password)){
            User::find(Auth::user()->id)->update(['password'=>Hash::make($request->password)]);
            return redirect('profile')->withStatus(__('Password is updated successfully.'));          
        }
        else{
            return Redirect::back()->with('error_msg','Current Password is wrong!');
        }
    }

    public function makePayment($id){
        $order = Order::with(['customer'])->find($id);
        return view('createPayment', compact('order'));
    }

    public function initialize(Request $request,$id)
    {
      Rave::initialize(route('callback',$id));
    }

    public function callback(Request $request,$id)
    {
        $payment_token = json_decode($request->resp)->tx->paymentId;
        $order = Order::find($id)->update(['payment_status'=>1,'payment_token'=> $payment_token]);
        return view('createPayment');
                 
    }

    public function changeLanguage($lang)
    {
        App::setLocale($lang);
        session()->put('locale', $lang);
        $directionData = Language::where('name',$lang)->first();
        $direction = 'ltr';
        if($directionData) {
            $direction = $directionData->direction;
        }
        session()->put('direction', $direction);
        return redirect()->back();
    }

    public function scanner(){
        if(Auth::user()->hasRole('admin'))
        {
            $scanners = User::role('scanner')->orderBy('id','DESC')->get();
        }
        else{
            $scanners = User::role('scanner')->where('org_id',Auth::user()->id)->orderBy('id','DESC')->get();
        }   
        foreach ($scanners as $value) {
            $value->total_event = Event::where('scanner_id',$value->id)->count();
        }             
        return view('admin.scanner.index',compact('scanners'));
    }
    
    public function scannerCreate(){
        return view('admin.scanner.create');
    }

    public function addScanner(Request $request){
      
        $request->validate([
            'first_name' => 'bail|required',
            'last_name' => 'bail|required',
            'email' => 'bail|required|email|unique:users',
            'phone' => 'bail|required',
            'password' => 'bail|required|min:6',
        ]);
        $data = $request->all();
        $data['org_id'] = Auth::user()->id;
        $data['password'] =  Hash::make($request->password); 
       
        $user = User::create($data);           
        $user->assignRole('scanner');
        return redirect('scanner')->withStatus(__('Scanner is added successfully.'));
    }

    public function blockScanner($id){
        $user = User::find($id);  
        $user->status = $user->status == "1" ? "0" : "1";
        $user->save();        
        return redirect('scanner')->withStatus(__('User status changed successfully.'));
    }

    public function getScanner($id){
        $data = User::where('org_id',$id)->orderBy('id','DESC')->get();
        return response()->json(['data' => $data,'success'=>true], 200); 
    }
}
