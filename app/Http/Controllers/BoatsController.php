<?php

namespace App\Http\Controllers;

use App\Models\Tax;
use ErrorException;
use Exception;
use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\PayoutType;
use App\Models\Maina;
use App\Models\DockType;
use App\Models\CancellationPolicy;
use App\Models\BoatOperator;
use App\Models\BoatModel;
use App\Models\BoatMakeCompany;
use App\Models\AvailabilityType;
use App\Models\ApplicablePhotos;
use App\Models\Applicable;
use App\Models\AllowedItems;
use App\Models\AdvanceNotice;
use App\Models\AllowedItemsReference;
use App\Models\BoatPrice;
use Illuminate\Http\Response;
use Log;
use Validator;
use Auth;

class BoatsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validationRules = [
            'step' => 'bail|required|integer|between:1,8',
            'category' => 'bail|required_if:step,1|exists:applicable,id',
            'reference' => 'bail|exists:events,id|required_if:step,2,3,4,5,6,7,8',
            'name' => 'bail|required_if:step,1',
            'pictures' => 'bail|required_if:step,1',
//            'pictures.*' => 'bail|mimes:jpg,png,jpeg',
            'description' => 'bail|required_if:step,1',
//            'location_type' => 'nullable|required_if:step,2',
            'maina_name' => 'bail|required_if:step,2',
            // 'slip_number' => 'bail|required_if:step,2',
            'dock_type' => 'bail|required_if:step,2|exists:dock_type,id',
            'country' => 'bail|required_if:step,2|exists:currency,id',
            'address' => 'bail|required_if:step,2',
//            'address_2' => 'nullable|required_if:step,2',
            'city' => 'bail|required_if:step,2',
//            'pincode' => 'nullable|required_if:step,2',
            'allowed_items' => 'bail|required_if:step,3',
            'allowed_items.*' => 'bail|exists:allowed_items,id',
            'availability_type' => 'bail|required_if:step,4|exists:availability_type,id',
            'availability_slots' => 'bail|required_if:step,4|array',
            'advance_notice' => 'bail|required_if:step,5',
            'advance_notice.*' => 'bail|exists:advance_notice,id',
            'boat_operator' => 'bail|required_if:step,5|exists:boat_operator,id',
            'multiple_booking_allowed' => 'bail|required_if:step,6|between:0,1',
            'time_gap' => 'bail|required_if:multiple_booking_allowed,1|between:1,23',
            'cancellation_policy' => 'bail|required_if:step,6|exists:cancellation_policy,id',
            'boat_prices' => 'bail|required_if:step,8',
            'boat_prices.*.hours' => 'bail|required_if:step,8|numeric',
            'boat_prices.*.price' => 'bail|required_if:step,8|numeric',
            'payout_type' => 'bail|required_if:step,8|exists:payout_type,id',
            'renter_pay_amount' => 'bail|required_if:step,8',

            'boat_model_year' => 'bail|required_if:step,7|numeric',
            'make_company' => 'bail|required_if:step,7',
            'boat_model' => 'bail|required_if:step,7',
            'boat_length' => 'bail|numeric|required_if:step,7',
            'total_rooms' => 'bail|numeric|required_if:step,7',
            'people' => 'bail|numeric|required_if:step,7',
            'boat_type' => 'bail|in:power,ordinary|required_if:step,7',
            // 'boat_category' => 'bail|required_if:step,7|exists:boat_model,boat_category',
            'rest_room' => 'bail|numeric|required_if:step,7',
        ];
        if($request->step == 1 && (isset($request->reference) && $request->reference != '' && is_numeric($request->reference))) {
            unset($validationRules['pictures']);
            unset($validationRules['pictures.*']);
        }
        if($request->step == 2 && !isset($request->pincode)) {
            $request->merge(['pincode' => null]);
        }
        $validation = Validator::make($request->all(),$validationRules);
//        \Log::info("Validation errors");
//        \Log::info($validation->errors()->all());
        if($validation->fails()) {
            \Log::info("Validation errors");
            \Log::info($validation->errors()->all());
            return response()->json(['msg' => 'Please fill all mandatory fields.', 'data' => implode(",",$validation->errors()->all()),'status'=>400], 400);
        }
        $status = ['status' => 400, 'data' => null, 'msg' => 'Something went wrong, try again later!!!'];
        $user = Auth::user();
        $events = new Event();
        if (isset($request->reference)) {
            $events = Event::find($request->reference);
        }
        switch ((int)$request->step) {
            case 1:
                try{
                    $events->applicable_type = $request->category;
                    $events->name = $request->name;
                    $events->description = $request->description;
                    $events->user_id = $user->id;
                    $events->status = 0;
                    $events->created_at = date("Y-m-d H:i:s");
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->save();
                    if($request->hasFile('pictures')) {
                        $imageRecords = [];
                        foreach ($request->file('pictures') as $key => $file) {
                            $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                            $fileName = $fileName.'.'.$file->getClientOriginalExtension();
                            $file->move(public_path('images/upload'),$fileName);
                            $imageRecords[] = [
                                'event_id' => $events->id,
                                'image_path' => 'images/upload/'.$fileName,
                                'status' => '1',
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                            ];
                        }
                        ApplicablePhotos::insert($imageRecords);
                    }
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
            case 2:
                try{
                    $events->location_type = $request->location_type;
                    $events->maina_name = $request->maina_name;
                    // $events->slip_number = $request->slip_number;
                    $events->dock_type = $request->dock_type;
                    $events->country = $request->country;
                    $events->address = $request->address;
                    $events->address_2 = $request->address_2;
                    $events->city = $request->city;
                    $events->pincode = $request->pincode;
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->status = 0;
                    if($request->lat) {
                        $events->lat = $request->lat;
                    }
                    if($request->lang) {
                        $events->lang = $request->lang;
                    }
                    $events->save();
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
            case 3:
                try{
                    $allowedItems = [];
                    foreach ($request->allowed_items as $key => $value) {
                        $allowedItems[] = [
                            'allowed_items' => $value,
                            'event_id' => $events->id,
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                        ];
                    }
                    AllowedItemsReference::where('event_id',$events->id)->delete();
                    AllowedItemsReference::insert($allowedItems);
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->status = 0;
                    $events->save();
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
            case 4:
                try{
                    $events->availability_type = $request->availability_type;
                    $events->availability_slots = $request->availability_slots;
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->status = 0;
                    $events->save();
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
            case 5:
                try{
                    $events->advance_notice = $request->advance_notice;
                    $events->boat_operator = $request->boat_operator;
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->status = 0;
                    $events->save();
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
            case 6:
                try{
                    $events->multiple_booking_allowed = $request->multiple_booking_allowed;
                    $events->cancellation_policy = $request->cancellation_policy;
                    if($request->multiple_booking_allowed) {
                        $events->time_gap = $request->time_gap;
                    }
                    $events->insurance_file = NULL;
                    if($request->insurance) {
                        $events->insurance = $request->insurance.'';
                        if($request->insurance == 1) {
                            if($request->hasFile('insurance_file')) {
                                $file = $request->file('insurance_file');
                                $fileName = 'insurance_'.$events->id.rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                                $fileName = $fileName.'.'.$file->getClientOriginalExtension();
                                $file->move(public_path('images/upload'),$fileName);
                                $events->insurance_file = 'images/upload/'.$fileName;
                            }
                        }
                    } else {
                        $events->insurance = '0';
                    }
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->status = 0;
                    $events->save();
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
            case 7:
                try{
                    $events->boat_model_year = $request->boat_model_year;
                    $events->make_company = $request->make_company;
                    $events->boat_model = $request->boat_model;
                    $events->boat_length = $request->boat_length;
                    $events->total_rooms = $request->total_rooms;
                    $events->people = $request->people;
                    $events->boat_type = $request->boat_type;
                    $events->boat_category = $request->boat_category;
                    $events->rest_room = $request->rest_room;
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->status = 0;
                    $events->save();
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
            default:
                try{
                    $boatPrices = [];
                    foreach ($request->boat_prices as $key => $value) {
                        $boatPrices[] = [
                            'event_id' => $events->id,
                            'hours' => $value['hours'],
                            'price' => $value['price'],
                            'created_at' => date("Y-m-d H:i:s"),
                            'updated_at' => date("Y-m-d H:i:s"),
                        ];
                    }
                    BoatPrice::where('event_id',$events->id)->delete();
                    BoatPrice::insert($boatPrices);
                    $events->payout_type = $request->payout_type;
                    $events->renter_pay_amount = $request->renter_pay_amount;
                    $events->is_complete = 1;
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->status = 0;
                    $events->save();
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Boat addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
        }
        if($status['status'] === 200) {
            $status['data'] = ['reference' => $events->id];
            $status['msg'] = 'success';
        }
        return response()->json($status, $status['status']);
    }

    public function updateImages(Request $request) {
        $validationRules = [
            'reference' => 'bail|exists:events,id|required',
            'pictures' => 'bail|required',
            'pictures.*' => 'bail|mimes:jpg,png,jpeg',
        ];
        $validation = Validator::make($request->all(),$validationRules);
        if($validation->fails()) {
            return response()->json(['msg' => 'Invalid details provided.', 'data' => implode(",",$validation->errors()->all()),'status'=>400], 400);
        }
        $status = ['status' => 400, 'data' => null, 'msg' => 'Something went wrong, try again later!!!'];
        $user = Auth::user();
        $events = Event::find($request->reference);
        try{
            $imageRecords = [];
            foreach ($request->file('pictures') as $key => $file) {
                $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $fileName = $fileName.'.'.$file->getClientOriginalExtension();
                $file->move(public_path('images/upload'),$fileName);
                $imageRecords[] = [
                    'event_id' => $events->id,
                    'image_path' => 'images/upload/'.$fileName,
                    'status' => '1',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ];
            }
            ApplicablePhotos::insert($imageRecords);
            $status['status'] = 200;
            $status['msg'] = 'success';
        } catch (Exception $e) {
            Log::info("Exception in Edit Image ".$request->reference);
            Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
        } catch (ErrorException $e) {
            Log::info("Error Exception in Edit Image ".$request->reference);
            Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
        }
        return response()->json($status, $status['status']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function boatSettings(Request $request) {
        $maniaNumbers = Maina::all();
        $allowedItems = AllowedItems::all();
        $cancellationPolicy = CancellationPolicy::all()->toArray();
        $splitPaymentCommission = Tax::where(['name' => 'split_payment_commission'])->first();
        if(!$splitPaymentCommission) {
            $splitPaymentCommission = [
                'name' => 'split_payment_commission',
                'price' => '0',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
                'allow_all_bill' => '1',
                'status' => '1',
                'user_id' => '1',
            ];
            $splitPaymentCommission = Tax::insert($splitPaymentCommission);
            $splitPaymentCommission = Tax::where(['name' => 'split_payment_commission'])->first();
        }
        // Group each object by applicable_for
        $groupedCancellationPolicy = [];
        foreach ($cancellationPolicy as $key => $value) {
            $groupedCancellationPolicy[$value['applicable_for']][] = $value;
        }
        $cancellationPolicy = $groupedCancellationPolicy;
        $boatModels = BoatModel::paginate(100);
        return view('admin.boat.index', compact('maniaNumbers','allowedItems','cancellationPolicy','boatModels','request','splitPaymentCommission'));
    }

    public function createMaina() {
        return view('admin.boat.create-maina');
    }

    public function createItems() {
        return view('admin.boat.create-items');
    }

    public function createModels() {
        return view('admin.boat.create-models');
    }

    public function editMaina($id) {
        $maina = Maina::find($id);
        return view('admin.boat.edit-maina', compact('maina'));
    }

    public function editItems($id) {
        $items = AllowedItems::find($id);
        return view('admin.boat.edit-items', compact('items'));
    }

    public function editCancellationPolicy($id) {
        $policy = CancellationPolicy::find($id);
        return view('admin.boat.edit-cancel-policy', compact('policy'));
    }

    public function editBoatModels($id) {
        $models = BoatModel::find($id);
        return view('admin.boat.edit-models', compact('models'));
    }

    public function editSplitPayment($id) {
        $models = Tax::find($id);
        return view('admin.boat.edit-split', compact('models'));
    }

    public function storeMaina(Request $request) {
        Maina::create($request->all());
        return redirect()->route('boat-settings')->withStatus(__('Maina is inserted successfully.'));
    }

    public function storeItems(Request $request) {
        $data = $request->all();
        if($request->hasFile('picture')) {
            $imageName = uniqid('allowed_items_').'.'.$request->picture->extension();
            $request->picture->storeAs('images',$imageName);
            $request->picture->move(public_path('images/upload'),$imageName);
            $data['image'] = "images/upload/".$imageName;
            unset($data['picture']);
        }
        unset($data['_token']);
        $data['status'] = '1';
        $data['created_at'] = $data['updated_at'] = date("Y-m-d H:i:s");
        $items = AllowedItems::insert([$data]);
        return redirect()->route('boat-settings')->withStatus(__('Allowed Item is updated successfully.'));
    }

    public function storeModels(Request $request) {
        BoatModel::create($request->all());
        return redirect()->route('boat-settings')->withStatus(__('Boat Model is inserted successfully.'));
    }

    public function updateMaina(Request $request, $id) {
        $Maina = Maina::find($id)->update($request->all());
        return redirect()->route('boat-settings')->withStatus(__('Maina is updated successfully.'));
    }

    public function updateItems(Request $request, $id) {
        $items = AllowedItems::find($id);
        $items->name = $request->name;
        $items->is_feature = $request->is_feature;
        $items->status = $request->status;
        if($request->hasFile('picture')) {
            $imageName = uniqid('allowed_items_').'.'.$request->picture->extension();
            $request->picture->storeAs('images',$imageName);
            $request->picture->move(public_path('images/upload'),$imageName);
            try{
                if(file_exists(public_path($items->image))) {
                    unlink(public_path($items->image));
                }
            } catch(\Exception $e) {

            }
            $items->image = "images/upload/".$imageName;
        }
        $items->updated_at = date("Y-m-d H:i:s");
        $items->save();
        return redirect()->route('boat-settings')->withStatus(__('Allowed Item is updated successfully.'));
    }

    public function updateModels(Request $request, $id) {
        $BoatModel = BoatModel::find($id)->update($request->all());
        return redirect()->route('boat-settings')->withStatus(__('Boat Model is updated successfully.'));
    }

    public function updateCancellationPolicy(Request $request, $id) {
        $CancellationPolicy = CancellationPolicy::find($id)->update($request->all());
        return redirect()->route('boat-settings')->withStatus(__('Cancellation Policy is updated successfully.'));
    }
    public function updateSplitPayment(Request $request, $id) {
        $Split = Tax::find($id)->update(['price' => $request->split_payment_commission]);
        return redirect()->route('boat-settings')->withStatus(__('Split payment commission is updated successfully.'));
    }
}
