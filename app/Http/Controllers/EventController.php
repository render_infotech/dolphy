<?php

namespace App\Http\Controllers;

use App\Http\Controllers\OrganizationApiController;
use App\Models\AllowedItemsReference;
use App\Models\ApplicablePhotos;
use App\Models\BoatPrice;
use App\Models\Event;
use App\Models\Category;
use App\Models\Location;
use App\Models\Ticket;
use App\Models\Order;
use App\Http\Controllers\AppHelper;
use App\Models\User;
use App\Models\AppUser;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Gate;
use Validator;
use App\Models\PayoutType;
use App\Models\Maina;
use App\Models\DockType;
use App\Models\CancellationPolicy;
use App\Models\BoatOperator;
use App\Models\BoatModel;
use App\Models\BoatMakeCompany;
use App\Models\AvailabilityType;
use App\Models\Applicable;
use App\Models\AllowedItems;
use App\Models\AdvanceNotice;
use App\Models\Currency;
use App\Models\Tax;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        abort_if(Gate::denies('event_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        if(Auth::user()->hasRole('admin')){
            $events = Event::with(['category','organization','pictures','applicable_type'])->where('is_deleted',0)->orderBy('id','DESC')->get();
        }
        elseif(Auth::user()->hasRole('organization')){
            $events = Event::with(['category','organization'])->where([['user_id',Auth::user()->id],['is_deleted',0]])->orderBy('id','DESC')->get();
            foreach ($events as $value) {
                $value->scanner = User::find($value->scanner_id);
            }
        }


        return view('admin.event.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        //
        $requestData = $request->all();
        abort_if(Gate::denies('event_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $category = Category::where('status',1)->orderBy('id','DESC')->get();
        $location = Location::where('status',1)->orderBy('id','DESC')->get();
        $users = User::role('organization')->orderBy('id','DESC')->get();
        if(Auth::user()->hasRole('admin')){
            $scanner = User::role('scanner')->orderBy('id','DESC')->get();
        }
        else if(Auth::user()->hasRole('organization')){
            $scanner = User::role('scanner')->where('org_id',Auth::user()->id)->orderBy('id','DESC')->get();
        }
        $selectedApplicable = 1;
        if(array_key_exists('category',$requestData) && $requestData['category'] != 1){
            $selectedApplicable = $requestData['category'];
            return view('admin.event.create', compact('category','location','users','scanner','selectedApplicable'));
        }
        $advanceNotice = AdvanceNotice::where('status','1')->get(['id','time']);
        $allowedItems = AllowedItems::where('status','1')->get(['id','name','image']);
        $boatOperator = BoatOperator::where('status','1')->get(['id','name']);
        $cancellationPolicy = CancellationPolicy::where('status','1')->get(['id','name']);
        $dockType = DockType::where('status','1')->get(['id','name']);
        $payoutType = PayoutType::where('status','1')->get(['id','name']);
        $maina = Maina::where('status','1')->get(['id','name']);
        $countries = Currency::get(['id','country']);
        $availabilitySlots = trans('content.availability_data');
        $availabilitySlots = $availabilitySlots[1]['data']['api_data'];
        $commission = Tax::where('name','commission')->where('user_id',1)->first('price');
        
        return view('admin.event.create-boat', compact( 'category','users','scanner','advanceNotice', 'allowedItems', 'boatOperator', 'cancellationPolicy', 'dockType', 'payoutType', 'maina', 'countries','selectedApplicable','availabilitySlots','commission'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        if((int)$request->applicable_type === 1){
            return $this->addBoat($request);
        }
        $data = $request->all();
        $validation = Validator::make($data,[
            'name' => 'bail|required',
            'pictures' => 'bail|required',
            'start_time' => 'bail|required',
            'end_time' => 'bail|required',
            'category_id' => 'bail|required',
            'type' => 'bail|required',
            'address' => 'bail|required_if:type,offline',
            'lat' => 'bail|required_if:type,offline',
            'lang' => 'bail|required_if:type,offline',
            'description' => 'bail|required',
            'people' => 'bail|required',
            'applicable_type' => 'bail|required',
        ]);
        if($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        if(!Auth::user()->hasRole('admin')){
            $data['user_id'] = Auth::user()->id;
            $data['status'] = 0;
        }
        unset($data['pictures']);
        if($data['applicable_type'] != 1){
            $event = Event::create($data);
            $imageRecords = [];
            foreach ($request->file('pictures') as $key => $file) {
                $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $fileName = $fileName.'.'.$file->getClientOriginalExtension();
                $file->move(public_path('images/upload'),$fileName);
                $imageRecords[] = [
                    'event_id' => $event->id,
                    'image_path' => 'images/upload/'.$fileName,
                    'status' => '1',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ];
            }
            ApplicablePhotos::where('event_id',$event->id)->delete();
            if(count($imageRecords)) {
                ApplicablePhotos::insert($imageRecords);
            }
            return redirect()->route('events.index')->withStatus(__('Event is added successfully.'));
        }
        return redirect()->route('events.index');
    }

    public function addBoat($request) {
        $data = $request->all();
        $rules = [
            'applicable_type' => 'bail|required|exists:applicable,id',
            'name' => 'bail|required',
            'description' => 'bail|required',
            'location_type' => 'bail|required',
            'maina_name' => 'bail|required|exists:maina,id',
            'slip_number' => 'bail|required',
            'dock_type' => 'bail|required|exists:dock_type,id',
            'country' => 'bail|required|exists:currency,id',
            'address' => 'bail|required',
            'address_2' => 'bail|required',
            'city' => 'bail|required',
            'pincode' => 'bail|required',
            'allowed_items' => 'bail|required',
            'allowed_items.*' => 'bail|exists:allowed_items,id',
            'availability_type' => 'bail|required|exists:availability_type,id',
            'advance_notice' => 'bail|required',
            'advance_notice.*' => 'bail|exists:advance_notice,id',
            'boat_operator' => 'bail|required|exists:boat_operator,id',
            'multiple_booking_allowed' => 'bail|required|between:0,1',
            'time_gap' => 'bail|required_if:multiple_booking_allowed,1|between:1,23',
            'cancellation_policy' => 'bail|required|exists:cancellation_policy,id',
            'payout_type' => 'bail|required|exists:payout_type,id',
            'insurance' => 'bail|required|between:0,1',
        ];
        $pics = 0;
        if(!array_key_exists('id',$data)) {
            $pics = 1;
            $rules['pictures'] = 'bail|required';
//            $rules['pictures.*'] = 'bail|mimes:jpg,png,jpeg';
        }
        $validation = Validator::make($request->all(),$rules);
        if($validation->fails()) {
            return redirect()->back()->withErrors($validation)->withInput();
        }
        $events = new Event();
        if(!$pics) {
            $events = Event::where('id',$data['id'])->first();
        }
        if(!Auth::user()->hasRole('admin')){
            $events->user_id = Auth::user()->id;
            $events->status = 0;
        }
        $events->applicable_type = $request->applicable_type;
        $events->name = $request->name;
        $events->description = $request->description;
        $events->location_type = $request->location_type;
        $events->maina_name = $request->maina_name;
        $events->slip_number = $request->slip_number;
        $events->dock_type = $request->dock_type;
        $events->country = $request->country;
        $events->address = $request->address;
        $events->address_2 = $request->address_2;
        $events->city = $request->city;
        $events->pincode = $request->pincode;
        $events->availability_type = $request->availability_type;
//        echo "<pre>";print_r($request->availability_slots);exit;
        if($request->availability_type == 2) {
            $availabilitySlots = trans('content.availability_data');
            $availabilitySlots = $availabilitySlots[1]['data']['api_data'];
            foreach ($request->availability_slots as $key => $value) {
                foreach ($value as $key1 => $value1) {
                    $availabilitySlots[$key]['data'][$key1]['status'] = true;
                }
            }
            $events->availability_slots = json_encode($availabilitySlots);
        }
        $events->advance_notice = json_encode($request->advance_notice);
        $events->boat_operator = $request->boat_operator;
        $events->multiple_booking_allowed = $request->multiple_booking_allowed;
        $events->insurance = $request->insurance;
        $events->user_id = Auth::user()->id;
        if(Auth::user()->hasRole('admin')){
            $events->user_id = $request->user_id;
        }
        $events->insurance_file = NULL;
        if($request->insurance == 1 && $request->hasFile('insurance_file')) {
            $file = $request->file('insurance_file');
            $fileName = 'insurance_admin_'.$events->user_id.rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/upload'),$fileName);
            $events->insurance_file = 'images/upload/'.$fileName;
        }
        $events->cancellation_policy = $request->cancellation_policy;
        if($request->multiple_booking_allowed) {
            $events->time_gap = $request->time_gap;
        }
        $events->payout_type = $request->payout_type;
        $events->is_complete = 1;
        $events->status = 0;
        $events->created_at = date("Y-m-d H:i:s");
        $events->updated_at = date("Y-m-d H:i:s");
        $events->save();


        $boatPrices = [];
        foreach ($request->boat_prices as $key => $value) {
            if($value['price'] != '') {
                $boatPrices[] = [
                    'event_id' => $events->id,
                    'hours' => $value['hours'],
                    'price' => $value['price'],
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ];
            }
        }
        BoatPrice::where('event_id',$events->id)->delete();
        BoatPrice::insert($boatPrices);
        $allowedItems = [];
        foreach ($request->allowed_items as $key => $value) {
            $allowedItems[] = [
                'allowed_items' => $value,
                'event_id' => $events->id,
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];
        }
        AllowedItemsReference::where('event_id',$events->id)->delete();
        AllowedItemsReference::insert($allowedItems);
        $statusText = "updated";
        if($pics) {
            $statusText = "added";
            $imageRecords = [];
            foreach ($request->file('pictures') as $key => $file) {
                $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                $fileName = $fileName.'.'.$file->getClientOriginalExtension();
                $file->move(public_path('images/upload'),$fileName);
                $imageRecords[] = [
                    'event_id' => $events->id,
                    'image_path' => 'images/upload/'.$fileName,
                    'status' => '1',
                    'created_at' => date("Y-m-d H:i:s"),
                    'updated_at' => date("Y-m-d H:i:s"),
                ];
            }
            ApplicablePhotos::where('event_id',$events->id)->delete();
            if(count($imageRecords)) {
                ApplicablePhotos::insert($imageRecords);
            }
        }
        return redirect()->route('events.index')->withStatus(__('Boat is '.$statusText.' successfully.'));
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        //
        $event = Event::with(['category','organization','pictures','applicable_type'])->where('id',$event->id)->get()->first();
        $event->ticket = Ticket::where([['event_id',$event->id],['is_deleted',0]])->orderBy('id','DESC')->get();
        $event->sales = Order::with(['customer:id,name','ticket:id,name'])->where('event_id',$event->id)->orderBy('id','DESC')->get();
        foreach ($event->ticket as $value) {
            $value->used_ticket = Order::where('ticket_id',$value->id)->sum('quantity');
        }
        return view('admin.event.view', compact( 'event'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function edit(Event $event)
    {
        //
        abort_if(Gate::denies('event_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        $category =  Category::where('status',1)->orderBy('id','DESC')->get();
        $users = User::role('organization')->orderBy('id','DESC')->get();
        if(Auth::user()->hasRole('admin')){
            $scanner = User::role('scanner')->orderBy('id','DESC')->get();
        }
        else if(Auth::user()->hasRole('organization')){
            $scanner = User::role('scanner')->where('org_id',Auth::user()->id)->orderBy('id','DESC')->get();
        }
        if((int)$event->applicable_type === 1){
            $event = Event::with(['maina_name','dock_type','availability_type','boat_operator','cancellation_policy','payout_type','category','organization','pictures','applicable_type'])->where('id',$event->id)->get()->first();
            $advanceNotice = AdvanceNotice::where('status','1')->get(['id','time']);
            $allowedItems = AllowedItems::where('status','1')->get(['id','name','image']);
            $boatOperator = BoatOperator::where('status','1')->get(['id','name']);
            $cancellationPolicy = CancellationPolicy::where('status','1')->get(['id','name']);
            $dockType = DockType::where('status','1')->get(['id','name']);
            $payoutType = PayoutType::where('status','1')->get(['id','name']);
            $maina = Maina::where('status','1')->get(['id','name']);
            $countries = Currency::get(['id','country']);
            $allowedItemsReference = AllowedItemsReference::where('event_id',$event->id)->get();
            if(count($allowedItemsReference)) {
                $allowedItemsReference = $allowedItemsReference->pluck('allowed_items')->toArray();
            } else {
                $allowedItemsReference = [];
            }
            $boatPrice = BoatPrice::where('event_id',$event->id)->get();
            if(count($boatPrice)) {
                $boatPrice = $boatPrice->pluck('price','hours')->toArray();
            } else {
                $boatPrice = [];
            }
            $commission = Tax::where('name','commission')->where('user_id',1)->first('price');
            $availabilitySlots = trans('content.availability_data');
            $availabilitySlots = $availabilitySlots[1]['data']['api_data'];

            return view('admin.event.edit_boat', compact( 'event','category','users','scanner','advanceNotice', 'allowedItems', 'boatOperator', 'cancellationPolicy', 'dockType', 'payoutType', 'maina', 'countries', 'allowedItemsReference','availabilitySlots','boatPrice','commission'));
        }
        $event = Event::with(['category','organization','pictures','applicable_type'])->where('id',$event->id)->get()->first();
        return view('admin.event.edit', compact( 'event','category','users','scanner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        //
        if((int)$event->applicable_type === 1){
            $rules = [
                'name' => 'bail|required',
                'description' => 'bail|required',
                'location_type' => 'bail|required',
                'maina_name' => 'bail|required|exists:maina,id',
                'slip_number' => 'bail|required',
                'dock_type' => 'bail|required|exists:dock_type,id',
                'country' => 'bail|required|exists:currency,id',
                'address' => 'bail|required',
                'address_2' => 'bail|required',
                'city' => 'bail|required',
                'pincode' => 'bail|required',
                'allowed_items' => 'bail|required',
                'allowed_items.*' => 'bail|exists:allowed_items,id',
                'availability_type' => 'bail|required|exists:availability_type,id',
                'availability_slots' => 'bail|required_if:availability_type,2|array',
                'advance_notice' => 'bail|required',
                'advance_notice.*' => 'bail|exists:advance_notice,id',
                'boat_operator' => 'bail|required|exists:boat_operator,id',
                'multiple_booking_allowed' => 'bail|required|between:0,1',
                'time_gap' => 'bail|required_if:multiple_booking_allowed,1|between:1,23',
                'insurance' => 'bail|required|between:0,1',
                'cancellation_policy' => 'bail|required|exists:cancellation_policy,id',
                'boat_prices' => 'bail|required|array',
                'payout_type' => 'bail|required|exists:payout_type,id',
            ];
            $validation = Validator::make($request->all(),$rules);
            if($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
            $data = $request->all();
        } else {
            $data = $request->all();
            $validation = Validator::make($data,[
                'name' => 'bail|required',
                'start_time' => 'bail|required',
                'end_time' => 'bail|required',
                'category_id' => 'bail|required',
                'type' => 'bail|required',
                'address' => 'bail|required_if:type,offline',
                'lat' => 'bail|required_if:type,offline',
                'lang' => 'bail|required_if:type,offline',
                'description' => 'bail|required',
                'people' => 'bail|required',
                'applicable_type' => 'bail|required',
            ]);
            if($validation->fails()) {
                return redirect()->back()->withErrors($validation)->withInput();
            }
            $data['start_time'] = date('Y-m-d H:i:s',strtotime($data['start_time']));
            $data['end_time'] = date('Y-m-d H:i:s',strtotime($data['end_time']));
        }

        if($request->insurance == 1 && $request->hasFile('insurance_file')) {
            $file = $request->file('insurance_file');
            $fileName = 'insurance_'.$event->id.rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/upload'),$fileName);
            $data['insurance_file'] = 'images/upload/'.$fileName;
        }
        if($request->insurance == 0) {
            $data['insurance_file'] = NULL;
        }
        $event = Event::find($event->id)->update($data);
        return redirect()->route('events.index')->withStatus(__('Event is updated successfully.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event)
    {
        //
        try{
            Event::find($event->id)->update(['is_deleted'=>1,'event_status'=>'Deleted']);
            $ticket = Ticket::where('event_id',$event->id)->update(['is_deleted'=>1]);
            return true;
        }catch(Throwable $th){
            return response('Data is Connected with other Data', 400);
        }


    }

    public function getMonthEvent(Request $request){
        $day = Carbon::parse($request->year.'-'.$request->month.'-01')->daysInMonth;
        if(Auth::user()->hasRole('organization')){
            $data = Event::whereBetween('start_time', [ $request->year."-".$request->month."-01 12:00 AM",  $request->year."-".$request->month."-".$day." 11:59 PM"])
            ->where([['status',1],['is_deleted',0],['user_id',Auth::user()->id]])
            ->orderBy('id','DESC')
            ->get();
        }
        if(Auth::user()->hasRole('admin'))
        {
            $data = Event::whereBetween('start_time', [ $request->year."-".$request->month."-01 12:00 AM",  $request->year."-".$request->month."-".$day." 11:59 PM"])
            ->where([['status',1],['is_deleted',0]])
            ->orderBy('id','DESC')
            ->get();
        }
        foreach ($data as $value) {
            $value->tickets = Ticket::where([['event_id',$value->id],['is_deleted',0]])->sum('quantity');
            $value->sold_ticket = Order::where('event_id',$value->id)->sum('quantity');
            $value->day = $value->start_time->format('D');
            $value->date = $value->start_time->format('d');
            $value->average = $value->tickets==0? 0: $value->sold_ticket*100/$value->tickets;
        }
        return response()->json([ 'data' => $data,'success'=>true], 200);
    }

    public function eventGallery($id){
        $data  = Event::where('id',$id)->with(['pictures','applicable_type'])->get();
        $data = $data[0];
        return view('admin.event.gallery',compact( 'data'));
    }

    public function addEventGallery(Request $request){
        if ($request->hasFile('file')) {
            $imageRecords = [];
            $file = $request->file('file');
            $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
            $fileName = $fileName.'.'.$file->getClientOriginalExtension();
            $file->move(public_path('images/upload'),$fileName);
            $imageRecords[] = [
                'event_id' => $request->id,
                'image_path' => 'images/upload/'.$fileName,
                'status' => '1',
                'created_at' => date("Y-m-d H:i:s"),
                'updated_at' => date("Y-m-d H:i:s"),
            ];
            ApplicablePhotos::insert($imageRecords);
        }
        return true;
    }

    public function removeEventImage($image,$id){
        $imagePath = ApplicablePhotos::find($id);
        try{
            if(file_exists(public_path($imagePath->image_path))){
                unlink(public_path($imagePath->image_path));
            }
        } catch (\Exception $e) {}
        $imagePath->delete();
        return redirect()->back();
    }

    public function store_app(Request $request) {
        $validationRules = [
            'step' => 'bail|required|integer|between:1,2',
            'category' => 'bail|required_if:step,1|exists:applicable,id',
            'reference' => 'bail|exists:events,id|required_if:step,2',
            'name' => 'bail|required_if:step,1',
            'event_category' => 'bail|required_if:step,1|exists:category,id',
            'people' => 'bail|required_if:step,1|numeric|min:1',
            'description' => 'bail|required_if:step,1',
            'type' => 'bail|required_if:step,1|in:online,offline',
            'pictures' => 'bail|required_if:step,1',
//            'pictures.*' => 'bail|mimes:jpg,png,jpeg',

            'address' => 'bail|required_if:step,1',
            'venue_title' => 'bail|required_if:step,1',
            'lat' => 'bail|required_if:step,1|numeric',
            'lang' => 'bail|required_if:step,1|numeric',
            'city' => 'bail|required_if:step,1',
//            'is_public' => 'bail|required_if:step,1|numeric',

            'start_time' => 'bail|required_if:step,2|date_format:Y-m-d',
            'end_time' => 'bail|required_if:step,2|date_format:Y-m-d',
            'availability_type' => 'bail|required_if:step,2|exists:availability_type,id',
            'availability_slots' => 'bail|required_if:step,2|array',
            'recurring' => 'bail|required_if:step,2|integer|between:0,1',
            'from_customer' => 'bail|required_if:step,2',
            'to_customer' => 'bail|required_if:step,2',
            'cancellation_policy' => 'bail|required_if:step,2|exists:cancellation_policy,id',
        ];
        if($request->step == 1 && (isset($request->reference) && $request->reference != '' && is_numeric($request->reference))) {
            unset($validationRules['pictures']);
//            unset($validationRules['pictures.*']);
        }
        $validation = Validator::make($request->all(),$validationRules);
//        \Log::info("Validation errors");
//        \Log::info($validation->errors()->all());
        if($validation->fails()) {
            return response()->json(['msg' => 'Please fill all mandatory fields.', 'data' => implode(",",$validation->errors()->all()),'status'=>400], 400);
        }
        $status = ['status' => 400, 'data' => null, 'msg' => 'Something went wrong, try again later!!!'];
        $user = Auth::user();
        $events = new Event();
        if (isset($request->reference)) {
            $events = Event::find($request->reference);
        }
        switch ((int)$request->step) {
            case 1:
                try{
                    $events->applicable_type = $request->category;
                    $events->name = $request->name;
                    $events->category_id = $request->event_category;
                    $events->people = $request->people;
                    $events->description = $request->description;
                    $events->type = $request->type;
                    $events->user_id = $user->id;
                    $events->status = 0;
                    $events->created_at = date("Y-m-d H:i:s");
                    $events->address = $request->address;
                    // $events->address_2 = $request->address_2;
                    $events->venue_title = $request->venue_title;
                    $events->lat = $request->lat;
                    $events->lang = $request->lang;
                    if($request->city) {
                        $events->city = $request->city;
                    }
                    $events->is_public = $request->is_public;
                    $events->save();
                    if($request->hasFile('pictures')) {
                        $imageRecords = [];
                        foreach ($request->file('pictures') as $key => $file) {
                            $fileName = time().rand(0, 1000).pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
                            $fileName = $fileName.'.'.$file->getClientOriginalExtension();
                            $file->move(public_path('images/upload'),$fileName);
                            $imageRecords[] = [
                                'event_id' => $events->id,
                                'image_path' => 'images/upload/'.$fileName,
                                'status' => '1',
                                'created_at' => date("Y-m-d H:i:s"),
                                'updated_at' => date("Y-m-d H:i:s"),
                            ];
                        }
                        ApplicablePhotos::insert($imageRecords);
                    }
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Game or Events addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Game or Events addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
            default:
                try{
                    $events->start_time = $request->start_time.' 00:00:00';
                    $events->end_time = $request->end_time.' 23:59:59';
                    $events->availability_type = $request->availability_type;
                    $events->availability_slots = $request->availability_slots;
                    $events->recurring = $request->recurring;
                    $events->from_customer = $request->from_customer;
                    $events->to_customer = $request->to_customer;
                    $events->cancellation_policy = $request->cancellation_policy;
                    $events->is_complete = 1;
                    $events->updated_at = date("Y-m-d H:i:s");
                    $events->status = 0;
                    $events->save();
                    $status['status'] = 200;
                } catch (Exception $e) {
                    Log::info("Exception in Game or Events addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                } catch (ErrorException $e) {
                    Log::info("Error Exception in Game or Events addition Step ".$request->step);
                    Log::info($e->getMessage()." in ".$e->getFile()." at ".$e->getLine());
                }
                break;
        }
        if($status['status'] === 200) {
            $status['data'] = ['reference' => $events->id];
            $status['msg'] = 'success';
        }
        return response()->json($status, $status['status']);
    }
}
