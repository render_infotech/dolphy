<?php

namespace App\Providers;

use \App\Models\Setting;
use \App\Models\Currency;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\URL;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Schema::defaultStringLength(191);      
        view()->composer('*', function ($view) {
            if(env('DB_DATABASE')!=null){
                $cur = Setting::find(1)->currency; 
                $currency = Currency::where('code',$cur)->first()->symbol;
                $currencycode = Currency::where('code',$cur)->first()->code; 
                $view->with('currency', $currency);
                $view->with('currencycode', $currencycode);
            }         
        });
        if(env('APP_ENV') !== 'local') {
            URL::forceScheme('https');
        }
    }
}
