<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CountryCode extends Model
{
    use HasFactory;
    protected $fillable = [
        'country',
        'code',
        'customer_status',
        'vendor_status',
    ];

    protected $table = 'country_codes';
}
