<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllowedItems extends Model
{
    use HasFactory;
    protected $table = "allowed_items";
    protected $fillable = ['name','status','is_feature','created_at','updated_at'];
}
