<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maina extends Model
{
    use HasFactory;
    protected $table = "maina";
    protected $fillable = ['name','status','created_at','updated_at'];
}
