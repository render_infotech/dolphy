<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CancellationPolicy extends Model
{
    use HasFactory;
    protected $table = "cancellation_policy";
    protected $fillable = ['name','status','created_at','updated_at','policy_description','applicable_for'];
}
