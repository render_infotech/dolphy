<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdvanceNotice extends Model
{
    use HasFactory;
    protected $table = "advance_notice";
    protected $fillable = ['time','status','created_at','updated_at'];
}
