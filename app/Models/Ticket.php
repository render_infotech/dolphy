<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $fillable = [
        'event_id',
        'user_id',
        'ticket_number',
        'name',
        'type',
        'quantity',
        'ticket_per_order',
        'start_time',
        'end_time',
        'price',
        'description', 
        'status',
        'is_deleted',
    ];
    protected $casts = [
        'created_at' => 'datetime:d M Y h:i A',
        'updated_at' => 'datetime:d M Y h:i A',
        'start_time' => 'datetime:Y-m-d H:i:s',
        'end_time' => 'datetime:Y-m-d H:i:s',
        
    ];

    protected $table = 'tickets';
    protected $dates = ['start_time','end_time'];

    public static function paymentHistory()
    {
        return Ticket::where('events.user_id', auth()->user()->id)->join('events', 'events.id', '=', 'tickets.event_id')->select('events.name', 'tickets.quantity', 'tickets.price', 'tickets.created_at')->orderBy('tickets.created_at','desc')->get();
    }
}
