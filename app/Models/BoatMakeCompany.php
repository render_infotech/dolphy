<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoatMakeCompany extends Model
{
    use HasFactory;
    protected $table = "make_company";
    protected $fillable = ['name','status','created_at','updated_at'];
}
