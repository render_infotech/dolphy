<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoatOperator extends Model
{
    use HasFactory;
    protected $table = "boat_operator";
    protected $fillable = ['name','status','created_at','updated_at'];
}
