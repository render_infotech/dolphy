<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DockType extends Model
{
    use HasFactory;
    protected $table = "dock_type";
    protected $fillable = ['name','status','created_at','updated_at'];
}
