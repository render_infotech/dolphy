<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chatroom extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'vendor_id',
        'status',
        
    ];

    protected $table = 'chatroom';


    public function messages()
    {
        return $this->hasMany('App\Models\Chatmessages', 'chat_id', 'id');
    }

    public function vendor()
    {
        return $this->hasOne('App\Models\User', 'id', 'vendor_id')->select('first_name','last_name','email','image','phone','id');
    }

    public function userdata()
    {
        return $this->hasOne('App\Models\AppUser', 'id', 'user_id')->select('name','last_name','email','image','phone','id');
    }
}
