<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AvailabilityType extends Model
{
    use HasFactory;
    protected $table = "availability_type";
    protected $fillable = ['name','status','created_at','updated_at'];
}
