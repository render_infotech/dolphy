<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id',
        'customer_id',
        'organization_id',
        'event_id',
        'ticket_id',
        'coupon_id',
        'quantity',
        'coupon_discount',
        'payment',
        'tax',
        'org_commission',
        'payment_type',
        'payment_status',
        'payment_token',        
        'order_status',   
        'org_pay_status',
        'advance_deposite',
        'total_payment',
        'min_payment'     
    ];    

    protected $table = 'orders';
    protected $appends = ['review'];

    public function event()
    {
        return $this->hasOne('App\Models\Event', 'id', 'event_id')->with('cancellation_policy')->with('pictures');
    }
    public function ticket()
    {
        return $this->hasOne('App\Models\Ticket', 'id', 'ticket_id');
    }
    public function customer()
    {
        return $this->hasOne('App\Models\AppUser', 'id', 'customer_id');
    }
    public function organization()
    {
        return $this->hasOne('App\Models\User', 'id', 'organization_id');
    }
    public function cancellation_policy()
    {
        return $this->hasOne('App\Models\CancellationPolicy', 'id', 'cancellation_policy');
    }
    public function getReviewAttribute()
    {
        return Review::where('order_id',$this->attributes['id'])->first();
    }
    public static function getOrganisationTotalEarnings()
    {
        $orders = Order::where('organization_id',auth()->user()->id)->with('event')->with('ticket')->get();
        $total = 0;
        if($orders){
            foreach($orders as $order){
                if(isset($order->ticket->price)){
                    $total += ($order->ticket->price * $order->quantity) - ($order->coupon_discount + $order->org_commission);
                }
            }
        }
        if ($total >= 1000) {
            $total = ($total/1000).'K';
        } else if ($total >= 1000000) {
            $total = ($total/1000000).'M';
        } else if ($total >= 1000000000) {
            $total = ($total/1000000000).'B';
        } else {
            $total = $total.'';
        }
        return $total;
    }
    public static function getOrganisationTotalBookings()
    {
        return Order::where('organization_id',auth()->user()->id)->with('event')->with('ticket')->count();
    }

    public function pictures() {
        return $this->hasMany('App\Models\ApplicablePhotos', 'event_id', 'id')->where('status','1')->select('id','event_id','image_path');
    }
}
