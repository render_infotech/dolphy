<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Splitpayment extends Model
{
    use HasFactory;
    protected $fillable = [
        'order_id',
        'user_id',
        'pay_amount',
        'paid_status',
        'stripe_payment_id',
        'is_main_user',
        'payment_url',
    ];

    protected $table = 'splitpayments';
    public function user()
    {
        return $this->hasOne('App\Models\AppUser', 'id', 'user_id') ;
    }
}
