<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SmsEvents extends Model
{
    use HasFactory;
    protected $table = 'sms_events';
    protected $fillable = ['mobile','otp','is_otp','attempts','resend_attempts','verified_at','valid_till','sms_content','created_at','updated_at'];
}
