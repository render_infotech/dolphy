<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ApplicablePhotos extends Model
{
    use HasFactory;
    protected $table = "pictures";
    protected $fillable = ['event_id','image_path','is_banner','status','created_at','updated_at'];
}
