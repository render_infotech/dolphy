<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankAccounts extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'user_type',
        'account_holder_name',
        'account_holder_type',
        'account_number',
        'currency',
        'bic',
        'country',
        'document_file',
        'stripe_account_id',
        'column1',
        'column2',
        'column3',
        'column4',
        'column5',
        'column6',
        'column7',
        'column8',
        'column9',
        'column10',
        'created_at',
        'updated_at',
    ];

    protected $table = 'bank_accounts';
}
