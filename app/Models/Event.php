<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Event extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'user_id',
        'type',
        'address',
        'category_id',
        'start_time',
        'end_time',
        'image',
        'gallery',
        'people',
        'lat',
        'lang',
        'description',
        'security',
        'status',
        'event_status',
        'is_deleted',
        'scanner_id',
        'tags',
        'applicable_type',
        'venue_title',
        'is_popular',
        'boat_model_year',
        'boat_length',
        'total_rooms',
        'boat_type',
        'boat_category',
        'insurance',
        'insurance_file',
        'rest_room',
        'renter_pay_amount',
        'recurring',
        'from_customer',
        'to_customer',

    ];

    protected $table = 'events';
    protected $dates = ['start_time','end_time'];
    protected $appends = ['imagePath','rate','totalTickets','soldTickets'];
    protected $casts = [
        'start_time' => 'datetime:Y-m-d H:i:s',
        'end_time' => 'datetime:Y-m-d H:i:s',
    ];

    public function category()
    {
        return $this->hasOne('App\Models\Category', 'id', 'category_id');
    }
    public function availabledays()
    {
        return $this->hasMany('App\Models\Availabledays', 'event_id', 'id');
    }

    public function organization()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
    public function ticket()
    {
        return $this->hasMany('App\Models\Ticket', 'event_id', 'id')->where('tickets.start_time','>=',date("Y-m-d H:i:s"));
    }
    public function allowed_items()
    {
        return $this->hasMany('App\Models\AllowedItemsReference', 'event_id', 'id')->join('allowed_items', 'allowed_items.id', '=', 'allowed_items_references.allowed_items')->select('allowed_items_references.*','allowed_items.is_feature','allowed_items.name','allowed_items.image');
    }
    public function boat_prices()
    {
        return $this->hasMany('App\Models\BoatPrice', 'event_id', 'id');
    }

    public function dock_type()
    {
        return $this->hasOne('App\Models\DockType', 'id', 'dock_type')->select('id','name');
    }

    public function maina_name()
    {
        return $this->hasOne('App\Models\Maina', 'id', 'maina_name')->select('id','name');
    }

    public function country()
    {
        return $this->hasOne('App\Models\Currency', 'id', 'country')->select('id','country');
    }

    public function availability_type()
    {
        return $this->hasOne('App\Models\AvailabilityType', 'id', 'availability_type')->select('id','name');
    }

    public function boat_operator()
    {
        return $this->hasOne('App\Models\BoatOperator', 'id', 'boat_operator')->select('id','name');
    }

    public function cancellation_policy()
    {
        return $this->hasOne('App\Models\CancellationPolicy', 'id', 'cancellation_policy')->select('id','name','policy_description');
    }

    public function payout_type()
    {
        return $this->hasOne('App\Models\PayoutType', 'id', 'payout_type')->select('id','name');
    }
    
    public function getImagePathAttribute()
    {
        return url('images/upload') . '/';
    }

    public function getTotalTicketsAttribute()
    {
        $timezone = Setting::find(1)->timezone;       
        $date = Carbon::now($timezone); 

        if($this->attributes['id'] == 1){
            return $this->attributes['people'];
        }else{
            return (int)Ticket::where([['event_id',$this->attributes['id']],['is_deleted',0],['status',1],['end_time', '>=',$date->format('Y-m-d H:i:s')],['start_time', '<=',$date->format('Y-m-d H:i:s')]])->sum('quantity');
        }
        
    }

    public function getSoldTicketsAttribute()
    {
        return (int)Order::where('event_id',$this->attributes['id'])->sum('quantity');
    }    

    public function getRateAttribute()
    {
        $review =  Review::where('event_id', $this->attributes['id'])->get(['rate']);
        if(count($review)>0){
            $totalRate = 0;
            foreach ($review as $r) {
                $totalRate=$totalRate+$r->rate;
            }
            return  round($totalRate / count($review)) ;
        }else{
            return 0;
        }  
    }

    public function scopeDurationData($query,$start , $end){
        $data =  $query->whereBetween('start_time', [ $start,  $end]);
        return $data;
    }

    public function getPendingEntries($userID) {
        $data = $this->where([['user_id',$userID],['is_complete',0]])->get();
        return $data;
    }

    public function pictures() {
        return $this->hasMany('App\Models\ApplicablePhotos', 'event_id', 'id')->where('status','1');
    }

    public function applicable_type() {
        return $this->hasOne('App\Models\Applicable', 'id', 'applicable_type')->where('status','1');
    }
}
