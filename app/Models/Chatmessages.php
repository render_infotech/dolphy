<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Chatmessages extends Model
{
    use HasFactory;
    protected $fillable = [
        'chat_id',
        'message',
        'msg_status',
        'action',
        'user_id',
        'vendor_id',
        'action_value',
        'is_vendor_replied'
        
    ];

    protected $table = 'chatmessages';
}
