<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxForms extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'file_path',
        'month',
        'year',
        'status',
        'created_at',
        'deleted_at',
    ];

    protected $table = 'tax_forms';

    public function organization()
    {
        return $this->hasOne('App\Models\User', 'id', 'user_id');
    }
}
