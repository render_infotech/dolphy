<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Applicable extends Model
{
    use HasFactory;
    protected $table = "applicable";
    protected $fillable = ['name','status','created_at','updated_at'];
}
