<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Availabledays extends Model
{
    use HasFactory;
    protected $fillable = [
        'event_id',
        'day',
    ];

    protected $table = 'availabledays';
}
