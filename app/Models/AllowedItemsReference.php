<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AllowedItemsReference extends Model
{
    use HasFactory;
    protected $table = 'allowed_items_references';
    protected $fillable = ['event_id','allowed_items','created_at','updated_at'];
}
