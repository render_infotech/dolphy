<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BoatModel extends Model
{
    use HasFactory;
    protected $table = "boat_model";
    protected $fillable = ['name','boat_category','make_company','status','created_at','updated_at'];
}
