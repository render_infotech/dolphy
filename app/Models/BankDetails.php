<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BankDetails extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'user_type',
        'account_number',
        'routing_number',
        'account_holder_type',
        'account_holder_name',
        'country',
        'currency',
        'email',
        'managed',
        'stripe_id',
        'stripe_response',
        'created_at',
        'updated_at',
    ];

    protected $table = 'bank_details';
}
