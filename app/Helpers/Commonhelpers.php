<?php

if(!function_exists('generateOTP')) {
    function generateOTP($length = 6,$isAlphaNumeric = false) {
        $alphaNumeric = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890";
        $numeric = "1234567890";
        $otp = "";
        if($isAlphaNumeric) {
            for($i = 0; $i < $length; $i++) {
                $otp .= $alphaNumeric[rand(0,(strlen($alphaNumeric)-1))];
            }
        } else {
            for($i = 0; $i < $length; $i++) {
                $otp .= $numeric[rand(0,(strlen($numeric)-1))];
            }
        }
        if((strlen(trim(env('TWILIO_SID'))) == 0 || strlen(trim(env('TWILIO_NUMBER'))) == 0)) {
            if($length == 4) {
                $otp = "1234";
            } else if($length == 6) {
                $otp = "123456";
            } else if($length == 8) {
                $otp = "12345678";
            }
        }
        return $otp;
    }
}

if(!function_exists('sendSMS')) {
    function sendSMS($phone, $message)
    {
        Log::info("SMS Log");
        Log::info($phone . " - " . $message);
        $status = false;
        if (strlen(trim(env('TWILIO_SID'))) > 0 && strlen(trim(env('TWILIO_NUMBER'))) > 0) {
            try {
                $client = new Twilio\Rest\Client(env('TWILIO_SID'), env('TWILIO_AUTH_TOKEN'));
                $client->messages->create($phone, ['from' => env('TWILIO_NUMBER'), 'body' => $message]);
                $status = true;
            } catch (Exception $e) {
                if($phone == '+19999999999' || $phone == '+919999999999') {   //For Play store and App store verification
                    $status = true;
                }
                Log::error("Exception in Twilio SMS SDK");
                Log::error($e->getMessage() . " in " . $e->getFile() . " at " . $e->getLine() . " for phone number " . $phone);
            } catch (ErrorException $e) {
                if($phone == '+19999999999' || $phone == '+919999999999') {   //For Play store and App store verification
                    $status = true;
                }
                Log::error("Error Exception in Twilio SMS SDK");
                Log::error($e->getMessage() . " in " . $e->getFile() . " at " . $e->getLine() . " for phone number " . $phone);
            }
        }
        //Below block to be removed once Twilio details are shared
        if((strlen(trim(env('TWILIO_SID'))) == 0 || strlen(trim(env('TWILIO_NUMBER'))) == 0)) {
            $status = true;
        }
        return $status;
    }
}

if(!function_exists('timeDiffInMins')) {
    function timeDiffInMins($start = null, $end = null) {
        if(!is_null($start) && !is_null($end)) {
            $start = date_create($start);
            $end = date_create($end);
            return date_diff($end,$start)->i." minutes";
        }
        return 0;
    }
}
?>