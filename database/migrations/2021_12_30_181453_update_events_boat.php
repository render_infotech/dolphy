<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateEventsBoat extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('events', function (Blueprint $table) {
            $table->integer('boat_model_year')->nullable();
            $table->integer('boat_length')->nullable();
            $table->integer('total_rooms')->nullable();
            $table->string('boat_type',100)->nullable();
            $table->string('boat_category',100)->nullable();
            $table->string('boat_model',100)->nullable()->change();
        });
        Schema::table('boat_model', function (Blueprint $table) {
            $table->string('make_company',100)->change();
        });
        if (Schema::hasColumn("boat_model", "boat_make_year")) //check the column
        {
            Schema::table("boat_model", function (Blueprint $table)
            {
                $table->dropColumn("boat_make_year"); //drop it
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
