<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateCancellationPolicy extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cancellation_policy', function (Blueprint $table) {
            $table->integer('applicable_for')->nullable()->default(null)->after('id');
        });
        Schema::table('events', function (Blueprint $table) {
            $table->text('from_customer')->nullable()->default(null);
            $table->text('to_customer')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
