<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('address_1',255)->nullable();
            $table->string('address_2',255)->nullable();
            $table->string('pincode',20)->nullable()->index('users_pincode');
            $table->string('city',100)->nullable()->index('users_city');
            $table->string('state',100)->nullable()->index('users_state');
            $table->index('phone','users_phone');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropColumns('users',['address_1','address_2','pincode','city','state']);
    }
}
