<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class BoatsEventsGames extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (!Schema::hasTable('applicable')) {
            Schema::create('applicable', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50)->comment("The type of data stored Boat/Event/Games")->index('applicable_name');
                $table->text('image')->nullable()->comment("The image path of the item");
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
            DB::table('applicable')->insert([
                [
                    'name' => 'Boats',
                    'image' => 'images/boat.png',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name' => 'Events',
                    'image' => 'images/events.png',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name' => 'Water Sports',
                    'image' => 'images/water_sports.png',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]);
        }
        if (!Schema::hasTable('maina')) {
            Schema::create('maina', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50)->comment("The Maina details of boat")->index('maina_name');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('dock_type')) {
            Schema::create('dock_type', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50)->comment("The type of dock DOCK/FLOOR")->index('dock_type_name');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });

            DB::table('dock_type')->insert([
                [
                    'name' => 'DOCK',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name' => 'FLOOR',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]);
        }
        if (!Schema::hasTable('allowed_items')) {
            Schema::create('allowed_items', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50)->comment("The Items that are allowed on Boat")->index('allowed_items_name');
                $table->text('image')->nullable()->comment("The image path of the item");
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('availability_type')) {
            Schema::create('availability_type', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50)->comment("The availability tyoe of Boat, values should be DEFAULT/CUSTOM")->index('availability_type_name');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });

            DB::table('availability_type')->insert([
                [
                    'name' => 'DEFAULT',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name' => 'CUSTOM',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]);
        }
        if (!Schema::hasTable('advance_notice')) {
            Schema::create('advance_notice', function (Blueprint $table) {
                $table->id();
                $table->integer('time')->comment("Advance Notice period in hours")->index('advance_notice_time');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });

            DB::table('advance_notice')->insert([
                [
                    'time' => 1,
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'time' => 3,
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'time' => 6,
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'time' => 12,
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]);
        }
        if (!Schema::hasTable('boat_operator')) {
            Schema::create('boat_operator', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50)->comment("Type of Boat operator ROUTER/CERTIFIER")->index('boat_operator_name');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });

            DB::table('boat_operator')->insert([
                [
                    'name' => 'ROUTER',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name' => 'CERTIFIER',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]);
        }
        if (!Schema::hasTable('cancellation_policy')) {
            Schema::create('cancellation_policy', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50)->comment("Cancellation policy FLEXIBLE/MODERATE/STRICT")->index('cancellation_policy_name');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });

            DB::table('cancellation_policy')->insert([
                [
                    'name' => 'FLEXIBLE',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name' => 'MODERATE',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name' => 'STRICT',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]);
        }
        if (!Schema::hasTable('payout_type')) {
            Schema::create('payout_type', function (Blueprint $table) {
                $table->id();
                $table->string('name', 50)->comment("Payout type I PAY/RENTER PAY")->index('cancellation_policy_name');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });

            DB::table('payout_type')->insert([
                [
                    'name' => 'I PAY',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
                [
                    'name' => 'RENTER PAY',
                    'status' => '1',
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s'),
                ],
            ]);
        }
        if (!Schema::hasTable('make_company')) {
            Schema::create('make_company', function (Blueprint $table) {
                $table->id();
                $table->string('name', 100)->comment("Boat make Company")->index('make_company_name');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('boat_model')) {
            Schema::create('boat_model', function (Blueprint $table) {
                $table->id();
                $table->string('name', 100)->comment("Boat Model")->index('boat_model_name');
                $table->string('boat_category', 100)->comment("Boat Category Ship,Cruiser etc")->index('boat_category_name');
                $table->integer('boat_make_year')->comment("Boat model made year")->index('boat_make_year');
                $table->integer('make_company')->comment("Boat make Company");
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('pictures')) {
            Schema::create('pictures', function (Blueprint $table) {
                $table->id();
                $table->integer('event_id')->comment("Refers to table events");
                $table->text('image_path');
                $table->enum('is_banner', ['0', '1'])->comment("To show as banner image of boat, values can be 0/1")->default('0');
                $table->enum('status', ['0', '1'])->default('1');
                $table->timestamps();
                $table->foreign('event_id')->references('id')->on('events')->onDelete('CASCADE');
            });
        }
        Schema::table('events', function (Blueprint $table) {
            $table->integer('applicable_type')->comment("The type of data stored with reference to applicable table")->after('id');
            $table->string('location_type',120)->comment("Only for Boat, the type of location for Boat")->nullable();
            $table->integer('maina_name')->comment("Only for Boat, Maina name with reference to maina_table")->nullable();
            $table->string('slip_number',50)->comment("Only for Boat")->nullable();
            $table->integer('dock_type')->comment("Only for Boat, for dock type from dock_type table")->nullable();
            $table->integer('country')->comment("Only for Boat, for country from currency table")->nullable();
            $table->string('address_2',100)->comment("Only for Boat and Games")->nullable();
            $table->string('city',50)->comment("Only for Boat")->nullable();
            $table->string('pincode',20)->comment("Only for Boat")->nullable();
            $table->integer('availability_type')->comment("Only for Boat, for type of boat available time from availability_type table")->nullable();
            $table->text('availability_slots')->comment("Only for Boat, Based on availability type, data to be stored in JSON format")->nullable();
            $table->text('advance_notice')->comment("Only for Boat, Advance notice to be sent, to refer advance_notice table")->nullable();
            $table->integer('boat_operator')->comment("Only for Boat, to refer boat_operator table")->nullable();
            $table->tinyInteger('multiple_booking_allowed')->comment("Only for Boat, if Multiple booking allowed for a boat, Values can be 0/1")->nullable();
            $table->integer('time_gap')->comment("Only for Boat, if Multiple booking allowed for a boat, then the time gap to be mentioned in hours")->nullable();
            $table->integer('cancellation_policy')->comment("Only for Boat, to refer cancellation_policy table")->nullable();
            $table->integer('payout_type')->comment("Only for Boat, to refer payout_type table")->nullable();
            $table->string('venue_title')->comment("Only for Games, to enter venue title")->nullable();
            $table->tinyInteger('is_public')->comment("Only for Games/EVENTS, 1 for public, 0 for private")->nullable();
            $table->integer('make_company')->comment("Only for Boat, to refer make_company table")->nullable();
            $table->integer('boat_model')->comment("Only for Boat, to refer boat_model table")->nullable();
            $table->integer('is_complete')->default(0)->comment("For app purpose, to know the details are completely submitted")->nullable();
            $table->integer('is_popular')->default(0)->comment("To consider the popular events marked by admin to show in home page for users")->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('applicable');
        Schema::drop('maina');
        Schema::drop('dock_type');
        Schema::drop('allowed_items');
        Schema::drop('availability_type');
        Schema::drop('advance_notice');
        Schema::drop('boat_operator');
        Schema::drop('cancellation_policy');
        Schema::drop('payout_type');
        Schema::drop('make_company');
        Schema::drop('boat_model');
        Schema::drop('pictures');
        $columns = ['applicable_type', 'location_type', 'maina_name', 'slip_number', 'dock_type', 'country', 'address_2', 'city', 'pincode', 'availability_type', 'availability_slots', 'advance_notice', 'boat_operator', 'multiple_booking_allowed', 'time_gap', 'cancellation_policy', 'payout_type', 'venue_title', 'is_public', 'make_company', 'boat_model'];
        Schema::dropColumns('events',$columns);
    }
}
