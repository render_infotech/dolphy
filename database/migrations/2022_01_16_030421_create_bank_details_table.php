<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->enum('user_type', [0,1])->default(0)->comment('0: Organisation, 1: Customer');
            $table->string('account_number', 255);
            $table->integer('routing_number');
            $table->enum('account_holder_type', ['company','individual'])->default('individual');
            $table->string('account_holder_name', 255);
            $table->string('country', 255)->default('US');
            $table->string('currency', 255)->default('USD');
            $table->string('email', 255)->nullable();
            $table->enum('managed',[0,1])->default(0);
            $table->string('stripe_id', 255)->nullable();
            $table->text('stripe_response')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_details');
    }
}
