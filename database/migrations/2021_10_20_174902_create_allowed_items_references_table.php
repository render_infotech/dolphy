<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAllowedItemsReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('allowed_items_references', function (Blueprint $table) {
            $table->id();
            $table->integer('event_id')->comment('To refer ID from Events table');
            $table->unsignedBigInteger('allowed_items')->comment("To refer ID from allowed_items table");
            $table->timestamps();
            $table->foreign('event_id')->references('id')->on('events')->onDelete('CASCADE');
            $table->foreign('allowed_items')->references('id')->on('allowed_items')->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('allowed_items_references');
    }
}
