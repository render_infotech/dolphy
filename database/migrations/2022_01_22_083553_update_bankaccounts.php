<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateBankaccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_accounts', function (Blueprint $table) {
          
            $table->string('account_holder_name', 255)->nullable();
            $table->string('account_holder_type', 255)->nullable();
            $table->string('account_number', 255)->nullable();
            $table->string('currency', 255)->nullable();
            $table->string('bic', 255)->nullable();
            $table->string('country', 255)->nullable();
            $table->text('document_file', 255)->nullable();

            $table->text('stripe_account_id', 255)->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
