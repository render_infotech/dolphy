<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateChatmessages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chatmessages', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('chat_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->unsignedBigInteger('vendor_id')->nullable();
            $table->text('message');
            $table->string('action');
            $table->string('action_value');
            $table->string('options');
            $table->string('user_action');
            $table->integer('msg_status')->default(0);
            $table->integer('is_vendor_replied')->default(0);

            $table->foreign('chat_id')->references('id')->on('chatroom')->onDelete('CASCADE');
            $table->foreign('user_id')->references('id')->on('app_user')->onDelete('CASCADE');
            $table->foreign('vendor_id')->references('id')->on('users')->onDelete('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chatmessages');
    }
}
