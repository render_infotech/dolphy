<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBankAccountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bank_accounts', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->enum('user_type', ['0', '1'])->default('0')->comment('0: Organisation, 1: Customer');
            $table->string('column1', 255)->nullable();
            $table->string('column2', 255)->nullable();
            $table->string('column3', 255)->nullable();
            $table->string('column4', 255)->nullable();
            $table->string('column5', 255)->nullable();
            $table->string('column6', 255)->nullable();
            $table->string('column7', 255)->nullable();
            $table->string('column8', 255)->nullable();
            $table->string('column9', 255)->nullable();
            $table->string('column10', 255)->nullable();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bank_accounts');
    }
}
