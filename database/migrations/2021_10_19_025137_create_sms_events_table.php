<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSmsEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_events', function (Blueprint $table) {
            $table->id();
            $table->string('mobile',255)->comment("To store the receiver mobile number")->index('sms_events_mobile');
            $table->string('otp',8)->nullable()->comment("To store the OTP")->index('sms_events_otp');
            $table->enum('is_otp',[0,1])->default(0)->comment("To store the SMS content is OTP or Regular SMS");
            $table->integer('attempts')->default(0)->comment("To store the number of attempts on particular OTP")->index('sms_events_attempts');
            $table->integer('resend_attempts')->default(0)->comment("To store the number of resend attempts on particular OTP")->index('sms_events_resend_attempts');
            $table->dateTime('verified_at')->nullable()->comment("To store when was the OTP verified")->index('sms_events_verified_at');
            $table->dateTime('valid_till')->nullable()->comment("To store till when the OTP valid")->index('sms_events_valid_till');
            $table->text('sms_content')->nullable()->comment("To store the SMS content send to users");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_events');
    }
}
